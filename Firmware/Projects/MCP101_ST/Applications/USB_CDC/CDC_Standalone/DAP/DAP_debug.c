
#include "common.h"
#include "DAP.h"

uint32_t ap_bank=0,dp_sel=0;
 void DAP_header_debug(uint8_t request_value,uint8_t *request,uint32_t request_count )
{
	extern int dbg_deep;
	uint32_t temp;
		if(dbg_deep > 0)
		{
			char ch_buff[20]={0};
			int ii=0;
			if(request_value & DAP_TRANSFER_MATCH_MASK)
			{	ch_buff[ii++] = 'M';ch_buff[ii++] = '1';ch_buff[ii++] = ':'; }	
			else
			{	ch_buff[ii++] = 'M';ch_buff[ii++] = '0';ch_buff[ii++] = ':'; }	
			if(request_value & DAP_TRANSFER_MATCH_VALUE)
			{	ch_buff[ii++] = 'V';ch_buff[ii++] = '1';ch_buff[ii++] = ':'; }	
			else
			{	ch_buff[ii++] = 'V';ch_buff[ii++] = '0';ch_buff[ii++] = ':'; }		
			if(request_value & DAP_TRANSFER_A3)
			{	ch_buff[ii++] = '1'; }	
			else
			{	ch_buff[ii++] = '0'; }		
			if(request_value & DAP_TRANSFER_A2)
			{	ch_buff[ii++] = '1';ch_buff[ii++] = ':'; }	
			else
			{	ch_buff[ii++] = '0';ch_buff[ii++] = ':'; }		
			if(request_value & DAP_TRANSFER_RnW)
			{	ch_buff[ii++] = 'R';ch_buff[ii++] = ':'; }	
			else
			{	ch_buff[ii++] = 'W';ch_buff[ii++] = ':'; }			
			if(request_value & DAP_TRANSFER_APnDP)
			{	ch_buff[ii++] = 'A';ch_buff[ii++] = 'P'; }
		    else
			{	ch_buff[ii++] = 'D';ch_buff[ii++] = 'P'; }	
			temp = 	  (*(request+0) <<  0) |
                      (*(request+1) <<  8) |
                      (*(request+2) << 16) |
                      (*(request+3) << 24);
			mtk_printf(ch_buff, temp);
			if(	!(request_value&DAP_TRANSFER_APnDP))   
				switch(request_value & (DAP_TRANSFER_A3|DAP_TRANSFER_A2))
				{
					case 0x00:
						if(request_value & DAP_TRANSFER_RnW)
							mtk_printf("DP read IDCODE",-1);
						else
							mtk_printf("AP ABORT",-1);
						break;
					case 0x04:
						if(dp_sel)
							mtk_printf("DP read/write WCR",-1);
						else
							mtk_printf("DP read/write CTRL/STAT",-1);
						break;
					case 0x08:
						if(request_value & DAP_TRANSFER_RnW)
							mtk_printf("DP RESEND",-1);
						else{
							mtk_printf("AP SELECT",-1);
							mtk_printf("AP SEL:",*(request+3));
							mtk_printf("AP BANK:", *(request+0) >> 4);
							mtk_printf("DP CTRLSEL", *(request+0) & 1);
							ap_bank = (*(request+0) >> 4) & 0x0F;
							dp_sel = *(request+0) & 1;
						}
						break;
					case 0x0c:
						if(request_value & DAP_TRANSFER_RnW)
							mtk_printf("DP Read Buffer RDBUFF",-1);
						else
							mtk_printf("DP ROUTESEL",-1);
						break;						
				}

			if(	(request_value&DAP_TRANSFER_APnDP == 1 ) )
			{	
				switch((ap_bank << 4) | (request_value & (DAP_TRANSFER_A3|DAP_TRANSFER_A2))) 
				{
					case 0x00: mtk_printf("AP CSW",-1);break;
					case 0x04: mtk_printf("AP TAR",-1);break;
					case 0x0C: mtk_printf("AP DRW",-1);break;
					case 0x10: mtk_printf("AP BD0",-1);break;
					case 0x14: mtk_printf("AP BD1",-1);break;
					case 0x18: mtk_printf("AP BD2",-1);break;
					case 0x1c: mtk_printf("AP BD3",-1);break;
					case 0xF4: mtk_printf("AP CFG",-1);break;
					case 0xF8: mtk_printf("AP BASE",-1);break;
					case 0xFc: mtk_printf("AP IDR",-1);break;
				}
				if((ap_bank << 4) | (request_value & (DAP_TRANSFER_A3|DAP_TRANSFER_A2)) == 0x0c) 			
				{
					if(temp < 0xe0000000)
						mtk_printf("unknow",-1);
					else if(temp < 0xe0001000)
						mtk_printf("Core ITM",-1);
					else if(temp < 0xe0002000)
						mtk_printf("Core DWT(watchpoint)",-1);
					else if(temp < 0xe0003000)
						mtk_printf("Core BPU(breakpoint)",-1);
					else if(temp < 0xe000e000)
						mtk_printf("unknow",-1);
					else if(temp < 0xe000f000)
						mtk_printf("Core SCS(controlblock)",-1);
				}
			}	
			printf_memory(request,request_count);
		}
	}		

	