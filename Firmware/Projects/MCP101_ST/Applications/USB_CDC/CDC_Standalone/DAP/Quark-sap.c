#include <errno.h>
//#include <stdlib.h>
#include "MyAlloc.h"
#include <stdio.h>
#include <stdarg.h>
#include "string.h"
#include "spider-sap.h" 
#include "serialprog.h"
#include "common.h"
#include "dap.h"
#include "nrf51.h"
#include "command.h"
#include "jtag/jtag.h"
#include "target.h"
#include "Quark_jtag.h"
#include "quark_se.h"

#ifdef __RTX_FreeRTOS__
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "spider-sap.h" 
#include "spider-funs.h"
#include "spider-i2c.h"
#include "spider-ser.h"
#include "spider-ver.h"
#include "semphr.h"
#include "../DAP/spider-spi.h"
#include "spider-adc.h"
#include "stm32f4xx_spi.h"
#include "sdio_stm32f4.h"
#include "usbd_conf.h"
#include "flash_if.h"
#include "usb_core.h"
#include "lwip/netif.h"



#include "ff.h"
portTASK_FUNCTION_PROTO(prvDAPCMDPktTask, pvParameters );

#endif

#define     SER_TYPE_GENERAL    0x00
#define     SER_TYPE_CRC32      0x21    // maybe not used
#define     SER_TYPE_DUMMY      0x30    // maybe not used

#define LMT_ID_CODE   0x38289013
#define CLTAP_ID_CODE 0x0e786013



#define SIG 0x69317812
#define CODE_ADDR 0xA8007000
#define DATA_ADDR 0xA800A000
#define CONFIG_ADDR 0xA8001000
#define RC_ADDR 0xA8001400


#define MC101_BOARD 1
//#undef MC101_BOARD
int gChkRomtable=0;
int gUp=0;
//__align(4)
	uint8_t	sapcache[0x10];	//////??????? XAP_LOAD_SIZE
//__align(4)
	uint8_t	linecache[0x40];	
//__align(4)
     xDAPPort pPort; // __attribute__( ( section( "CCM")) ) ;
//__align(4)
     xSapFile xSapfile; // __attribute__( ( section( "CCM")) ) ;
//__align(4)
__IO uint16_t adcbuf0[PORT_COUNT];
//__IO uint16_t adcbuf1[4];

//__align(4)
    static xSAPTable xst[MAX_SAP_SECTION+1];// __attribute__( ( section( "CCM")) ) ;
//__align(4)
    static uint32_t enckeys[]  __attribute__( ( section( "CCM")) )  = {0x16896168, 0x47617279,0x4A657365,0x526F6D65};

//__align(4)
    static xDAPBuffer xBuffer;
    static xDAPBuffer *xBuff;

//__align(4)
    static xSerFile xSF;// __attribute__( ( section( "CCM")) );
    xSapFile *xap;
    xSerFile *xsf = NULL;
    /*---------------------------------------------------------------------------*/
//    FATFS fs;
//    FATFS fsusb;
//    static xDAPCfg xCfg;// __attribute__( ( section( "CCM")) );
    U8	respNULL[] = {0xFF, 0xFF, 0xFF, 0xFF};
/*__align(4)
    static char logbuf[128];// __attribute__( ( section( "CCM")) ) ;
    static char logcache[100]; // __attribute__( ( section( "CCM")) ) ;
  */  
		//static int logs = 0;
static uint8_t currcmd = 0;

extern uint16_t build_number;
extern uint16_t firmware_version;

int clk_flag;//if clk_flag is 1,try different reset mode
int rst_flag;//if rst_flag is 1,try different reset mode
int gAtmelHasErase=0;
int gAtmelHasPlug=0;		
int		gAtmelNeedLock=0;
int runrc;

int gNeedLockChip=0;
int	gNeedErasePin=0;
int	gUseArc = 0;
int	gNrfUICRExtra=0;
int gEraseAllChip = 1;
int gHadEraseAllChip = 0;
int gHaRetried = 0;
NRF_UICR_Type pnrf_uicr_t;		

int ErrorCode = 0;

int runrc;

int gBeepAfterProg;
int gAutoProg;

static int gUseIap = 0;
static int gUseArcAlg = 0;
static int gFixC1000 = 0;
struct target Quark_target; 
struct target Quark_Arc_target; 
struct target *common_target; 
struct command_context *global_cmd_ctx;
xDAPPacket *global_xPacket;
struct jtag_tap TapArray[4];
struct target TargetArray[2];
struct command_context ContextArray[1];
//extern uint8_t fc_data_buff[FC_BYTE_PAGE_SIZE];

static uint8_t Veribuf[0x400];
//int MsgOutput(MsgType type,const char fmt[], ...);
void printxPacket(U8 io, xDAPPacket *xPkt);
uint32_t uint32(uint8_t *strno);
int		(*runfunc)(xDAPPacket *xPacket);	
static void run_SAPSequence(xDAPPacket *xPacket,xSAPSeq *Seq,uint8_t SEQCnt);
char *ErrMsg(uint8_t errco, uint8_t opt);
#define SPIP	4
typedef enum OP_Type
{
	OP_ERASE = 0,
	OP_PROG,
	OP_VERIFY
}OP_t;
typedef struct Config_tag
{
	uint32_t sig;
	 uint32_t type;
	 uint32_t address;
	 uint32_t size;
	 uint32_t crc;
}Config_t;
Config_t OP_Config_t;
enum {
	/* general purpose registers */
	EAX = 0,
	ECX,
	EDX,
	EBX,
	ESP,
	EBP,
	ESI,
	EDI,
	/* instruction pointer & flags */
	EIP,
	EFLAGS,

	/* segment registers */
	/*
	CS,
	SS,
	DS,
	ES,
	FS,
	GS,
	*/
	/* floating point unit registers */
	
	/*
	ST0,
	ST1,
	ST2,
	ST3,
	ST4,
	ST5,
	ST6,
	ST7,
	FCTRL,
	FSTAT,
	FTAG,
	FISEG,
	FIOFF,
	FOSEG,
	FOOFF,
	FOP,
	*/
	/* control registers */
	CR0,
	CR2,
	CR3,
	CR4,

	/* debug registers */
	DR0,
	DR1,
	DR2,
	DR3,
	DR6,
	DR7,

	/* descriptor tables */
	/*
	IDTB,
	IDTL,
	IDTAR,
	GDTB,
	GDTL,
	GDTAR,
	TR,
	LDTR,
	LDTB,
	LDTL,
	LDTAR,
*/
	/* segment registers */
	
	CSB,
	CSL,
	CSAR,
	DSB,
	DSL,
	DSAR,
	//ESB,
	//ESL,
	//ESAR,
	//FSB,
	//FSL,
	//FSAR,
	//GSB,
	//GSL,
	//GSAR,
	//SSB,
	//SSL,
	SSAR,
	//TSSB,
	//TSSL,
	//TSSAR,

	/* PM control reg */
	PMCR,
};
static char* reg_name[]={"ECX","EDX","EBX","ESP","EBP","ESI","EDI","EIP","EFLAGS","CS","SS","DS","ES","FS","GS"};
/*
extern int dbg_deep;
#ifndef USE_SYSLOG
#define  uP(...)       if(dbg_deep>0)MsgOutput(USERMSG,__VA_ARGS__)
#else
#define  uP(...)       if(dbg_deep>0)mtk_sf_printf(__VA_ARGS__,-1)
#endif

#ifndef USE_SYSLOG
#define  cP(...)       if(dbg_deep>1)MsgOutput(USERMSG,__VA_ARGS__)
#else
#define  cP(...)       if(dbg_deep>1)mtk_sf_printf(USERMSG,__VA_ARGS__,-1)
#endif

#ifndef USE_SYSLOG
#define  eP(...)       if(dbg_deep>0) MsgOutput(ERRMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // Error
#else
#define  eP(...)       if(dbg_deep>0) mtk_sf_printf(__VA_ARGS__,-1)//mtk_sf_printf(__VA_ARGS__,-1) // Error
#endif

#define  wP(...)       if(dbg_deep>1) MsgOutput(WARNINGMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)	    // Warning
#define  iP(...)       if(dbg_deep>2) MsgOutput(INFOMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)	    // Info

#define  dP(...)       	  if(dbg_deep>1) MsgOutput(DEBUGMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)  // debug
#define  DP(...)       	  if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)  // full print
#define  vP(...)       	   if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // full print
#define  VP(...)       	   if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // full print
#define  printPacket(x,y)  if(dbg_deep>2) printf_memory(y,x) // 

*/
extern int gProgOnLine;
uint8_t xI2CMsgPacketOut(uint8_t type, uint32_t len, uint32_t addr, char *data,uint8_t flag);

#if 0
#define  IcP(a,b,c,d)    
#define  IeP(a,b,c,d)    
#define  IwP(a,b,c,d)    
#define  IiP(a,b,c,d)    
#define  IdP(a,b,c,d)    
#define  IfP(a,b,c,d)    
#else
#define  IcP(a,b,c,d)     //if (!gProgOnLine)   xI2CMsgPacketOut(a,b,c,d,0)
#define  IeP(a,b,c,d)    if (!gProgOnLine)    xI2CMsgPacketOut(a,b,c,d,0)
#define  IwP(a,b,c,d)    // if (!gProgOnLine)    xI2CMsgPacketOut(a,b,c,d,0)
#define  IiP(a,b,c,d)    //if (!gProgOnLine)      xI2CMsgPacketOut(a,b,c,d,0)
#define  IdP(a,b,c,d)    // if (!gProgOnLine)      xI2CMsgPacketOut(a,b,c,d,0)
#define  IfP(a,b,c,d)    // if (!gProgOnLine)       xI2CMsgPacketOut(a,b,c,d,0)
#endif
void dump_targetmem(xDAPPacket *xPacket,uint32_t addr);
void ReportMsgByAck(xDAPPort *dap,uint8_t ack)
{
	if(ack>SWD_ACK_WAIT)
	{
		if(ack==SWD_ACK_FATAL)
			IeP(dap->manstate, OPT_NF|OPT_PORT|OPT_CODE|OPT_ERR, NULL, ErrMsg(TE_NO_ACK, OPT_ERR));
		else
			IeP(dap->manstate, OPT_NF|OPT_PORT|OPT_CODE|OPT_ERR, NULL, ErrMsg(TE_INVALID_ACK, OPT_ERR));
	}
}
#ifdef STM32F10X_MD
static void Reinit_RST(int type)
{
	
	
}
#else
static void Reinit_RST(int type)
{
	if(!type)
		  /* Configure I/O pin nRESET */
	{
		PIN_nRESET_PORT->PCR[PIN_nRESET_BIT]       = PORT_PCR_MUX(1);  /* GPIO */
																		
		PIN_nRESET_GPIO->PSOR  = 1 << PIN_nRESET_BIT;                    /* High level */
		PIN_nRESET_GPIO->PDDR |= 1 << PIN_nRESET_BIT;                    /* Output */
	}else
	{
	  PIN_nRESET_PORT->PCR[PIN_nRESET_BIT]       = PORT_PCR_MUX(1)	|  /* GPIO */
                                               PORT_PCR_PE_MASK 	|  /* Pull enable */
                                               PORT_PCR_PS_MASK 	|  /* Pull-up */
                                               PORT_PCR_ODE_MASK;  /* Open-drain */
  PIN_nRESET_GPIO->PSOR  = 1 << PIN_nRESET_BIT;                    /* High level */
  PIN_nRESET_GPIO->PDDR |= 1 << PIN_nRESET_BIT;                    /* Output */
	}
}
#endif
void DAP_SetLed(int a, int b,int c)
{
}
void xUIMsg(uint8_t cmd, uint32_t opt, uint8_t port, uint8_t errco)
{
		xI2CMsgPacketOut(cmd, opt, port, MSGSTR[errco],0);
}
void vTaskDelay(int ms)
{
	Delayms(ms);
}

void xI2CMsgQPut(void *xPkt, uint8_t type)
{
}
void xI2CMsgQOptPut(uint8_t cmd, uint8_t opt, uint8_t code, uint8_t port, uint32_t addr, uint8_t *data)
{
}
uint32_t getTick()
{
	return HAL_GetTick();
}
int xTaskGetTickCount()
{
	return getTick();
}
//#define ADCx ADC0
 void ADC_SoftwareStartConv(void* ADC)
 {
#ifndef STM32F10X_MD
	 adcbuf0[0]=Read_ADC((ADC_Type*)ADC,0);
#else
	 adcbuf0[0]= get_targe_voltage();
#endif
	
 }	 
int  hw_version(int n)
{
	return 2;
}	
void power_control(uint8_t voltage, uint8_t port)
{
	switch(voltage)
	{
		case 0:
				output_0v_io();
				break;
		case 1:
				output_1v8_io();
				break;
		case 2:
				output_2v5_io();
				break;
		case 4:
				output_3v3_io();
				break;
		case 8:
				output_5v_io();
				break;
		default:
			break;
	}
		
}
char *ErrMsg(uint8_t errco, uint8_t opt)
{
	return MSGSTR[errco];
}
void bootloader(uint32_t fidx)
{}
int serial_log(xSerFile *xsf, uint32_t action)
{return 0;}
int serial_readlines(xSerFile *xsf, int port)
{return 0;}		

/*
 *
 * xConfig()
 *  id:
 *  data
 *  action: 
 *      0: get
 *      1: set
 *      2: save
 */
int xConfig(uint32_t id, uint8_t *data, uint8_t action)
{
    if (action == 0) {
    }
    if (action & 0x01) {
    }
    if (action & 0x02) {
    }
    return 0;
}
void printports(char *msgstr)
{
    U8 i,x=0;

    xDAPPort *dap = &pPort;
    if (xap->CICO & (DAP_INTF_UART|DAP_INTF_USB)) {
        iP("\nPort{");
        for(i=0;i<PORT_COUNT;i++) {
            if(dap->rport & (1<<i)) {
                iP("%s%d", (x++>0)?",":"", i+1);
            }
        }

        iP("} %s, elapsed %dms.", msgstr, xTaskGetTickCount() - dap->startTick);
    }
}
/*
 * check_alg_result()
 *      fco: fail code
 *      pco: pass code
 */
void check_alg_result(uint8_t fco, uint8_t pco)
{
    xDAPPort *dap = &pPort;
    int i;
		volatile  char *fmsg, *pmsg;
 ///   if (fco)
 ///       fmsg = ErrMsg(fco, OPT_ERR);
    if (pco)
        pmsg = (char *)MSGSTR[pco];

    dap->vport = 0;
    dap->rport = 0;
    for (i=0;i<PORT_COUNT;i++) {
        if (dap->xport & (1<<i)) {
            dap->port = (1<<i);
            if (dap->rets[i] > 0) {
                dap->vport |= (1<<i);
                dap->xport &= ~(1<<i);
                DAP_SetLed((1<<i), REDLED, LEDON);
                if (fco) {
                    //IwP(dap->manstate, OPT_NF|OPT_PORT|OPT_A2C|OPT_ERR, fco, fmsg);
                    wP("\ncheck_alg_result[%1X], %s", dap->manstate, fmsg);
                }
            } else {
                dap->rport |= (1<<i);
                if (pco) {
                    //IwP(dap->manstate, OPT_NF|OPT_PORT, NULL, pmsg);
                    wP("\ncheck_alg_result[%1X], %s", dap->manstate, pmsg);
                }
            }
        }
    }
}
void swd_calctime(void)
{
    int i = 0, j;
    uint32_t ti=0;
    uint32_t tEr, tPg, tBc, tVr, acc;

    if (xap->funToDo & DO_ERASESECTOR) {
#if 0        
        j = 0;
        for (i=1;i<MAX_SECTOR_COUNT;i++) {
            if ((xap->sectors[i].szSector == 0xFFFFFFFF) || (xap->sectors[i].startSector == 0xFFFFFFFF) || (xap->szData < xap->sectors[i].startSector)) {
                j += ((xap->szData - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
                break;
            } else {
                j += ((xap->sectors[i].startSector - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
            }
        }
        tEr = xap->toErase * j;
#else
        if (xap->szData > xap->szFlash)
            tEr = xap->toProg * xap->szFlash / xap->szProg;
        else
            tEr = xap->toProg * (xap->szData + xap->szProg)/xap->szProg;
#endif
        ti += tEr;
    }
    if (xap->funToDo & DO_ERASECHIP) {
#if 0        
        j = 0;
        for (i=1;j<MAX_SECTOR_COUNT;i++) {
            if (xap->sectors[i].szSector == 0xFFFFFFFF || xap->sectors[i].startSector == 0xFFFFFFFF) {
                j +=  ((xap->szFlash - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
                break;
            } else {
                j += ((xap->sectors[i].startSector - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
            }
        }
        tEr = j * xap->toErase;
#else
        tEr = xap->toProg * xap->szFlash/xap->szProg;
#endif        
        ti += tEr;
    }
    if (xap->funToDo & DO_BLANKCHECK) {
        tBc = xap->toErase >> 1;
        ti += tBc;
    }

    if (xap->funToDo & DO_PROGRAM) {
        if (xap->szData > xap->szFlash)
            tPg = (xap->szFlash * xap->toProg) / xap->szProg;
        else
            tPg = (xap->szData * xap->toProg) / xap->szProg;
        ti += tPg;
    }

    if (xap->funToDo & DO_CRC32) {
        if (xap->szData > xap->szFlash)
            tVr = (xap->szFlash * xap->toProg) / xap->szProg;
        else
            tVr = (xap->szData * xap->toProg) / xap->szProg;
        tVr = 1+tVr>>3;

        //tVr = (2 * xap->toErase * xap->szData)/xap->szFlash;  
        ti += tVr;
    }
    i = 0;
    /*
       xap->estimate[i++] = SM_CONNECT;
       xap->estimate[i++] = 100 * 255 / ti;
       xap->estimate[i++] = SM_DAPRESET;
       xap->estimate[i++] = 100 * 255 / ti;
       xap->estimate[i++] = SM_RESET_HALT;
       xap->estimate[i++] = 100 * 255 / ti;
       xap->estimate[i++] = SM_LOAD_CODE;
       xap->estimate[i++] = 100 * 255 / ti;
       */

    acc = 0;
    if (xap->funToDo & DO_BLANKCHECK) {
        xap->estimate[i++] = SM_BLANKCHECK;
        acc += 255 * tBc / ti;
        xap->estimate[i++] = acc;
        dP("\nBlankcheck ESTI:%d", acc);
    }

    if (xap->funToDo & DO_ERASESECTOR) {
        xap->estimate[i++] = SM_ERASESECTOR;
        j = 255 * tEr / ti;
        //j = (j > 200)?150:j;
        acc += j;
        xap->estimate[i++] = acc;
        dP("\nEraseSector ESTI:%d", acc);
    }

    if (xap->funToDo & DO_ERASECHIP) {
        xap->estimate[i++] = SM_ERASECHIP;
        j = 255 * tEr / ti;
        //j = (j > 200)?150:j;
        acc += j;
        xap->estimate[i++] = acc;
        dP("\nEraseChip ESTI:%d", acc);
    }

    if (xap->funToDo & DO_PROGRAM) {
        xap->estimate[i++] = SM_PROGPAGE;
        j = 255 * tPg / ti;
        //j = (j > 200)?150:j;
        acc += j;
        xap->estimate[i++] = acc;
        dP("\nProgram ESTI:%d", acc);
    }

    if (xap->funToDo & DO_CRC32) {
        xap->estimate[i++] = SM_CRC32;
        acc = 0xFF;
        xap->estimate[i++] = acc; //255 * tVr / ti;
        dP("\nVerify ESTI:%d", acc);
    }

    if (xap->estimate[i-1] < 0xFF)
        xap->estimate[i-1] = 0xFF;

    xap->estimate[i++] = 0xFF;
    xap->estimate[i++] = 0xFF;

}

extern int gProgOnLine;
extern int gStartProgI2C;
extern uint8_t aTxBuffer[I2CRXBUFFERSIZE];
	/* Buffers used for displaying Time and Date */
//uint8_t aShowTime[50] = {0};
uint8_t aShowDate[50] = {0};
char Timetemp[50];
int MsgOutput(MsgType type,const char fmt[], ...)
{
    va_list arp;
    static int rc;
    uint32_t szRW = 0;
    int res;
	  char logbuf[128];
		char buf[128];
	
	//char Timetemp[32];
		//int Ticktemp = xTaskGetTickCount();
	

		//RTC_GetCalendar(Timetemp,aShowDate);
		//sprintf(Timetemp,"%02d:%02d:%02d.%04d",(Ticktemp/1000)/3600,((Ticktemp/1000)/60)%60,((Ticktemp/1000)%3600)%60,Ticktemp%1000);
#ifdef __ONLINE__	
    if (gProgOnLine)//xap->logging > 0) 
		{
        va_start(arp, fmt);
        rc = vsprintf(buf, fmt, arp);
        va_end(arp);
			switch(type)
			{
				case USERMSG:
					sprintf(logbuf,"%s",buf);
					break;
				case ERRMSG:
					sprintf(logbuf,"[%s]Error:%s",Timetemp,buf);
					break;
				case WARNINGMSG:
					sprintf(logbuf,"[%s]Warning:%s",Timetemp,buf);
					break;
				case INFOMSG:
					sprintf(logbuf,"%s",buf);
					break;
				case DEBUGMSG:
					sprintf(logbuf,"[%s]Debug:%s",Timetemp,buf);
					break;
				case FULLMSG:
					sprintf(logbuf,"[%s]%s",Timetemp,buf);
					break;
				case IICMSG:
					sprintf(aTxBuffer,"[%s]%s",Timetemp,buf);
				default:
					sprintf(logbuf,"%s",buf);
					break;
				
			}
			if(type!=IICMSG)
					mtk_printf_usbser(logbuf,-1);
			//else
				//mtk_printf_iic(buf,-1);	
			#if 0
        if ((rc + logs) > 1024) {  // write logcache
            FIL logfile;
            if ((res = f_open(&logfile, (const TCHAR *)xap->logname, FA_OPEN_ALWAYS|FA_WRITE)) != 0) {
                printf("\nCan not Open Log file:%s, %s!", xap->logname, MSGSTR[res]);
                return -1;
            }
            /* Seek */
            res = f_lseek(&logfile, f_size(&logfile));
            if (res) {
                printf("\nSeek EOF failed, %s!", MSGSTR[res]);
                f_close(&logfile);
                return -1;
            }	
            res = f_write(&logfile, logcache, logs, &szRW);
            if (res) {
                printf("\nWrite Logging failed, %s!", MSGSTR[res]);
            }
			f_close(&logfile);
            logs = 0;
        }
        // copy logbuf to logcache
        for (szRW=0;szRW<rc;szRW++) 
            logcache[logs++] = logbuf[szRW];
				#endif
    }
#endif		
#ifndef __ONLINE__		
    if (xap->tDebug) {
        va_start(arp, fmt);
        rc = vprintf(fmt, arp);
        va_end(arp);
    }
#endif
    return rc;
}
int MsgErrOutput(const char fmt[], ...)
{
    va_list arp;
    static int rc;
    uint32_t szRW = 0;
    int res;
	  static char logbuf[128];
#ifdef __ONLINE__	
    if (gProgOnLine)//xap->logging > 0) 
		{
        va_start(arp, fmt);
        rc = vsprintf(logbuf, fmt, arp);
        va_end(arp);
			mtk_printf_usbser("\r\nError:",-1);
				mtk_printf_usbser(logbuf,-1);
			#if 0
        if ((rc + logs) > 1024) {  // write logcache
            FIL logfile;
            if ((res = f_open(&logfile, (const TCHAR *)xap->logname, FA_OPEN_ALWAYS|FA_WRITE)) != 0) {
                printf("\nCan not Open Log file:%s, %s!", xap->logname, MSGSTR[res]);
                return -1;
            }
            /* Seek */
            res = f_lseek(&logfile, f_size(&logfile));
            if (res) {
                printf("\nSeek EOF failed, %s!", MSGSTR[res]);
                f_close(&logfile);
                return -1;
            }	
            res = f_write(&logfile, logcache, logs, &szRW);
            if (res) {
                printf("\nWrite Logging failed, %s!", MSGSTR[res]);
            }
			f_close(&logfile);
            logs = 0;
        }
        // copy logbuf to logcache
        for (szRW=0;szRW<rc;szRW++) 
            logcache[logs++] = logbuf[szRW];
				#endif
    }
#endif		
#ifndef __ONLINE__		
    if (xap->tDebug) {
        va_start(arp, fmt);
        rc = vprintf(fmt, arp);
        va_end(arp);
    }
#endif
    return rc;
}
int iDAPOpen(void)
{
	extern uint8_t FlashBuf[];
	xDAPPort *dap = &pPort;
	dap->xport=(1<<PORT_COUNT)-1;
  //  FRESULT fres;
    xBuff = &xBuffer;
	xBuff->buffo = FlashBuf;
    xap = &xSapfile;
    xap->cache = sapcache;
    //xap->tDebug = 16;
    xap->tDebug = OL_INFO;
    xap->swj = SWJ_SWD;
    xap->dapspeed = SWJ_CLOCK_SPEED;
	
    xsf = (xSerFile *)&xSF;
    xsf->line = linecache;

    return 0;
}

int	WaitForHaltTimeout(struct target *t,uint32_t TimeOut)
{
		int rc = 0;
		int cnt = 0;
	while(1)
	{
		rc =	Quark_poll(t);
		if(rc!=ERROR_OK)
			return rc;
		if(Quark_Arc_target.state == TARGET_HALTED)		
		{
			return ERROR_OK;
		}
		cnt += 1;
		//HAL_Delay(10);
		if((cnt%1000) == 0)
			uP(".");
		if((TimeOut != 0xFFFFFFFF)&&cnt >=TimeOut)
		{
			return ERROR_WAIT;
		}
	}
}
/*---------------------------------------------------------------------------*/
#ifndef __SPIDER__
U32 SWD_FLUSH_CMD(xDAPPort *dap) 
{
	uint8_t *request = dap->bufo;
	//uint8_t response[512]={0};
	memset(xBuff->buffi,0,sizeof(xBuff->buffi));
//	disable_usb_irq();
	DAP_ProcessCommand(request, xBuff->buffi /*response*/);
//	enable_usb_irq();
	dap->cmdoff = 0;
	dap->cmdidx = 0;
	return *((U32*)&xBuff->buffi[4]);
}

#else
#endif

/*---------------------------------------------------------------------------*/
/*
weib:if SWD_FLUSH,flush;else get a long buf include one !SPS+multi data
*/
U32 swd_buff_cmd(U16 port, U8 req, U8 *data)
{
    xDAPPort *dap = &pPort;

    if ((port & SWD_NOBUFFER)==0) {
        if (dap->cmdidx++ == 0) {
            dap->cmdoff = 0;
            spi_preamble();
            dap->bufo[dap->cmdoff++] = 0x05;
            dap->bufo[dap->cmdoff++] = 0x00;
            dap->bufo[dap->cmdoff++] = dap->cmdidx;
        } else { 
            dap->bufo[2+SPIP] = dap->cmdidx;		// dap->cmdidx should be updated to result of ++
        }

        dap->bufo[dap->cmdoff++] = req;

        if ((req & SWD_RW_MASK)==0 || (req & SWD_RD_MASK) || (req & SWD_WR_MASK))	{	// Write or read value match
            dap->bufo[dap->cmdoff++] = data[0];
            dap->bufo[dap->cmdoff++] = data[1];
            dap->bufo[dap->cmdoff++] = data[2];
            dap->bufo[dap->cmdoff++] = data[3];
            if (req & SWD_WR_MASK) {
                dap->writemask = (data[0] | (data[1]<<8) | (data[2]<<16) | (data[3]<<24));
            }
        }
    }

/*
            dap->cmdoff = 0;
            spi_preamble();
            dap->bufo[dap->cmdoff++] = 0x05;
            dap->bufo[dap->cmdoff++] = 0x00;
			dap->bufo[dap->cmdoff++] = req;

        if ((req & SWD_RW_MASK)==0 || (req & SWD_RD_MASK) || (req & SWD_WR_MASK))	{	// Write or read value match
            dap->bufo[dap->cmdoff++] = data[0];
            dap->bufo[dap->cmdoff++] = data[1];
            dap->bufo[dap->cmdoff++] = data[2];
            dap->bufo[dap->cmdoff++] = data[3];
            if (req & SWD_WR_MASK) {
                dap->writemask = (data[0] | (data[1]<<8) | (data[2]<<16) | (data[3]<<24));
            }
        }
*/
    //if ((port & SWD_FLUSH)) {
        return SWD_FLUSH_CMD(dap);
    //}
//    return DAP_OK;
}


/*---------------------------------------------------------------------------*/
U32 App_DAP_JTAG_Sequence(U16 port, U8 length, U8 *data)
{						
    xDAPPort *dap = &pPort;
    int i;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);

    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x14;
    for (i=0;i<length;i++)
        dap->bufo[dap->cmdoff++] = data[i];

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_JTAG_Configure(U16 port, U8 count, U8 IR)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x15;
    dap->bufo[dap->cmdoff++] = count;
    dap->bufo[dap->cmdoff++] = IR;

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_JTAG_IDCODE(U16 port, U8 index)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x16;
    dap->bufo[dap->cmdoff++] = index;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_TransferAbort(U16 port)
{						
    xDAPPort *dap = &pPort;

    dap->cmdoff = 0;

    dap->bufo[dap->cmdoff++] = 0x07;

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
void swd_printinfo(uint16_t port, uint8_t *resp, uint8_t *header)
{
    switch(resp[1]) {
        case 0:
            break;
        case 1:
            iP("\nPort[%1x]:%s [%d]", port, header, resp[2]); break;
        case 2:
            iP("\nPort[%1x]:%s [%d]", port, header, resp[2] + (resp[3]<<8)); break;
        default:
            iP("\nPort[%1x]:%s [%s]", port, header, &resp[2]); break;
    }
}

/*---------------------------------------------------------------------------*/
int swd_led(xDAPPacket *xPacket)
{
	return DAP_GONEXT;
}
/*---------------------------------------------------------------------------*/
int swd_showled(xDAPPacket *xPacket)
{
    #ifndef MC101_BOARD
	U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    //U8	*resp = SUBRESPONSE(xPacket);

    switch (dap->substate) {
        case 0:
            SYNCSEQ();
            nextstate(dap);
            cP("Led: %d, OnOff:%d, Port:%02X", dap->regs[0], dap->regs[1], dap->regs[2]);
            dap->exp = App_DAP_Led(dap->regs[2], dap->regs[0], dap->regs[1]); 
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:
            break;
    }
		#endif
    return DAP_OK;
}





/*----------------------------------------------------------------------------*/
/* 
 * loadSapFileSeg()
 *	 Using a buffer to cache SAP file segment to avoid read file segment again and again.
 *		Return request buffer address if requested buffer inside cache range
 *		otherwise, read file segment into cache and return new address of buffer
 */
U8 *loadSapFile(uint8_t *fname, uint32_t offset, uint32_t sz)
{

	   extern int read_times;
	   int len;
	#if 0
	   uint32_t abs_offset = (read_times-1) * 4096;

	   if(offset < 4096  && read_times == 1) 
	   {
			xap->cache = xBuff->buffo + offset;   
			xap->offCache = offset;
			if(offset+sz > 4096)
				xap->szCache = 4096 - offset;
			else
				xap->szCache = sz;
		read_times++;
			return xap->cache;
	   }else{
			//len = GetSAPFile(xap->sapfname,xBuff->buffo,offset);
			 len = GetSAPFile(xap->sapfname,rBuf,offset);
			if(len < 0)
				return NULL;
			
			//xap->cache = xBuff->buffo;   	
			xap->offCache = 0;
			xap->szCache = len;
			return rBuf;//xap->cache;


	   }
		 #endif
	  len = GetSAPFile(xap->sapfname,xBuff->buffo,sz,offset);
		if(len < 0)
		{
			CloseSAPFile();
			return NULL;
			
		}
			
		xap->cache = xBuff->buffo;   	
		xap->offCache = 0;
		xap->szCache = len;
		CloseSAPFile();
		return xap->cache;	 


}


/* --------------------------------------------------------------------------*/
U32 toint(U8 *buf, U8 sz)
{
    U32 retval = 0;
    U8 i;
    for (i=0;i<=sz;i++)
        retval |= (buf[i] << (i*8));
    return retval;
}
/* --------------------------------------------------------------------------*/
uint8_t swd_gettime(uint8_t cmd)
{
    int i;
    for (i=0;xap->estimate[i]<0xFF;i+=2) {
        if (xap->estimate[i] == cmd)
            return xap->estimate[i+1];
    }
    return 0;
}


/* ------------------------------------------------------------------------------------------------ */
/*
weib:Get and check SAP Header (new and old).
*/
int parseSAPH(uint8_t *fname)
{
    return DAP_OK;
}


/*--parseSAPBody()--------------------------------------------------------------------------*/
int parseSAPB(U8 sid)
{
    uint32_t temp;
    int i = 0, j= 0;
    U8 *ch, *buff;
    U8 sz;
		uint32_t ParseLen = 192; 
    buff = loadSapFile(xap->sapfname, xst[sid].offset, ParseLen/*512*/);   //xap->sapfname, 0, 512);
    ch = buff;
    buff += ParseLen/*512*/;
    //if (ch[0] != (FT_MAGIC & 0xff) || ch[1] != ((FT_MAGIC>>8)&0xff) || ch[2] != ((FT_MAGIC>>16)&0xff) || ch[3] != ((FT_MAGIC>>24)&0xff)) {
    if (FT_MAGIC != *(uint32_t *)ch) {
        wP("Not SAP file, %08X vs %08X \n", *(uint32_t *)ch, FT_MAGIC);
        return ERROR_NOT_SAP_FILE;
    }

    i = 2 * FATNAMELEN + 4;
    //memset((void*)((char *)(xap)+ i), 0, sizeof(xSapFile) -i); 

    xap->patErase = 0;
    xap->resett= 0;
    xap->funCRC32  = 0;
    xap->funVerify = 0;
    xap->sap_version= 0;
    xap->funInit = 0;
    xap->funUnInit =  0;
    xap->funEraseChip = 0;
    xap->funBlankCheck =  0;
    xap->funEraseSector = 0;
    xap->funProgPage =  0;
    xap->szProg= 0;
    xap->toProg = 0;
    xap->toErase = 0;
    xap->szFlash = 0;

    xap->offCode = 0;
    xap->offCode = 0;
    xap->szCode = 0;
    xap->offData = 0;
    xap->offData = 0;
    xap->szData = 0;
    xap->szRam = 0;
    xap->vendor = 0;
    xap->dapid  = 0;
    xap->cpuid  = 0;
    xap->szPrgCode = 0;
    xap->szPrgData = 0;
    xap->startPC =   0;
    xap->startSP=    0;
    xap->startBF=    0;
    xap->startFlash = 0;
    xap->crcCode =  0;
    xap->crcData =  0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->funToDo =  0;
    xap->voltage =  0;
    xap->logging =  0;
    xap->swj     =  0x01;   // default to SWJ
		xap->ProgOffset=0;
		xap->NrfLock=0;

    i = 0;

    ch += 4;

    while(ch < buff) {
        // printf("SAP(%02x), %08x\n", *ch, (unsigned)ch);
        sz = *ch >> 6;//weib:flag group ensure the following data number.
        switch (*ch) {
            case FT_FLASH_TYPE:		   break;
            case FT_ERASE_PATTERN:	   xap->patErase =       toint(ch+1, sz); break;
            case FT_RAM_TYPE:		   break;
            case FT_RESET_TT:	       xap->resett=          toint(ch+1, sz); break;
            case FT_VOLTAGE:	       xap->voltage=         toint(ch+1, sz); break;
            case FT_FUN_CRC32:	       xap->funCRC32  =      toint(ch+1, sz); break;
            case FT_FUN_VERIFY:	       xap->funVerify =      toint(ch+1, sz); break;
            case FT_SAP_VERSION:       xap->sap_version=     toint(ch+1, sz); break;
            case FT_FUN_INIT:	       xap->funInit =        toint(ch+1, sz); break;
            case FT_FUN_UNINIT:        xap->funUnInit =      toint(ch+1, sz); break;
            case FT_FUN_ERASECHIP:     xap->funEraseChip =   toint(ch+1, sz); break;
            case FT_FUN_BLANKCHECK:    xap->funBlankCheck =  toint(ch+1, sz); break;
            case FT_FUN_ERASESECTOR:   xap->funEraseSector = toint(ch+1, sz); break;
            case FT_FUN_PROGPAGE:      xap->funProgPage =    toint(ch+1, sz); break;
            case FT_DRIVER_VER:		   	 xap->devver =    toint(ch+1, sz);		break;
            case FT_PROG_SIZE:	       xap->szProg=          toint(ch+1, sz); break;	/* TODO */
            case FT_PP_TIMEOUT:        xap->toProg =         toint(ch+1, sz); break;
            case FT_ES_TIMEOUT:        xap->toErase =        toint(ch+1, sz); break;
            case FT_FLASH_SIZE:	       xap->szFlash =        toint(ch+1, sz); break;

            case FT_CODE_OFFSET:       
                xap->offCode =        toint(ch+1, sz); 
                xap->offCode +=       xst[sid].offset;
                break;
            case FT_CODE_SIZE:         xap->szCode =         toint(ch+1, sz); break;

            case FT_DATA_OFFSET:       
                xap->offData =        toint(ch+1, sz); 
                xap->offData +=       xst[sid].offset;
                break;
            case FT_DATA_SIZE:	       xap->szData =         toint(ch+1, sz); break;
            case FT_RAM_SIZE:          xap->szRam =          toint(ch+1, sz); break;
            case FT_SECTOR_SIZE:       xap->sectors[i++>>1].szSector =    toint(ch+1, sz); break;
            case FT_SECTOR_START_ADDR: xap->sectors[i++>>1].startSector = toint(ch+1, sz); break;
            case FT_CMDAV:             xap->cfgregs[j++] =     toint(ch+1, sz); break;
            case FT_VENDOR:            xap->vendor =        toint(ch+1, sz); break;
            case FT_DAPID:             xap->dapid  =        toint(ch+1, sz); break;
            case FT_CPUID:             xap->cpuid  =        toint(ch+1, sz); break;
            case FT_DAPSPEED:          xap->dapspeed  =     toint(ch+1, sz); break;
            case FT_TARGET_SECTION:	   xap->gSpeedUp = 			toint(ch+1, sz);break;
            case FT_RAM_SECTION:       break;
            case FT_FLASH_SECTION:     break;
            case FT_CODE_SECTION:      break;
            case FT_DATA_SECTION:      break;
            case FT_SCRIPT_SECTION:	   break;
            case FT_PRGCODE_SIZE:      xap->szPrgCode =      toint(ch+1, sz); break;
            case FT_PRGDATA_SIZE:      xap->szPrgData =      toint(ch+1, sz); break;
            case FT_RAM_START_ADDR:    xap->startPC =        toint(ch+1, sz); break;
            case FT_STACK_START_ADDR:  xap->startSP=         toint(ch+1, sz); break;
            case FT_BUFFER_ADDR:       xap->startBF=         toint(ch+1, sz); break;
            case FT_SERIAL_SIG:        xap->serialsig=       toint(ch+1, sz); break;
            case FT_FLASH_START_ADDR:  xap->startFlash =     toint(ch+1, sz); break;
            case FT_CODE_CRC32:        xap->crcCode =        toint(ch+1, sz); break;
            case FT_DATA_CRC32:        xap->crcData =        toint(ch+1, sz); break;
            case FT_ID0:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<<24; break;
            case FT_ID1:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<<16; break;
            case FT_ID2:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<< 8; break;
            case FT_ID3:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<< 0; break;
            case FT_ID4:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<<24; break;
            case FT_ID5:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<<16; break;
            case FT_ID6:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<< 8; break;
            case FT_ID7:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<< 0; break;
            case FT_FUNTODOS:	       xap->funToDo =        toint(ch+1, sz); break;
            case FT_LOGGING:	       xap->logging =        toint(ch+1, sz); break;
            case FT_SWJ:	           xap->swj     =        toint(ch+1, sz); break;
						case FT_FUN_EXTRA:
						{
							uint32_t SeqCnt=toint(ch+1, sz);
							xSAPSeq *tmpDapExtraFun=NULL;	
							xap->ExtraFunCnt=SeqCnt;
							ch += sz + 2;
							xap->SapExtraFun=(xSAPSeq *)malloc(sizeof(xSAPSeq)*SeqCnt);
							if(xap->SapExtraFun)
							{				
								tmpDapExtraFun=xap->SapExtraFun;
								do{
									sz=4;
									tmpDapExtraFun->SeqAddress=toint(ch, sz);
									ch+=sz;
									tmpDapExtraFun->IsNeedConvert=(toint(ch, sz)>>16)&0xFF;
									tmpDapExtraFun->RW=(toint(ch, sz)>>8)&0xFF;
									tmpDapExtraFun->SeqLength=toint(ch, sz)&0xFF;
									ch+=sz;
									tmpDapExtraFun->SeqVal=toint(ch, sz);
									SeqCnt--;
								if(SeqCnt)
								{
									tmpDapExtraFun++;
									ch+=4;
								}
								else
									ch-=2;//should be 4,it is for the old fun
								}while(SeqCnt>0);
 							}
						}
							break;						
						case FT_SEQUENCE:	
						{
							uint32_t SeqCnt=toint(ch+1, sz);
							xSAPSeq *tmpSeq=NULL;
							xap->SEQCnt=SeqCnt;
							xap->SapSequence=(xSAPSeq *)malloc(sizeof(xSAPSeq)*SeqCnt);
							ch += sz + 2;
							if(xap->SapSequence)
							{
								tmpSeq=xap->SapSequence;
								do{
								sz=4;

								tmpSeq->SeqAddress=toint(ch, sz);
								ch+=sz;
								tmpSeq->IsNeedConvert=(toint(ch, sz)>>16)&0xFF;
								tmpSeq->RW=(toint(ch, sz)>>8)&0xFF;	
								tmpSeq->SeqLength=toint(ch, sz);
								ch+=sz;
								tmpSeq->SeqVal=toint(ch, sz);
								SeqCnt--;
								if(SeqCnt)
								{
									ch+=4;
									tmpSeq++;
								}
								else
									ch-=2;//should be 4,it is for the old fun
								}while(SeqCnt>0);
							}
						}
						break;
						case 	FT_AT_ERASEPIN:
						{
							sz=1;
							gNeedErasePin=toint(ch+1, sz);
						}
						break;
						case FT_NRF_EXTRAFUN:
						{
							
							int cnt;
							sz=4;
							if(NRF_UICR_BASE==toint(++ch, sz))//UICR
							{
								gNrfUICRExtra=1;
								ch+=sz;
								cnt=toint(ch, sz);
								ch+=sz;
								pnrf_uicr_t.CLENR0=toint(ch+UICR_CLENR0_OFFSET, sz);

								pnrf_uicr_t.RBPCONF=toint(ch+UICR_RBPCONF_OFFSET, sz);
		
								pnrf_uicr_t.XTALFREQ=toint(ch+UICR_XTALFREQ_OFFSET, sz);

								pnrf_uicr_t.FWID=toint(ch+UICR_FWID_OFFSET, sz);

								pnrf_uicr_t.BOOTLOADERADDR=toint(ch+UICR_BOOTLOADERADDR_OFFSET, sz);
								if(cnt)
									ch+=cnt;
								ch-=sz;
								ch-=2;
								
							}
						
						
						}
							
							break;	
						case FT_PROG_OFFSET:  	 xap->ProgOffset  = 	 toint(ch+1, sz); break;
						case FT_NRF_LOCK:  	 			xap->NrfLock  = 	 toint(ch+1, sz); break;
            case FT_END_SYMBOL:       ch = buff;                             break;

            default:		           break;
        }
        ch += sz + 2;
    }
if(xap->szProg>0x400)
	xap->szProg=0x400;
    if (xap->vendor == (KINETIS << 24)) {
        if (xap->startBF == 0) {
            if ((xap->szRam + (xap->startPC & 0xFFFFFFFE))  > (((xap->startSP+0x3FF)&0xFFFFFC00) + xap->szProg)) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else {
                temp = (xap->startPC & 0xFFFFFFFE) + xap->szPrgCode + xap->szPrgData;
                temp = (temp + 0x0F) & 0xFFFFFFF0;   // buffer address
                xap->startBF = temp;
            }
        }
    } else {
        if (xap->startBF == 0) {
            if (xap->szRam == 0) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else if (xap->szRam > 0 && ((xap->szRam + (xap->startPC & 0xFFFFFFFE))  > (((xap->startSP+0x3FF)&0xFFFFFC00) + xap->szProg))) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else {
                temp = (xap->startPC & 0xFFFFFFFE) + xap->szPrgCode + xap->szPrgData;
                temp = (temp + 0x0F) & 0xFFFFFFF0;   // buffer address
                xap->startBF = temp;
            }
        }
    }
		if (xap->vendor == (NODRIC << 24)) {
			if(xap->NrfLock)
				gNeedLockChip=1;
		}
    iP("\n RAM Size=%d, PC=0x%08X, SP=0x%08X, BF=%08X", xap->szRam, xap->startPC, xap->startSP, xap->startBF);

    xap->sectors[i>>1].szSector = 0xFFFFFFFF;
    xap->sectors[i>>1].startSector = 0xFFFFFFFF;
    xap->cfgregs[j++] = 0xFFFFFFFF;
    xap->cfgregs[j++] = 0xFFFFFFFF;

   // if (xap->vendor == (KINETIS << 24))
    //    xap->resett = 0x41;

    swd_calctime();

    xap->dapstate |= PREQ_SAPLOADED;
    dP("\nparseSAP Body()::Finished");
    if (xap->sreset)
        xap->resett = xap->sreset;

    if (xap->svoltage)
        xap->voltage = xap->svoltage;

    if (xap->sswj) 
        xap->swj = xap->sswj;

    if (xap->swj != SWJ_JTAG)
        xap->swj = SWJ_SWD;
    else
        xap->dapspeed = 100000;

    if (xap->dapspeed == 0)
        xap->dapspeed = SWJ_CLOCK_SPEED;
		if(clk_flag)
			xap->dapspeed = SWJ_CLOCK_SPEED/2;//try the half clock
    if (xap->slogging)
        xap->logging = xap->slogging;

    //if (xap->logging > 0) {
    //    xap->logging <<= 2;

    //}
		gBeepAfterProg =  (xap->logging>>2)&0x01;
		gAutoProg = (xap->logging>>3)&0x01;
//		xap->szProg=xap->szProg*2;
		if(xap->devver == QUARK_D20XX)
			gUseIap = xap->gSpeedUp;
		else
			gUseIap = 0;
    return DAP_OK;
}

/*----------------------------------------------------------------------------*/
/* fileoff -> ram address, size
 * R0      -> R1,          R2
 * xap->offCode, xap->startPC & (0xFFFFFFFE), xap->szCode 
 * */
static	U32 rest=0;
static	U32 off=0;
static	U32 read_time=0;
volatile static	U32 read_cnt=0;
static	U32 start_add=0;
static	U32 CodeRest=0;
static	U32 CodeRead=0;
#define MIN(a,b) (a<b?a:b)

/* --------------------------------------------------------------------------- */
int swd_runtarget(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = FUNRESPONSE(xPacket);

    
    return DAP_OK;
}
int quark_arc_runtarget(xDAPPacket *xPacket,struct target *t)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 tmp,rc;
    U8	*resp = FUNRESPONSE(xPacket);
		arc32_set_current_pc(&Quark_Arc_target,CODE_ADDR);
		//arc32_get_current_pc(&Quark_Arc_target,&tmp);
		rc = Quark_resume(&Quark_Arc_target,1,0,0,0);
		if(rc!=ERROR_OK)
			return DAP_ERROR;
		
		rc = WaitForHaltTimeout(&Quark_Arc_target,0xFFFFFFFF);
		if(rc != ERROR_OK)
			return rc;
		rc =	Quark_halt(0,&Quark_Arc_target);
		if(rc!=ERROR_OK)
			return DAP_ERROR;
    
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */

int swd_check_power(void)
{
	return 0;

}

/* --------------------------------------------------------------------------- */
int swd_power_control(xDAPPacket *xPacket)
{
	//return DAP_GONEXT;
	#if 1 /////////??????????
    int i, j, k;
    uint8_t pwrsts[PORT_COUNT];
		xDAPPort *dap = &pPort;
    uint8_t VCHECK = PWR_3V3;
		uint16_t port=xPacket->port;	
    switch (dap->substate) {
        //default:
        //    eP("\nswd_power_control() unknown state(%d)?", dap->substate);
        case 0:
            dap->led[0] = 0;
            dap->led[1] = 0;
            if ((hw_version(0) < 2)) {
                dap->regs[16] = DAP_GONEXT;
                goto POWER_LED;
            }
            dap->CTRL |= CTRL_NOQUERY;  // stop query

            /** Check if power ready */
            for(i=0;i<PORT_COUNT;i++)
                pwrsts[i] = 0;
                ///cP("\nChecking Voltage...");
            for(i=0;i<8;i++) {                  /// Sampling 8 times
                ADC_SoftwareStartConv(ADCx);
                vTaskDelay(10);

                for(j=0;j<PORT_COUNT;j++) {
                    pwrsts[j] = pwrsts[j]<<1;
                    if (adcbuf0[j] > 0x400)
                        pwrsts[j] |= 1;
                }
            }

            dap->xport = 0;
            for(i=0;i<PORT_COUNT;i++) {
                if ((pwrsts[i] & 0x7) == 0x7) {
                    dap->xport |= (1<<i);
                }
            }

            if (dap->xport) {
                for(i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                    } else {
                        xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_NO_POWER);
                        DAP_SetLed((1<<i), REDLED, LEDON);
                        cP("\nTarget No Power[%1X]!", (1<<i));
                    }
                }
                cP("\nTarget already powered.");
                dap->regs[16] = DAP_GONEXT;
                goto POWER_LED;
            } else if (xap->voltage == 0) {
                for(i=0;i<PORT_COUNT;i++) {
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_NO_POWER);
                    DAP_SetLed((1<<i), REDLED, LEDON);
                    eP("\nTarget No Power[%1X]!", (1<<i));
                }
                dap->regs[16] = DAP_GONEXT;
                goto POWER_LED;
            }
#if 0
            EXTI->IMR &= ~EXTI_Line0;    // disable overcurrent interrupt 
            EXTI->IMR &= ~EXTI_Line3;    // disable overcurrent interrupt 
            EXTI->IMR &= ~EXTI_Line10;    // disable overcurrent interrupt 
            EXTI->IMR &= ~EXTI_Line14;    // disable overcurrent interrupt 
#endif
             cP("\nPower ON[T:%d] ", xTaskGetTickCount());          
						for (k=0;k<5;k++) {
                /** Check if short */

                power_control(VCHECK, 0xF);  // 3V3, should be 1.8V final, power on
                dP("[T:%d] ", xTaskGetTickCount());
                for (j=0;j<PORT_COUNT;j++)
                    pwrsts[j] = 0;

                for (i=0;i<6;i++) {
                    ADC_SoftwareStartConv(ADCx);
                    vTaskDelay(10);

  ///                  cP("\nChecking Short...");
                    for(j=0;j<PORT_COUNT;j++) {
                        pwrsts[j] = pwrsts[j]<<1;
                        if (adcbuf0[j] < 0x200)
                            pwrsts[j] |= 1; // short
                    }
                }

                dap->xport = (1<<PORT_COUNT) - 1;
                cP("[T:%d] ", xTaskGetTickCount());
                for(i=0;i<PORT_COUNT;i++) {
                    if (pwrsts[i] & 0x07) {
                        dap->xport &= ~(1<<i); 
                    }
                }

                dap->vport = 0;
                for(i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                    } else {
                        //xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_PWR_SHORT);
                        //DAP_SetLed((1<<i), REDLED, LEDON);
                        cP("\nPort short[%1X]!", (1<<i));
                        dap->vport |= (1<<i);
                    }
                }
                if (dap->vport == 0)
                    break;
                else {
                    power_control(0,0);  // power off
                    cP("Re-Check Short...");
                    vTaskDelay(30);
                }
            }
 //           EXTI->IMR |= VDDFAULT_EXTI_Pins;    // enable overcurrent interrupt

            for(i=0;i<PORT_COUNT;i++) {
                if (dap->xport & (1<<i)) {
                } else {
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_PWR_SHORT);
                    DAP_SetLed((1<<i), REDLED, LEDON);
                    eP("\nPort short[%1X]!", (1<<i));
                }
            }

            if (dap->xport == 0) {
                dap->regs[16] = DAP_ERROR;
            } else {
                if (xap->voltage != VCHECK) {
                    power_control(PWR_0V,0);  // power off
                    vTaskDelay(10);
                    power_control(xap->voltage, dap->xport);
                    dP("[T:%d] ", xTaskGetTickCount());
                }
                dap->regs[16] = DAP_GONEXT;
            }
POWER_LED:
            dap->CTRL &= ~CTRL_NOQUERY;
            dap->funstate = 0;
            JUMPSUB(1);
            return swd_led(xPacket);            

        case 1:
            if (DAP_GONEXT != swd_led(xPacket))
                return DAP_OK;
            xap->dapstate |= PREQ_PWR_READY;
            return dap->regs[16];
    }
#endif	
}

/* ------------------------------------------------------------------------------*/
uint32_t Get_Quark_Chip(void)
{
	return (uint32_t)xap->devver;//QUARK_SE_C10XX;
}	
	static	int loop_cnt=0; 
//#define WITH_TIMEOUT
extern struct target_type quark_d20xx_target;

extern struct target_type  quark_se_target;
extern struct jtag_interface cmsis_dap_interface;

extern struct target_type  arc32_target;
int target_create(struct command_context *cmd_ctx,struct target *target);
#if 1

void ZeroAllInstance(void)
{
	memset(TapArray,0,sizeof(TapArray));
	memset(TargetArray,0,sizeof(TargetArray));
	memset(ContextArray,0,sizeof(ContextArray));
	memset(&Quark_target,0,sizeof(Quark_target));
}
extern int target_create_c(struct command_context *cmd_ctx,struct target *target,char *cp);
int InitInstance()
{
	struct jtag_tap *tap1=&TapArray[0];//calloc(1,sizeof(struct jtag_tap));
	struct jtag_tap *tap2=&TapArray[1];//calloc(1,sizeof(struct jtag_tap));
	struct jtag_tap *tap3=&TapArray[2];//calloc(1,sizeof(struct jtag_tap));
	struct jtag_tap *tap4=&TapArray[3];//calloc(1,sizeof(struct jtag_tap));	
	
	struct target *curTarget=&TargetArray[0];
	struct command_context *context=&ContextArray[0];
	
	int ChipType;
	int retval;
	
	ZeroAllInstance();
		
	global_cmd_ctx = context;	
	
	retval=Quark_init_jtag_interface(context,&cmsis_dap_interface,"jtag");//cmsis dap jtag	
	if( retval != ERROR_OK)	
		return DAP_ERROR;
	
	ChipType=Get_Quark_Chip();	
	
	/*****init tap list******/	
	if(ChipType==(int)QUARK_D20XX)
	{		
		uint32_t IdCode=0;

		Quark_target.type=&quark_d20xx_target;
		strcpy(tap1->chip,"quark_d2000");	
		strcpy(tap1->tapname,"quark");	
		tap1->ir_length=8;
		tap1->ir_capture_mask=0xFF;
		IdCode=0x38289013;
		tap1->expected_ids[0]=IdCode;
		tap1->enabled = false;	
		Quark_newtap_cmd(tap1);
		
		strcpy(tap2->chip,"quark_d2000");
		strcpy(tap2->tapname,"cltap");	
		tap2->ir_length=8;
		tap2->ir_capture_mask=0xFF;
		IdCode=0x0e786013;
		tap2->expected_ids[0]=IdCode;
		tap2->enabled = true;			
		Quark_newtap_cmd(tap2);
		target_create_c(context,&Quark_target,"quark_d20xx");
		
		gUseArc = 0;		
		gUseArcAlg = 0;		
		common_target = &Quark_target;
	}else if(ChipType==(int)QUARK_SE_C10XX)
	{
		uint32_t IdCode=0;

		Quark_target.type=&quark_se_target;//&quark_se_target;
		Quark_Arc_target.type = &arc32_target;
		
		strcpy(tap3->chip,"arc32");
		strcpy(tap3->tapname,"arc-em");	
		tap3->ir_length=4;
		tap3->ir_capture_mask=0xFF;
		IdCode=0x200044b1;
		tap3->expected_ids[0]=IdCode;	
		tap3->enabled = false;		
		
		Quark_newtap_cmd(tap3);

		
		strcpy(tap1->chip,"quark_se");	
		strcpy(tap1->tapname,"quark");	
		tap1->ir_length=8;
		tap1->ir_capture_mask=0xFF;
		IdCode=0x38289013;
		tap1->expected_ids[0] = IdCode;	
		tap1->enabled = false;	
		Quark_newtap_cmd(tap1);
		
		strcpy(tap2->chip,"quark_se");
		strcpy(tap2->tapname,"cltap");	
		tap2->ir_length=8;
		tap2->ir_capture_mask=0xFF;
		IdCode=0x0e765013;
		tap2->expected_ids[0] = IdCode;	
		tap2->enabled = true;		
		Quark_newtap_cmd(tap2);
		
		gUseArc = 1;		
		gUseArcAlg = 1;
		if(gUseArc)
		{
			target_create_c(context,&Quark_Arc_target,"arc32");
		}
		target_create_c(context,&Quark_target,"quark_se");
		common_target = &Quark_Arc_target;
	}else
	{
		 uP("Can not recognize the Quark core type.PLS check the SAP file,It should be D2000 or C1000.\r\n\r\n");
		ErrorCode =  FR_NOT_SAP_FILE;
			return DAP_ERROR;
	}	
 

	
}
			uint32_t buf[4] = {0x12345678,0x87654321,0x5A5A5A5A,0x5AA55AA5};
			
int Quark_Action_From_External(uint8_t cmdType)
{
	int retval,rc=0;
	xDAPPort *dap = &pPort;
	xDAPPacket xPacket = {0,0,0};
  xap->rrport = 0;
  dap->xport = xap->xport;   //((SPIx_RDY_GPIO_PORT->IDR) >> SPIx_RDY_SHIFT) & SPIx_RDY_MASK;
  xPacket.port = dap->xport;
	int i = 0,j=0;
	switch(cmdType)
	{
		case	USBSer_ACTION_CONNECT:
		{
			Jtag_IO_Init();
			InitInstance();
			struct command_context *context = global_cmd_ctx;
			uP("Checking Power..\r\n");
			rc = swd_power_control(&xPacket);
			if (rc != DAP_GONEXT) {
					return rc;
			}
			output_3v3_io();
			uP("Connecting..\r\n");
			dap->substate = 0;
			loop_cnt=0;
			retval=Quark_Connect(context,&cmsis_dap_interface,&Quark_target);
			if( retval != ERROR_OK)	
			{
				ErrorCode  =   TE_NO_ACK;
				return DAP_ERROR;
			}
			if(*(Quark_target.tap->next_tap->expected_ids)!=Quark_target.tap->next_tap->idcode)//cltap ID
			{
				if(Get_Quark_Chip()==QUARK_D20XX)
				{
					uP("quark_d2000.cltap ID  not found ");
				}
				else if(Get_Quark_Chip()==QUARK_SE_C10XX)
					uP("quark_c1000.cltap ID  not found");
				ErrorCode =   TE_TARGET_ID_MISMATCH;
				return DAP_ERROR;
			}
			else
			{
				if(Get_Quark_Chip()==QUARK_D20XX)
				{
					uP("quark_d2000.cltap ID found: 0x%x\r\n",*(Quark_target.tap->next_tap->expected_ids));
				}
				else if(Get_Quark_Chip()==QUARK_SE_C10XX)
					uP("quark_c1000.cltap ID found: 0x%x\r\n",*(Quark_target.tap->next_tap->expected_ids));
			}
			
		retval=Quark_target_init(&context,&Quark_Arc_target);
		if( retval != ERROR_OK)	
		{
			  ErrorCode =   TE_UNKNOWN_ERR;
				return DAP_ERROR;
		}	
		dap->substate = 0;
		uP("Power reset and halt the target..\r\n");						
		loop_cnt=0;
		dap->manstate = SM_RESET_HALT;	
		if(gUseArc == 0)
		{
			Quark_target.reset_halt = 1;//halt,but no reset
			retval=Quark_halt_and_reset(&Quark_target);		
		}else
		{
			Quark_Arc_target.reset_halt = 1;//halt,but no reset
			retval=Quark_halt(0,&Quark_Arc_target);
		}		
		//Quark_ChipErase(&Quark_Arc_target,0x40030000);
		//Quark_ChipErase(&Quark_Arc_target,0x40000000);
		//Quark_ChipErase(&Quark_Arc_target,0xFFFFE000);
		if( retval != ERROR_OK)	
			{
				 ErrorCode =   TE_HALT_FAIL;
				return DAP_ERROR;
			}

		dap->substate = 0;
		loop_cnt=0;
		}		

			break;
		
		case USBSer_ACTION_BLANKCHECK:
		{
			dap->manstate = SM_BLANKCHECK;
			xap->funToDo |= DO_BLANKCHECK;
			dap->substate = 0;
			uP("Blank Check.\r\n");
			if (dap->manstate == SM_BLANKCHECK && xap->funToDo & DO_BLANKCHECK) {
			rc = Quark_BlankCheck(&xPacket,&Quark_target);
			if (rc != DAP_OK) {
					return rc;
			}
			}
			
		}
		break;
		case USBSer_ACTION_ERASE:

		{
			if (xap->funToDo & DO_ERASESECTOR) {
					dap->manstate = SM_ERASESECTOR;
			} else if (xap->funToDo & DO_ERASECHIP) {
					xap->doChipEraseBySector = 0;
					if (0/*xap->funEraseChip == 0x00*/ ) 
						{
							if (xap->funEraseSector > 0) {
									dap->funToDo = xap->funToDo;
									dap->manstate = SM_ERASESECTOR;
									xap->funToDo &= ~DO_ERASECHIP;
									xap->funToDo |= DO_ERASESECTOR;
									xap->doChipEraseBySector = 1;
							} else {
									IiP(SM_ERASECHIP, OPT_NF|OPT_ERR|OPT_MSG, NULL, ErrMsg(ALG_EC_NOT_SUPPORT, OPT_ERR));
									return DAP_ERROR;
							}
					} else {
							dap->manstate = SM_ERASECHIP;
					}
			}
			dap->substate = 0;
			if(dap->manstate == SM_ERASECHIP)
			{
				//uP("Erase all chip.\r\n");
			}
			else if(dap->manstate == DO_ERASESECTOR)
			{
				uP("Erase sector.\r\n");
			}
			if (dap->manstate == SM_ERASESECTOR && xap->funToDo & DO_ERASESECTOR) {
					rc = Quark_erasesector(&xPacket,&Quark_target);
					if (rc != DAP_OK) {
							return rc;
					}
			}

			if (dap->manstate == SM_ERASECHIP && xap->funToDo & DO_ERASECHIP) {
					uP("Erase all chip.\r\n");
					rc = Quark_erasechip(&xPacket,&Quark_target);
					if (rc != DAP_OK) {
							return rc;
					}
			}				
		}
			break;
		case USBSer_ACTION_PROGRAM:
		{
			dap->manstate = SM_PROGPAGE;
			dap->substate = 0;
			if (xap->funToDo & DO_PROGRAM) {
				HAL_Delay(200);
				uP("Programming ..!!!\r\n");
				HAL_Delay(200);
				rc = Quark_Prog(&xPacket,&Quark_target);
				if (rc != DAP_OK) {
					return rc;
				}
			}	
		}			
			break;
		case USBSer_ACTION_CRC:		
		{
			dap->manstate = SM_CRC32;
			//if (xap->funToDo & DO_CRC32)
				uP("Verify!!!\r\n");							
			dap->substate = 0;
				if (xap->funToDo & DO_CRC32) {
					if(!gUseIap)
						rc = Quark_Verify(&xPacket,&Quark_target);

						if (rc != DAP_OK) {
								return rc;
						}else{
						//	if(gNeedLockChip)
							//LockNrfChip(xPacket);
							dP("\nCurrent SAP[%d] id = %d", xap->sapid, xst[xap->sapid].id);
							dP("\nNext    SAP[%d] id = %d", xap->sapid+1, xst[xap->sapid+1].id);

							if (xst[xap->sapid+1].id > 0) {
									return DAP_NEXTSAP;
							} else {
									xap->sapid = 0;
							}

							if (dap->program_serial) {
									dP("\nLogging serial...");
									for(rc=0;rc<PORT_COUNT;rc++) {
											if (dap->xport & (1<<rc)) 
													xsf->state[rc] = 0x88;
									}
									/* if log error, then stop */
									if(serial_log(xsf, 1) > 0) {
											dap->program_serial += 1;   
									}
							}							
								
						
					}
				}	
			}				
			break;
		case USBSer_ACTION_DISCONNECT:
			rc = Quark_DisConnect(global_cmd_ctx,&cmsis_dap_interface,&Quark_target);
			if(rc!= DAP_OK)
				return rc;
		break;
		
	}
	return DAP_OK;
}
	
int swd_auto(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    int rc, i,j;
	//uint8_t data_t[128];
		int retval = ERROR_OK;
		int gNeedInitClk;
		//uint8_t val[256]={0};		
		InitInstance();
		struct command_context *context = global_cmd_ctx;
		Jtag_IO_Init();
	
		//gUseIap = 1; //weib: for debug
		dap->substate=0;
		
		output_3v3_io();
		uP("Checking Power..\r\n");
		rc = DAP_OK;
		rc = swd_power_control(&xPacket);
		if (rc != DAP_GONEXT) {
				return rc;
		}
		output_3v3_io();
				HAL_Delay(200);	
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_SET);
		uP("Connecting..\r\n");
		dap->substate = 0;
		loop_cnt=0;
		retval=Quark_Connect(context,&cmsis_dap_interface,&Quark_target);
		if( retval != ERROR_OK)	
		{
			if(gHaRetried)
			{
				gHaRetried = 0;
				 ErrorCode =   TE_NO_ACK;
				return DAP_ERROR;	
			}else
			{
				uP("Retry...!!!\r\n\r\n");
				gHaRetried = 1;
				return DAP_AGAIN;
			}
		}	
		if(*(Quark_target.tap->next_tap->expected_ids)!=Quark_target.tap->next_tap->idcode)//cltap ID
		{
			 ErrorCode =   TE_INVALID_ACK;
			return DAP_ERROR;
		}
		else
		{
			if(Get_Quark_Chip()==QUARK_D20XX)
			{
				uP("quark_d2000.cltap ID found: 0x%x\r\n",*(Quark_target.tap->next_tap->expected_ids));
			}
			else if(Get_Quark_Chip()==QUARK_SE_C10XX)
			{
				uP("quark_c1000.cltap ID found: 0x%x\r\n",*(Quark_target.tap->next_tap->expected_ids));
			}
		}
		if(gUseArc)
			retval=Quark_target_init(&context,&Quark_Arc_target);
		else
			retval=Quark_target_init(&context,&Quark_target);		
		
		if( retval != ERROR_OK)	
		{
			ErrorCode =   TE_UNKNOWN_ERR;
			return DAP_ERROR;
		}	
		
		dap->substate = 0;
		uP("Power reset and halt the target..\r\n");						
		loop_cnt=0;
		dap->manstate = SM_RESET_HALT;	
		if(gUseArc == 0)
		{
			Quark_target.reset_halt = 1;//halt,but no reset
			retval=Quark_halt(0,&Quark_target);		
		}else
		{
			Quark_Arc_target.reset_halt = 1;//halt,but no reset
			retval = Quark_halt(0,&Quark_Arc_target);
			if( retval != ERROR_OK)	
			{
				ErrorCode =  TE_HALT_FAIL;
				uP("Can not halt the core.\r\n\r\n");
				return DAP_ERROR;	
			}
			if(gEraseAllChip)
			{
				retval = Quark_ChipErase(&Quark_Arc_target,0x40000000);
				if( retval != ERROR_OK)	
				{
					if(gHaRetried)
					{
						gHaRetried = 0;
						uP("Check the core fail.\r\n\r\n");
						 ErrorCode =    ALG_TIMEOUT;
						return DAP_ERROR;	
					}else
					{
						uP("Retry...!!!\r\n\r\n");
						gHaRetried = 1;
						return DAP_AGAIN;
					}
				}	
				retval = Quark_ChipErase(&Quark_Arc_target,0x40030000);
				if( retval != ERROR_OK)	
				{
					if(gHaRetried)
					{
						gHaRetried = 0;
						 ErrorCode =    ALG_TIMEOUT;
						return DAP_ERROR;	
					}else
					{
						uP("Check the core fail. Retry...!!!\r\n\r\n");
						gHaRetried = 1;
						return DAP_AGAIN;
					}
				}	
				//retval = Quark_ChipErase(&Quark_Arc_target,0xFFFFE000);				

				//HAL_GPIO_WritePin(GPIOC,GPIO_PIN_11,0);
				HAL_Delay(200);
				//HAL_GPIO_WritePin(GPIOC,GPIO_PIN_11,1);	
				gHadEraseAllChip = 1;				
			}
		}
		#if 0
		{
			uint32_t tmp = 0x01020304;
			
					//	arc32_get_current_pc(&Quark_Arc_target,&tmp);
			Quark_WriteMem(&Quark_Arc_target,CODE_ADDR ,4,4, buf);
			Quark_ReadMem(&Quark_Arc_target,CODE_ADDR ,4,4, Veribuf);
			Quark_WriteMem(&Quark_Arc_target,CODE_ADDR ,4,1, &buf);
			
		//	tmp = 0x7;
			//Quark_WriteMem(&Quark_Arc_target,0xB0800008 ,4,1, &tmp);
			//tmp = 0x87;
			//Quark_WriteMem(&Quark_Arc_target,0xB0800008 ,4,1, &tmp);
		}
#endif
		#if 0
		{

			//Quark_WriteMem(&Quark_Arc_target,0xA8000000 ,4,4, buf);	
			memset(Veribuf,0,sizeof(Veribuf));
			Quark_ReadMem(&Quark_Arc_target,0x40030000 ,4,256, Veribuf);
			Quark_ReadMem(&Quark_Arc_target,0xA8000004 ,4,1, Veribuf);			
				Quark_ReadMem(&Quark_Arc_target,0xA8000008 ,4,1, Veribuf);	
			Quark_ReadMem(&Quark_Arc_target,0xA800000C ,4,1, Veribuf);				
		}
		#endif
		dap->substate = 0;
		loop_cnt=0;
		if(gUseIap)
		{

					
			dap->manstate = SM_LOAD_CODE;
			uP("\r\nDownload the algorithm...\r\n");							
			dap->funToDo = 0;
			if (xap->funToDo & DO_LOADCODE) {
				if(1)
				{
					rc = Quark_loadcode(xPacket,&Quark_target,0x01);
					if (rc != DAP_OK) {
							return rc;
					}else
					{
						dap->substate = 0;
						
						//return rc;
					}
									
				}
			}
			rc = Quark_loadcode(xPacket,&Quark_target,0x10);
			
			Quark_target.reset_halt = 0;//halt,then reset
			retval=Quark_halt_and_reset(&Quark_target);
			if( retval != ERROR_OK)	
			{
				ErrorCode =  TE_HALT_FAIL;
				return DAP_ERROR;
			}
			Quark_target.reset_halt = 1;//halt,then no reset
			retval=Quark_halt_and_reset(&Quark_target);
			if( retval != ERROR_OK)
			{	
				ErrorCode =  TE_HALT_FAIL;
				return DAP_ERROR;
			}
	
			retval=Quark_resume(&Quark_target,0,0,1,0);
			if(retval!=ERROR_OK)
			{
				ErrorCode =  TE_HALT_FAIL;
				return DAP_ERROR;	
			}	
			uint32_t  tmp;
			Quark_halt(0,&Quark_target);
			retval=Quark_read_hw_reg(&Quark_target,EIP,(uint32_t *)&tmp);
			if(retval!=ERROR_OK)
			{
				ErrorCode =  TE_HALT_FAIL;
				return DAP_ERROR;
			}
			//Quark_ReadMem(&Quark_target,0x40030000 ,4,64, val);
			//Quark_ReadMem(&Quark_target,0xFFFFFFF0 ,4,64, val);	
			//Quark_ReadMem(&Quark_target,0xFFFFE200 ,4,64, val);		
			//Quark_ReadMem(&Quark_target,0xA8012800 ,4,64, val);				
			//Quark_ReadMem(&Quark_target,0x180000 ,4,64, val);
			//if(val != 0xFFFFFFFF) //ROM is not blank,the image of the first 2K is used for algorithm,so the 2K image shouble be saved. 
			//{
			//	Quark_ReadMem(&Quark_target,0x0 ,4,64, val);
			//	WriteBufToSpiFlash();
			//}
			//Quark_ChipErase(&Quark_target,0x200000);
			retval=Quark_resume(&Quark_target,0,0,1,0);
			if(retval!=ERROR_OK)
			{
				ErrorCode =  TE_HALT_FAIL;
				return DAP_ERROR;	
			}

			
			retval = Quark_DisConnect(global_cmd_ctx,&cmsis_dap_interface,&Quark_target);
			HAL_Delay(200);
			
			//SerialVerifyTargetFlash(USART_SERIAL,0x181800,0);
			//retval = SerialEraseTargetFlash(USART_SERIAL,0x181800,1);
			//retval = SerialWriteTargetFlash(USART_SERIAL,0x181800,0x8000000,1024);
			//led_green_on();	
			//Quark_target.reset_halt = 1;//halt,then reset
			//retval=Quark_halt_and_reset(&Quark_target);	
			if(retval != 0)
			{
				led_red_on();	
				return retval;
			}				
		}


		if(gUseArcAlg&&gUseArc)
		{
			uint32_t tmp = 0;		
			arc32_get_current_pc(&Quark_Arc_target,&tmp);
			dap->manstate = SM_LOAD_CODE;
			uP("\r\nDownload the algorithm...\r\n");							
			dap->funToDo = 0;
			if (xap->funToDo & DO_LOADCODE) {
				if(1)
				{
					rc = Quark_loadcode(xPacket,&Quark_Arc_target,0x100);
					if (rc != DAP_OK) {
							return rc;
					}else
					{
						dap->substate = 0;
					}
									
				}
			}

			Quark_ReadMem(&Quark_Arc_target,0xFFFFE200,4,1,&tmp);//OSC0_CFG1
			if(tmp != 0xFFFFFFFF)
			{
				uint32_t CFG1_VAL = 0;
				Quark_ReadMem(&Quark_Arc_target,0xB0800000+8,4,1,&CFG1_VAL);//OSC0_CFG1
				if((CFG1_VAL >> OSC0_CFG1_SI_FREQ_SEL_OFFS)&0xFF !=0x00)//CLK_SYS_HYB_OSC_32MHZ
				{
					gNeedInitClk = 1;
				}
				if((CFG1_VAL )&0x02 ==0x00)//QM_OSC0_EN_SI_OSC
				{
					gNeedInitClk = 1;
				}
			}
			if(tmp == 0xFFFFFFFF||gNeedInitClk == 1)//if no code
			{
				uP("Reinit clock\r\n\r\n");
				Quark_ReadMem(&Quark_Arc_target,0xB0800000+8,4,1,&tmp);//OSC0_CFG1
				
				
				tmp &= ~OSC0_CFG1_SI_FREQ_SEL_MASK;
				//tmp |= (CLK_SYS_HYB_OSC_32MHZ << OSC0_CFG1_SI_FREQ_SEL_OFFS);
				tmp &= ~(0x11<<OSC0_CFG1_SI_FREQ_SEL_OFFS);
		/* Enable the silicon oscillator */
				tmp |= QM_OSC0_EN_SI_OSC;
				Quark_WriteMem(&Quark_Arc_target,0xB0800000+8,4,1,&tmp);
				//OSC0_CFG1	
				do{
					Quark_ReadMem(&Quark_Arc_target,0xB0800000+4,4,1,&tmp);
				}while(!(tmp& QM_OSC0_LOCK_SI));	
						
				
				Quark_ReadMem(&Quark_Arc_target,0xB0800000+0x38,4,1,&tmp);
					
				tmp |= QM_CCU_SYS_CLK_SEL |
								 (CLK_SYS_DIV_1 << QM_CCU_SYS_CLK_DIV_OFFSET);	
				//tmp |= 1<<0;
				
				Quark_WriteMem(&Quark_Arc_target,0xB0800000+0x38,4,1,&tmp);
				
					tmp |=QM_CCU_SYS_CLK_DIV_EN;
				Quark_WriteMem(&Quark_Arc_target,0xB0800000+0x38,4,1,&tmp);
			}
				
				
				
				
			tmp = CODE_ADDR;				
			//Quark_halt(0,&Quark_Arc_target);
		//	Quark_ReadMem(&Quark_Arc_target,0x40030000 ,4,256, Veribuf);
			Quark_WriteMem(&Quark_Arc_target,0xA8000000 ,4,1, &tmp);
			 tmp = 1<<24;		
			Quark_WriteMem(&Quark_Arc_target,0xB0800600 ,4,1, &tmp);
//			Quark_ReadMem(&Quark_target,0x40030000 ,4,8, val);
		//	retval=Quark_read_hw_reg(&Quark_target,EIP,(uint32_t *)&val[0]);
		//	if(retval!=ERROR_OK)
		//		return DAP_ERROR;
		//	retval=Quark_Step(&Quark_Arc_target,0,0,1);
		//	if(retval!=ERROR_OK)
		//		return DAP_ERROR;	
		//	retval=Quark_read_hw_reg(&Quark_target,EIP,(uint32_t *)&val[0]);
		//	if(retval!=ERROR_OK)
		//		return DAP_ERROR;	

		  //Quark_ReadMem(&Quark_target,0x280800 ,4,8, val);	
			//Quark_Arc_target.reset_halt = 0;//halt,then no reset
			//retval=Quark_halt_and_reset(&Quark_Arc_target);
			//if( retval != ERROR_OK)	
			//	return DAP_ERROR;	
			//HAL_Delay(1000);
			//retval=Quark_halt(0,&Quark_Arc_target);
			arc32_set_current_pc(&Quark_Arc_target,0xA8000000);
			arc32_get_current_pc(&Quark_Arc_target,&tmp);
		//	retval=Quark_resume(&Quark_Arc_target,1,0,0,0);
			//if(retval!=ERROR_OK)
			//	return DAP_ERROR;		
			retval=Quark_halt(0,&Quark_Arc_target);	
			if(retval!=ERROR_OK)
			{
				ErrorCode =  TE_HALT_FAIL;
				return DAP_ERROR;		
			}
			//memset(Veribuf,0,sizeof(Veribuf));
			//Quark_ReadMem(&Quark_Arc_target,0x40030000 ,4,256, Veribuf);
//			retval=Quark_read_hw_reg(&Quark_Arc_target,EIP,(uint32_t *)&tmp);
//			if(retval!=ERROR_OK)
	//			return DAP_ERROR;
			//Quark_ReadMem(&Quark_target,0x40030000 ,4,64, val);
			//Quark_ReadMem(&Quark_target,0xFFFFFFF0 ,4,64, val);	
			//Quark_ReadMem(&Quark_target,0xFFFFE200 ,4,64, val);		
			//Quark_ReadMem(&Quark_target,0xA8012800 ,4,64, val);				
			//Quark_ReadMem(&Quark_target,0x180000 ,4,64, val);
			//if(val != 0xFFFFFFFF) //ROM is not blank,the image of the first 2K is used for algorithm,so the 2K image shouble be saved. 
			//{
			//	Quark_ReadMem(&Quark_target,0x0 ,4,64, val);
			//	WriteBufToSpiFlash();
			//}
			//Quark_ChipErase(&Quark_target,0x200000);
//			retval=Quark_resume(&Quark_Arc_target,1,0,0,0);
//			if(retval!=ERROR_OK)
//				return DAP_ERROR;	
			

			
//			retval = Quark_DisConnect(global_cmd_ctx,&cmsis_dap_interface,&Quark_Arc_target);
//			HAL_Delay(200);
			
			//SerialVerifyTargetFlash(USART_SERIAL,0x181800,0);
			//retval = SerialEraseTargetFlash(USART_SERIAL,0x181800,1);
			//retval = SerialWriteTargetFlash(USART_SERIAL,0x181800,0x8000000,1024);
			//led_green_on();	
			//Quark_target.reset_halt = 1;//halt,then reset
			//retval=Quark_halt_and_reset(&Quark_target);	
//			if(retval != 0)
//			{
//				led_red_on();	
//				return retval;
//			}			
		}			
		//xap->startFlash = 0x180000;
//Load Code is dummy
			/*
			 * BLANKCHECK/ERASESECTOR/ERASECHIP can only be executed one of them, or none.
			 */
		_NEXT_SAP:
		if(Get_Quark_Chip()==(int)QUARK_D20XX)
		{		
		uint32_t IdCode=0;
		if(xap->startFlash<0x1FFF)
		{
			uP("\r\nUpdate Quark D2000 rom.\r\n\r\n");
		}else if(xap->startFlash<0x00187FFF&&xap->startFlash>=0x00180000)
		{
			uP("\r\nProgram Quark D2000 Flash Code.\r\n\r\n");
		}else if(xap->startFlash<0x00200FFF&&xap->startFlash>=0x00200000)
		{		
			uP("\r\nProgram Quark D2000 OTP Data\r\n\r\n");
		}else
			uP("\r\nUnvalible address for Quark D2000\r\n\r\n");
		}else if(Get_Quark_Chip()==(int)QUARK_SE_C10XX)
		{
			if(xap->startFlash<0x40030000&&xap->startFlash>=0x40000000)
			{
				uP("\r\nProgram Quark C1000 flash 0.\r\n\r\n");
			}else if(xap->startFlash<0x40060000&&xap->startFlash>=0x40030000)
			{
				uP("\r\nProgram Quark  C1000 flash 1.\r\n\r\n");
			}else if(xap->startFlash>=0xFFFFE000)
			{
				uP("\r\nUpdate Quark C1000 rom\r\n\r\n");
			}else
				uP("\r\nUnvalible address for Quark C1000\r\n\r\n");
		}
		
		
		
			if (xap->funToDo & DO_BLANKCHECK) {
					dap->manstate = SM_BLANKCHECK;
			} else if (xap->funToDo & DO_ERASESECTOR) {
					dap->manstate = SM_ERASESECTOR;
			} else if (xap->funToDo & DO_ERASECHIP) {
					xap->doChipEraseBySector = 0;
					if (0/*xap->funEraseChip == 0x00*/ ) 
						{
							if (xap->funEraseSector > 0) {
									dap->funToDo = xap->funToDo;
									dap->manstate = SM_ERASESECTOR;
									xap->funToDo &= ~DO_ERASECHIP;
									xap->funToDo |= DO_ERASESECTOR;
									xap->doChipEraseBySector = 1;
							} else {
									IiP(SM_ERASECHIP, OPT_NF|OPT_ERR|OPT_MSG, NULL, ErrMsg(ALG_EC_NOT_SUPPORT, OPT_ERR));
									return DAP_ERROR;
							}
					} else {
							dap->manstate = SM_ERASECHIP;
					}
			}
			dap->substate = 0;
			if(dap->manstate == SM_ERASECHIP)
			{
					vTaskDelay(200);	
				uP("Erase all chip.\r\n");
					vTaskDelay(200);	
			}
			else if(dap->manstate == DO_ERASESECTOR)
			{
				uP("Erase sector.\r\n");
			}
			else if(dap->manstate == DO_BLANKCHECK)
			{
				uP("Blank Check.\r\n");
			}
			if (dap->manstate == SM_BLANKCHECK && xap->funToDo & DO_BLANKCHECK) {
					rc = Quark_BlankCheck(xPacket,&Quark_target);
					if (rc != DAP_OK) {
							return rc;
					}
					if (dap->xport == 0)
							return rc;
			}
			if (dap->manstate == SM_ERASESECTOR && xap->funToDo & DO_ERASESECTOR) {
					rc = Quark_erasesector(&xPacket,&Quark_target);
					if (rc != DAP_OK) {
							return rc;
					}
			}

			if (dap->manstate == SM_ERASECHIP && xap->funToDo & DO_ERASECHIP) {
					if(gHadEraseAllChip)
					{
						rc = DAP_OK;
						vTaskDelay(200);	
						uP("\r\nErase Done.\r\n\r\n"); 
						vTaskDelay(200);	
					}
					else
					{
						rc = Quark_erasechip(&xPacket,&Quark_Arc_target);
					}
					if (rc != DAP_OK) {
							return rc;
					}
			}
			//memset(Veribuf,0,sizeof(Veribuf));
			//Quark_ReadMem(&Quark_Arc_target,xap->startFlash+0x200 ,4,256, Veribuf);
			//if(Veribuf[0] != 0xFFFFFFFF||Veribuf[1] != 0xFFFFFFFF)
			//	return DAP_ERROR;
			if (dap->funToDo ) {
				 // xap->funToDo = dap->funToDo;        // restore xap->funToDo
				//  dap->funToDo = 0;
			}
			dap->manstate = SM_PROGPAGE;					
			dap->substate = 0;
			if (xap->funToDo & DO_PROGRAM) {
				uP("Programming...\r\n");					
				rc = Quark_Prog(&xPacket,&Quark_Arc_target);
				if (rc != DAP_OK) {
					ErrorCode =  ALG_PP_FAIL;
					return rc;
				}
			}
			gUp=0;
			dap->manstate = SM_CRC32;						
			dap->substate = 0;
				if (xap->funToDo & DO_CRC32) {
						uP("Verify!!!\r\n");	
						if(!gUseIap&&!gUseArcAlg) 
						{
					   rc = Quark_Verify(&xPacket,&Quark_Arc_target);
						}else if(gUseArcAlg)
						{
							uint32_t tmp,VerifyCrc;	
							arc32_set_current_pc(&Quark_Arc_target,CODE_ADDR);
								//arc32_get_current_pc(&Quark_Arc_target,&tmp);
							
					//		#define SIG 0x69317812
					//#define CODE_ADDR CODE_ADDR
					//#define DATA_ADDR 0xA8002000
					//#define CONFIG_ADDR 0xA8001000
							
							OP_Config_t.address = xap->startFlash;
							OP_Config_t.sig = SIG;
							OP_Config_t.size = xap->szData;
							OP_Config_t.type = OP_VERIFY;
							OP_Config_t.crc = xap->crcData;
							dap->exp = Quark_WriteMem(&Quark_Arc_target,CONFIG_ADDR,4,sizeof(OP_Config_t)>>2,(U8 *)&(OP_Config_t));
							//dap->exp = Quark_ReadMem(&Quark_Arc_target,CONFIG_ADDR,4,sizeof(OP_Config_t)>>2,Veribuf);	
							rc = Quark_resume(&Quark_Arc_target,1,0,0,0);
							if(rc!=ERROR_OK)
							{
								ErrorCode =  TE_HALT_FAIL;
								return DAP_ERROR;
							}
							uP(".");
							rc = WaitForHaltTimeout(&Quark_Arc_target,10000);
							if(rc != ERROR_OK)
									return rc;
							uP(".");
							rc =	Quark_halt(0,&Quark_Arc_target);
							if(rc!=ERROR_OK)
								return DAP_ERROR;
							dap->exp = Quark_ReadMem(&Quark_Arc_target,RC_ADDR ,4,1, &VerifyCrc);
							if(VerifyCrc == xap->crcData)
							{
								 char crc32[18];
								uP("\r\nVerify OK!! CRC is 0x%X\r\n\r\n",VerifyCrc);
								 sprintf(crc32,"%s0X%08X ",MSGSTR[ALG_CRC_PASS], VerifyCrc);
								IeP(SM_CRC32, OPT_NF|OPT_PORT|OPT_ADDR, dap->rets[i], crc32);
								rc = DAP_OK;
							}
							else
								rc = DAP_ERROR;
						}
					  else
						{
							if(xap->devver == QUARK_D20XX&&xap->startFlash ==0x200000)
								rc = DAP_OK;
							else if(xap->devver == QUARK_SE_C10XX&&gFixC1000 ==1)
								rc = DAP_OK;
							else
							 rc = SerialVerifyTargetFlash(USART_SERIAL,xap->startFlash+xap->ProgOffset,xap->szData,xap->crcData);
							
							if (rc != DAP_OK)
							{
									uP("Verify FAILED!!!\r\n");
									ErrorCode =  ALG_CRC_FAIL;
							}	
							else
							{

								uP("Verify OK!!!\r\n");	
							}
						}
						if (rc != DAP_OK) {
								uP("Verify FAILED!!!\r\n");	
								ErrorCode =  ALG_CRC_FAIL;
								return DAP_ERROR;
						}else{
						//	if(gNeedLockChip)
							//LockNrfChip(xPacket);
							dP("\nCurrent SAP[%d] id = %d", xap->sapid, xst[xap->sapid].id);
							dP("\nNext    SAP[%d] id = %d", xap->sapid+1, xst[xap->sapid+1].id);

							if (xst[xap->sapid+1].id > 0) {
								xPacket->port = 0; xPacket->offset = 0; xPacket->length = 0;							
								xap->sapid += 1;
								xap->dapstate = 0;
								rc=parseSAPB(xap->sapid);
								if(rc!=DAP_OK)
									return rc;
								goto _NEXT_SAP;
							} else {
									xap->sapid = 0;
							}

							if (dap->program_serial) {
									uP("\nLogging serial...");
									for(rc=0;rc<PORT_COUNT;rc++) {
											if (dap->xport & (1<<rc)) 
													xsf->state[rc] = 0x88;
									}
									/* if log error, then stop */
									if(serial_log(xsf, 1) > 0) {
											dap->program_serial += 1;   
									}
							}							
								
						
					}
				}
				if(xap->devver == QUARK_SE_C10XX&&gUseIap)
				{
					uP("\r\n\r\nClean the IAP Code!!\r\n");
						//MyMemInit();
					
					Quark_Action_From_External(USBSer_ACTION_CONNECT);
					Quark_PageErase(&Quark_target,0x4002F000);
					Quark_PageErase(&Quark_target,0x4002F800);
					if(gFixC1000)
					{
					//	for(i = 0;i<(4*1024)/sizeof(val);i++)
					//	{
					//		ReadBufFromSpiFlash(val,sizeof(val)*i,sizeof(val),QUARKROMFILE);
					//		Quark_WriteMem(&Quark_target,0x4002F000 + sizeof(val)*i,4,sizeof(val)>>2,val);
					//	}
						gFixC1000 = 0;
						
					}
					retval = Quark_DisConnect(global_cmd_ctx,&cmsis_dap_interface,&Quark_target);
					HAL_Delay(200);
				}
				dap->manstate = SM_RESET_GO;
				if (xap->funToDo & DO_RESETGO) 
					uP("Reset and go!!\r\n");							
				dap->substate = 0;

				if (xap->funToDo & DO_RESETGO) {
						//retval=Quark_halt_and_reset(&Quark_target);
						output_0v_io();
						HAL_Delay(200);
						output_3v3_io();	
						retval = ERROR_OK;			
						if( retval != ERROR_OK)	
							return DAP_ERROR;
	
				}
		dap->manstate = 0;
		dap->substate = 0;
    return DAP_OK;
}
#endif


/*----------------------------------------------------------------------------*/
int swd_runpreq(xDAPPacket *xPacket, uint32_t preq)
{
  #ifndef MC101_BOARD  
	xDAPPort *dap = &pPort;
    int rc;

    if (preq & PREQ_SAPSELECTED) {
        if (xap->sap_index == 0) {
            rc = DAPConfig(0);
            if (rc != DAP_OK) {
                eP("\nConfig file error:%s", MSGSTR[rc]);
                xI2CMsgQOptPut(dap->manstate, OPT_ERR|OPT_MSG, NOCODE, NOPORT, NOADDR, (uint8_t *)MSGSTR[rc]);
                xI2CMsgQPut(NULL, 0);
                return DAP_ERROR;
            }
        }

        if(DAP_SAP_SELECTED == fflistdir(NULL, xap->sap_index, xap->sapfname, NULL, SAPEXT)) {
            if (parseSAPH(xap->sapfname) < 0) {					/* parse sap file */
                eP("Parsing SAP file Failed!!!\n" );
                return DAP_ERROR;
            } else {
                xap->dapstate |= PREQ_SAPSELECTED;
            }
        } else {
            eP("\nSAP file not found(%d)", xap->sap_index);
            return DAP_ERROR;
        }
        dap->substate = 0;
        dap->funstate = 0;
    } 

    if (preq & PREQ_SAPLOADED) {
        if (parseSAPB(xap->sapid) < 0) {					/* parse sap file */
            eP("Parsing SAP file Failed!!!\n" );
            return DAP_ERROR;
        }
        dap->substate = 0;
        dap->funstate = 0;
    } 
    
    if (preq & PREQ_PWR_READY) {
        dap->manstate = SM_START;
        rc = swd_power_control(xPacket);
        if (rc != DAP_GONEXT) {
            return rc;  //DAP_GOON;
        }
        dap->substate = 0;
        dap->funstate = 0;
    }

    if (preq & PREQ_CONNECTED) {
        dap->manstate = SM_CONNECT;
        rc = swd_connect(xPacket);
        if (DAP_GONEXT != rc) {
            return rc;  //DAP_GOON;
        }
        dap->substate = 0;
        dap->funstate = 0;
    }

    if (preq & PREQ_INITED) {
        dap->manstate = SM_DAPRESET;
        rc = swd_powerup_reset(xPacket);
        if (rc == DAP_ERROR) { 
            dap->substate = 0;
            dap->funstate = 0;
            return rc;
        } else
            if (DAP_GONEXT != rc) {
                return DAP_GOON;
            }
        dap->substate = 0;
        dap->funstate = 0;
    }

    if (preq & PREQ_HALTED) {
        dap->manstate = SM_RESET_HALT;
        if (DAP_GONEXT != swd_reset_halt(xPacket)) {
            return DAP_GOON;
        }
        dap->substate = 0;
        dap->funstate = 0;
    }
    if (preq & PREQ_CODELOADED) {
        dap->manstate = SM_LOAD_CODE;
        if (DAP_GONEXT != swd_loadcode(xPacket)) {
            return DAP_GOON;
        }
        dap->substate = 0;
        dap->funstate = 0;
    }
    dap->substate = 0;
    dap->funstate = 0;
#endif
    return DAP_GONEXT;
}

/*----------------------------------------------------------------------------*/
/* 
 *
 *  */
uint32_t uint32(uint8_t *strno)
{
    int base=10;
    uint8_t *ch = strno;
    uint32_t ret = 0;
    U8 start = 0;

    while(*ch) {
        if (*ch == 'x' || *ch == 'X') {
            base = 16; ret = 0;
            ch++;
            start++;
            continue;
        }
        if (*ch >= '0' && *ch <= '9') {
            ret = ret * base + *ch - '0';
            ch++;
            start++;
            continue;
        }
        if (base==16 && *ch >= 'A' && *ch <= 'F') {
            ret = ret * base + *ch - 'A' + 10;
            ch++;
            start++;
            continue;
        }
        if (base==16 && *ch >= 'a' && *ch <= 'f') {
            ret = ret * base + *ch - 'a' + 10;
            ch++;
            start++;
            continue;
        }
        if (start==0 && (*ch==' ' ||*ch=='\t')) {
            ch++;
            continue;
        }
        break;
    }
    return ret;
}
/* ------------------------------------------------------------------------------*/
/*
 * nextarg(string)
 *      return next token
 */
uint8_t *nextarg(uint8_t *oparg)
{
    while(*oparg) {
        if (*oparg <= 32) {
            while(*oparg <= 32)
                oparg ++;
            return oparg;
        }
        oparg ++;
    }
    return oparg;
}
/*----------------------------------------------------------------------------*/
int strequ(uint8_t *pp, uint8_t *qq)	//const TCHAR *qq)
{
    while(*pp>32 || *qq>32) {
        if (*pp++ != *qq++)
            return 0;
    }
    return 1;
}

/*-----------------------------------------------------------------*/
int swd_setreset(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    if (dap->regs[0]) {
        xap->sreset = dap->regs[0];
        cP("\nForce reset to (%1x)", xap->sreset);
    }

    cP("\n low 4 bit:");
    cP("\n\t reset == 00 AUTO Reset.");
    cP("\n\t bit 0 ==> HW Reset.");
    cP("\n\t bit 1 ==> SYS Reset.");
    cP("\n\t bit 2 ==> VECT Reset(not tested).");
    cP("\n");
    cP("\n\t bit 4 ==> Normal connect(not supported).");
    cP("\n\t bit 5 ==> Connect with pre-reset.");
    cP("\n\t bit 6 ==> Connect with under-reset.");
    cP("\n\t bit 7 ==> reset after Connect(always and not affected by this bit.");

    return DAP_OK;
}

/*----------------------------------------------------------------------------*/

int swd_calcCRC32(xDAPPacket *xPacket)
{
    size_t sz = xap->szData, szC = 0;
    uint32_t toRead, crc = 0;
    U8 *buff;

    xap->szCache = 0;
    xap->offCache = 0;
    while(szC < sz) {
        toRead = sz - szC;
        if (toRead > 0x400)
            toRead = 0x400; 

     ////   buff = loadSapFileSeg(xap->sapfname, (xap->offData+szC), toRead, NULL);
        crc = calcCRC32(buff, toRead, crc);
        szC += toRead;
    }
    cP("\nCRC32=%08X", crc);
    return (int)crc;
}

/*----------------------------------------------------------------------------*/
#ifdef __SPIDER__
int swd_select_sap(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;

    if (dap->regs[0] > 0) {
        if(DAP_SAP_SELECTED == fflistdir(NULL, dap->regs[0], xap->sapfname, NULL, SAPEXT)) {
            cP("\nSAP file:%s, selected.", xap->sapfname);
            if (parseSAPH(xap->sapfname) < 0) {
                cP("\nParsing SAP file Failed!" );
            } 

            if (parseSAPB(dap->regs[1]) < 0) {
                cP("\nParsing SAP file, id=%d Failed!", dap->regs[1]);
            } else {
                xap->sapid = dap->regs[1];
            }
        } else {
            cP("\nSAP file no not found(%d)", dap->regs[0]);
        }
    } else {
        cP("\nsap usage:\n\tsap x(x=1..n)");
    }
    return DAP_OK;
}
#else

int parseSAPHeader(uint8_t *buff)
{
    // check magic, 
    uint8_t *ch, fid;
    int i,j=0;
    uint32_t cksum = 0, cks, *ul;
	//uint32_t temp;
		 U8 sz;
//    int sid = 0; ////?????
		int ret=0;
//    xDAPPort *dap = &pPort;
		int SapSectionCnt = 0;
    for(i=0;i<=MAX_SAP_SECTION;i++) {
        xst[i].id = 0;
        xst[i].offset = 0;
    }

    xap->szCache = 0;

	ul = (uint32_t *)buff;
    if (*ul != FT_MAGIC) {
        i = 0;
        do {
            dP("\n[%d] %08x", i, *ul);
            fid = (*ul) & 0xFF;
            xst[i].id = fid;
            cksum += *ul++;

            cksum += *ul;
            dP("#%08x", *ul);
            xst[i].offset = *ul++;

            cksum += *ul;
            dP("#%08x", *ul);
            if (fid == 0)
                cks = *ul;
            //xst[i].size = *ul ++; // not used
            ul++;

            dP("#%08x", cksum);
            i++;
            if (i > MAX_SAP_SECTION && fid != 0) {
                wP("\nSAP file header error.");
                return ERROR_NOT_SAP_FILE;
            }

        } while (fid > 0);

        if (cksum != (cks<<1)) {    // checksum should be zero, otherwise raise error.
            wP("\nSAP file header check sum error. %08X", cksum);
            return ERROR_NOT_SAP_FILE;
        }
    } else {
        xst[0].id = 1;
        xst[0].offset = 0;
        xst[1].id = 0;
        dP("\nOld SAP: [%d] %08X", xst[0].id, xst[0].offset);
    }
    for (i=0;xst[i].id!=0;i++) {
        dP("\nid[%d] offset[%08X].", xst[i].id, xst[i].offset);
        //ch = buff+xst[i].offset;
        //ul = (uint32_t *)ch;
				ret = GetSAPFile(xap->sapfname,(uint8_t *)ul,4,xst[i].offset);
			if(!ret)
				return ERROR_NOT_SAP_FILE;
        if (*ul != FT_MAGIC) {
            wP("\nNot SAP file, %08X vs %08x \n", *ul, FT_MAGIC);
            return ERROR_NOT_SAP_FILE;
        }
				SapSectionCnt ++;
				CloseSAPFile(xap->sapfname);
    }
//	xap->sapid = 0;
    xap->dapstate &= ~PREQ_SAPLOADED;
    xap->dapstate |= PREQ_SAPSELECTED;
////?????   dap->program_serial = SerFileInit(xsf, (char *)xap->sapfname); 
    // 0: no SER file existed
    // 1: do serial programming
    // 2: SER file problematic, prevent programming with bad serial file.
#if 0
	ch = buff+xst[xap->sapid)].offset;
    buff = (uint8_t *)ul+512;
	
    i = 2 * FATNAMELEN + 4;


    xap->patErase = 0;
    xap->resett= 0;
    xap->funCRC32  = 0;
    xap->funVerify = 0;
    xap->sap_version= 0;
    xap->funInit = 0;
    xap->funUnInit =  0;
    xap->funEraseChip = 0;
    xap->funBlankCheck =  0;
    xap->funEraseSector = 0;
    xap->funProgPage =  0;
    xap->szProg= 0;
    xap->toProg = 0;
    xap->toErase = 0;
    xap->szFlash = 0;

    xap->offCode = 0;
    xap->offCode = 0;
    xap->szCode = 0;
    xap->offData = 0;
    xap->offData = 0;
    xap->szData = 0;
    xap->szRam = 0;
    xap->vendor = 0;
    xap->dapid  = 0;
    xap->cpuid  = 0;
    xap->szPrgCode = 0;
    xap->szPrgData = 0;
    xap->startPC =   0;
    xap->startSP=    0;
    xap->startBF=    0;
    xap->startFlash = 0;
    xap->crcCode =  0;
    xap->crcData =  0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->funToDo =  0;
    xap->voltage =  0;
    xap->logging =  0;
    xap->swj     =  0x01;   // default to SWJ

    i = 0;
    ch += 4;
    while(ch < buff) {
        // printf("SAP(%02x), %08x\n", *ch, (unsigned)ch);
        sz = *ch >> 6;//weib:flag group ensure the following data number.
        switch (*ch) {
            case FT_FLASH_TYPE:		   break;
            case FT_ERASE_PATTERN:	   xap->patErase =       toint(ch+1, sz); break;
            case FT_RAM_TYPE:		   break;
            case FT_RESET_TT:	       xap->resett=          toint(ch+1, sz); break;
            case FT_VOLTAGE:	       xap->voltage=         toint(ch+1, sz); break;
            case FT_FUN_CRC32:	       xap->funCRC32  =      toint(ch+1, sz); break;
            case FT_FUN_VERIFY:	       xap->funVerify =      toint(ch+1, sz); break;
            case FT_SAP_VERSION:       xap->sap_version=     toint(ch+1, sz); break;
            case FT_FUN_INIT:	       xap->funInit =        toint(ch+1, sz); break;
            case FT_FUN_UNINIT:        xap->funUnInit =      toint(ch+1, sz); break;
            case FT_FUN_ERASECHIP:     xap->funEraseChip =   toint(ch+1, sz); break;
            case FT_FUN_BLANKCHECK:    xap->funBlankCheck =  toint(ch+1, sz); break;
            case FT_FUN_ERASESECTOR:   xap->funEraseSector = toint(ch+1, sz); break;
            case FT_FUN_PROGPAGE:      xap->funProgPage =    toint(ch+1, sz); break;
            case FT_DRIVER_VER:		   break;
            case FT_PROG_SIZE:	       xap->szProg=          toint(ch+1, sz); break;	/* TODO */
            case FT_PP_TIMEOUT:        xap->toProg =         toint(ch+1, sz); break;
            case FT_ES_TIMEOUT:        xap->toErase =        toint(ch+1, sz); break;
            case FT_FLASH_SIZE:	       xap->szFlash =        toint(ch+1, sz); break;

            case FT_CODE_OFFSET:       
                xap->offCode =        toint(ch+1, sz); 
                xap->offCode +=       xst[sid].offset;
                break;
            case FT_CODE_SIZE:         xap->szCode =         toint(ch+1, sz); break;

            case FT_DATA_OFFSET:       
                xap->offData =        toint(ch+1, sz); 
                xap->offData +=       xst[sid].offset;
                break;
            case FT_DATA_SIZE:	       xap->szData =         toint(ch+1, sz); break;
            case FT_RAM_SIZE:          xap->szRam =          toint(ch+1, sz); break;
            case FT_SECTOR_SIZE:       xap->sectors[i++>>1].szSector =    toint(ch+1, sz); break;
            case FT_SECTOR_START_ADDR: xap->sectors[i++>>1].startSector = toint(ch+1, sz); break;
            case FT_CMDAV:             xap->cfgregs[j++] =     toint(ch+1, sz); break;
            case FT_VENDOR:            xap->vendor =        toint(ch+1, sz); break;
            case FT_DAPID:             xap->dapid  =        toint(ch+1, sz); break;
            case FT_CPUID:             xap->cpuid  =        toint(ch+1, sz); break;
            case FT_DAPSPEED:          xap->dapspeed  =     toint(ch+1, sz); break;
            case FT_TARGET_SECTION:	   break;
            case FT_RAM_SECTION:       break;
            case FT_FLASH_SECTION:     break;
            case FT_CODE_SECTION:      break;
            case FT_DATA_SECTION:      break;
            case FT_SCRIPT_SECTION:	   break;
            case FT_PRGCODE_SIZE:      xap->szPrgCode =      toint(ch+1, sz); break;
            case FT_PRGDATA_SIZE:      xap->szPrgData =      toint(ch+1, sz); break;
            case FT_RAM_START_ADDR:    xap->startPC =        toint(ch+1, sz); break;
            case FT_STACK_START_ADDR:  xap->startSP=         toint(ch+1, sz); break;
            case FT_BUFFER_ADDR:       xap->startBF=         toint(ch+1, sz); break;
            case FT_SERIAL_SIG:        xap->serialsig=       toint(ch+1, sz); break;
            case FT_FLASH_START_ADDR:  xap->startFlash =     toint(ch+1, sz); break;
            case FT_CODE_CRC32:        xap->crcCode =        toint(ch+1, sz); break;
            case FT_DATA_CRC32:        xap->crcData =        toint(ch+1, sz); break;
            case FT_ID0:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<<24; break;
            case FT_ID1:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<<16; break;
            case FT_ID2:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<< 8; break;
            case FT_ID3:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<< 0; break;
            case FT_ID4:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<<24; break;
            case FT_ID5:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<<16; break;
            case FT_ID6:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<< 8; break;
            case FT_ID7:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<< 0; break;
            case FT_FUNTODOS:	       xap->funToDo =        toint(ch+1, sz); break;
            case FT_LOGGING:	       xap->logging =        toint(ch+1, sz); break;
            case FT_SWJ:	           xap->swj     =        toint(ch+1, sz); break;
            case FT_END_SYMBOL:        ch = buff;                             break;
            default:		           break;
        }
        ch += sz + 2;
    }

    if (xap->vendor == (KINETIS << 24)) {
        if (xap->startBF == 0) {
            if ((xap->szRam + (xap->startPC & 0xFFFFFFFE))  > (((xap->startSP+0x3FF)&0xFFFFFC00) + xap->szProg)) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else {
                temp = (xap->startPC & 0xFFFFFFFE) + xap->szPrgCode + xap->szPrgData;
                temp = (temp + 0x0F) & 0xFFFFFFF0;   // buffer address
                xap->startBF = temp;
            }
        }
    } else {
        if (xap->startBF == 0) {
            if (xap->szRam == 0) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else if (xap->szRam > 0 && ((xap->szRam + (xap->startPC & 0xFFFFFFFE))  > (((xap->startSP+0x3FF)&0xFFFFFC00) + xap->szProg))) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else {
                temp = (xap->startPC & 0xFFFFFFFE) + xap->szPrgCode + xap->szPrgData;
                temp = (temp + 0x0F) & 0xFFFFFFF0;   // buffer address
                xap->startBF = temp;
            }
        }
    }

    iP("\n RAM Size=%d, PC=0x%08X, SP=0x%08X, BF=%08X", xap->szRam, xap->startPC, xap->startSP, xap->startBF);

    xap->sectors[i>>1].szSector = 0xFFFFFFFF;
    xap->sectors[i>>1].startSector = 0xFFFFFFFF;
    xap->cfgregs[j++] = 0xFFFFFFFF;
    xap->cfgregs[j++] = 0xFFFFFFFF;

    if (xap->vendor == (KINETIS << 24))
        xap->resett = 0x41;

    swd_calctime();

    xap->dapstate |= PREQ_SAPLOADED;
    dP("\nparseSAP Body()::Finished");
    if (xap->sreset)
        xap->resett = xap->sreset;

    if (xap->svoltage)
        xap->voltage = xap->svoltage;

    if (xap->sswj) 
        xap->swj = xap->sswj;

    if (xap->swj != SWJ_JTAG)
        xap->swj = SWJ_SWD;
    else
        xap->dapspeed = 100000;

    if (xap->dapspeed == 0)
        xap->dapspeed = SWJ_CLOCK_SPEED;

/*    if (xap->slogging)
        xap->logging = xap->slogging;

    if (xap->logging > 0) {
        xap->logging <<= 2;
        file_ext_replace((char *)xap->sapfname, (char *)xap->logname, "LOG");
    }
*/
#endif
    return DAP_OK;
}

int swd_select_sap(xDAPPacket *xPacket)
{
	
    xDAPPort *dap = &pPort;
    int ret;
		int RstMode=0;
		uint32_t *vendor_rev=(uint32_t *)0x8002400;
		uint32_t	DEFAULT_VENDOR=(*vendor_rev)&0xFFFF;
	  uint32_t  RomC1000Addr = 0xFFFFE000;
	  uint32_t  RomD2000Addr = 0x0;	
		xSAPTable xst_sav[MAX_SAP_SECTION + 1 + 1];
		int xst_index = 0,xst_flag = 0;;
		memset(xst_sav,0,sizeof(xst_sav));
	
	if(xap->SapExtraFun)
			free(xap->SapExtraFun);
	if(xap->SapSequence)
			free(xap->SapSequence);
	memset((void *)xap, 0, sizeof(xSapFile)); 
	
	ret = GetSAPFileName((char *)xap->sapfname);
	if (ret) return ret;
	
	ret = GetSAPFile(xap->sapfname,xBuff->buffo,0x400,0);
	if(ret<=0) 
	{	
		ReportMsgByAck(dap,FR_NOT_SAP_FILE);
		return ret;
	}
	CloseSAPFile(xap->sapfname);
	xap->sapid = 0;
	parseSAPHeader(xBuff->buffo);
	xst_index = 1;
	/* Rom image should operation after all others */
	while(xst[xap->sapid].id !=0)
	{	
		parseSAPB(xap->sapid);
		if(xap->startFlash == RomC1000Addr ||xap->startFlash == RomD2000Addr )
		{
			memcpy(&xst_sav[MAX_SAP_SECTION+1],&xst[xap->sapid],sizeof(xSAPTable));
			xst_flag = 1;
		}else
		{
			memcpy(&xst_sav[xst_index],&xst[xap->sapid],sizeof(xSAPTable));
			xst_index ++;
		}
		xap->sapid ++;
	}
	if(xst_flag)
	{
		memcpy(&xst_sav[0],&xst_sav[MAX_SAP_SECTION+1],sizeof(xSAPTable));
		memcpy(&xst[0],&xst_sav[0],sizeof(xSAPTable)*(xst_index));
	}else
		memcpy(&xst[0],&xst_sav[1],sizeof(xSAPTable)*(xst_index));
	xap->sapid = 0;
	parseSAPB(xap->sapid);
	
	
	if (xap->vendor == (STM32 << 24))
	{
		RstMode=1;	
		//Reinit_RST(1);
	}
	else if(xap->vendor == (KINETIS << 24))
	{
		RstMode=0;		
		//Reinit_RST(0);
	}else if(xap->vendor == (NVC << 24))
	{
		RstMode=1;
		//Reinit_RST(1);
	}else if(xap->vendor == (ATMEL<<24))
	{
		RstMode=0;
		//Reinit_RST(1);
	}else
	{
		RstMode=1;
		//Reinit_RST(0);
	}
	if(rst_flag)
	{
		RstMode=((~RstMode)&0x01);
	}
	Reinit_RST(RstMode);
#ifdef USE_VENDOR_LIMIT
	{

		unencrypt(3,0,4,(uint8_t *)&DEFAULT_VENDOR);
		if(xap->vendor!=(DEFAULT_VENDOR&0xFFFF)<<24)
			return DAP_ERROR;
	}
#endif	
	return DAP_OK;
}
int swd_close_sap(xDAPPacket *xPacket)
{
	CloseSAPFile(xap->sapfname);
	return 0;
}
#endif

#ifdef __VERBOSE_HELP__
/*----------------------------------------------------------------------------*/
#define HLPSAP		"sap\t\tSelect SAP file in SD card, please use <ls 0> command to list the SAP file.\n\t\t(eg. sap [1..n])."
#define HLPCSAP		"close\t\tClose opened file."
#define HLPBIN		"bin\t\tSelect BIN file in SD card, please use <ls 1> command to list the BIN file.\n\t\t(eg. bin [1..n])."
#define HLPSSAP		"ssap\t\tShow current SAP configs."
#define HLPSFUN		"sfun\t\tSet Function Todos.\n\t\t(eg. sfun 0x35)"
#define HLPCONN		"connect\t\tConnect to target."
#define HLPPWRON    "powerauto\tpower control."
#define HLPDISC     "disconn\t\tDisconnect target."
#define HLPTPIN     "testpin\t\tJTAG Pins loopback test, TCK-TMS, TDI-TDO, nRESET-nTRST."
#define HLPSPIN     "swjpin\t\tSet PIN (select state)."
#define HLPSLED     "dapled\t\tSet LED (pin onoff)."
#define HLPINFO		"dapinfo\t\tRead DAP Infos."
#define HLPINIT 	"init\t\tInitialize target debugger."
#define HLPSROM 	"scanrom\t\tScan ROM Table."
#define HLPRHALT 	"resethalt\tReset and Halt target."
#define HLPRSTG 	"resetgo\t\tReset target and go."
#define HLPRSTT 	"reset\t\tReset target without halt."
#define HLPHALT 	"halt\t\tHalt target."
#define HLPRSUM 	"resume\t\tResume target."
#define HLPAUTO 	"auto\t\tExecute auto run procedure, need to select SAP first."
#define HLPERASE	"erasechip\tErase target flash chip."
#define HLPBLANK	"blankcheck\tBlank Check, erase if not blank.(according SAP file)"
#define HLPESSEC	"erasesector\tErasesector(according SAP file)"
#define HLPPROG		"program\t\tProgram flash.(according SAP file)"
#define HLPVERIFY	"verify\t\tVerify CRC32.(according SAP file)"
#define HLPLOAD		"loadcode\tLoad flash algorithm inside SAP file."
#define HLPDEBUG	"debug\t\tSet debug output level.\n\t\t(eg. debug 0x10)."
#define HLPSPWR     "setpower\tSET Power Output."
#define HLPSSPD     "setspeed\tSET DAP Speed."
#define HLPSSET     "setreset\tSET Target RESET."
#define HLPSLOG     "setlog\t\tSET log to file level(0==>No log)."
#define HLPSSWJ     "setswj\t\tSET Target to SWD or JTAG(0x02) Mode."
#define HLPSSTS     "setstate\tSET DAP state."
#define HLPLIST		"ls\t\tList SAP files in SD card."
#define HLPCCRC     "crc\t\tCalculate CRC32 according binary in SAP."
#define HLPLISTU	"lsusb\t\tList files in USB MSC."
#define HLPVER		"version\t\tList SAP files in SD card."
#define HLPBOOT		"bootloader\tReset to bootloader."
#define HLPTXT		"txtest\t\tSPI Packet Transfer Test\n\t\t(eg. txtest 0x400)."
#define HLPRUNF		"runfun\t\tRun a function(eg. runfun R0 R1 R2 PC, in hex)."
#define HLPSDRT		"sdrtest\t\tTest SD read file."
#define HLPDUMP     "dump\t\tDump to SD file.\n\t\t(eg. dump 0x0 0x400 abc.bin"
#define HLPHELP		"help\t\tShow this help."
#define HLPADC		"adc\t\tShow ADC readings."
#define HLPPWR      "power\t\tpower control(eg. power 1 0xf)."
#define HLPCPWR     "chkpwr\t\tpower check."
#else
#define HLPSAP				"sap\t\tSelect SAP file in spiflash."
#define HLPCSAP				NULL
#define HLPBIN				NULL
#define HLPSSAP				NULL
#define HLPSFUN				NULL
#define HLPCONN				"connect\t\tConnect to target."
#define HLPPWRON  		"poweron\t\tpower on the target."
#define HLPDISC          NULL
#define HLPTPIN          NULL
#define HLPSPIN          NULL
#define HLPSLED          NULL
#define HLPINFO		       NULL
#define HLPINIT 	      "reset\t\treset the target."
#define HLPSROM 	       NULL
#define HLPRHALT 	       "halt\t\thalt the target."
#define HLPRSTG 	       NULL
#define HLPRSTT 	       NULL
#define HLPHALT 	       NULL
#define HLPRSUM 	       NULL
#define HLPAUTO 	       "auto\t\tExecute auto run procedure, need to select SAP first."
#define HLPERASE	       "erasechip\tErase target flash chip."
#define HLPBLANK	       "blankcheck\tBlank Check, erase if not blank.(according SAP file)"
#define HLPESSEC	       NULL
#define HLPPROG		       "program\t\tProgram flash.(according SAP file)"
#define HLPVERIFY	       "verify\t\tVerify CRC32.(according SAP file)"
#define HLPLOAD		       NULL
#define HLPDEBUG	       NULL
#define HLPSPWR          NULL
#define HLPSSPD          NULL
#define HLPSSET          NULL
#define HLPSLOG          NULL
#define HLPSSWJ          NULL
#define HLPSSTS          NULL
#define HLPLIST		       NULL
#define HLPCCRC          NULL
#define HLPLISTU	       NULL
#define HLPVER		NULL
#define HLPBOOT		NULL
#define HLPTXT		NULL
#define HLPRUNF		NULL
#define HLPSDRT		NULL
#define HLPDUMP   NULL
#define HLPHELP		NULL
#define HLPADC		NULL
#define HLPPWR    NULL
#define HLPCPWR   NULL
#endif
int swd_usage(xDAPPacket *xPacket);
static xDAPCmd xCmds[] /*__attribute__( ( section( "CCM")) )*/ = {
    /*command,		function,		argc,  preq, name & help message */
    { SM_NONSWD,	&swd_setreset,		1, 0x00, HLPSSET},
    { SM_NONSWD,	&swd_calcCRC32,		0, 0x00, HLPCCRC},
    { SM_NONSWD,	&swd_usage,			0, 0x00, HLPHELP},
    //{ SM_DUMP,		&swd_dump,		 0x47, 0x3F, HLPDUMP},
    { SM_NONSWD,	&swd_select_sap, 0x03, 0x00, HLPSAP},
    { SM_NONSWD,	&swd_close_sap,  0x03, 0x00, HLPCSAP},
    { SM_START,	    &swd_power_control,	0, 0x03, HLPPWRON},
    { SM_SWD,	    &swd_showled,    	7, 0x00, HLPSLED},
    { SM_AUTO,		&swd_auto,		    0, 0x03, HLPAUTO},
    { 0,		    NULL,       		0, 0,    NULL},	
    NULL
} ;
/* ------------------------------------------------------------------------------*/
int swd_usage(xDAPPacket *xPacket)
{
    int i, j;

    for(i=0;xCmds[i].op;i++) {
        if (xCmds[i].name_hlp != NULL) {
            mtk_printf(xCmds[i].name_hlp,-1);
            if (xCmds[i].argc > 0) {
                mtk_printf("\n\t\targuments:",-1);
                for (j=0;j<4;j++) {
                    if (xCmds[i].argc & (1<<j)) {
                        if (xCmds[i].argc & (1<<(j+4))) {
                            mtk_printf(" <string>",-1);
                        } else {
                            mtk_printf(" <hex>",-1);
                        }
                    }
                }
                cP(".");
            }
			mtk_printf("\n",-1);	
        }
    }
    mtk_printf("\n\n",-1);
    return DAP_OK;
}

/* ------------------------------------------------------------------------------*/
xDAPCmd *parsecmd(uint8_t *command)
{
    int i, j;
    uint8_t *oparg;
    xDAPPort *dap = &pPort;

    xap->doChipEraseBySector = 0;
    if (strequ(command, "erasechip") &&  xap->funEraseChip == 0x00 && xap->funEraseSector > 0) {
        strcpy((char *)command, "erasesector");
        xap->doChipEraseBySector = 1;
    }

    for(i=0;xCmds[i].op;i++) {
        if (strequ(command, xCmds[i].name_hlp)) {
            oparg = command;
            for (j=0;j<4;j++) {
                if (xCmds[i].argc & (1<<j)) {
                    oparg = nextarg(oparg);
                    if (xCmds[i].argc & (1<<(4+j))) {         // string
                        if (*oparg) {
                            dap->regs[j] = (uint32_t)oparg;
                        } else {
                            dap->regs[j] = 0;
                        }
                    } else {
                        dap->regs[j] = uint32(oparg);       // integer
                    }
                }
            }
            return &xCmds[i];
        }
    }
    return NULL;
}
/* ------------------------------------------------------------------------------*/
xDAPCmd *parsecmdPkt(xCMDPacket *xPkt)
{
    int i;

    if (xPkt->cmd == SM_ERASECHIP && (xap->dapstate & PREQ_SAPLOADED) == 0) {
        if ((xap->dapstate & PREQ_SAPSELECTED) == 0) {
            if (parseSAPH(xap->sapfname) < 0) {					/* parse sap file */
                eP("Parsing SAP file Failed!!!\n" );
                return NULL;
            } 
        } 
        if (parseSAPB(xap->sapid) < 0) {					/* parse sap file */
            eP("Parsing SAP file Failed!!!\n" );
            return NULL;
        }
    }
    xap->doChipEraseBySector = 0;
    if (xPkt->cmd == SM_ERASECHIP && xap->funEraseChip == 0x00 && xap->funEraseSector > 0) { /* DO_ERASECHIP without xap->funEraseChip() */
        xPkt->cmd = SM_ERASESECTOR;
        xap->doChipEraseBySector = 1;
    }

    for(i=0;xCmds[i].op;i++) {
        if (xPkt->cmd == xCmds[i].op) {
            return &xCmds[i];
        }
    }
    return NULL;
}
/* ------------------------------------------------------------------------------*/
int cmdLine(uint8_t *command)
{
	int MCP_DAPCMDLineTask (xDAPCmd *cmd ); 
     xDAPCmd *cmd;
     xDAPPacket xPacket = {0,0,0};
    xDAPPort *dap = &pPort;
	int ret = DAP_CMD_NOT_SUPPORT;

    xap->rrport = 0;
    dap->xport = xap->xport;   //((SPIx_RDY_GPIO_PORT->IDR) >> SPIx_RDY_SHIFT) & SPIx_RDY_MASK;
    xPacket.port = dap->xport;
   // xap->CICO |= DAP_INTF_I2C;
    if (command != NULL && NULL != (cmd = parsecmd(command))) {
        if (cmd->op < SM_NONSWD) {
            if ((cmd->preq & xap->dapstate) == cmd->preq) {
               ret = MCP_DAPCMDLineTask(cmd);
            } else {
                uint32_t preq;

                preq = (xap->dapstate ^ cmd->preq) & cmd->preq;

                cP("\nPlease check dap state(%02X / %02X) before run this command!!!", xap->dapstate, cmd->preq);
                cP("\n\tNeed issue following commands first:");
                if (preq & (PREQ_SAPSELECTED|PREQ_SAPLOADED)) cP("\n\t\tsap <n> <id>\t\t==> select SAP file.");
                if (preq & PREQ_CONNECTED  ) cP("\n\t\tconnect\t\t==> connect target.");
                if (preq & PREQ_INITED	   ) cP("\n\t\tinit\t\t==> Init Target DAP.");
                if (preq & PREQ_HALTED     ) cP("\n\t\tresethalt\t\t==> Reset and Halt Target.");
                if (preq & PREQ_CODELOADED ) cP("\n\t\tloadcode\t\t==> Load Algorithm.");
            }
        } else
           ret = cmd->func(&xPacket);
		
		if(dbg_deep>0) mtk_printf("cmdline return",ret);
        return ret;
    } 
    return ret;
}


/* ------------------------------------------------------------------------------*/
int MCP_DAPCMDLineTask (xDAPCmd *cmd ) 
{
    xDAPPacket xPacket = {0,0,0};
    xDAPPort *dap = &pPort;
  //   portCHAR cDMA = 0xFF;
   // xTaskHandle hDAPTask;
  //  xDAPCmd *cmd = (xDAPCmd *)pvParameters;
volatile    U32 temp;
    int rc = DAP_ERROR;

    if (cmd->func == NULL)
        return rc;

    dap->startTick = getTick();
    cP("\nStart Tick(%d)", dap->startTick);
  
    INITSEQ(0);
	if(!gProgOnLine)
	{
    if (cmd->op == SM_AUTO)
        dap->manstate = SM_START;
    else
        dap->manstate = cmd->op;
	}else
	{
		
	}
		dap->substate = 0;
    dap->funstate = 0;
    dap->timeout = 2500;
    dap->xport = (1<<PORT_COUNT) - 1;
    xPacket.port = 0; xPacket.offset = 0; xPacket.length = 0;

	for (;;) {

            if (xPacket.port) {
                xap->portstate[P2I(xPacket.port)] = (dap->manstate<<16)|(dap->substate<<8)|(dap->funstate);
                xap->portaddr[P2I(xPacket.port)] = dap->adr;
                dap->inseq[P2I(xPacket.port)]++;
            }
            //printPacket(0, &xPacket);
			xPacket.port=1;
            rc = cmd->func(&xPacket);
						if(rc==DAP_NEXTSAP)
						{
								int ret=0;
								//ret = GetSAPFile(xap->sapfname,xBuff->buffo,0x400,0);
								//if(ret<0) return ret;
                xPacket.port = 0; xPacket.offset = 0; xPacket.length = 0;							
								xap->sapid += 1;
								output_0v_io(); // cut off power to target
                	Delayms(1000);  // wait for hardware stable
							dap->manstate = SM_START;
                //xap->dapstate &= ~(PREQ_SAPLOADED | PREQ_CODELOADED);
							xap->dapstate = 0;
								ret=parseSAPB(xap->sapid);
								if(ret!=DAP_OK)
									return ret;
						}else
						{
							if(!gUseIap)
							Quark_DisConnect(global_cmd_ctx,&cmsis_dap_interface,&Quark_target);
					 }
           if (rc == DAP_ERROR) {
						 {
              
							 if(gAtmelHasErase)
								 gAtmelHasErase=0;
							if(gAtmelHasPlug)
								gAtmelHasPlug=0;		
							if(gAtmelNeedLock)
								gAtmelNeedLock=0;
							 break;
						 }
            }

						if(rc==DAP_AGAIN)
						{
							if(gAtmelHasErase)
								 gAtmelHasErase=0;
							if(gAtmelHasPlug)
								gAtmelHasPlug=0;		
							if(gAtmelNeedLock)
								gAtmelNeedLock=0;
							 break;
						}
						if(rc==DAP_CONTINUE)
						{
							return rc;
						
						}
			if (dap->manstate == SM_DONE)
			{
				rc = DAP_OK;
				gAtmelHasErase=0;
				gAtmelHasPlug=0;		
				gAtmelNeedLock=0;
				break;
			}	
				
	}
    temp = getTick();
	if(rc!=DAP_AGAIN)
    cP("\nFinished Elapse(%d)ms, now(%d)ms.", temp - dap->startTick, temp);
		
    dap->manstate = SM_DONE;
    dap->substate = 0;
    dap->funstate = 0;
    dap->adr = 0;
	return rc;
}
int Quark_erasesector(xDAPPacket *xPacket,struct target *t)
{
	xDAPPort *dap = &pPort;
	xFun *sub = (xFun *)dap->pvFun;
	int i;
	int rc;
	sub->i = 0;		// sector index
	sub->j = 0;

	while(xap->sectors[sub->i].szSector != 0xFFFFFFFF && xap->sectors[sub->i].startSector != 0xFFFFFFFF)
	{		
		if (( xap->sectors[sub->i].startSector + sub->j * xap->sectors[sub->i].szSector) >= xap->sectors[sub->i + 1].startSector) {
				sub->j = 0;
				sub->i += 1;
		}
		dap->adr = xap->startFlash + xap->sectors[sub->i].startSector + sub->j * xap->sectors[sub->i].szSector;	
		rc=Quark_PageErase(t,xap->sectors[sub->i].startSector);
		if(rc!=DAP_OK)
			return rc;
	}
		return DAP_OK;
}
int Quark_erasechip(xDAPPacket *xPacket,struct target *t)
{
	xDAPPort *dap = &pPort;
	xFun *sub = (xFun *)dap->pvFun;
	int i;
	int rc;
	sub->i = 0;		// sector index
	sub->j = 0;
	if(!gUseIap)
		rc=Quark_ChipErase(t,xap->startFlash+xap->ProgOffset);
	else
		rc= SerialEraseTargetFlash(USART_SERIAL,xap->startFlash+xap->ProgOffset,0xFF);//all erase
	if(rc!=DAP_OK)
		return DAP_ERROR;
		vTaskDelay(200);	
	uP("\r\nErase Done.\r\n\r\n"); 
	vTaskDelay(200);	
	return DAP_OK;
}



int Quark_loadcode(xDAPPacket *xPacket,struct target *t,int type)
{		// load flash algorithm
	  xDAPPort *dap = &pPort;
    int	rc;
	  xFun *sub = (xFun *)dap->pvFun;
	uint8_t buf[32];
	uint32_t fixStaticTrim[2] = {0x0208020A,0x022E022D};

		uint32_t off =0, rest = 0,BytesCnt = 0,RomAddr =0,RamAddr = 0;
		dap->regs[0] = xap->offCode;
    dap->regs[1] = xap->startPC & 0xFFFFFFFE;
    dap->regs[2] = xap->szCode;
			
	/* load rom bin to rom
	size: 0x800
	offset:dap->regs[0]
	*/

	if(Get_Quark_Chip() == QUARK_D20XX)
	{
		RomAddr = 0x150;
		RamAddr = 0x280800;
		/* load rom bin to rom
		size: 0x800
		offset:dap->regs[0]
		*/
		if(type&0x1)
		{
			rc=Quark_PageErase(t,0);
			if(rc!=DAP_OK)
				return -1;
			rest = 0x800;
			while(rest >0)
			{
				if(rest >= 0x400)
				{
					BytesCnt = 0x400;
				}
				else
				{
					BytesCnt = rest;
				}
				rest -= BytesCnt;
				sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+xap->funInit-1+off, BytesCnt);//offset 0x39
				off += BytesCnt;

				dap->exp = Quark_WriteMem(t,RomAddr ,4,BytesCnt>>2, (U8 *)(sub->buf));
				RomAddr +=  BytesCnt;
		}

	 }
		/* load uart bin to ram
		offset:dap->regs[0]
		*/
		if(type&0x10)
		{	
			rest = dap->regs[2] - 0x800-0x38;
			off = 0x800;
			while(rest >0)
			{
				if(rest >= 0x400)
				{
					BytesCnt = 0x400;
				}
				else
				{
					BytesCnt = rest;
				}
				rest -= BytesCnt;
				sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+xap->funInit-1+off, BytesCnt);
				off += BytesCnt;

				dap->exp = Quark_WriteMem(t,RamAddr ,4,BytesCnt>>2, (U8 *)(sub->buf));

				RamAddr +=  BytesCnt;
				if(BytesCnt%4)
					dap->exp = Quark_WriteMem(t,RamAddr ,1,BytesCnt%4, (U8 *)(sub->buf+BytesCnt-BytesCnt%4));
			}
			Quark_ReadMem(t,0x281800 ,4,8, buf);
		}
	}else if(Get_Quark_Chip() == QUARK_SE_C10XX)
	{
		RomAddr = 0xFFFFE200;
		uint32_t RomResetAddr = 0xFFFFFFF0;
		RamAddr = 0x4002F400;
		
		/* load rom bin to rom
		size: 0x800
		offset:dap->regs[0]
		*/
		if(type&0x1)
		{
			rc=Quark_PageErase(t, 0xFFFFE000);
			rc=Quark_PageErase(t, 0xFFFFE800);			
			rc=Quark_PageErase(t, 0xFFFFF800);		
			if(rc!=DAP_OK)
				return -1;
			rest = 0x800;
			while(rest >0)
			{
				if(rest >= 0x400)
				{
					BytesCnt = 0x400;
				}
				else
				{
					BytesCnt = rest;
				}
				rest -= BytesCnt;
				sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+0x39-1+off, BytesCnt);//offset 0x39
				off += BytesCnt;

				dap->exp = Quark_WriteMem(t,RomAddr ,4,BytesCnt>>2, (U8 *)(sub->buf));
				RomAddr +=  BytesCnt;
			}
			BytesCnt = 16;
			sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+0x39-1+off, BytesCnt);//offset 0x39
			off += BytesCnt;
			dap->exp = Quark_WriteMem(t,RomResetAddr ,4,BytesCnt>>2, (U8 *)(sub->buf));
			
			
		}
		/* load uart bin to ram
		offset:dap->regs[0]
		*/
	if(type&0x10)
	{	
		uint32_t UARTSwitchCode = 0xFFFFFFFF;
		rc=Quark_PageErase(t, 0x4002F000);
		rc=Quark_PageErase(t, 0x4002F800);
		if(rc!=DAP_OK)
			return -1;
		
		if(gUseIap == 2)
		{
			UARTSwitchCode = 0x68697413;
			Quark_WriteMem(t,0x4002F200 ,4,1, (U8 *)(&UARTSwitchCode));
			Quark_ReadMem(t,0x4002F200 ,4,1, buf);
		}

		rest = dap->regs[2] - 0x800-0x38-0x10;
		off = 0x800+0x10;
		while(rest >0)
		{
			if(rest >= 0x400)
			{
				BytesCnt = 0x400;
			}
			else
			{
				BytesCnt = rest;
			}
			rest -= BytesCnt;
			sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+0x39-1+off, BytesCnt);
			off += BytesCnt;

			dap->exp = Quark_WriteMem(t,RamAddr ,4,BytesCnt>>2, (U8 *)(sub->buf));
			RamAddr +=  (BytesCnt>>2)<<2;
			if(BytesCnt%4)
				dap->exp = Quark_WriteMem(t,RamAddr ,1,BytesCnt%4, (U8 *)(sub->buf+BytesCnt-BytesCnt%4));
		}
	Quark_WriteMem(t,0x4002F000 ,4,2, (U8 *)(fixStaticTrim));
	}
	if(type&0x100)
	{	
		RamAddr = CODE_ADDR;
		rest = 0x800 + 0x10;
		off = 0;
		while(rest >0)
		{
			if(rest >= 0x400)
			{
				BytesCnt = 0x400;
			}
			else
			{
				BytesCnt = rest;
			}
			rest -= BytesCnt;
			sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+0x39-1+off, BytesCnt);
			off += BytesCnt;

			dap->exp = Quark_WriteMem(t,RamAddr ,4,BytesCnt>>2, (U8 *)(sub->buf));
			//					Quark_ReadMem(&Quark_Arc_target,RamAddr ,4,BytesCnt>>2, Veribuf);
			RamAddr +=  (BytesCnt>>2)<<2;
			if(BytesCnt%4)
				dap->exp = Quark_WriteMem(t,RamAddr ,1,BytesCnt%4, (U8 *)(sub->buf+BytesCnt-BytesCnt%4));
		}
	}
    return DAP_OK;

}
	    return DAP_OK;
	}


int quark_load_bin(xDAPPacket *xPacket,struct target *t)
{		// load flash algorithm
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    U32 temp;
    int i;
    U8	*resp = FUNRESPONSE(xPacket);
	
		if(dbg_deep > 0)
     cP("\nswd_load_ram(file offset=%08x, target address=%08x, size=%08x)", dap->regs[0], dap->regs[1], dap->regs[2]);
		SYNCSEQ();

		sub->adr = 0;
		dap->xport = 1;
		CodeRest=dap->regs[2];
		rest=dap->regs[2];
		start_add=dap->regs[1];			
		start_add=dap->regs[1]+off;	
		
		sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+off, rest);	
		sub->adr = 0;
		CodeRead=rest;
		CodeRest=rest;
		rest=0;
		
		if (sub->buf == NULL) {
				DAP_SetLed(dap->xport, REDLED, LEDON);
				dap->xport = 0;
				xUIMsg(0, OPT_NF|OPT_ERR|OPT_CODE, 0, FR_READ_FAIL);
				rest=0;
				off=0;
				read_time=0;
				read_cnt=0;
				return DAP_ERROR;
		}
			
    return DAP_OK;

}
int  Quark_BlankCheck(xDAPPacket *xPacket,struct target *t)
{
		xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    int i,rc,cnt;
		dap->funstate = 0;
		if (currcmd == SM_AUTO) { 
				dap->port = swd_gettime(SM_PROGPAGE);
		} else
				dap->port = 0xFF;
		IiP(SM_PROGPAGE, OPT_NF|OPT_ESTI, xap->szData|xap->startFlash, MSGSTR[ALG_PP_START]);
		//temp = xTaskGetTickCount();
		iP("\n%s", MSGSTR[ALG_PP_START]);
		
		dap->regs[0] = xap->startFlash;
		dap->regs[1] = xap->speedClock;
		dap->regs[2] = 2;	/* function, 2: Program  */
		dap->regs[9] = xap->szPrgCode+ (xap->startPC & 0xFFFFFFFE);
		dap->regs[13] = xap->startSP;
		dap->regs[14] = xap->startPC;
		dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
		dap->regs[16] = 0x01000000;
		dap->regacc = 0x1E207;	
		dap->timeout = 3000;

		DP("\nswd_progpage::Init()::OK(%1x)Failed(%1x)", dap->rport, dap->vport);
		dap->regs[4] =  0;       //xap->startFlash;	

		while ((dap->regs[4] <xap->szData) && (dap->regs[4] < xap->szFlash)) {
				//IiP(SM_PROGPAGE, OPT_NF|OPT_ADDR, dap->regs[4], NULL);
				dap->regs[0] = xap->offData + dap->regs[4];	// file offset
				dap->regs[1] = xap->startBF;        // target memory address
			if((dap->regs[4] +xap->szProg)<xap->szData)
				dap->regs[2] = xap->szProg;					// size
			else
				dap->regs[2] = xap->szData-dap->regs[4];					// size
				dap->regs[5] = dap->regs[1];				// data buffer starting address
				dap->funstate = 0;
				dap->substate = 2;
				i = quark_load_bin(xPacket,t);
			 if (xap->sap_version & SAP_ENCRYPTED) {    // data encrypted
						int j;
						uint32_t *ramsource = (uint32_t *)sub->buf;

						for(j=0;j<dap->regs[2]>>2;) {
							decTEA(&ramsource[j], enckeys);
								j += 2;
						}
						xap->szCache = 0;
				}
		dap->regs[8]=0;
		dap->adr = dap->regs[4] + xap->startFlash+xap->ProgOffset;	// flash address to write
		if((dap->regs[4]%(1*(xap->szProg)))==0)
			tP(".");//print a '.' ervey 50 packet
		if((dap->regs[4]%(5*(xap->szProg)))==0)
			SendHeartAck();
		dap->writemask = 0xFFFFFFFF;
		dap->ap_tar_value = 0xFFFFFFFF;
		dap->dp_sel_value = 0xFFFFFFFF;

		dap->regs[0] = dap->adr;
		if ((dap->regs[4] + xap->szProg) < xap->szData)
				dap->regs[1] = xap->szProg;						// program size
		else
				dap->regs[1] = xap->szData - dap->regs[4];						// program size
		if(dbg_deep>1)
			uP("\nVerify PAGE::Addr[%08x], Size[%04x]", dap->adr, dap->regs[1]);
		dap->regs[2] = dap->regs[5];					// address of data to write to flash
		dap->regs[3] = xap->toProg;						// timeout
		dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
		dap->regs[13] = xap->startSP;
		dap->regs[14] = xap->startPC;
		dap->regs[15] = xap->funProgPage + (xap->startPC & 0xFFFFFFFE);
		dap->regs[16] = 0x01000000;
		dap->regacc = 0x1E20f;
		dap->reto = xap->toProg * (xap->szFlash/0x1000);			
		if (xap->tDebug > 0x10)
				dap->reto <<= 2;
		dap->funstate = 0;
			if(dap->regs[1]>0x400)
				cnt = 0x400;
			else
				cnt=dap->regs[1]-dap->regs[1]%4;
{
		 if (dap->regs[1] > 4) {
					dap->exp = Quark_ReadMem(t,dap->regs[0] ,4,cnt>>2, Veribuf);//only max 0x400,or it will be a long long time
			//if(memcmp(Veribuf,(U8 *)(sub->buf +sub->adr+dap->regs[8]),cnt>>2)!=0)
			{
				int i=0;
				uint8_t val=0;
				for(i=0;i<cnt;i++)
				{
					if(Veribuf[i]!=0xFF)
					{
						uP("Not Blank!!!\r\n The address 0x%08x should be 0xFF,it is 0x%02x.Exit!!!\r\n",dap->regs[0]+i,Veribuf[i]);
						return DAP_OK;
					}
				}
			}
					dap->regs[8] += dap->regs[1]-dap->regs[1]%4;
					dap->regs[0]+= dap->regs[1]-dap->regs[1]%4;
			}  
		 if (dap->regs[1] % 4) {                   
					dap->exp = Quark_ReadMem(t,dap->regs[0] ,1,dap->regs[1]%4, Veribuf);
					//if(memcmp(Veribuf,(U8 *)(sub->buf +sub->adr+dap->regs[8]),dap->regs[1]%4)!=0)
					{
						int i=0;
						uint8_t val=0;
						for(i=0;i<dap->regs[1]%4;i++)
						{
							if(Veribuf[i]!=0xFF)
							{
								uP("Not Blank!!!\r\n The address 0x%08x should be 0xFF,it is 0x%02x.Exit!!!\r\n",dap->regs[0]+i,Veribuf[i]);
								return DAP_OK;
							}
						}
						
					}

					dap->regs[8] += dap->regs[1]%4;

					dap->regs[8] = dap->regs[7];
				} 
			}
				check_alg_result(ALG_PP_FAIL, NULL);
				dap->regs[4] += xap->szProg;
	} 
	tP("\r\n");
	vTaskDelay(200);	
	uP("Blank OK.\r\n\r\n"); 
	vTaskDelay(200);
 return DAP_OK;	
}
int  Quark_Verify(xDAPPacket *xPacket,struct target *t)
{
		xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    int i,rc,cnt;
		//uint8_t Veribuf[0x400];
		dap->funstate = 0;
		if (currcmd == SM_AUTO) { 
				dap->port = swd_gettime(SM_PROGPAGE);
		} else
				dap->port = 0xFF;
		IiP(SM_PROGPAGE, OPT_NF|OPT_ESTI, xap->szData|xap->startFlash, MSGSTR[ALG_PP_START]);
		//temp = xTaskGetTickCount();
		iP("\n%s", MSGSTR[ALG_PP_START]);
		
		dap->regs[0] = xap->startFlash;
		dap->regs[1] = xap->speedClock;
		dap->regs[2] = 2;	/* function, 2: Program  */
		dap->regs[9] = xap->szPrgCode+ (xap->startPC & 0xFFFFFFFE);
		dap->regs[13] = xap->startSP;
		dap->regs[14] = xap->startPC;
		dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
		dap->regs[16] = 0x01000000;
		dap->regacc = 0x1E207;	
		dap->timeout = 3000;

		DP("\nswd_progpage::Init()::OK(%1x)Failed(%1x)", dap->rport, dap->vport);
		dap->regs[4] =  0;       //xap->startFlash;	

		while ((dap->regs[4] <xap->szData) && (dap->regs[4] < xap->szFlash)) {
				//IiP(SM_PROGPAGE, OPT_NF|OPT_ADDR, dap->regs[4], NULL);
				dap->regs[0] = xap->offData + dap->regs[4];	// file offset
				dap->regs[1] = xap->startBF;        // target memory address
			if((dap->regs[4] +xap->szProg)<xap->szData)
				dap->regs[2] = xap->szProg;					// size
			else
				dap->regs[2] = xap->szData-dap->regs[4];					// size
				dap->regs[5] = dap->regs[1];				// data buffer starting address
				dap->funstate = 0;
				dap->substate = 2;
				i = quark_load_bin(xPacket,t);
			 if (xap->sap_version & SAP_ENCRYPTED) {    // data encrypted
						int j;
						uint32_t *ramsource = (uint32_t *)sub->buf;

						for(j=0;j<dap->regs[2]>>2;) {
							decTEA(&ramsource[j], enckeys);
								j += 2;
						}
						xap->szCache = 0;
				}
		dap->regs[8]=0;
		dap->adr = dap->regs[4] + xap->startFlash+xap->ProgOffset;	// flash address to write
		if((dap->regs[4]%(10*(xap->szProg)))==0)
			tP(".");//print a '.' ervey 50 packet
		if((dap->regs[4]%(50*(xap->szProg)))==0)
			SendHeartAck();
		dap->writemask = 0xFFFFFFFF;
		dap->ap_tar_value = 0xFFFFFFFF;
		dap->dp_sel_value = 0xFFFFFFFF;

		dap->regs[0] = dap->adr;
		if ((dap->regs[4] + xap->szProg) < xap->szData)
				dap->regs[1] = xap->szProg;						// program size
		else
				dap->regs[1] = xap->szData - dap->regs[4];						// program size
		if(dbg_deep>1)
			uP("\nVerify PAGE::Addr[%08x], Size[%04x]", dap->adr, dap->regs[1]);
		dap->regs[2] = dap->regs[5];					// address of data to write to flash
		dap->regs[3] = xap->toProg;						// timeout
		dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
		dap->regs[13] = xap->startSP;
		dap->regs[14] = xap->startPC;
		dap->regs[15] = xap->funProgPage + (xap->startPC & 0xFFFFFFFE);
		dap->regs[16] = 0x01000000;
		dap->regacc = 0x1E20f;
		dap->reto = xap->toProg * (xap->szFlash/0x1000);			
		if (xap->tDebug > 0x10)
				dap->reto <<= 2;
		dap->funstate = 0;
			if(dap->regs[1]>0x400)
				cnt = 0x400;
			else
				cnt=dap->regs[1]-dap->regs[1]%4;
	{
		 if (dap->regs[1] > 4) {
					dap->exp = Quark_ReadMem(t,dap->adr ,4,cnt>>2, Veribuf);//only max 0x400,or it will be a long long time
			if(memcmp(Veribuf,(U8 *)(sub->buf +sub->adr+dap->regs[8]),cnt)!=0)
			{
				int i=0;
				uint8_t val=0;
				for(i=0;i<cnt;i++)
				{
					val=*((U8 *)((sub->buf +sub->adr+dap->regs[8])+i));
					if(Veribuf[i]!=val)
						uP("Verify Failed!!!\r\n The address 0x%08x should be 0x%02x,it is 0x%02x.Exit!!!\r\n",dap->adr+i,val,Veribuf[i]);
				}
				return DAP_ERROR;
			}
					dap->regs[8] += dap->regs[1]-dap->regs[1]%4;
					dap->regs[0]+= dap->regs[1]-dap->regs[1]%4;
			}
		 
		 if (dap->regs[1] % 4) {                   
					dap->exp = Quark_ReadMem(t,dap->regs[0] ,1,dap->regs[1]%4, Veribuf);
					if(memcmp(Veribuf,(U8 *)(sub->buf +sub->adr+dap->regs[8]),dap->regs[1]%4)!=0)
					{
						int i=0;
						uint8_t val=0;
						for(i=0;i<dap->regs[1]%4;i++)
						{
							val=*((U8 *)((sub->buf +sub->adr+dap->regs[8])+i));
							if(Veribuf[i]!=val)
							uP("Verify Failed!!!\r\n The address 0x%08x should be 0x%02x,it is 0x%02x.Exit!!!\r\n",dap->regs[0]+i,val,Veribuf[i]);
						}
						return DAP_ERROR;
					}

					dap->regs[8] += dap->regs[1]%4;

					dap->regs[8] = dap->regs[7];
				} 
		}
				check_alg_result(ALG_PP_FAIL, NULL);
				dap->regs[4] += xap->szProg;
	} 
	tP("\r\n");
	vTaskDelay(200);	
	uP("Verify OK.\r\n\r\n"); 
	vTaskDelay(200);
 return DAP_OK;	
}

int Quark_Prog(xDAPPacket *xPacket,struct target *t)
{
  xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    int i,rc;
		int index = 0;
	uint32_t tmp;
	uint32_t sram_data_addr = DATA_ADDR;
	uint32_t flash_data_addr = xap->startFlash;	
		dap->funstate = 0;
		if (currcmd == SM_AUTO) { 
				dap->port = swd_gettime(SM_PROGPAGE);
		} else
				dap->port = 0xFF;
		IiP(SM_PROGPAGE, OPT_NF|OPT_ESTI, xap->szData|xap->startFlash, MSGSTR[ALG_PP_START]);
		//temp = xTaskGetTickCount();
		iP("\n%s", MSGSTR[ALG_PP_START]);
		
		dap->regs[0] = xap->startFlash;
		dap->regs[1] = xap->speedClock;
		dap->regs[2] = 2;	/* function, 2: Program  */
		dap->regs[9] = xap->szPrgCode+ (xap->startPC & 0xFFFFFFFE);
		dap->regs[13] = xap->startSP;
		dap->regs[14] = xap->startPC;
		dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
		dap->regs[16] = 0x01000000;
		dap->regacc = 0x1E207;	
		dap->timeout = 3000;
	//	if(xap->devver == QUARK_SE_C10XX&&!gUseIap)	
	//		arc_mem_write_block32_fast_start(t);
		DP("\nswd_progpage::Init()::OK(%1x)Failed(%1x)", dap->rport, dap->vport);
		dap->regs[4] =  0;       //xap->startFlash;	
		OP_Config_t.size = 0;
		if(gUseArcAlg)
		{
			arc32_set_current_pc(&Quark_Arc_target,CODE_ADDR);
			arc32_get_current_pc(&Quark_Arc_target,&tmp);
		}	
		while ((dap->regs[4] <xap->szData) && (dap->regs[4] < xap->szFlash)) {
				//IiP(SM_PROGPAGE, OPT_NF|OPT_ADDR, dap->regs[4], NULL);
				dap->regs[0] = xap->offData + dap->regs[4];	// file offset
				dap->regs[1] = xap->startBF;        // target memory address
			if((dap->regs[4] +xap->szProg)<xap->szData)
				dap->regs[2] = xap->szProg;					// size
			else
				dap->regs[2] = xap->szData-dap->regs[4];					// size
				dap->regs[5] = dap->regs[1];				// data buffer starting address
				dap->funstate = 0;
				dap->substate = 2;
				i = quark_load_bin(xPacket,t);
			 if (xap->sap_version & SAP_ENCRYPTED) {    // data encrypted
						int j;
						uint32_t *ramsource = (uint32_t *)sub->buf;

						for(j=0;j<dap->regs[2]>>2;) {
							decTEA(&ramsource[j], enckeys);
								j += 2;
						}
						xap->szCache = 0;
				}
			dap->regs[8]=0;
		dap->adr = dap->regs[4] + xap->startFlash+xap->ProgOffset;	// flash address to write
		//if((dap->regs[4]%(2*(xap->szProg)))==0)
		//	tP(".");//print a '.' ervey 50 packet
		if((dap->regs[4]%(5*(xap->szProg)))==0)
			SendHeartAck();
		dap->writemask = 0xFFFFFFFF;
		dap->ap_tar_value = 0xFFFFFFFF;
		dap->dp_sel_value = 0xFFFFFFFF;

		dap->regs[0] = dap->adr;
		if ((dap->regs[4] + xap->szProg) < xap->szData)
				dap->regs[1] = xap->szProg;						// program size
		else
				dap->regs[1] = xap->szData - dap->regs[4];						// program size
		if(dbg_deep>1)
			uP("\nPROGRAM PAGE::Addr[%08x], Size[%04x]", dap->adr, dap->regs[1]);
		dap->regs[2] = dap->regs[5];					// address of data to write to flash
		dap->regs[3] = xap->toProg;						// timeout
		dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
		dap->regs[13] = xap->startSP;
		dap->regs[14] = xap->startPC;
		dap->regs[15] = xap->funProgPage + (xap->startPC & 0xFFFFFFFE);
		dap->regs[16] = 0x01000000;
		dap->regacc = 0x1E20f;
		dap->reto = xap->toProg * (xap->szFlash/0x1000);			
		if (xap->tDebug > 0x10)
				dap->reto <<= 2;
		dap->funstate = 0;

	if(gUseArcAlg)
	{
			uint8_t bytebuf[4] = {0xFF,0xFF,0xFF,0xFF};	
			uint8_t restCnt = 0;

		
//		#define SIG 0x69317812
//#define CODE_ADDR 0xA8007000
//#define DATA_ADDR 0xA8002000
//#define CONFIG_ADDR 0xA8001000
		
		
		dap->exp = Quark_WriteMem(t,sram_data_addr ,4,dap->regs[1]>>2, (U8 *)(sub->buf +sub->adr+dap->regs[8]));
		//dap->exp = Quark_ReadMem(t,sram_data_addr ,4,dap->regs[1]>>2, Veribuf);
			sram_data_addr += (dap->regs[1]>>2)<<2;
			restCnt = dap->regs[1] % 4;
		if(restCnt)
		{
			for(i = 0;i < restCnt;i++)
				bytebuf[i] =  *(sub->buf+(((dap->regs[1])>>2)<<2)+i);	
			dap->exp = Quark_WriteMem(t,sram_data_addr ,4,1, bytebuf);
			sram_data_addr += restCnt;
		}

	  OP_Config_t.size += (dap->regs[1]>>2)<<2+restCnt;
		
		if((OP_Config_t.size % (10*1024)) == 0)
		{
			uP(".");
		}
		
		if(OP_Config_t.size >= (39*1024))
		{		
			sram_data_addr = DATA_ADDR;
			OP_Config_t.address = flash_data_addr;
			flash_data_addr +=OP_Config_t.size;
			OP_Config_t.sig = SIG;
			//OP_Config_t.size = (dap->regs[1]>>2)<<2;
			OP_Config_t.type = OP_PROG;
			dap->exp = Quark_WriteMem(t,CONFIG_ADDR,4,sizeof(OP_Config_t)>>2,(U8 *)&(OP_Config_t));
			//dap->exp = Quark_ReadMem(t,CONFIG_ADDR,4,sizeof(OP_Config_t)>>2,Veribuf);
			OP_Config_t.size = 0;
			quark_arc_runtarget(xPacket,t);
			uP(".");
		}
	}else
	{
		 if (dap->regs[1] > 4) {
			 if(!gUseIap)
			 {
					dap->exp = Quark_WriteMem(t,dap->regs[0] ,4,dap->regs[1]>>2, (U8 *)(sub->buf +sub->adr+dap->regs[8]));
			 }else
			 {
				if((dap->regs[0] + dap->regs[1]) > 0x4002F000&&(dap->regs[0] + dap->regs[1]) < 0x40030000)
				{
					if(dap->regs[0] >= 0x4002F000)
					{
						WriteBufToSpiFlash((U8 *)(sub->buf +sub->adr+dap->regs[8]),dap->regs[0] - 0x4002F000,dap->regs[1],QUARKROMFILE);
					}else if(dap->regs[0] < 0x4002F000)
					{
						dap->exp = SerialWriteTargetFlash(USART_SERIAL,dap->regs[0],(U8 *)(sub->buf +sub->adr+dap->regs[8]),(0x4002F000-dap->regs[0]));
						
						WriteBufToSpiFlash((U8 *)(sub->buf +sub->adr+dap->regs[8] +(0x4002F000-dap->regs[0]) ),0,dap->regs[1]-(0x4002F000-dap->regs[0]),QUARKROMFILE);
					}
					gFixC1000 = 1;
				}else					
				 dap->exp = SerialWriteTargetFlash(USART_SERIAL,dap->regs[0],(U8 *)(sub->buf +sub->adr+dap->regs[8]),dap->regs[1]);
				 index ++;
			 }
			 if(index%50)
				 uP(".");
				if(dap->exp!=DAP_OK)
				{	
					uP("Programme failed!!");
					return dap->exp;
				}
				dap->regs[8] += dap->regs[1]-dap->regs[1]%4;
				dap->regs[0]+= dap->regs[1]-dap->regs[1]%4;
			}  
		 if(!gUseIap)
		 {
				if (dap->regs[1] % 4) {                   
					dap->exp = Quark_WriteMem(t,dap->regs[0] ,1,dap->regs[1]%4, (U8 *)(sub->buf +sub->adr+dap->regs[8]));
					if(dap->exp!=DAP_OK)
					{	
						uP("Programme failed!!");
						return dap->exp;
					}
					dap->regs[8] += dap->regs[1]%4;

					dap->regs[8] = dap->regs[7];
				} 
			}
		}
				check_alg_result(ALG_PP_FAIL, NULL);
				dap->regs[4] += xap->szProg;
	}
	if(sram_data_addr !=DATA_ADDR)
	{		
		sram_data_addr = DATA_ADDR;
		OP_Config_t.address = flash_data_addr;
		flash_data_addr = xap->startFlash;
		OP_Config_t.sig = SIG;
		//OP_Config_t.size = (dap->regs[1]>>2)<<2;
		OP_Config_t.type = OP_PROG;
		dap->exp = Quark_WriteMem(t,CONFIG_ADDR,4,sizeof(OP_Config_t)>>2,(U8 *)&(OP_Config_t));
		//dap->exp = Quark_ReadMem(t,CONFIG_ADDR,4,sizeof(OP_Config_t)>>2,Veribuf);
		//dap->exp = Quark_ReadMem(t,dap->regs[0] ,4,dap->regs[1]>>2, Veribuf);
		OP_Config_t.size = 0;		
		quark_arc_runtarget(xPacket,t);
	}
	//if(xap->devver == QUARK_SE_C10XX&&!gUseIap)	
	//	arc_mem_write_block32_fast_end(t);
	tP("\r\n");
	vTaskDelay(200);	
	uP("Programming Done.\r\n\r\n"); 
	vTaskDelay(200);	
 return DAP_OK;

}
extern struct target *common_target; 
int Jtag_read_targetmem(xDAPPacket *xPacket,uint32_t addr,uint16_t cnt,uint8_t *buf)
{
	int rc=0;	
	if (!cnt || !buf) {
		LOG_ERROR("%s invalid params count=0x%x, buf=%p, addr=0x%08x",
				__func__, cnt, buf, addr);
		}
	if(cnt>4)
	{
		rc = Quark_ReadMem(common_target,addr,4,cnt>>2,buf);
		if(rc!=ERROR_OK)
			return rc;
		buf+=cnt>>2;
	}
	if(cnt%4)
	{
		rc = Quark_ReadMem(common_target,addr,1,cnt%4,buf);
		if(rc!=ERROR_OK)
			return rc;
	}
}
int32_t get_sap_vendor()
{
	return xap->vendor;
}
static U16	funToDoSav=0;
int SetFunTodo(uint8_t action)
{
	xDAPPort *dap = &pPort;
	if(!xap)
		return -1;
	
	return DAP_OK;
}
void ResetDapStatus()
{
	int ret=0;	
	xDAPPort *dap = &pPort;
			//ret = GetSAPFile(xap->sapfname,xBuff->buffo,0x400,0);
			//if(ret<0) return ret;					
			xap->sapid += 1;
			output_0v_io(); // cut off power to target
				Delayms(1000);  // wait for hardware stable
		dap->manstate = SM_START;
			//xap->dapstate &= ~(PREQ_SAPLOADED | PREQ_CODELOADED);
		xap->dapstate = 0;
			ret=parseSAPB(xap->sapid);
}
void SendErrMsg(char *msg)
{
	eP(msg);
}
xSAPTable *GetSapTable(void)
{
	return xst;
}
xSapFile* GetSapFile(void)
{
	return xap;
}
void SendLastErrCodeI2C()
{
	IeP(0, 0, 0, MSGSTR[ErrorCode]);
}