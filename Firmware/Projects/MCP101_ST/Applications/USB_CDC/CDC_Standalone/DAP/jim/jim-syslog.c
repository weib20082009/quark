/* Syslog interface for tcl
 * Copyright Victor Wagner <vitus@ice.ru> at
 * http://www.ice.ru/~vitus/works/tcl.html#syslog
 *
 * Slightly modified by Steve Bennett <steveb@snapgear.com>
 * Ported to Jim by Steve Bennett <steveb@workware.net.au>
 */
//#include <syslog.h>
#include <string.h>

#include <jim.h>

typedef struct
{
    int logOpened;
    int facility;
    int options;
    char ident[32];
} SyslogInfo;

#ifndef LOG_AUTHPRIV
# define LOG_AUTHPRIV LOG_AUTH
#endif

static const char * const facilities[] = {

};

static const char * const priorities[] = {

};

/**
 * Deletes the syslog command.
 */
static void Jim_SyslogCmdDelete(Jim_Interp *interp, void *privData)
{

}

/* Syslog_Log -
 * implements syslog tcl command. General format: syslog ?options? level text
 * options -facility -ident -options
 *
 * syslog ?-facility cron|daemon|...? ?-ident string? ?-options int? ?debug|info|...? text
 */
int Jim_SyslogCmd(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
    int i = 1;
   

    return JIM_OK;
}

int Jim_syslogInit(Jim_Interp *interp)
{
    SyslogInfo *info;


    return JIM_OK;
}
