#include "common.h"
#include "string.h"

//#include "fsapi.h"
//#ifdef  STM32F10X_MD 
#define __HAVE_SAP__
//#endif

uint8_t FlashBuf[0x440];

char config_file[]="config.mcp";
char config_list[20][30]={0};
char unsec_file[]="unsecure.sap";
static uint8_t gNeedUnsec=0;

extern int GetFileFlashInfo(char* filename,  uint32_t* flash_off, uint32_t* psize);
int GetConfigFile(uint32_t* poffset, uint32_t* psz )
{
	int ret,strlen,comment=0;
	char str_in_config[300];
	char ch;
	ret = GetFileFlashInfo(config_file, poffset,psz);		
	if(ret) return ret;
	
	strlen=*psz > 300 ? 300:*psz;
	spiflash_read((uint8_t *)str_in_config,*poffset,strlen);
	{
		int ii,jj=0,kk=0,line_break=0;
		//divide string to comand list
		memset(config_list,0,sizeof(config_list));
		for(ii=0;ii<strlen && jj<20 && kk<30;ii++)
		{
			ch = str_in_config[ii];
			if((ch ==0x0D)||(ch == 0x0A))
			{	
				comment = 0;
				line_break=1;
				continue; 
			}
			
			if(((32 <= ch) && (ch <= 126)))
			{
				if((ch=='#') || (ch == ';'))
					comment = 1;

				if(comment) continue;
				
				if(line_break) {jj++; kk =0;line_break=0;}
			    config_list[jj][kk] = ch;
				kk++;
			}		
			
		}	
	}
	return 0;
}

#ifdef PROGRAMMER

uint32_t JTAG_SWD_Get_IDCODE(int jtag_swd)
{
	int ii=0;
	uint32_t ret=0,*ptr;
	char in_buf[][32]={ \
		{02,01,0x00},
		{0x11,0x40,0x42,0x0f,0x00,0x00,0x00},
		{0x04,0x00,0x64,0x00,0x00,0x00},
		{0x13,0x00,0x00,0x00},
		{0x12,0x33,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff},
		{0x12,0x10,0x9e,0xe7,0x00},
		{0x12,0x33,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff},
		{0x12,0x08,0x00},
		{0x05,0x00,0x01,0x02,0x00,0x00},
		{0},
		};
	char end_buf[][32]={ \
		{03,00,0x00},
		{01,00,0x00},
		{0},
		};
	char out_buf[100]={0};

	if(jtag_swd == 2) //SWD
	{
		for(;;ii++)
		{
			if(in_buf[ii][0]==0)
				break;
			DAP_ProcessCommand((uint8_t *)in_buf[ii],(uint8_t *)out_buf);
			Delayms(10);
		}
		ptr = (void*)&out_buf[3];
		ret = *ptr;
		mtk_printf("SWD ID",ret);
		for(;;ii++)
		{
			if(end_buf[ii][0]==0)
				break;
			DAP_ProcessCommand((uint8_t *)in_buf[ii], (uint8_t *)out_buf);
			Delayms(10);
		}
	}
	
	return ret;
}
int GetLedMode(uint8_t *led_mode)
{
	int ret,prg_id,ii;
	uint32_t offset, sz,secure_encode=0;
	*led_mode=0;
	if(GetConfigFile(&offset, &sz ))
		return -1;
	
    for(ii=0;ii<10;ii++)
	{
		if(0==(strncmp(config_list[ii],"LED_mode=",9)))
			*led_mode=config_list[ii][9]-0x30;
	}
	return 0;
	
}
#ifdef USE_SYSLOG
int GetDebugMode(uint8_t *debug_mode)
{
	int ret,prg_id,ii;
	uint32_t offset, sz,secure_encode=0;
	*debug_mode=0;
	if(GetConfigFile(&offset, &sz ))
		return -1;
	
    for(ii=0;ii<10;ii++)
	{
		if(0==(strncmp(config_list[ii],"Debug_mode=",9)))
			*debug_mode=config_list[ii][9]-0x30;
	}
	return 0;
	
}
#endif
int GetSecStatus(uint8_t *status)//weib:maybe need to rewrite the function
{
	int ret,prg_id,ii;
	uint32_t offset, sz,secure_encode=0;
	*status=0;
	if(GetConfigFile(&offset, &sz ))
		return -1;
	
    for(ii=0;ii<10;ii++)
	{
		if(0==(strncmp(config_list[ii],"SEC_stat=",9)))
			*status=config_list[ii][9]-0x30;
	}
	return 0;
	
}
int context=0,read_times=0;
static void *cfgfpos=NULL;
uint32_t offset_sap_file, sz_sap_file;
SFHandle *f=NULL;
int GetSAPFileName(char * filename)
{
	int ret,prg_id,ii;
	uint32_t offset, sz,secure_encode=0;
	char sap_file[30]={0};
	char sap_secure[30]={0};
#if 0
	if(GetConfigFile(&offset, &sz ))
		return -1;
	
    for(ii=0;ii<10;ii++)
	{
		if(0==(strncmp(config_list[ii],"SAP=",4)))
			strncpy(sap_file,&config_list[ii][4],30-4);
		if(0==(strncmp(config_list[ii],"SAP_sec=",7)))
			strncpy(sap_secure,&config_list[ii][8],30-8);
			
	}	
	if(sap_file[0] == 0)
		return -1;
	
	memcpy(filename, sap_file, 12);
	secure_encode = strtoul(sap_secure,0,0);	
#endif
	memcpy(filename, "*.sap", 5);
	return 0;
	
}

int GetSAPFile(char * filename,unsigned char* buff,uint32_t sz,int offset)
{
	 int len,ret,bw;
		//spiflash_read(buff,offset_sap_file+offset,sz);
		f=SW_SF_Open(filename,MODE_R);
		f->offset+=offset;
		SW_SF_Read(f,buff,sz,&bw);
		len=bw;
	   return len;
}

int CloseSAPFile(char * filename)
{
    //SW_FS_Close(cfgfpos);
		SW_SF_Close(f);
		return 0;
}

#ifdef __HAVE_SAP__
extern int cmdLine(uint8_t *command);
int program_process()
{
	int ret =0;
	int context,len;
	uint32_t id_code;
	int check_id = 0;
	int jtag_swd = 2; // 1-Jtag, 2-SWD
	int ii;
	uint8_t status=0;
	//	MyMemInit();
#ifdef PROGRAMMER	
	//2. read SAP file
	GetSecStatus(&status);
	if(status)//need to unsec before pro
	{
		gNeedUnsec=1;
		ret = cmdLine("sap");
		if (ret)
			return ret;
		ret = cmdLine("auto");
		
		output_0v_io(); // cut off power to target
		if(ret)
			return ret;
		gNeedUnsec=0;
	}
	ret = cmdLine("sap");
	if (ret)
		return ret;

	//4. Program
	ret = cmdLine("auto");

	output_0v_io(); // cut off power to target

#endif
	return ret;
}
#endif
#endif



#ifdef UDISK
char buff[2048];
#endif
int update_itself_process()
{
#ifdef UDISK
	int ret,ii;
	uint32_t offset, sz,secure_encode=0;
	char firmware_fileH[30]={0};
	char firmware_fileL[30]={0};
	char firmware_secure[30]={0};
   	
	uint32_t curr;
	uint32_t flash_base;
	int size;
	int skip=0, shift=0;	
	
	
	if(GetConfigFile(&offset, &sz ))
		return -1;
	
    for(ii=0;ii<10;ii++)
	{
		if(0==(strncmp(config_list[ii],"FirmwareH=",10)))
			strncpy(firmware_fileH,&config_list[ii][10],30-10);
		if(0==(strncmp(config_list[ii],"FirmwareL=",10)))
			strncpy(firmware_fileL,&config_list[ii][10],30-10);
		if(0==(strncmp(config_list[ii],"Firm_sec=",9)))
			strncpy(firmware_secure,&config_list[ii][9],30-9);
			
	}	
	if(firmware_fileH[0] == 0)
		return -1;
	
	secure_encode = atoi(firmware_secure);
	
	
	flash_init(SystemCoreClock);	
		
#define INTER_FLASH_BASE_H 		0x10000000	
#define INTER_FLASH_BASE_L 		0x20000	

#define INTER_FLASH_SEC_SIZE   	2048	

	ret = GetFileFlashInfo(firmware_fileH,&offset, &sz);	
	if(ret)
		return ret;

	size = sz;
	curr = offset;
	flash_base = INTER_FLASH_BASE_H;
	
	skip = (secure_encode & 0xffff0000) >> 16;  
	shift = secure_encode & 0x0000ffff;

	while(1)  // High sectore binary program
    {
		if(size < 0)
			break;
		ret = spiflash_read(buff,curr,INTER_FLASH_SEC_SIZE);
		mtk_printf("read file from SPI flash\n",ret);
		if(ret)
			return ret;
		
		if(secure_encode)
		{
			ii = 0;
			if(skip && (curr == offset)) ii +=skip;
			
			for(;ii<INTER_FLASH_SEC_SIZE;ii++)
				if(buff[ii] & 0x80)
					buff[ii] = (buff[ii] << 1) + 1 ;
				else
					buff[ii] = (buff[ii] << 1);	
		}
		
		ret = flash_erase_sector(flash_base + (curr - offset));
		mtk_printf("flash_erase_chip finish",flash_base + (curr - offset));
		if(ret)
			return ret;
		
		ret = flash_program_page(flash_base + (curr - offset),
					size > INTER_FLASH_SEC_SIZE?INTER_FLASH_SEC_SIZE:size,
					buff);
		mtk_printf("flash_program finish",ret);
		if(ret)
			return ret;
		
		size -= INTER_FLASH_SEC_SIZE;
		curr +=	INTER_FLASH_SEC_SIZE;
	}

	ret = GetFileFlashInfo(firmware_fileL,&offset, &sz);	
	if(ret)
		return ret;

	size = sz;
	curr = offset;
	flash_base = INTER_FLASH_BASE_L;
	
	skip = (secure_encode & 0xffff0000) >> 16;  
	shift = secure_encode & 0x0000ffff;

	//flash_init(SystemCoreClock);

	while(1)  //
    {
		if(size < 0)
			break;
		ret = spiflash_read(buff,curr,INTER_FLASH_SEC_SIZE);
		mtk_printf("read file from SPI flash\n",ret);
		if(ret)
			return ret;
		
		if(secure_encode)
		{
			ii = 0;
			if(skip && (curr == offset)) ii +=skip;
			
			for(;ii<INTER_FLASH_SEC_SIZE;ii++)
				if(buff[ii] & 0x80)
					buff[ii] = (buff[ii] << 1) + 1 ;
				else
					buff[ii] = (buff[ii] << 1);	
		}
		
		ret = flash_erase_sector(flash_base + (curr - offset));
		mtk_printf("flash_erase_chip finish",flash_base + (curr - offset));
		if(ret)
			return ret;
		
		ret = flash_program_page(flash_base + (curr - offset),
					size > INTER_FLASH_SEC_SIZE?INTER_FLASH_SEC_SIZE:size,
					buff);
		mtk_printf("flash_program finish",ret);
		if(ret)
			return ret;
		
		size -= INTER_FLASH_SEC_SIZE;
		curr +=	INTER_FLASH_SEC_SIZE;

	}
	return 0;
#endif	
}
#ifdef USE_SYSLOG
struct DAP_SYS_Config SysConfig_t;

void get_sys_config(void)
{
	spiflash_read(&SysConfig_t,0,sizeof(SysConfig_t));
}
void update_sys_config(void)
{
		spiflash_erase(0,0x1000);
		spiflash_write(&SysConfig_t,0x0,sizeof(SysConfig_t));
}
void init_sys_config(void)
{
	get_sys_config();
	if(SysConfig_t.s1[0]==0xFF)//empty
	{
		memset(&SysConfig_t,0,sizeof(SysConfig_t));
		strcpy(SysConfig_t.s1,"#Firmware Ver.");
		SysConfig_t.s1[14]='\r';
		SysConfig_t.s1[15]='\n';
		SysConfig_t.firm_ver[0]=MAR_VER;
		SysConfig_t.firm_ver[1]='_';
		SysConfig_t.firm_ver[2]=MIN_VER;
		
		strcpy(SysConfig_t.s2,"\r\n#Board Type");
		SysConfig_t.s2[14]='\r';
		SysConfig_t.s2[15]='\n';
		strcat(SysConfig_t.board_type,"stm32");
		strcpy(SysConfig_t.s3,"\r\n#Unique ID.");
		SysConfig_t.s3[14]='\r';
		SysConfig_t.s3[15]='\n';		
		strcat(SysConfig_t.unique_id,"411321345");//need to up
	}
	update_sys_config();
}
#endif
