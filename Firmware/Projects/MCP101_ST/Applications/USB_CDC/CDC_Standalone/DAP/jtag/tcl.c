/***************************************************************************
 *   Copyright (C) 2005 by Dominic Rath                                    *
 *   Dominic.Rath@gmx.de                                                   *
 *                                                                         *
 *   Copyright (C) 2007-2010 Øyvind Harboe                                 *
 *   oyvind.harboe@zylin.com                                               *
 *                                                                         *
 *   Copyright (C) 2009 SoftPLC Corporation                                *
 *       http://softplc.com                                                *
 *   dick@softplc.com                                                      *
 *                                                                         *
 *   Copyright (C) 2009 Zachary T Welch                                    *
 *   zw@superlucidity.net                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.           *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "jtag.h"
#include "swd.h"
#include "minidriver.h"
#include "interface.h"
#include "interfaces.h"
#include "tcl.h"

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#include <helper/time_support.h>

/**
 * @file
 * Holds support for accessing JTAG-specific mechanisms from TCl scripts.
 */

static const Jim_Nvp nvp_jtag_tap_event[] = {
	{ .value = JTAG_TRST_ASSERTED,          .name = "post-reset" },
	{ .value = JTAG_TAP_EVENT_SETUP,        .name = "setup" },
	{ .value = JTAG_TAP_EVENT_ENABLE,       .name = "tap-enable" },
	{ .value = JTAG_TAP_EVENT_DISABLE,      .name = "tap-disable" },

	{ .name = NULL, .value = -1 }
};



static bool scan_is_safe(tap_state_t state)
{
	switch (state) {
	    case TAP_RESET:
	    case TAP_IDLE:
	    case TAP_DRPAUSE:
	    case TAP_IRPAUSE:
		    return true;
	    default:
		    return false;
	}
}



enum jtag_tap_cfg_param {
	JCFG_EVENT
};

static Jim_Nvp nvp_config_opts[] = {
	{ .name = "-event",      .value = JCFG_EVENT },

	{ .name = NULL,          .value = -1 }
};

static int jtag_tap_configure_event(Jim_GetOptInfo *goi, struct jtag_tap *tap)
{


	Jim_Nvp *n;
	//int e = Jim_GetOpt_Nvp(goi, nvp_jtag_tap_event, &n);
	///if (e != JIM_OK) {
		//Jim_GetOpt_NvpUnknown(goi, nvp_jtag_tap_event, 1);
	//	return e;
	//}


	struct jtag_tap_event_action *jteap  = tap->event_action;
	/* replace existing event body */
	bool found = false;
	while (jteap) {
		if (jteap->event == (enum jtag_event)n->value) {
			found = true;
			break;
		}
		jteap = jteap->next;
	}

	
	return JIM_OK;
}

static int jtag_tap_configure_cmd(Jim_GetOptInfo *goi, struct jtag_tap *tap)
{
	/* parse config or cget options */
	//while (goi->argc > 0) {
//		Jim_SetEmptyResult(goi->interp);

		Jim_Nvp *n;
		int e =0; //Jim_GetOpt_Nvp(goi, nvp_config_opts, &n);
		//if (e != JIM_OK) {
			//Jim_GetOpt_NvpUnknown(goi, nvp_config_opts, 0);
		//	return e;
		//}

		switch (n->value) {
			case JCFG_EVENT:
				e = jtag_tap_configure_event(goi, tap);
				if (e != JIM_OK)
					return e;
				break;
			default:
				return JIM_ERR;
		}
	

	return JIM_OK;
}

static int is_bad_irval(int ir_length, jim_wide w)
{
	jim_wide v = 1;

	v <<= ir_length;
	v -= 1;
	v = ~v;
	return (w & v) != 0;
}



#define NTAP_OPT_IRLEN     0
#define NTAP_OPT_IRMASK    1
#define NTAP_OPT_IRCAPTURE 2
#define NTAP_OPT_ENABLED   3
#define NTAP_OPT_DISABLED  4
#define NTAP_OPT_EXPECTED_ID 5
#define NTAP_OPT_VERSION   6



static void jtag_tap_handle_event(struct jtag_tap *tap, enum jtag_event e)
{
	struct jtag_tap_event_action *jteap;

	//for (jteap = tap->event_action; jteap != NULL; jteap = jteap->next) {
//		if (jteap->event != e)
	//		continue;

	//	Jim_Nvp *nvp = Jim_Nvp_value2name_simple(nvp_jtag_tap_event, e);
	//	LOG_DEBUG("JTAG tap: %s event: %d (%s)\n\taction: %s",
	//		tap->dotted_name, e, nvp->name,
	//		Jim_GetString(jteap->body, NULL));

	/*	
		if (Jim_EvalObj(jteap->interp, jteap->body) != JIM_OK) {
			Jim_MakeErrorMessage(jteap->interp);
			LOG_USER("%s", Jim_GetString(Jim_GetResult(jteap->interp), NULL));
			continue;
		}*/ //weib

		switch (e) {
			case  JTAG_TRST_ASSERTED:
				jtag_execute_queue();
				break;
			case JTAG_TAP_EVENT_SETUP:
			case JTAG_TAP_EVENT_ENABLE:
				Quark_Tap_en(1);
				break;
			case JTAG_TAP_EVENT_DISABLE:
				Quark_Tap_en(0);
				break;
			default:break;
		}
		switch (e) {
		    case JTAG_TAP_EVENT_ENABLE:
		    case JTAG_TAP_EVENT_DISABLE:
				/* NOTE:  we currently assume the handlers
				 * can't fail.  Right here is where we should
				 * really be verifying the scan chains ...
				 */
			    tap->enabled = (e == JTAG_TAP_EVENT_ENABLE);
			    LOG_INFO("JTAG tap: %s %s", tap->dotted_name,
				tap->enabled ? "enabled" : "disabled");
			    break;
		    default:
			    break;
		}
	//}
}



static bool jtag_tap_enable(struct jtag_tap *t)
{
	if (t->enabled)
		return false;
	jtag_tap_handle_event(t, JTAG_TAP_EVENT_ENABLE);
	if (!t->enabled)
		return false;

	/* FIXME add JTAG sanity checks, w/o TLR
	 *  - scan chain length grew by one (this)
	 *  - IDs and IR lengths are as expected
	 */
	//jtag_call_event_callbacks(JTAG_TAP_EVENT_ENABLE);
	return true;
}
static bool jtag_tap_disable(struct jtag_tap *t)
{
	if (!t->enabled)
		return false;
	jtag_tap_handle_event(t, JTAG_TAP_EVENT_DISABLE);
	if (t->enabled)
		return false;

	/* FIXME add JTAG sanity checks, w/o TLR
	 *  - scan chain length shrank by one (this)
	 *  - IDs and IR lengths are as expected
	 */
//	jtag_call_event_callbacks(JTAG_TAP_EVENT_DISABLE);
	return true;
}




void jtag_notify_event(enum jtag_event event)
{
	struct jtag_tap *tap;

	for (tap = jtag_all_taps(); tap; tap = tap->next_tap)
		jtag_tap_handle_event(tap, event);
}

