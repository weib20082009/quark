#ifndef FILEUTILS_H
#define FILEUTILS_H
#include "stdint.h"
#define FILESEG_NAME_LEN    16

#define     SER_TYPE_GENERAL    0x00
#define     SER_TYPE_CRC32      0x21    // maybe not used
#define     SER_TYPE_DUMMY      0x30    // maybe not used

enum {
    SERP_OK = 0,
    SERP_NOTHEADER= -2,
    SERP_CHECKSUM = -3,
    SERP_NOTHEX   = -4,
    SERP_LENGTHERR= -5,
};
//
// serialheader
//      type, addr, size, data[0..3]
typedef struct SerialStru {
    uint8_t     state;              // state:   0x00 -> read not write
                                    //          0x01 -> writed successful
                                    //          0x02 -> logged to RES
                                    //          0x80 -> invalid record
    uint8_t     type;
    uint8_t     size;               // size write to flash
    uint8_t     sz;                 //  size write to memory
    uint32_t    addr;               // address write to flash
    uint32_t    adr;                // address write to memory
    char        *data[4];           // PORT_COUNT
    uint16_t    offRec;
    uint16_t    reserv;
} xSerial;                          // size = 

typedef struct SerialFile {
    uint8_t     sbuff[0x10/*0x800*/];
    uint8_t     *line;
    uint8_t     inclog; // obsoleted due to even if SER_TYPE_CRC32 only, it still need one line per target.
                                    //  log incremental.
                                    // For SER_TYP_CRC32:
                                    //      no need to log.
                                    //      no need to read serial.
                                    //
    uint8_t     cntSer;              // serial count in each line
    uint16_t    linesize;           // size of each line, for buffer allocation
    uint16_t    toW;
    uint16_t    toV;
    char        sername[16];        //  serial file name
    char        resname[16];        //  serial offset file name(record the writed serial line)
    char        *sapname;
    uint32_t	offLine;			    // line to file start
    uint32_t	signature;			    // signature to match with SAP file
	uint32_t	lines[4];			// line for each port.
	uint8_t	    state[4];			// write state for each port.
} xSerFile;
#if 0
#define HEX2I(x)    (x < 0x30)?(-1):( \
                    (x < 0x3A)?(x-0x30):( \
                    (x < 0x41)?(-1):(  \
                    (x < 0x47)?(x-55): ( \
                    (x < 0x61)?(-1):( \
                    (x < 0x67)?(x-87):(-1) \
                    )))))
#endif

int serial_log(xSerFile *xsf, uint32_t action);
int serial_readlines(xSerFile *xsf, int port);
//xSerFile *SerFileInit(char *sapfname);
int SerFileInit(xSerFile *xsf, char *sapfname);
int fileAppend(const char *fname, uint8_t *buf, uint32_t sz);
int file_ext_replace(char *src, char *dst, uint8_t *ext);

#endif
