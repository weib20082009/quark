/*
 * Copyright(c) 2015 Intel Corporation.
 *
 * Ivan De Cesaris (ivan.de.cesaris@intel.com)
 * Jessica Gomez (jessica.gomez.hernandez@intel.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Contact Information:
 * Intel Corporation
 */

/*
 * @file
 * Debugger for Intel Quark SE
 * This file implements any Quark SE SoC specific features such as resetbreak
 */

#ifndef QUARK_SE_H
#define QUARK_SE_H
#include <jtag/jtag.h>
#include <helper/types.h>

/* Quark SE FLASH ADDRESS */
#define FLASH0_STTS      ((uint32_t)0xB0100014)
#define FLASH0_CTL       ((uint32_t)0xB0100018)
#define FLASH0_WR_CTL    ((uint32_t)0xB010000C)
#define FLASH0_WR_DATA   ((uint32_t)0xB0100010)
#define FLASH0_BASE_ADDR ((uint32_t)0x40000000)
#define FLASH0_LIMT      ((uint32_t)0x4002FFFF)

#define FLASH1_STTS      ((uint32_t)0xB0200014)
#define FLASH1_CTL       ((uint32_t)0xB0200018)
#define FLASH1_WR_CTL    ((uint32_t)0xB020000C)
#define FLASH1_WR_DATA   ((uint32_t)0xB0200010)
#define FLASH1_BASE_ADDR ((uint32_t)0x40030000)
#define FLASH1_LIMT      ((uint32_t)0x4005FFFF)

#define ROM_WR_CTL     ((uint32_t)0xB0100004)
#define ROM_WR_DATA    ((uint32_t)0xB0100008)
#define ROM_BASE_ADDR  ((uint32_t)0xFFFFE000)
#define ROM_LIMIT      ((uint32_t)0xFFFFFFFF)

#define FLASH_CTL_WR_DIS_MASK  ((uint32_t)0x00000010) /* CTL */
#define ROM_CTL_WR_DIS_MASK    ((uint32_t)0x00000004) /* FLASH_STTS */

/* FLASH0, FLASH1 and ROM generic */
#define FC_CTL_ADDR      		7
#define FC_CTL_INFO_ADDR      		6
#define FC_WR_CTL_ADDR      2
#define FC_WR_CTR_DREQ    (1<<1)
#define FC_WR_CTL_WREQ    (1<<0)
#define FC_STTS_WR_DONE   (1<<1)
#define FC_STTS_ER_DONE   (1<<0)
#define FC_BYTE_PAGE_SIZE (4*512)


#define QM_CCU_SYS_CLK_SEL BIT(0)
#define QM_SCSS_CCU_SYS_CLK_SEL BIT(0)
#define QM_SCSS_CCU_C2_LP_EN BIT(1)
#define QM_SCSS_CCU_SS_LPS_EN BIT(0)
#define QM_CCU_RTC_CLK_EN BIT(1)
#define QM_CCU_RTC_CLK_DIV_EN BIT(2)
#define QM_CCU_SYS_CLK_DIV_EN BIT(7)
#define QM_CCU_SYS_CLK_DIV_MASK (0x00000300)


#define QM_OSC0_SI_FREQ_SEL_DEF_MASK (0xFFFFFCFF)
#define QM_CCU_GPIO_DB_DIV_OFFSET (2)
#define QM_CCU_GPIO_DB_CLK_DIV_EN BIT(1)
#define QM_CCU_GPIO_DB_CLK_EN BIT(0)
#define QM_CCU_RTC_CLK_DIV_OFFSET (3)
#define QM_CCU_SYS_CLK_DIV_OFFSET (8)
#define QM_CCU_DMA_CLK_EN BIT(6)


#define BIT(x) (1U << (x))


typedef enum {
	CLK_SYS_HYB_OSC_32MHZ, /**< 32MHz Hybrid Oscillator Clock. */
	CLK_SYS_HYB_OSC_16MHZ, /**< 16MHz Hybrid Oscillator Clock. */
	CLK_SYS_HYB_OSC_8MHZ,  /**< 8MHz Hybrid Oscillator Clock. */
	CLK_SYS_HYB_OSC_4MHZ,  /**< 4MHz Hybrid Oscillator Clock. */
	CLK_SYS_RTC_OSC,       /**< Real Time Clock. */
	CLK_SYS_CRYSTAL_OSC    /**< Crystal Oscillator Clock. */
} clk_sys_mode_t;

typedef enum {
	CLK_SYS_DIV_1, /**< Clock Divider = 1. */
	CLK_SYS_DIV_2, /**< Clock Divider = 2. */
	CLK_SYS_DIV_4, /**< Clock Divider = 4. */
	CLK_SYS_DIV_8, /**< Clock Divider = 8. */
	CLK_SYS_DIV_NUM
} clk_sys_div_t;
/** System Core register map. */
typedef struct {
	 uint32_t osc0_cfg0;    /**< Hybrid Oscillator Configuration 0. */
	 uint32_t osc0_stat1;   /**< Hybrid Oscillator status 1. */
	 uint32_t osc0_cfg1;    /**< Hybrid Oscillator configuration 1. */
	 uint32_t osc1_stat0;   /**< RTC Oscillator status 0. */
	 uint32_t osc1_cfg0;    /**< RTC Oscillator Configuration 0. */
	 uint32_t usb_pll_cfg0; /**< USB Phase lock look configuration. */
	 uint32_t
	    ccu_periph_clk_gate_ctl; /**< Peripheral Clock Gate Control. */
	 uint32_t
	    ccu_periph_clk_div_ctl0; /**< Peripheral Clock Divider Control. 0 */
	 uint32_t
	    ccu_gpio_db_clk_ctl; /**< Peripheral Clock Divider Control 1. */
	 uint32_t
	    ccu_ext_clock_ctl; /**< External Clock Control Register. */
	/** Sensor Subsystem peripheral clock gate control. */
	 uint32_t ccu_ss_periph_clk_gate_ctl;
	 uint32_t ccu_lp_clk_ctl; /**< System Low Power Clock Control. */
	 uint32_t reserved;
	 uint32_t ccu_mlayer_ahb_ctl; /**< AHB Control Register. */
	 uint32_t ccu_sys_clk_ctl; /**< System Clock Control Register. */
	 uint32_t osc_lock_0;      /**< Clocks Lock Register. */
} qm_scss_ccu_reg_t;

#define QM_SCSS_CCU_BASE (0xB0800000)
#define QM_SCSS_CCU ((qm_scss_ccu_reg_t *)QM_SCSS_CCU_BASE)

/* Silicon Oscillator parameters. */
#define OSC0_CFG1_FTRIMOTP_MASK (0x3FF00000)
#define OSC0_CFG1_FTRIMOTP_OFFS (20)
#define OSC0_CFG1_SI_FREQ_SEL_MASK (0x00000300)
#define OSC0_CFG1_SI_FREQ_SEL_OFFS (8)

#define QM_OSC0_MODE_SEL BIT(3)
#define QM_OSC0_LOCK_SI BIT(0)
#define QM_OSC0_LOCK_XTAL BIT(1)
#define QM_OSC0_EN_SI_OSC BIT(1)

/* public interface */
int quark_se_flash_write(struct target *t, uint32_t addr,
			uint32_t size, uint32_t count, const uint8_t *buf);

#endif /* QUARK_SE_H */
