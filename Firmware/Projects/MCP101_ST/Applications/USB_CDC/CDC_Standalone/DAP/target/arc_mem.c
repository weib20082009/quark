/***************************************************************************
 *   Copyright (C) 2013-2014 Synopsys, Inc.                                *
 *   Frank Dols <frank.dols@synopsys.com>                                  *
 *   Mischa Jonker <mischa.jonker@synopsys.com>                            *
 *   Anton Kolesov <anton.kolesov@synopsys.com>                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.           *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "arc.h"

/* ----- Supporting functions ---------------------------------------------- */

static int arc_mem_read_block(struct target *target, uint32_t addr,
	int size, int count, void *buf)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;

	LOG_DEBUG("Read memory: addr=0x%" PRIx32 ", size=%i, count=%i",
			addr, size, count);
	assert(!(addr & 3));
	assert(size == 4);

	/* Always call D$ flush, it will decide whether to perform actual
	 * flush. */
	retval = arc32_dcache_flush(target);
	if (ERROR_OK != retval)
		return retval;

	retval = arc_jtag_read_memory(&arc32->jtag_info, addr, count, buf);

	return retval;
}

/* Write word at word-aligned address */
int arc_mem_write_block32(struct target *target, uint32_t addr, int count,
	void *buf)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;

	/* Check arguments */
	if (addr & 0x3u)
		return ERROR_TARGET_UNALIGNED_ACCESS;

	/* No need to flush cache, because we don't read values from memory. */
	retval = arc_jtag_write_memory( &arc32->jtag_info, addr, count, (uint32_t *)buf);
	if (ERROR_OK != retval)
		return retval;

	/* Invalidate caches. */
	retval = arc32_cache_invalidate(target);

	return retval;
}
int arc_mem_write_block32_fast(struct target *target, uint32_t addr, int count,
	void *buf)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;

	/* Check arguments */
	if (addr & 0x3u)
		return ERROR_TARGET_UNALIGNED_ACCESS;

	/* No need to flush cache, because we don't read values from memory. */
	//retval = arc_jtag_write_memory( &arc32->jtag_info, addr, count, (uint32_t *)buf);
	//if (ERROR_OK != retval)
	//	return retval;
	retval = arc_jtag_write_memory_fast( &arc32->jtag_info, addr, count, (uint32_t *)buf);
	if (ERROR_OK != retval)
		return retval;	
	/* Invalidate caches. */
	retval = arc32_cache_invalidate(target);

	return retval;
}
int arc_mem_write_block32_fast_start(struct target *target)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;
	retval = arc_jtag_write_memory_fast_start(&arc32->jtag_info);
	if (ERROR_OK != retval)
		return retval;
}
int arc_mem_write_block32_fast_end(struct target *target)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;
	retval = arc_jtag_write_memory_fast_end(&arc32->jtag_info);
	retval = arc32_cache_invalidate(target);
	if (ERROR_OK != retval)
		return retval;
}
#if 0
int arc_mem_write_block32_fast(struct target *target, uint32_t addr, int count,
	void *buf)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;

	/* Check arguments */
	if (addr & 0x3u)
		return ERROR_TARGET_UNALIGNED_ACCESS;

	/* No need to flush cache, because we don't read values from memory. */
	retval = arc_jtag_write_memory_fast( &arc32->jtag_info, addr, count, (uint32_t *)buf);
	if (ERROR_OK != retval)
		return retval;

	/* Invalidate caches. */
//	retval = arc32_cache_invalidate(target);

	return retval;
}
#endif
#define FC_WR_CTL_ADDR      2
#define FC_WR_CTR_DREQ    (1<<1)
#define FC_WR_CTL_WREQ    (1<<0)
#define FC_STTS_WR_DONE   (1<<1)
#define FC_STTS_ER_DONE   (1<<0)

int arc_flash_write_block32_fast(struct target *target, uint32_t addr, int count,
	uint32_t wr_data_addr,uint32_t wr_ctrl_addr,uint32_t stts,void *buf)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;
	int dot_cnt = 0;
	uint8_t ctl_buff[4];
	/* Check arguments */
	if (addr & 0x3u)
		return ERROR_TARGET_UNALIGNED_ACCESS;
				arc_jtag_write_memory_fast_start(&arc32->jtag_info);
		for (uint32_t i = 0; i <count /*512*/; i++) {
			
			if(i%127 == 0)
			{
				uP("^");
			}
			if(i%512==0)
			{
				dot_cnt ++;	
				if(dot_cnt%20 == 0)
					uP("\r\n");
				else	
					uP(".");		
			}
			
	/* Write word data */
			//if (target_write_memory(t, FC_WR_DATA, 4, 1, &buf[i*4]/*&fc_data_buff[i*4]*/) != ERROR_OK) {
			//	LOG_ERROR("%s: Couldn't write WR_DATA register in SRAM", __func__);
			//	return ERROR_FAIL;
			//}

			if(arc_jtag_write_mem_reg(&arc32->jtag_info, wr_data_addr, 1, ((uint32_t *)buf)+i/*&fc_data_buff[i*4]*/) != ERROR_OK) 
			{
				LOG_ERROR("%s: Couldn't write WR_DATA register in SRAM", __func__);
				return ERROR_FAIL;				
			}
			/* Select address and flag WR request */
			buf_set_u32(ctl_buff, 0, 32, (((addr) << FC_WR_CTL_ADDR) | FC_WR_CTL_WREQ));
			//if (target_write_memory(t, FC_WR_CTL, 4, 1, ctl_buff) != ERROR_OK) {
			//	LOG_ERROR("%s: Couldn't write WR_CTL register in SRAM", __func__);
			//	return ERROR_FAIL;
			//}
			if (arc_jtag_write_mem_reg(&arc32->jtag_info, wr_ctrl_addr,  1, ctl_buff) != ERROR_OK) {
				LOG_ERROR("%s: Couldn't write WR_CTL register in SRAM", __func__);
				return ERROR_FAIL;
			}
			/* Check for FLASH_STTS */
			//if (flash_word_write_wait) {
			//	if (quark_se_wait_flash_mask(t, FC_STTS, FC_STTS_WR_DONE) != ERROR_OK) {
			//		LOG_ERROR("%s: Bit WR_DONE in FLASH_STTS Timeout!", __func__);
			//		return ERROR_FAIL;
			//	}	
			//arc_jtag_exec();
			//if (quark_se_wait_flash_mask(target, stts, FC_STTS_WR_DONE) != ERROR_OK) {
			//	LOG_ERROR("%s: Bit WR_DONE in FLASH_STTS Timeout!", __func__);
			//	return ERROR_FAIL;
			//}			
			if(i%16 == 0)
				arc_jtag_exec();
			addr += 4;
			if(0)
			{
					arc_jtag_write_memory_fast_end(&arc32->jtag_info);
					arc_jtag_write_memory_fast_start(&arc32->jtag_info);
			}
			}

			/* given that doing word writes */

	
	
	
	
	/* No need to flush cache, because we don't read values from memory. */
	//retval = arc_jtag_write_memory_fast( &arc32->jtag_info, addr, count, (uint32_t *)buf);
	//if (ERROR_OK != retval)
	//	return retval;

	/* Invalidate caches. */
//	retval = arc32_cache_invalidate(target);

	return retval;
}
/* Write half-word at half-word-aligned address */
static int arc_mem_write_block16(struct target *target, uint32_t addr, int count,
	void *buf)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;
	int i;

	LOG_DEBUG("Write memory (16bit): addr=0x%" PRIx32 ", count=%i", addr, count);

	/* Check arguments */
	if (addr & 1u)
		return ERROR_TARGET_UNALIGNED_ACCESS;

	/* We will read data from memory, so we need to flush D$. */
	retval = arc32_dcache_flush(target);
	if (ERROR_OK != retval)
		return retval;

	uint32_t buffer_he;
	uint8_t buffer_te[sizeof(uint32_t)];
	uint8_t halfword_te[sizeof(uint16_t)];
	/* non-word writes are less common, than 4-byte writes, so I suppose we can
	 * allowe ourselves to write this in a cycle, instead of calling arc_jtag
	 * with count > 1. */
	for(i = 0; i < count; i++) {
		/* We can read only word at word-aligned address. Also *jtag_read_memory
		 * functions return data in host endianness, so host endianness !=
		 * target endianness we have to convert data back to target endianness,
		 * or bytes will be at the wrong places.So:
		 *   1) read word
		 *   2) convert to target endianness
		 *   3) make changes
		 *   4) convert back to host endianness
		 *   5) write word back to target.
		 */
		retval = arc_jtag_read_memory(&arc32->jtag_info,
				(addr + i * sizeof(uint16_t)) & ~3u, 1, &buffer_he);
		target_buffer_set_u32(target, buffer_te, buffer_he);
		/* buf is in host endianness, convert to target */
		target_buffer_set_u16(target, halfword_te, ((uint16_t *)buf)[i]);
		memcpy(buffer_te  + ((addr + i * sizeof(uint16_t)) & 3u),
                        halfword_te, sizeof(uint16_t));
		buffer_he = target_buffer_get_u32(target, buffer_te);
		retval = arc_jtag_write_memory(&arc32->jtag_info,
                        (addr + i * sizeof(uint16_t)) & ~3u, 1, &buffer_he);

		if (ERROR_OK != retval)
			return retval;
	}

	/* Invalidate caches. */
	retval = arc32_cache_invalidate(target);

	return retval;
}

/* Write byte at address */
static int arc_mem_write_block8(struct target *target, uint32_t addr, int count,
	void *buf)
{
	struct arc32_common *arc32 = target_to_arc32(target);
	int retval = ERROR_OK;
	int i;

	/* We will read data from memory, so we need to flush D$. */
	retval = arc32_dcache_flush(target);
	if (ERROR_OK != retval)
		return retval;

	uint32_t buffer_he;
	uint8_t buffer_te[sizeof(uint32_t)];
	/* non-word writes are less common, than 4-byte writes, so I suppose we can
	 * allowe ourselves to write this in a cycle, instead of calling arc_jtag
	 * with count > 1. */
	for(i = 0; i < count; i++) {
		/* See comment in arc_mem_write_block16 for details. Since it is a byte
		 * there is not need to convert write buffer to target endianness, but
		 * we still have to convert read buffer. */
		retval = arc_jtag_read_memory(&arc32->jtag_info, (addr + i) & ~3, 1, &buffer_he);
		target_buffer_set_u32(target, buffer_te, buffer_he);
		memcpy(buffer_te  + ((addr + i) & 3), (uint8_t*)buf + i, 1);
		buffer_he = target_buffer_get_u32(target, buffer_te);
		retval = arc_jtag_write_memory(&arc32->jtag_info, (addr + i) & ~3, 1, &buffer_he);

		if (ERROR_OK != retval)
			return retval;
	}

	/* Invalidate caches. */
	retval = arc32_cache_invalidate(target);

	return retval;
}

/* ----- Exported functions ------------------------------------------------ */

int arc_mem_read(struct target *target, uint32_t address, uint32_t size,
	uint32_t count, uint8_t *buffer)
{
	int retval = ERROR_OK;

	LOG_DEBUG("Read memory: addr=0x%" PRIx32 ", size=%i, count=%i",
			address, size, count);

	if (target->state != TARGET_HALTED) {
		LOG_WARNING("target not halted");
		return ERROR_TARGET_NOT_HALTED;
	}

	/* Sanitize arguments */
	if (((size != 4) && (size != 2) && (size != 1)) || (count == 0) || !(buffer))
		return ERROR_COMMAND_SYNTAX_ERROR;

	if (((size == 4) && (address & 0x3u)) || ((size == 2) && (address & 0x1u)))
	    return ERROR_TARGET_UNALIGNED_ACCESS;

	//void *tunnel_he;
	//uint8_t *tunnel_te;
	uint32_t words_to_read, bytes_to_read;

	/* TODO: I think this function might be made much more clear if it
	 * would be splitted onto three based on size (4/2/1). That would
	 * duplicate some logic, but it would be much easier to understand it,
	 * those bit operations are just asking for a trouble. And they emulate
	 * size-specific logic, that is smart, but dangerous.  */
	/* Reads are word-aligned, so padding might be required if count > 1.
	 * NB: +3 is a padding for the last word (in case it's not aligned;
	 * addr&3 is a padding for the first word (since address can be
	 * unaligned as well).  */
	bytes_to_read = (count * size + 3 + (address & 3u)) & ~3u;
	words_to_read = bytes_to_read >> 2;
	//uint8_t tunnel_he[bytes_to_read];
	//uint8_t tunnel_te[bytes_to_read];
	//tunnel_he = malloc(bytes_to_read);
	//tunnel_te = malloc(bytes_to_read);
	//if (!tunnel_he || !tunnel_te) {
	//	LOG_ERROR("Out of memory");
	//	return ERROR_FAIL;
	//}

	/* We can read only word-aligned words. */
	retval = arc_mem_read_block(target, address & ~3u, sizeof(uint32_t),
		words_to_read, buffer/*tunnel_he*/);

	/* arc32_..._read_mem with size 4/2 returns uint32_t/uint16_t in host */
	/* endianness, but byte array should represent target endianness      */
#if 0
	if (ERROR_OK == retval) {
		switch (size) {
		case 4:
			target_buffer_set_u32_array(target, buffer, count,
				tunnel_he);
			break;
		case 2:
			target_buffer_set_u32_array(target, tunnel_te,
				words_to_read, tunnel_he);
			/* Will that work properly with count > 1 and big endian? */
			memcpy(buffer, tunnel_te + (address & 3u),
				count * sizeof(uint16_t));
			break;
		case 1:
			target_buffer_set_u32_array(target, tunnel_te,
				words_to_read, tunnel_he);
			/* Will that work properly with count > 1 and big endian? */
			memcpy(buffer, tunnel_te + (address & 3u), count);
			break;
		}
	}
#endif
	//free(tunnel_he);
	//free(tunnel_te);

	return retval;
}
#define MAX_BUF_SIZE 0x400
int arc_mem_write(struct target *target, uint32_t address, uint32_t size,
	uint32_t count, const uint8_t *buffer)
{
	int retval = ERROR_OK;

	LOG_DEBUG("address: 0x%08" PRIx32 ", size: %" PRIu32 ", count: %" PRIu32,
		address, size, count);

	if (target->state != TARGET_HALTED) {
		LOG_WARNING("target not halted");
		return ERROR_TARGET_NOT_HALTED;
	}

	/* sanitize arguments */

	if (((size != 4) && (size != 2) && (size != 1)) || (count == 0) || !(buffer))
		return ERROR_COMMAND_SYNTAX_ERROR;

	if (((size == 4) && (address & 0x3u)) || ((size == 2) && (address & 0x1u)))
		return ERROR_TARGET_UNALIGNED_ACCESS;

	/* correct endianess if we have word or hword access */
	//void *tunnel = NULL;
	//uint8_t tunnel[MAX_BUF_SIZE];
	uint32_t cnt = count * size * sizeof(uint8_t)/MAX_BUF_SIZE ;
	uint32_t rest = count * size * sizeof(uint8_t)%MAX_BUF_SIZE ;
	int i =0;
	for(i = 0;i<cnt;i++)
	{
		#if 0
		if (/*size > 1*/0) {
			/*
			 * arc32_..._write_mem with size 4/2 requires uint32_t/uint16_t
			 * in host endianness, but byte array represents target endianness.
			 */
			//tunnel = malloc(count * size * sizeof(uint8_t));
			//if (tunnel == NULL) {
			//	LOG_ERROR("Out of memory");
			//	return ERROR_FAIL;
			//}
			
			switch (size) {
			case 4:
				target_buffer_get_u32_array(target, buffer+i*sizeof(tunnel), sizeof(tunnel)/4,
					(uint32_t *)tunnel);
				break;
			case 2:
				target_buffer_get_u16_array(target, buffer+i*sizeof(tunnel), sizeof(tunnel)/2,
					(uint16_t *)tunnel);
				break;
			}
			//buffer = tunnel;
		}
		#endif
		if (size == 4) {
			//retval = arc_mem_write_block32(target, address, count, (void *)buffer);
			//retval = arc_mem_write_block32(target, address, MAX_BUF_SIZE>>2, (void *)buffer);
			retval = arc_mem_write_block32_fast(target, address, MAX_BUF_SIZE>>2, (void *)buffer);
		} else if (size == 2) {
			/* We convert buffer from host endianness to target. But then in
			 * write_block16, we do the reverse. Is there a way to avoid this without
			 * breaking other cases? */
			retval = arc_mem_write_block16(target, address, MAX_BUF_SIZE>>1, (void *)buffer);
		} else {
			retval = arc_mem_write_block8(target, address, MAX_BUF_SIZE, (void *)buffer);
		}
		buffer  += MAX_BUF_SIZE;
		address += MAX_BUF_SIZE>>2;
	}
	if(rest)
	{
		uint32_t r_cnt = rest;
		uint32_t r_rest = rest%4;
		uint8_t dummy[4] = {0xFF,0xFF,0xFF,0xFF};
		if (0) {
			/*
			 * arc32_..._write_mem with size 4/2 requires uint32_t/uint16_t
			 * in host endianness, but byte array represents target endianness.
			 */
			//tunnel = malloc(count * size * sizeof(uint8_t));
			//if (tunnel == NULL) {
			//	LOG_ERROR("Out of memory");
			//	return ERROR_FAIL;
			//}
			
			switch (size) {
			case 4:
				target_buffer_get_u32_array(target, buffer, rest/4,
					(uint32_t *)buffer);
				break;
			case 2:
				target_buffer_get_u16_array(target, buffer, rest/2,
					(uint16_t *)buffer);
				break;
			}
			//buffer = tunnel;
		}
		
		if (size == 4) {
			retval = arc_mem_write_block32_fast(target, address, r_cnt>>2, (void *)buffer);
		} else if (size == 2) {
			/* We convert buffer from host endianness to target. But then in
			 * write_block16, we do the reverse. Is there a way to avoid this without
			 * breaking other cases? */
			retval = arc_mem_write_block16(target, address, r_cnt>>1, (void *)buffer);
		} else {
			retval = arc_mem_write_block8(target, address, r_cnt, (void *)buffer);
		}
		buffer +=(r_cnt>>2)<<2;
		address += r_cnt;
		if(r_rest)
		{
			memcpy(dummy,buffer,r_rest);
			retval = arc_mem_write_block32(target, address, 1, (void *)dummy);
		}
	}
	//if (tunnel != NULL)
		//free(tunnel);

	return retval;
}

int arc_mem_checksum(struct target *target, uint32_t address, uint32_t count,
	uint32_t *checksum)
{
	int retval = ERROR_OK;

	LOG_ERROR("arc_mem_checksum NOT SUPPORTED IN THIS RELEASE.");

	return retval;
}

int arc_mem_blank_check(struct target *target, uint32_t address,
	uint32_t count, uint32_t *blank)
{
	int retval = ERROR_OK;

	LOG_ERROR("arc_mem_blank_check NOT SUPPORTED IN THIS RELEASE.");

	return retval;
}

/* ......................................................................... */

int arc_mem_run_algorithm(struct target *target,
	int num_mem_params, struct mem_param *mem_params,
	int num_reg_params, struct reg_param *reg_params,
	uint32_t entry_point, uint32_t exit_point,
	int timeout_ms, void *arch_info)
{
	int retval = ERROR_OK;

	LOG_ERROR("arc_mem_run_algorithm NOT SUPPORTED IN THIS RELEASE.");

	return retval;
}

int arc_mem_start_algorithm(struct target *target,
	int num_mem_params, struct mem_param *mem_params,
	int num_reg_params, struct reg_param *reg_params,
	uint32_t entry_point, uint32_t exit_point,
	void *arch_info)
{
	int retval = ERROR_OK;

	LOG_ERROR("arc_mem_start_algorithm NOT SUPPORTED IN THIS RELEASE.");

	return retval;
}

int arc_mem_wait_algorithm(struct target *target,
	int num_mem_params, struct mem_param *mem_params,
	int num_reg_params, struct reg_param *reg_params,
	uint32_t exit_point, int timeout_ms,
	void *arch_info)
{
	int retval = ERROR_OK;

	LOG_ERROR("arc_mem_wait_algorithm NOT SUPPORTED IN THIS RELEASE.");

	return retval;
}

/* ......................................................................... */

int arc_mem_virt2phys(struct target *target, uint32_t address,
	uint32_t *physical)
{
	int retval = ERROR_OK;

	LOG_ERROR("arc_mem_virt2phys NOT SUPPORTED IN THIS RELEASE.");

	return retval;
}

int arc_mem_read_phys_memory(struct target *target, uint32_t phys_address,
	uint32_t size, uint32_t count, uint8_t *buffer)
{
	int retval = ERROR_OK;

	LOG_ERROR("arc_mem_read_phys_memory NOT SUPPORTED IN THIS RELEASE.");

	return retval;
}

int arc_mem_write_phys_memory(struct target *target, uint32_t phys_address,
	uint32_t size, uint32_t count, const uint8_t *buffer)
{
	int retval = ERROR_OK;

	LOG_ERROR("arc_mem_write_phys_memory NOT SUPPORTED IN THIS RELEASE.");

	return retval;
}

int arc_mem_mmu(struct target *target, int *enabled)
{
	int retval = ERROR_OK;

	/* (gdb) load command runs through here */

	LOG_DEBUG("arc_mem_mmu NOT SUPPORTED IN THIS RELEASE.");
	LOG_DEBUG("    arc_mem_mmu() = entry point for performance upgrade");

	return retval;
}
