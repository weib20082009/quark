#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "string.h"

#include "ff.h"
#include "serialprog.h"
#include "spider-sap.h"
//#define DBG_FILEUTILS
 #define  wP(...)       	    printf(__VA_ARGS__)
#ifndef  DBG_FILEUTILS
 #define  dP(...)       	    // debug
 #define  DP(...)       	    // full print
 #define  vP(...)       	    // full print
 #define  VP(...)       	    // full print
#else
 #define  dP(...)       printf(__VA_ARGS__)	    // debug
 #define  DP(...)       printf(__VA_ARGS__)	    // full print
 #define  vP(...)       printf(__VA_ARGS__)	    // full print
 #define  VP(...)       printf(__VA_ARGS__)	    // full print
#endif
#define SERINPUT_BIGENDIAN
/*
 * file_ext_replace():   replace file extension
 *      src: source filename
 *      dst: destination filename
 *      ext: file extension to be replaced.
 *      replace begin with '.'.
 */
int file_ext_replace(char *src, char *dst, uint8_t *ext)
{
    int replace=0;
    while(*src) {
        if (replace == 0) {
            if( *src == '.') 
                replace = 1;
            *dst++ = *src;
        } else {
            *dst++ = *ext++;
        }
        src ++;
    }
    *dst = 0;
    return 0;
}
int HEX2I(char ch) 
{
    if (ch < 0x30) return -1;
    if (ch < 0x3A) return (ch - 0x30);
    if (ch < 0x41) return -1;
    if (ch < 0x47) return (ch - 55);
    if (ch < 0x61) return -1;
    if (ch < 0x67) return (ch-87);
    return -1;
}
/*
 * parseser()
 *  input: filename, offset
 *  output: records
 *  // record length
 */

int serial_fill_structure(xSerFile *xsf)
{
    FRESULT res;
    FIL src;
    UINT szRead = 0;
    int i, j, k, x, y;
    uint32_t temp;
    uint8_t rtype[16];
    uint8_t rlen[16];
    uint32_t raddr[16];

    /* Open File */
    res = f_open(&src, (const TCHAR *)xsf->sername, FA_READ);
    if (res) {
        //wP("\nCan not Open SER file:%s, %s!", xsf->sername, MSGSTR[res]);
        return 0;
    }	

    /* read */
    i = 1040;   // signature + line
    res = f_read(&src, xsf->line, i, &szRead);
    if (res) {
        wP("\nRead %s file failed, %s", xsf->sername, MSGSTR[res]);
        return 2;
    }	
    res = f_close(&src);

    if (szRead == 0) {
        wP("\nNo data Read(%s)!", xsf->sername);
        return 2;
    }


    /* 
     * If serial file contains signature(need to be matched with SAP),
     * it will not start with ':', and should be a format with 0xXXXXXXXX
     */

    i = 0;
    xsf->signature = 0;
    while(xsf->line[i] != ':' && i<szRead) {
        if ((x = HEX2I(xsf->line[i])) >= 0) {
            xsf->signature = (xsf->signature << 4) | x;
        }
        i++;
    }

    j = 0;
    k = -1;
    y = 0;
    while ( i < szRead ) {
        if (xsf->line[i] == '\n' || xsf->line[i] == '\r') {
            i++;
            if (xsf->line[i] == '\n' || xsf->line[i] == '\r') 
                i++;
            k++;
            // end of line
            break;
        } else
        if (xsf->line[i] == ':') {    // start ':'
            k ++;            // serial number count
            rtype[k] = 0; rlen[k] = 0; raddr[k] = 0; i++;
            // get type
            if ((x = HEX2I(xsf->line[i++])) < 0) return 2;
            rtype[k] = x << 4;
            if ((x = HEX2I(xsf->line[i++])) < 0) return 2;
            rtype[k] |= x;
            // get length
            if ((x = HEX2I(xsf->line[i++])) < 0) return 2;
            rlen[k] = x << 4;
            if ((x = HEX2I(xsf->line[i++])) < 0) return 2;
            rlen[k] |= x;

            if (rlen[k] > 56) {
                wP("\nSerial[%d] Data more than 56 bytes[%02X]!.", k, rlen[k]);
                return 2;
            }
            if (rlen[k] % 4) {
                wP("\nSerial[%d] Data not 4 alignment[%02X]!.", k, rlen[k]);
                return 2;
            }
            wP("\nSerial[%d]:type:%02X, Len:%02X, %03d: ", k, rtype[k], rlen[k], i);
            y += rlen[k];
            // get address
            for(j = 0; j< 4; j++) {
                temp = 0;
                if ((x = HEX2I(xsf->line[i++])) < 0) return 2;
                temp |= x << 4;
                if ((x = HEX2I(xsf->line[i++])) < 0) return 2;
                temp |= x; 

#ifdef SERINPUT_BIGENDIAN
                raddr[k] = raddr[k] << 8 | temp;    // big endian
#else                
                raddr[k] |= temp << (j*8);
#endif             
            }
            wP("Addr[%08X]", raddr[k]);
            // skip data
            for (j=0;j<rlen[k];j++)
                i+=2;
            i+=2;   // skip checksum
        }
    }
    if (i>=1024) {
        wP("\nSerial Programming Line longer than 1024!");
        return 2;
    }

    xsf->linesize = i;
    xsf->cntSer = k;

    /* fill data structure */
    temp = (uint32_t)xsf->sbuff + k * sizeof(xSerial);
    y = 0;
    //xsf->inclog = 0;
    for(i=0;i<k;i++) {
        xSerial *xSer = (xSerial *) ((uint32_t)xsf->sbuff + i * sizeof(xSerial));
        xSer->state= 0;
        xSer->type = rtype[i];
        //if (rtype[i] != SER_TYPE_CRC32)
        //    xsf->inclog |= 1;
        xSer->size = rlen[i];
        if (xSer->size > 56) {
            wP("\nSerial Programming Record longer than 56!");
            return 2;
        }
        xSer->addr = raddr[i];
        xSer->offRec = y + 6;
        y += 6 + rlen[i] + 1;         // next record start
        for(j=0;j<PORT_COUNT;j++) {
            xSer->data[j] = (char *)temp;
            temp += rlen[i];
        }
    }

    return 1;
}
int serial_fill_data(xSerFile *xsf, char *line, int port)
{
    int i = 0, j = 0;
    uint8_t cksum = 0;
    int x, y;
    uint8_t *dstb, *srcb;

    if (line[i++] != ':') {             // 1st char should be ':'
        wP("\nSerial Start Error!");
        return SERP_NOTHEADER;
    }

    /* Convert Hex ASCII to Binary:
     *      remove ':', SOR (start of record).
     *      HEX2I:  Convert HEX ASCII to binary(byte).
     *          store the binary to original buffer.
     *      Calculate checksum: must be zero when meet SOR of EOL.
     *      Break when met linefeed or carriage.
     */

    while ( i < 1024) {
        if (line[i] == '\n' || line[i] == '\r') {
            if (cksum > 0) {
                wP("\nSerial Checksum Error!");
                return SERP_CHECKSUM;
            }
            i++;
            if (line[i] == '\n' || line[i] == '\r')
                i++;

            break;
        } else if (line[i] == ':') {    // start ':'
            if (cksum > 0) {
                wP("\nSerial Checksum Error!");
                return SERP_CHECKSUM;
            }
            i++;
        } else {
            if ((y = HEX2I(line[i++])) < 0) {
                wP("\nSerial Hex Error!");
                return SERP_NOTHEX;
            }

            if ((x = HEX2I(line[i++])) < 0) {
                wP("\nSerial Hex Error!");
                return SERP_NOTHEX;
            }

            y = (y << 4) | x;

            line[j++] = y;
            cksum += y;
        }
    }

    if (i != xsf->linesize) {
        wP("\nSerial Length Error!");
        return SERP_LENGTHERR;
    }

    /*
     * Fill data into serial structure of specified port
     */
    wP("\nPort[%d]", port);
    for (i=0;i<xsf->cntSer;i++) {
        xSerial *xSer = (xSerial *) (xsf->sbuff + i * sizeof(xSerial));
        dstb = (uint8_t *) xSer->data[port];
        srcb = (uint8_t *) (line + xSer->offRec);
        wP("\n\tFill data: Serial[%d], type=%d, len=%d, addr=%08X, data=>", i, xSer->type, xSer->size, xSer->addr);
        for (j=0;j<(xSer->size>>2);j++) {
            wP("%02X%02X%02X%02X ", srcb[4*j+0],srcb[4*j+1],srcb[4*j+2],srcb[4*j+3]);
#ifdef SERINPUT_BIGENDIAN
            dstb[4*j+0] = srcb[4*j+0];
            dstb[4*j+1] = srcb[4*j+1];
            dstb[4*j+2] = srcb[4*j+2];
            dstb[4*j+3] = srcb[4*j+3];
#else            
            dstb[4*j+0] = srcb[4*j+3];
            dstb[4*j+1] = srcb[4*j+2];
            dstb[4*j+2] = srcb[4*j+1];
            dstb[4*j+3] = srcb[4*j+0];
#endif            
            //*dstb ++ = *srcb ++;
        }
    }
    return SERP_OK;
}
/* 
 * display_serials()
 *      Display serial contents for debug only.
 *
 */
int display_serials(xSerFile *xsf, uint8_t part)
{
    int i, j, k;
    xSerial *xser;

    wP("\nLine Size:%04d, Serial Count=%d ", xsf->linesize, xsf->cntSer);
    for (i=0;i<PORT_COUNT;i++) {
        wP("\n\tline:%04d ", xsf->lines[i]);
    }

    for (i=0;i<xsf->cntSer;i++) {
        xser = (xSerial *)(xsf->sbuff + i * sizeof(xSerial));
        wP("\ntype:%02X, Len:%02X, Addr:%08X, offRec:%02X ", xser->type, xser->size, xser->addr, xser->offRec);
        if (part == 0)
            continue;
        for(j=0;j<PORT_COUNT;j++) {
            wP("\n\tPort[%d], data:", j);
            for(k=0;k<xser->size;k++) {
                wP("%02X ", xser->data[j][k]);
            }
        }
    }
    return 0;

}
int serial_readlines(xSerFile *xsf, int port)
{
    int res;
    FIL src;
    uint32_t szRead;
    int i;
    int port_feeded = 0;

    /* Open File */
    res = f_open(&src, (const TCHAR *)xsf->sername, FA_READ);
    if (res) {
        wP("\nOpen %s file failed!", xsf->sername);
        return -1;
    }	

    /* Seek */
    if (xsf->offLine > 0) {
        res = f_lseek(&src, (xsf->offLine - 1) * xsf->linesize);
        if (res) {
            wP("\nSeek %s file failed, %s.", xsf->sername, MSGSTR[res]);
            return -1;
        }	
    }

    for (i=0;i<PORT_COUNT;i++) {
        if (port & (1<<i)) {
            if (xsf->lines[i] > 0) {
                wP("\nLine (%d) not logged!", xsf->lines[i]);
                continue;
            }
            /* read */
            res = f_read(&src, xsf->line, xsf->linesize, &szRead);
            if (res) {
                wP("\nRead %s file failed, %s!", xsf->sername, MSGSTR[res]);
                break;
            }	
            if (szRead < xsf->linesize) {
                wP("\nRead %s file failed, %s!", xsf->sername, MSGSTR[res]);
                break;
            }
            res = serial_fill_data(xsf, (char *)xsf->line, i);
            if (res < 0) {
                wP("\nFile Structure failed(%d)!", xsf->offLine);
                break;
            }
            xsf->lines[i] = xsf->offLine;

            xsf->state[i] = 0;
            xsf->offLine ++;
            port_feeded |= (1<<i);
        }
    }
    res = f_close(&src);
    return port_feeded;
}
/* ---------------------------------------------------------------------------*/
int fileAppend(const char *fname, uint8_t *buf, uint32_t sz)
{
    int res;
    uint32_t szW = 0;
    FIL fp;

    /* Open File */
    if ((res = f_open(&fp, (const TCHAR *)fname, FA_OPEN_ALWAYS|FA_WRITE)) !=0 ) {
        wP("\nCan not Open RES file:%s, %s!", fname, MSGSTR[res]);
        return -1;
    }

    /* Seek */
    if ((res = f_lseek(&fp, f_size(&fp))) != 0) {
        wP("\nSeek EOF failed, %s!", MSGSTR[res]);
        f_close(&fp);
        return -1;
    }	

    /* write */
    if ((res = f_write(&fp, buf, sz, &szW)) != 0) {
        wP("Write RES failed, %s\n", MSGSTR[res]);
        f_close(&fp);
        return -1;
    }

    res = f_close(&fp);
    return (int)szW;
}
/* ---------------------------------------------------------------------------*/
/*
 * serial_offset_cfg()
 *      action == 0:   get offset from *.SER file
 *      action > 0:    append offset to *.SER file
 *      return 0: OK
 *      others: Error
 *      format: [+|-]:%08X\n\r
 */
int serial_log(xSerFile *xsf, uint32_t action)
{
    char buf[16];
    int res;
    FIL fp;
    size_t off;
    UINT szRW = 0;
    int x, i;

    if (action) {  // append
        res = f_open(&fp, (const TCHAR *)xsf->resname, FA_OPEN_ALWAYS|FA_WRITE);
        /* Open File */
        if (res) {
            wP("\nCan not Open RES file:%s, %s!", xsf->resname, MSGSTR[res]);
            return 1;
        }	
        /* Seek */
        res = f_lseek(&fp, f_size(&fp));
        if (res) {
            wP("\nSeek EOF failed, %s!", MSGSTR[res]);
            f_close(&fp);
            return 1;
        }	
        /* write */
        for (i=0;i<PORT_COUNT;i++) {
            if (xsf->lines[i] > 0) {
                if (xsf->state[i] == 0x88) 
                    sprintf(buf, "+:%08X\r\n", xsf->lines[i]);
                else
                    sprintf(buf, "-:%08X\r\n", xsf->lines[i]);

                res = f_write(&fp, buf, 12, &szRW);
                if (res || szRW < 12) {
                    wP("Write RES failed, %s\n", MSGSTR[res]);
                    f_close(&fp);
                    return 1;
                }	
                xsf->lines[i] = 0;
                xsf->state[i] = 0;
            }
        }	
        res = f_close(&fp);
    } else {

        xsf->offLine = 1;
        /* Open File */
        res = f_open(&fp, (const TCHAR *)xsf->resname, FA_READ);
        if (res) {  // file not existed
            wP("\nRES: Open %s file failed, %s!", xsf->resname, MSGSTR[res]);
            return 0;
        }	

        /* get offset for seek */
        off = f_size(&fp);
        if (off % 12) {
            wP("\nRES: %s file format error!", xsf->resname);
            res = f_close(&fp);
            return 1;
        }
        wP("\nRES: %s file size=%d", xsf->resname, off);

        if (off == 0) { // empty file
            res = f_close(&fp);
            return 0;
        }

        off -= 12;

        /* Seek */
        res = f_lseek(&fp, off);
        if (res) {
            res = f_close(&fp);
            wP("\nSeek %s file failed, %s!", xsf->resname, MSGSTR[res]);
            return 1;
        }	
        /* read */
        res = f_read(&fp, buf, 12, &szRW);
        if (res) {
            res = f_close(&fp);
            wP("\nRead %s file failed, %s!", xsf->resname, MSGSTR[res]);
            return 1;
        }	

        res = f_close(&fp);
        if (szRW < 12) {
            wP("\nRead %s: line too short!", xsf->resname);
            return 1;
        }

        i = szRW - 3;
        off = 0;
        // wP("\nRES: %s:>>:", buf);
        i = 2;
        while (i < (szRW - 2)) {
            //wP("%c%c", buf[i], buf[i+1]);
            off <<= 8;
            if ((x = HEX2I(buf[i++])) < 0) {
                wP("\nRead %s File Format Error!", xsf->resname);
                return 1;
            }

            off |= x << 4;
            if ((x = HEX2I(buf[i++])) < 0) {
                wP("\nRead %s File Format Error!", xsf->resname);
                return 1;
            }
            off |= x;
        }
        xsf->offLine = off+1;
        wP("\nRES:Read::%s offset = %d", xsf->resname, off);
    }
    return 0;
}
/*
 * SerFileInit()
 *      Initialize serial file
 */
//xSerFile *SerFileInit(char *sapfname)
int SerFileInit(xSerFile *xsf, char *sapfname)
{
    int rc;
#if 0
    xsf = (xSerFile *) malloc(sizeof(xSerFile));
    if (xsf == NULL) {
        wP("\nMemory Allocation Failed!");
        //return NULL;
        return -1;
    }
#endif

    xsf->sapname = sapfname;
    file_ext_replace(sapfname, xsf->sername, "SER");

    // serial file existed?
    rc = serial_fill_structure(xsf);
    if (rc != 1) {
        return rc;
        //return NULL;
    }
#if 0   // debug only   
    serial_readlines(xsf, 0xF);
    display_serials(xsf, 1);
#endif    

    file_ext_replace(sapfname, xsf->resname, "RES");
    rc += serial_log(xsf, 0);
    return rc;
}
