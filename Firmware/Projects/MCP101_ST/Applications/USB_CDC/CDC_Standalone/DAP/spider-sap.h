#ifndef __SPIDER_SAP_H__
#define __SPIDER_SAP_H__
#include <stdint.h>
#define PORT_COUNT		    ( 1 )
#define PORTMASK           ( PORT_COUNT - 1 )
#define FATNAMELEN          16
#define SHORT_BLOCK_SIZE	56
#define LONG_BLOCK_SIZE		1024
#define SHORT_SPI_SIZE		68
#define LONG_SPI_SIZE		(LONG_BLOCK_SIZE +4 + 5)
#define DAP_PACKET_COUNT	( 64 )
#define MAX_SECTOR_COUNT    8
#define DAP_BUFF_SIZE       4096
#define FW_FILE_HEADER_SIZE 12
#define MAX_SAP_SECTION     16 

#define GREENLED            1
#define REDLED              0
#define LEDON               1
#define LEDOFF              0

#define PWR_0V              0
#define PWR_1V8             1
#define PWR_2V5             2
#define PWR_3V3             4
#define PWR_5V0             8


#define SWD(x,y,z)			(x|y|z)
#define FILEALIGN           (0xF & 0xFFFFFFFF)

#define FWMICE              (*((volatile unsigned long *) (0x08000400)))
#define SWJ_CLOCK_SPEED     20000000
//#define SWJ_CLOCK_SPEED     1000000
#define SWJ_SWD             0x01
#define SWJ_JTAG            0x02
#define SWD_BOUNDARY        0x400

#define FW_GET_CURRENT      0x00
#define FW_GET_NEXT         0x01
#define FW_GET_FILE         0x02
#define FW_M4_APP           0x03
#define FW_M4_BOOT          0x04
#define FW_M0_APP           0x05
#define FW_M0_BOOT          0x06
#define FW_GET_PREV         0xFF

#define CTRL_NOACK          0x00000001
#define CTRL_NOQUERY        0x00000002
#define CTRL_STOPQUERY      0x00000004

#define SWD_BLOCK           0x06


#define MEM_AP				0x00
#define MDM_AP				0x01
#define MDM_AP_REG_STS      0x00
#define MDM_AP_REG_CTRL     0x04
#define MDM_AP_REG_IDR      0xFC
#define MDM_AP_IDR          0x001C0000

#define KINETIS             0x01
#define STM32               0x02
#define NVC									0x05
#define ATMEL               0x07
#define XMC                 0x09
#define TOSHIBA             0x0B
#define NODRIC              0x0E






#define KINETIS_SECURED     0x04
 // STM32F4 Flash interface
/*
 *  STM32F1  FLASH_OBR(0x4002201C), F100xx, F10xxx, F100xxx XL
 *      RDPRT       (bit1 == 1, read protected)
 *
 *  STM32F0 FLASH_OBR(ox4002201C)
 *  STM32F3 FLASH_OBR(0x4002201C), F37xx, F38xx
 *      LEVEV1_PROT (bit 1)
 *      LEVEL2_PROT, not accessable by debugger
 *
 *  STM32F4 FLASH_OPTCR(0x40023C14), F205/215, F207/217, F405/F15, F407/417
 *      RDP (bit 8:16)
 *          0xAA: Level 0
 *          0xCC: Level 2, not accessable by debugger
 *          Others: Level 1
 *
 *  STM32F4     CPUID = 0x410fc241
 *  STM32F051   CPUID = 0x410cc200
 *
 *
 *
 *  MCU IDCODE E0042000, 40015800(STM32F051)
 *
 *  STM32F051:  0x440
 *  STM32F1  :  0x410, 0x414, 0x430, 0x418
 *  STM32F3  :  0x432
 *
 *  STM32F2  :  0x411
 *  STM32F4  :  0x413, 0x419
 *
 */
#define FLASH_OPTCR         0x40023C14
#define FLASH_OBR           0x4002201C
                                            
#define Cortex_M0           0x0BB11477      // F051                                        
#define SWDID_M3r2p0        0x2BA01477      // F2                                     
#define SWDID_M4F           0x1BA01477      // F3, M3r1p1, F1                              
#define SWDID_M4Fr0p1       0x2BA01477      // F4                                      

#define SWD_DP				0x00
#define SWD_AP				0x01
#define SWD_READ			0x02 
#define SWD_WRITE			0x00

#define SWD_STS_OK		    0x0
#define SWD_ACK_OK			0x1
#define SWD_ACK_WAIT		0x2
#define SWD_ACK_FAULT		0x4
#define SWD_ACK_FATAL       0x7
//
#define SD_INITED           0x02
#define SD_CARD_CHANGED     0x80

#define SWD_DPAP_MASK		0x01
#define SWD_RW_MASK			0x02
#define SWD_RD_MASK			0x10
#define SWD_WR_MASK			0x20

#define SWD_TIMEOUT         0x1000
#define SWD_NOBUFFER		0x4000
#define SWD_FLUSH			0x8000
#define SWD_DP_READOK       0x40
/* Fields of the MEM-AP's CSW register */
#define CSW_8BIT		0
#define CSW_16BIT		1
#define CSW_32BIT		2
#define CSW_ADDRINC_MASK	(3 << 4)
#define CSW_ADDRINC_OFF		0
#define CSW_ADDRINC_SINGLE	(1 << 4)
#define CSW_ADDRINC_PACKED	(2 << 4)
#define CSW_DEVICE_EN		(1 << 6)
#define CSW_TRIN_PROG		(1 << 7)
#define CSW_SPIDEN			0x00400000			//(1 << 23)
/* 30:24 - implementation-defined! */
#define CSW_HPROT			0x01000000			// (1 << 25)		/* ? */
#define CSW_MASTER_DEBUG	0x10000000			//(1 << 29)		/* ? */
#define CSW_DBGSWENABLE		0x80000000			//(1 << 31)

#define swd_flush(x)		(x|SWD_FLUSH)
#define swd_buffer(x)		(x & ~SWD_NOBUFFER)
#define swd_response(x, y)	(((xDAPBuffer *)x)->buffi + ((xDAPPacket *)y)->offset ) 
#ifdef __SPIDER__
#define spi_preamble() 	if(SPIP){dap->bufo[dap->cmdoff++] = '!'; \
													dap->bufo[dap->cmdoff++] = 'S';\
													dap->bufo[dap->cmdoff++] = 'P';\
													dap->bufo[dap->cmdoff++] = 'S';\
												}
#else
#define spi_preamble()  {}
#endif

#define INITSEQ(x) 	        dap->outseq = x; \
                            dap->inseq[0] = x; \
														if(PORT_COUNT>1) \
                            dap->inseq[1] = x; \
														if(PORT_COUNT>2) \
                            dap->inseq[2] = x; \
														if(PORT_COUNT>3) \
                            dap->inseq[3] = x;

#define SYNCSEQ() 	        dap->inseq[0] = dap->outseq; \
														if(PORT_COUNT>1) \
                            dap->inseq[1] = dap->outseq; \
														if(PORT_COUNT>2) \
                            dap->inseq[2] = dap->outseq; \
														if(PORT_COUNT>3) \
                            dap->inseq[3] = dap->outseq; 


#define nextstate(x)        x->substate ++; \
                            x->rport = 0; \
                            x->vport = 0;

#define PORTSSYNC(dap)  (dap->rport == 1)//dap->xport) 
#define PORTSEQEQ(x)    (1) ////?????  (dap->exp == dap->inseq[P2I(x)])

#define PORTSASYNC(dap)  ((dap->rport != 1/*dap->xport*/) && ((dap->rport|dap->vport) ==1 /*dap->xport*/))
#define PORTSINGLE(x)   ((x & (x-1)) == 0)
#define P2I(x)          ((x < 3)?(x-1):((x<8)?2:3))
#define P2S(x)          ((x < 3)?(x):((x<8)?3:4))
/*
#define SUBRESPONSE(x) (dap->substate && (((xDAPPacket *)x)->port != SWD_TIMEOUT))?(((xDAPBuffer *)xBuff)->buffi + ((xDAPPacket *)x)->offset):respNULL
#define FUNRESPONSE(x) (dap->funstate && (((xDAPPacket *)x)->port != SWD_TIMEOUT))?(((xDAPBuffer *)xBuff)->buffi + ((xDAPPacket *)x)->offset):respNULL
*/
#define SUBRESPONSE(x) ((xDAPBuffer *)xBuff)->buffi
#define FUNRESPONSE(x) ((xDAPBuffer *)xBuff)->buffi

#define FUNJUMP(x, y)       x->funstate = y ; \
                            x->rport = 0; \
                            x->vport = 0;
#define JUMPSUB(x)          dap->substate = x ; \
                            dap->rport = 0; \
                            dap->vport = 0;
#define JUMPFUN(x)          dap->funstate = x ; \
                            dap->rport = 0; \
                            dap->vport = 0;

//#define  PORTREADY()       ~((SPIx_RDY_GPIO_PORT->IDR) >> SPIx_RDY_SHIFT) & SPIx_RDY_MASK

/* A[3:0] for DP registers; A[1:0] are always zero.
 * - JTAG accesses all of these via JTAG_DP_DPACC, except for
 *   IDCODE (JTAG_DP_IDCODE) and ABORT (JTAG_DP_ABORT).
 * - SWD accesses these directly, sometimes needing SELECT.CTRLSEL
 */
 #if 0
#define DP_IDCODE		0		/* SWD: read */
#define DP_ABORT		0		/* SWD: write */
#define DP_CTRL_STAT		0x4		/* r/w */
#define DP_WCR			0x04		/* SWD: r/w (mux CTRLSEL) */
#define DP_RESEND		0x08		/* SWD: read */
#define DP_SELECT		0x08		/* JTAG: r/w; SWD: write */
#define DP_RDBUFF		0x0C		/* read-only */
#endif
  /* MEM-AP register addresses */
#define MEM_AP_REG_CSW		0x00
#define MEM_AP_REG_TAR		0x04
#define MEM_AP_REG_DRW		0x0C
#define MEM_AP_REG_BD0		0x10
#define MEM_AP_REG_BD1		0x14
#define MEM_AP_REG_BD2		0x18
#define MEM_AP_REG_BD3		0x1C
#define MEM_AP_REG_CFG		0xF4		/* big endian? */
#define MEM_AP_REG_BASE		0xF8
#define MEM_AP_REG_IDR		0xF

/* Debug Control Block */
#define DCB_DHCSR	0xE000EDF0
#define DCB_DCRSR	0xE000EDF4
#define DCB_DCRDR	0xE000EDF8
#define DCB_DEMCR	0xE000EDFC

#define SCB_CPUID   0xE000ED00
#define SCB_AIRCR	0xE000ED0C
#define MCU_IDCODE  0xE0042000
#define MCU_IDCODE_M0  0x40015800

#define DCRSR_WnR	(1 << 16)
/* DCB_DHCSR bit and field definitions */
#define DBGKEY		0xA05F0000
#define C_DEBUGEN	(1 << 0)
#define C_HALT		(1 << 1)
#define C_STEP		(1 << 2)
#define C_MASKINTS	(1 << 3)
#define S_REGRDY	(1 << 16)
#define S_HALT		(1 << 17)
#define S_SLEEP		(1 << 18)
#define S_LOCKUP	(1 << 19)
#define S_RETIRE_ST	(1 << 24)
#define S_RESET_ST	(1 << 25)

/* RESET */
#define RST_HW      1
#define RST_SYS     2
#define RST_VECT    4
#define RST_DUMB    8
#define RST_AUTO    0

#define C_NORM      (1 << 4)
#define C_PRE_RST   (1 << 5)
#define C_UNDER_RST (1 << 6)

#define RST_C_RST   (1 << 7)

/* SAP record tag */
#define	FT_MAGIC			0x16896168
#define	FT_FLASH_TYPE		0x00
#define	FT_ERASE_PATTERN	0x01
#define	FT_RAM_TYPE			0x02
#define	FT_RESET_TT         0x03
#define	FT_VOLTAGE          0x04
#define	FT_LOGGING          0x05
#define	FT_SWJ              0x06

#define	FT_FUN_CRC32		0x40
#define	FT_FUN_INIT			0x41
#define	FT_FUN_UNINIT		0x42
#define	FT_FUN_ERASECHIP	0x43
#define	FT_FUN_BLANKCHECK	0x44
#define	FT_FUN_ERASESECTOR	0x45
#define	FT_FUN_PROGPAGE		0x46
#define	FT_DRIVER_VER		0x47
#define	FT_PROG_SIZE		0x48
#define	FT_PP_TIMEOUT		0x49	// Time Out of Program Page Function
#define	FT_ES_TIMEOUT		0x4A	// Time Out of Erase Sector Function
#define	FT_FUN_VERIFY		0x4B
#define	FT_SAP_VERSION		0x4C

/*weib add 2015-0602*/
#define	FT_FUN_EXTRA	0xD7//extra operation
#define	FT_SEQUENCE		0xD8//operation  SEQUENCE
#define	FT_PROG_OFFSET		0xD9//data offset

/*weib add 2015-0907*/
#define	FT_AT_ERASEPIN		0xDA//data offset
/*weib add 2015-0710*/
#define	FT_NRF_EXTRAFUN		0xDB//data offset


/*weib add end*/

#define	FT_FLASH_SIZE		0x80
#define	FT_CODE_OFFSET		0x81
#define	FT_CODE_SIZE		0x82
#define	FT_DATA_OFFSET		0x83
#define	FT_DATA_SIZE		0x84
#define	FT_RAM_SIZE			0x85
#define	FT_SECTOR_SIZE		0x86
#define	FT_TARGET_SECTION	0x87
#define	FT_RAM_SECTION		0x88
#define	FT_FLASH_SECTION	0x89
#define	FT_CODE_SECTION		0x8A
#define	FT_DATA_SECTION		0x8B
#define	FT_SCRIPT_SECTION	0x8C
#define	FT_PRGCODE_SIZE     0x8D
#define	FT_PRGDATA_SIZE     0x8E


#define	FT_RAM_START_ADDR	0xC0
#define	FT_STACK_START_ADDR	0xC1
#define	FT_FLASH_START_ADDR	0xC2
#define	FT_SECTOR_START_ADDR	0xC3
#define	FT_CODE_CRC32		0xC4
#define	FT_DATA_CRC32		0xC5
#define	FT_ID0				0xC6
#define	FT_ID1				0xC7
#define	FT_ID2				0xC8
#define	FT_ID3				0xC9
#define	FT_ID4				0xCA
#define	FT_ID5				0xCB
#define	FT_ID6				0xCC
#define	FT_ID7				0xCD
#define	FT_FUNTODOS		    0xCE
#define	FT_DAPID		    0xCF
#define	FT_CPUID		    0xD0
#define	FT_VENDOR		    0xD1
#define	FT_DAPSPEED		    0xD2
#define	FT_CMDAV 		    0xD3
#define	FT_BUFFER_ADDR      0xD4
#define	FT_SERIAL_SIG       0xD5

#define	FT_NRF_LOCK       0xD6//weib add for NRF51 chip LOCK 2015-06-17

#define	FT_END_SYMBOL		0xFF
#define	FT_MAX_SECTORS		512

#define	XAP_LOAD_SIZE		0x3000
#define SAP_ENCRYPTED       0x8000
/* functions to do */
#define DO_CONNECT		0x00
#define DO_ERASESECTOR		0x01
#define DO_ERASECHIP		0x02
#define DO_PROGRAM			0x04
#define DO_BLANKCHECK		0x08
#define DO_CRC32			0x10
#define DO_RESETGO			0x20
#define DO_KINETIS_ME     0x8000
#define DO_LOADCODE			(DO_ERASESECTOR | DO_ERASECHIP | DO_PROGRAM | DO_BLANKCHECK| DO_CRC32)

/* program page interleaving control */
#define PAGENUMBER			(0xFFFF)
#define PAGELOADED			(1<<16)
#define PROGRAMMING			(1<<17)
/* dapstate / command preq */
#define PREQ_SAPSELECTED	(1<<0)      // 1
#define PREQ_SAPLOADED		(1<<1)      // 0x03
#define PREQ_PWR_READY      (1<<2)      // 0x07
#define PREQ_CONNECTED		(1<<3)      // 0x0f
#define PREQ_INITED			(1<<4)      // 0x1F
#define PREQ_HALTED			(1<<5)      // 0x3F
#define PREQ_CODELOADED		(1<<6)      // 0x7F

#define DAP_INTF_USB        0x01
#define DAP_INTF_I2C        0x02
#define DAP_INTF_ETH        0x04
#define DAP_INTF_UART       0x08

/* OPTION */
#define OPT_A2C             0x0200    // put addr[0] to code
#define OPT_MADR            0x0400
#define OPT_CODE            0x80
#define OPT_PORT            0x40
#define OPT_ADDR            0x20
#define OPT_MSG             0x10
#define OPT_NF              0x08    // COMMAND not finished, prevent from accept command
#define OPT_ERR             0x04
#define OPT_ESTI            0x02    // estimate needed time(total 255)
#define OPT_TIME            0x01
#define OPT_RETS            0x01

#define OPT_NONE            0x00


#define NOCODE              NULL
#define NOPORT              NULL
#define NOADDR              NULL

#define CONFIG_FILE         "config"
#define SAPEXT              ".SAP"
#define BINEXT              ".BIN"
/* Error Codes */
static const uint8_t MSGCODS[] = {
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
    0x20, 0x21, 0x22, 
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 
    0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
    0x50,                   // Blankcheck fail
    0x58, 0x60,             // Erase chip fail, erase sector fail
    0x70, 0x71,             // Program page fail, program serial fail
    0x78,                   // Verify
    0x80, 0x81, 0x82
};
/* Error Msg */
static char* MSGSTR[] /*__attribute__( ( section( "CCM")) )*/ = {
    // File/SD Card related Errors 0x00
    "File Read OK.",
    "Disk Error",
    "Internal ERROR",
    "SD Card Not Ready",
    "No File",
    "No Path",
    "Invalid Name",
    "Denied",
    "Existed",
    "Invalid Object",
    "Write Protected",
    "Invalid Drive",
    "Not Enabled",
    "No File System",
    "FatFS mkfs Aborted",
    "SD Card Timeout",
    "File Locked",
    "LFN Not Enough Memory",
    "Too Many Opened File",
    "Invalid Parameter",
    "Empty File",          // FR_FILE_EMPTY=0x14
    "No More File",
    "Not SAP File or Wrong SAP File",
    "File Read Failed", 
    "File Seek Failed",
    "SD Card Removed", 

    "No Power",       // 0x21
    "Power Short",       // 0x21
    "Over Current",     // 0x22

    // Target Connect Error // 0x30~
    "Invalid Target Response",
    "No Target Response",
    "Target Different",
    "Target Debugger PowerUp Failed",
    "Target Halt Failed",
    "HW Reset Failed",
    "SW Reset Failed",
    "SPI CRC Error",
    "SPI Protocol Error",
    "Transfer Stalled Error", 
    "Target Device ID Mismatch",
    "Target CPUID Mismatch",
    "Target DAPID Mismatch",
    "Target Secured",   // 0x3D

    // Algorithm operation error,  0x40 ~
    "Operation Timeout",
    "Algorithm Init Failed",
    "Algorithm UnInit Failed",
    "Algorithm Erase Chip Not Supported",
    "Error",
    "Invalid Response",
    "Algorithm Running Failed",
    "Serial Format Error",  // 0x47
    "Serial Used Up",       // 0x48
    "Signature Not Match",       // 0x49
    //
    "Blank Check Failed",
    "Erase Chip Failed",
    "Erase Sector Failed", 
    "Programming Failed",
    "Programming Serial Failed",
    "CRC32 Verify Failed",
    "Command Failed",
    "Command Not Supported", 
    "Command Not Finished", 
    // None Error Msgs
    "CONNECTING TARGETS.",
    "Target Halted.",
    "BLANK CHECKING.",
    "Blank Check Passed.",
    "ERASING CHIP.",
    "Erase Chip Passed.",
    "ERASING SECTOR.",
    "Erase Sector Passed.",
    "PROGRAMMING PAGE.",
    "Programming Passed.",
    "PROGRAMMING SERIAL.",
    "Programming Serial Passed.",
    "CRC32 VERIFYING.",
    "CRC32 = ",
    "RESET TARGET and RUN.",
    "Target Reset & Running.",
    "Command Finished.", 
    "Command Aborted.",
    "SAP Loaded."
};
typedef enum {
	FR_FILE_EMPTY = 0x14,				/* (0) Succeeded */
    FR_NO_MORE_FILE,
    FR_NOT_SAP_FILE,
    FR_READ_FAIL,
    FR_SEEK_FAIL,
    FR_CARD_REMOVED,

    TE_NO_POWER,
    TE_PWR_SHORT,
    TE_PWR_OC,


    TE_INVALID_ACK,
    TE_NO_ACK,
    TE_TARGET_DIFF,
    TE_DBGPWR_FAIL,
    TE_HALT_FAIL,
    TE_HWRST_FAIL,
    TE_SWRST_FAIL,
    TE_SPICRC_ERR,
    TE_SPIPRO_ERR,
    TE_TX_STALL,
    TE_TARGET_ID_MISMATCH,
    TE_TARGET_CPUID_MISMATCH,
    TE_TARGET_DAPID_MISMATCH,
    TE_TARGET_SECURED,          // 0x3D

    ALG_TIMEOUT,
    ALG_INIT_FAIL,
    ALG_UNINIT_FAIL,
    ALG_EC_NOT_SUPPORT,
    TE_UNKNOWN,
    TE_UNKNOWN_ERR,
    ALG_RUN_FAIL,
    SERP_FORMATERR,             // 0x47
    SERP_USEUP,             // 0x48
    SERP_SIG_ERR,

    ALG_BC_FAIL,
    ALG_EC_FAIL,
    ALG_ES_FAIL,
    ALG_PP_FAIL,
    ALG_PS_FAIL,
    ALG_CRC_FAIL,

    ALG_CMD_FAIL,
    ALG_CMD_NOT_SUPPORT,
    ALG_CMD_NOT_FINISH,
    // Non-Errors
    ALG_CONN_START,
    ALG_CONN_HALT,
    ALG_BC_START,
    ALG_BC_PASS,
    ALG_EC_START,
    ALG_EC_PASS,
    ALG_ES_START,
    ALG_ES_PASS,
    ALG_PP_START,
    ALG_PP_PASS,
    ALG_PS_START,
    ALG_PS_PASS,
    ALG_CRC_START,
    ALG_CRC_PASS,
    ALG_RR_START,
    ALG_RR_RUNNING,
    ALG_CMD_FINISH,
    ALG_CMD_ABORT,
    FR_SAP_LOADED
} ERRCODES;
/* Output Level */
typedef enum {
    OL_CRITICAL = 0,    
    OL_ERROR = 0x01,
    OL_WARNING= 0x02,
    OL_INFO= 0x04,
    OL_DEBUG= 0x08,
    OL_DEBUGV= 0x10,
    OL_DEBUGVV= 0x20,
    OL_DEBUGVVV= 0x40,
    OL_FULL= 0x80,
}OLCODES;

/* Error codes */
typedef enum {
/*	ERROR_OK = 0, */
//	DAP_GONEXT = 0x00,
	DAP_OK = 0,
	DAP_GOON = 1,
	DAP_SAP_SELECTED = 0x80,
	DAP_CMD_NOT_SUPPORT= 0xA0,
	DAP_GONEXT = 0xF0,
//	DAP_NEXT_OP =0xF0,
	DAP_NEXTSAP = 0xF1,
	DAP_AGAIN	= 0xF2,//weib add for unstable error
	DAP_CONTINUE	= 0xF3,//weib add for unstable error
	DAP_ERROR = 0xFF,
	ERROR_NOT_SAP_FILE = -3,
}DAP_Status;

typedef enum {
    SM_DONE=0,
    SM_DAPINFO,
    SM_BOOT,
    SM_TXT,
	SM_SWD=0x10,
	SM_START,
	SM_CONNECT,
	SM_DAPRESET,
	SM_RESET_HALT,
	SM_LOAD_CODE,
	SM_SCANROM,
    SM_DISCONNECT,
	SM_READMEM,
    SM_HALT,
    SM_RUNUNINIT,
    
    SM_ERASESECTOR=0x21,
	SM_ERASECHIP=0x22,
    
	SM_PROGPAGE=0x23,
	SM_CRC32=0x24,
	SM_RESET_GO=0x25,
    SM_AUTO = 0x27,
    SM_BLANKCHECK=0x28,
	SM_PROGSERIAL=0x29,
    SM_RESUME,
    
    SM_DIAGNOSTIC=0x30,
    SM_TESTPINS=0x31,
	SM_DUMP=0x40,
	SM_ACTION=0x41,
	SM_NONSWD=0x80,
    SM_QUERY=0x81,
    SM_CONFIG=0x82,
    SM_SAPNAV=0x83,
    SM_BTLOADER=0x85,
    SM_FIRMWARE=0x86,
    SM_TXFILE=0x87,
    SM_IDLE = 0xA5,
    SM_TIME=0xF0,
    SM_ABORT=0xF1,
    SM_BOOTERR=0xFF,
}SAP_CMD_T;
#if 1
typedef uint8_t U8;
typedef uint16_t U16;
typedef uint32_t  U32;
#else
typedef unsigned portCHAR U8;
typedef unsigned portSHORT U16;
//typedef unsigned portLONG  U32;
typedef uint32_t  U32;
#endif

typedef struct CMD_PACKET
{
	uint8_t len;
    uint8_t cmd;
    uint8_t opt;
    uint8_t data[61];
} xCMDPacket;
typedef struct DAP_CFGFILE
{
    uint8_t autorunauto;
    uint8_t mode;
    uint8_t sapfile[FATNAMELEN];
    uint8_t music;
} xDAPCfg;

typedef struct MCP_CFG 
{
    uint32_t ip;
    uint32_t network;
    uint32_t netmask;
    uint32_t gw;
    uint8_t ethernet;   // 0: disable, 1:static, 2: dhcp
} xMCPCFG;

typedef struct FLPARAM
{
    uint8_t block;      // FF: unused, 00: obsolete 
    uint8_t id;         // parameter id
    uint8_t len;        // length
    uint8_t cksum;      // checksum, not used
    uint8_t data[];     // data
} xFlashParam;

typedef struct DAP_PACKET
{
	U16 port;
	U16 offset;
	U16 length;
} xDAPPacket;

typedef struct DAP_COMMAND
{
	uint8_t op;
	int		(*func)(xDAPPacket *xPacket);	//, xCMDPacket *cpkt);
	uint8_t argc;
	uint8_t	preq;
	uint8_t *name_hlp;
} xDAPCmd;

typedef struct DAP_BUFFER
{
	U16 inoff, outoff;
	U8 buffi[512]; 
//	U8 buffo[5*2]; 
//	U8 buffi[ DAP_BUFF_SIZE ];   //SHORT_SPI_SIZE * DAP_PACKET_COUNT ];
	U8 *buffo; //[ DAP_BUFF_SIZE * 2];   //SHORT_SPI_SIZE * DAP_PACKET_COUNT ];
} xDAPBuffer;

typedef struct FlashSectors {
	U32	   szSector;			// Sector Size in Bytes
	U32	startSector;			// Address of Sector
} xFlashSectors;

typedef struct blankcheckfun {
	U16		i,j;
	U32		adr;
	U8 		*buf;
} xFun;

typedef struct FileSeg {
	uint32_t	offFile;				// offset to file start
	uint32_t    szFile;				    // size of readed data in cache
	uint32_t    szMax;				    // size of the buffer
    uint8_t     filename[FATNAMELEN];
	uint8_t		*buff;	
} xFileSeg;
typedef struct SAPSeq
{
	uint32_t SeqAddress;
	uint8_t reserved;
	uint8_t IsNeedConvert;
	uint8_t RW;
	uint8_t SeqLength;
	uint32_t SeqVal;
}xSAPSeq;
typedef struct SapFile {
/*  SAP File Segment Cache, this cache avoid read file segment again and again */
//	uint8_t	cache[XAP_LOAD_SIZE];
    uint8_t	*cache;
    uint8_t sapfname[FATNAMELEN];   // SAP file name
    uint8_t cachefname[FATNAMELEN];   // cache file name
    uint8_t logname[FATNAMELEN];   // cache file name
    uint32_t cfgregs[32];
    U32     dapid;
    U32     cpuid;
		U32			devver;
    U32     dapspeed;
    U32     vendor; 
    U32     offSerial;                 // Serial offset of serial file name
	U32     startPC;
	U32     startSP;
	U32     startBF;
	U32     startFlash;
	U32     szCode;
	U32     offCode;
	U32     szData;
	U32     offData;
	U32     crcCode;
	U32     crcData;
	U32     szFlash;
	U32     szRam;
	U32     szPrgCode;
	U32     szPrgData;
	U32     adrPrgData;
	U32		speedClock;
	U32		offCache;				// offset to file start
	U32		szCache;				// size of readed data in cache
	U32		dapstate;
    U32     portstate[PORT_COUNT];
    U32     portaddr[PORT_COUNT];
    U32     deviceid[2];
    U32     serialsig;
	xFlashSectors sectors[8];
	U16     funCRC32;
	U16     funInit;
	U16     funUnInit;
	U16     funBlankCheck;
	U16     funEraseChip;
	U16     funEraseSector;
	U16     funProgPage;
	U16     szProg;
	U16     funVerify;
	U16     toProg;
	U16	    toErase;
	U16		funToDo;		// bit 0~1:erasechip/erase sector/donot erase
	U16		sap_index;
    U16     sap_version;
    U16     sfunToDo;
    U8      CICO;           // in  0~3:I2C/USART/USB/LAN, bit // out 4~7:I2C/USART/USB/LAN 
							// bit 2:program, 3: Verify, 4: Target Reset GO
	U8      patErase;
	U8		tDebug; // : debug, 40:error, 20:info
    U8      xport;  // port in action
    U8      rrport; // round robin port to query
    U8      resett;  // bit 0~3 HW RESET/SYSRESET/VECTRESET
                    // bit 4~7 pre-reset/UNDER RESET
    U8      estimate[16];
    U8      saptables;
    U8      sapid;
    U8      sreset;
    U8      svoltage;
    U8      voltage;
        // 0x00: NC
        // 0x01: 1.8 V
        // 0x02: 2.5 V
        // 0x04: 3.3 V
        // 0x08: 5.0 V
        // 0x10: CTM POWER
        // 0x80: CTM POWER and AUTO
    U8      logging;
    U8      slogging;
    U8      swj;        
        // 0x00: SWD
        // 0x01: SWD
        // 0x02: JTAG
    U8      sswj;        
    U8      doChipEraseBySector;
		U8			ExtraFunCnt;
		U8			SEQCnt;
		U32     ProgOffset;
		U32			NrfLock;
		xSAPSeq 		*SapExtraFun;
		xSAPSeq 		*SapSequence;
		uint32_t gSpeedUp;
} xSapFile;
typedef struct DAP_PORT
{
    U32     led[2];            // bit16~31:RLED, bit0~15:GLED
	U32		regs[17];
    U32     rets[PORT_COUNT];
    U32     idcode[PORT_COUNT];
    U32     crcs[PORT_COUNT];
    U32     errs[PORT_COUNT];
	U32		dp_sel_value;
	U32 	ap_csw_value;
	U32 	ap_tar_value;
	U32 	ap_cfg_value;
    U32     dapver[4];
    U32     dapbver[4];
	U32 	writemask;
	U32		regacc;
	U32		timestamp;
	U32		timeout;
	U32		exp;
    U32     adr;
    U32     radr;
    U32     outseq;
    U32     inseq[PORT_COUNT];
    U32     startTick;
	U32     reto;
	U32     CTRL;
	U8		bufo[0x440]; //DAP_PACKET_SIZE];
	U16		retries;
	U16		cmdoff;
	U16		pktsize;
    U8		manstate;
    U8		substate;
    U8		funstate;
    U8		port;	// 
	U8		cmdidx;
    U8      xport;
	U8		pvFun[12];
	U16		funToDo;		// for backup xap->funToDo
    U8      rport;
    U8      vport;
    U8      program_serial;
		U32			timetick;
} xDAPPort;
typedef struct
{
    uint32_t offset;
    uint32_t size;
    uint32_t crc32;
    uint8_t  enc;
    //uint8_t fid;
    //uint8_t update;
    //uint8_t reserv[2];
    //uint32_t offset;    // total file size for ending info
    //uint32_t file_size; // header checksum for ending info
} FWSTRUCT;

typedef struct
{
	uint8_t file_id;	// 0 for ending info
	uint8_t update;
	uint8_t reserv[2];
	uint32_t offset;	// total file size for ending info
	uint32_t file_size; // header checksum for ending info
	
} dta_flash_head_s;

typedef struct 
{
    uint8_t id;
    uint8_t res[3];
    uint32_t offset;
//    uint32_t size;    // size is not used.
} xSAPTable;

typedef struct 
{
    uint32_t address;
    uint32_t len;
    uint32_t val;
//    uint32_t size;    // size is not used.
} SEQTable;
typedef struct 
{
	uint8_t sequence_len;	
	SEQTable *SeqTable;
//    uint32_t size;    // size is not used.
} SecSEQ;


void write_targetmem(xDAPPacket *xPacket,uint32_t addr,uint16_t cnt,uint8_t *buf);
int Jtag_read_targetmem(xDAPPacket *xPacket,uint32_t addr,uint16_t cnt,uint8_t *buf);
void write_targetreg(xDAPPacket *xPacket,uint32_t regaddr,uint8_t ByteCnt,uint32_t buf);
void read_targetreg(xDAPPacket *xPacket,uint32_t regaddr,uint8_t ByteCnt,uint8_t *buf);
int SetFunTodo(uint8_t action);
void SendErrMsg(char *msg);



extern int jtag_arp_init();


#endif

