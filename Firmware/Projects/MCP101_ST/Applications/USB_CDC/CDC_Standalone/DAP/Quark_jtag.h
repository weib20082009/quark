
#ifndef QUARK_JTAG_H
#define QUARK_JTAG_H
#include "command.h"
#include "jtag/jtag.h"
#include "jtag/minidriver.h"
#include "jtag/interface.h"
#include "jtag/interfaces.h"
#include "jtag/tcl.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/


#define NTAP_OPT_IRLEN     0
#define NTAP_OPT_IRMASK    1
#define NTAP_OPT_IRCAPTURE 2
#define NTAP_OPT_ENABLED   3
#define NTAP_OPT_DISABLED  4
#define NTAP_OPT_EXPECTED_ID 5
#define NTAP_OPT_VERSION   6



/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
uint32_t Get_Quark_Chip(void);
int Quark_init_jtag_interface(struct command_context *context,struct jtag_interface *interface,char *transport_name);
int Quark_jtag_init(struct command_context *context,struct target *t);
int Quark_adapter_khz(unsigned int khz);
int Quark_newtap_cmd(struct jtag_tap *tap);
int Quark_target_configure(struct target *target);
int Quark_Connect(struct command_context *context,struct jtag_interface *interface,struct target *t);
int Quark_target_init(struct command_context *context,struct target *curtarget);
int Quark_halt(int ChipType,struct target *t);
int Quark_halt_and_reset(struct target *t);
int Quark_read_hw_reg(struct target *t,int reg,uint32_t *regval);
int Quark_write_hw_reg(struct target *t,int reg,uint32_t regval);
int Quark_WriteMem(struct target *t, uint32_t addr,
			uint32_t size, uint32_t count, const uint8_t *buf);

int Quark_ReadMem(struct target *t, uint32_t addr,
			uint32_t size, uint32_t count, uint8_t *buf);

int Quark_PageErase(struct target *t,int addr);
int Quark_MassErase(struct target *t);
int Quark_Step(struct target *t, int current,
			uint32_t address, int handle_breakpoints);
int Quark_resume(struct target *t, int current, uint32_t address,
			int handle_breakpoints, int debug_execution);
#endif