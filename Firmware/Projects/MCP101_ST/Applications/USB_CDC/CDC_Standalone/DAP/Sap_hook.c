#include "common.h"

uint32_t CPU_table[][4]=
{ 
  //CPUID, 		reset open-drain, reserve, reserved
    {0x0BB11477, 1,				  0,		0 },
	{0,0,0,0}
};

void Pre_program_Hook(uint32_t id_code)
{
	int ii;
	for(ii=0;;ii++)
	{
		if(CPU_table[ii][0] == 0)
			return;
		if(CPU_table[ii][0] == id_code)
			break;
	}
#ifndef STM32F10X_MD	
	if(CPU_table[ii][1])
	{
		PIN_nRESET_PORT->PCR[PIN_nRESET_BIT]   = PORT_PCR_MUX(1); //|  /* GPIO */
                                               PORT_PCR_PE_MASK |  /* Pull enable */
                                               PORT_PCR_PS_MASK ;//|  /* Pull-up */
                                               PORT_PCR_ODE_MASK;  /* Open-drain */
		PIN_nRESET_GPIO->PSOR  = 1 << PIN_nRESET_BIT;                    /* High level */
		PIN_nRESET_GPIO->PDDR |= 1 << PIN_nRESET_BIT;                    /* Output */		
	}		
#endif	
	
}
