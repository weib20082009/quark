#include "Quark_jtag.h"
#include "target_type.h"
#include "spider-sap.h"
#include "x86_32_common.h"
#include "MyAlloc.h"

extern int transport_select(struct command_context *ctx, const char *name);
extern int interface_init(struct command_context *ctx,struct jtag_interface *interface);

int Quark_init_jtag_interface(struct command_context *context,struct jtag_interface *interface,char *transport_name)
{
	int retval = ERROR_OK;
	interface_init(context,interface);
	transport_select(context,transport_name);
	return retval;
}
extern uint32_t Jtag_Common_Sequence();
extern int target_examine(void);
int Quark_jtag_init(struct command_context *context,struct target *t)
{
	int retval = ERROR_OK;
	uint32_t id;
	if(!context)
		return ERROR_FAIL;
	//rc = jtag_init(context);

	retval=jtag_init_reset(context);
	if(retval!=ERROR_OK)
		return retval;
	
	return 	ERROR_OK;
}
int Quark_newtap_cmd(struct jtag_tap *tap)
{
	struct jtag_tap *pTap=tap;
	int x;
	int e;

	uint8_t cp[20];
	/* name + dot + name + null */
	x = strlen(pTap->chip) + 1 + strlen(pTap->tapname) + 1;
	sprintf(cp, "%s.%s", pTap->chip, pTap->tapname);
	strcpy(pTap->dotted_name,cp);

	LOG_DEBUG("Creating New Tap, Chip: %s, Tap: %s, Dotted: %s, %d params",
		pTap->chip, pTap->tapname, pTap->dotted_name, goi->argc);

	if (!transport_is_jtag()) {
		/* SWD doesn't require any JTAG tap parameters */
		pTap->enabled = true;
		jtag_tap_init(pTap);
		return 0;
	}
		/* IEEE specifies that the two LSBs of an IR scan are 01, so make
	 * that the default.  The "-ircapture" and "-irmask" options are only
	 * needed to cope with nonstandard TAPs, or to specify more bits.
	 */
	if(pTap->ir_capture_mask ==0)
		pTap->ir_capture_mask = 0x03;
	pTap->ir_capture_value = 0x01;


	/* default is enabled-after-reset */
	pTap->disabled_after_reset=!pTap->enabled;

	/* Did all the required option bits get cleared? */
	if (pTap->ir_length != 0) {
		jtag_tap_init(pTap);
		return ERROR_OK;
	}
	jtag_tap_free(pTap);
	return ERROR_FAIL;
	
}
int Quark_tap_release(void)
{
	struct jtag_tap *pTap=jtag_all_taps();
	struct jtag_tap *pTap2;
	int x;
	int e;
	char *cp;
	while(pTap)
	{
		pTap2=pTap;
		pTap=pTap->next_tap;
		pTap2->next_tap=NULL;
		jtag_tap_free(pTap2);
	}
	jtag_free_taps();
}
int Quark_target_configure(struct target *target)
{
	/* parse config or cget options ... */
		target->event_action=NULL;
		target->endianness = TARGET_LITTLE_ENDIAN;
	
		target->working_area        = 0x0;
		target->working_area_size   = 0x0;
		target->working_areas       = NULL;
		struct jtag_tap *tap;
	if(Get_Quark_Chip()==QUARK_D20XX)
		tap = jtag_tap_by_string("quark_d2000.quark");
	else if(Get_Quark_Chip()==QUARK_SE_C10XX)
	{
		if(strcmp(target->type->name,"quark_se") == 0)
		{
			tap = jtag_tap_by_string("quark_se.quark");
			if (tap == NULL)
				return ERROR_FAIL;
		}else
		{
			tap = jtag_tap_by_string("arc32.arc-em");
			if (tap == NULL)
				return ERROR_FAIL;
		}
	}
		/* make this exactly 1 or 0 */
			target->tap = tap;
			
		
	/* loop for more e*/
		/* done - we return */
	return ERROR_OK;
}
 extern int target_init(struct command_context *cmd_ctx);
int Quark_target_init(struct command_context *context,struct target *curtarget)
{
		//if(ChipType==(int)QUARK_D20XX)
		//	curtarget->type->init_target(context,curtarget);
	int retval = ERROR_OK;
	retval = target_init(context);
	if(retval!=ERROR_OK)
		return retval;
}

int Quark_adapter_khz(unsigned int khz)
{
		int retval = ERROR_OK;
		int cur_speed = 0;
		retval = jtag_config_khz(khz);
		if (ERROR_OK != retval)
			return retval;
	

	cur_speed = jtag_get_speed_khz();
	retval = jtag_get_speed_readable(&cur_speed);
	if (ERROR_OK != retval)
		return retval;

}
int Quark_adapter_clk(unsigned int mhz)	
{
	
}
int Quark_read_hw_reg(struct target *t,int reg,uint32_t *regval)
{
	int retval = ERROR_OK;
	struct x86_32_common *x86_32 = target_to_x86_32(t);
	retval=x86_32->read_hw_reg(t,reg,regval,0);//No cache
	return retval;
}
int Quark_write_hw_reg(struct target *t,int reg,uint32_t regval)
{
	int retval = ERROR_OK;
	struct x86_32_common *x86_32 = target_to_x86_32(t);
	retval=x86_32->write_hw_reg(t,reg,regval,0);//No cache
	return retval;	
}
int Quark_poll(struct target *t)
{
	int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
		retval=	t->type->poll(t);
	return retval;
}
int Quark_halt(int ChipType,struct target *t)
{
		int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
		retval=	t->type->halt(t);
	return retval;
}

int Quark_halt_and_reset(struct target *t)
{
		int retval = ERROR_OK;	
		retval=	t->type->assert_reset(t);
	return retval;
}

int Quark_WriteMem(struct target *t, uint32_t addr,
			uint32_t size, uint32_t count, const uint8_t *buf)
{
		int retval = ERROR_OK;	
//	if(ChipType==(int)QUARK_D20XX)
	if(t->type->write_memory)
		retval=	t->type->write_memory(t,addr,
			size, count, buf);
	else 
		retval = ERROR_FAIL;
	
	return retval;
}
int Quark_ReadMem(struct target *t, uint32_t addr,
			uint32_t size, uint32_t count, uint8_t *buf)
{
		int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
		if(t->type->read_memory)
			retval=	t->type->read_memory(t,addr,size, count, buf);
		else 
			retval = ERROR_FAIL;
		return retval;
}
int Quark_PageErase(struct target *t,int addr)
{
		int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
		if(t->type->pageerase)
			retval=	t->type->pageerase(t,addr);
		else 
			retval = ERROR_FAIL;
		return retval;
}
int Quark_ChipErase(struct target *t,int addr)
{
		int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
		if(t->type->chiperase)
			retval=	t->type->chiperase(t,addr);
		else 
			retval = ERROR_FAIL;
		return retval;
}
int Quark_MassErase(struct target *t)
{
		int retval = ERROR_OK;	

		if(t->type->pageerase)
			retval=	t->type->masserase(t);
		else 
			retval = ERROR_FAIL;
		return retval;
}

int Quark_Step(struct target *t, int current,
			uint32_t address, int handle_breakpoints)
{
		int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
		if(t->type->step)
			retval=	t->type->step(t,current,address,handle_breakpoints);
		else 
			retval = ERROR_FAIL;
		return retval;
}
int Quark_resume(struct target *t, int current, uint32_t address,
			int handle_breakpoints, int debug_execution)
{
		int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
		if(t->type->resume)
			retval=	t->type->resume(t, current, address,
			handle_breakpoints, debug_execution);
		else 
			retval = ERROR_FAIL;
		return retval;
}
int Quark_WriteBuf(int ChipType,struct target *t, uint32_t addr,
			uint32_t size, uint32_t count, const uint8_t *buf)
{
		int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
	if(t->type->write_buffer)
		retval=	t->type->write_buffer(t, addr,count, buf);
	return retval;
}
int Quark_ReadBuf(int ChipType,struct target *t, uint32_t addr,
			uint32_t size, uint32_t count, uint8_t *buf)
{
		int retval = ERROR_OK;	
	//if(ChipType==(int)QUARK_D20XX)
		if(t->type->read_buffer)
			retval=t->type->read_buffer(t,addr,count, buf);
		return retval;
}
extern  struct target Quark_target; 
int Quark_Tap_en(int en)
{
	lakemont_tapenable(en);
	Quark_jtag_runtest(10);
	if(Get_Quark_Chip() == QUARK_D20XX)
		lakemont_gettapstatus(&Quark_target);
	
}
int Quark_jtag_tlrreset()
{
	jtag_add_tlr();
	return jtag_execute_queue();
}
int Quark_jtag_runtest(num_clocks)
{
	jtag_add_runtest(num_clocks, TAP_IDLE);
	return jtag_execute_queue();
}
extern struct jtag_interface cmsis_dap_interface;
int  Quark_Connect(struct command_context *context,struct jtag_interface *interface,struct target *t)
{

//set LMT_ID_CODE   0x38289013
//set CLTAP_ID_CODE 0x0e786013

//# scan_chain
//jtag newtap quark_d2000 lmt    -irlen 8  -irmask 0xff  -expected-id  $LMT_ID_CODE    -disable
//jtag newtap quark_d2000 cltap  -irlen 8  -irmask 0xff  -expected-id  $CLTAP_ID_CODE  -enable

//jtag 		
	Quark_adapter_khz(1000);	
	Quark_jtag_init(context,t);
	
	
Quark_adapter_khz(1000);	



	


}
extern int adapter_quit(void);
extern void target_quit(void);
extern	UART_HandleTypeDef UartHandle;
int  Quark_DisConnect(struct command_context *context,struct jtag_interface *interface,struct target *t)
{
	//adapter_quit();	
	Quark_tap_release();
	lakemont_free(&Quark_target);
	jtag_command_queue_reset();
	adapter_quit();
	target_quit();
	//if(context)
	//	free(context);
	//MyMemReInit();
Jtag_IO_DeInit();

return 0;
}