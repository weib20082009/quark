#include "common.h"

uint32_t calcCRC32(const uint8_t* buffer, size_t length, uint32_t crc)
{ 
    uint8_t i;

    if (crc == 0)
        crc = 0xFFFFFFFF;
    //crc = crc?crc:0xFFFFFFFF;
    while(length--) {
        crc ^= (*buffer++)<<24;
        for(i=0;i<8;i++) {
            if (crc & 0x80000000) {
                crc <<= 1;
                crc ^= 0x04c11db7;
            } else {
                crc <<= 1;
            }
        }
    }
    return crc;
/*
		#python code:
		r3 = 0x04c11db7
		r0 = 0xFFFFFFFF
		buf = open(fn, 'rb').read()
		for cc in buf:
			r5 = ord(cc)
			r5 = UNSIGNED( r5 << 24 )
			r0 = UNSIGNED( r0 ^ r5 )
			for i in range(8):
				if r0 & 0x80000000:
					r0 = UNSIGNED( r0 << 1 )
					r0 = UNSIGNED( r0 ^ r3 )
				else:
					r0 = UNSIGNED( r0 << 1 )
		return r0
        */
}
void encTEA(uint32_t* v, uint32_t* k) {
    uint32_t v0=v[0], v1=v[1], sum=0, i;           /* set up */
    uint32_t delta=0x9e3779b9;                     /* a key schedule constant */
    uint32_t k0=k[0], k1=k[1], k2=k[2], k3=k[3];   /* cache key */
    for (i=0; i < 32; i++) {                       /* basic cycle start */
        sum += delta;
        v0 += ((v1<<4) + k0) ^ (v1 + sum) ^ ((v1>>5) + k1);
        v1 += ((v0<<4) + k2) ^ (v0 + sum) ^ ((v0>>5) + k3);  
    }                                              /* end cycle */
    v[0]=v0; v[1]=v1;
}
 
void decTEA(uint32_t* v, uint32_t* k) {
    uint32_t v0=v[0], v1=v[1], sum=0xC6EF3720, i;  /* set up */
    uint32_t delta=0x9e3779b9;                     /* a key schedule constant */
    uint32_t k0=k[0], k1=k[1], k2=k[2], k3=k[3];   /* cache key */
    for (i=0; i<32; i++) {                         /* basic cycle start */
        v1 -= ((v0<<4) + k2) ^ (v0 + sum) ^ ((v0>>5) + k3);
        v0 -= ((v1<<4) + k0) ^ (v1 + sum) ^ ((v1>>5) + k1);
        sum -= delta;                                   
    }                                              /* end cycle */
    v[0]=v0; v[1]=v1;
}
