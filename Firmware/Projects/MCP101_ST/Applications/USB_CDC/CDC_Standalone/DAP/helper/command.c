/***************************************************************************
 *   Copyright (C) 2005 by Dominic Rath                                    *
 *   Dominic.Rath@gmx.de                                                   *
 *                                                                         *
 *   Copyright (C) 2007,2008 Øyvind Harboe                                 *
 *   oyvind.harboe@zylin.com                                               *
 *                                                                         *
 *   Copyright (C) 2008, Duane Ellis                                       *
 *   openocd@duaneeellis.com                                               *
 *                                                                         *
 *   part of this file is taken from libcli (libcli.sourceforge.net)       *
 *   Copyright (C) David Parrish (david@dparrish.com)                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.           *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* see Embedded-HOWTO.txt in Jim Tcl project hosted on BerliOS*/
#define JIM_EMBEDDED

/* @todo the inclusion of target.h here is a layering violation */
#include <jtag/jtag.h>
#include <target/target.h>
#include "command.h"
#include "configuration.h"
#include "log.h"
#include "time_support.h"
#include "jim-eventloop.h"
#include "main.h"
/* nice short description of source file */
#define __THIS__FILE__ "command.c"

static int run_command(struct command_context *context,
		struct command *c, const char *words[], unsigned num_words);

struct log_capture_state {
	Jim_Interp *interp;
	Jim_Obj *output;
};

static void tcl_output(void *privData, const char *file, unsigned line,
	const char *function, const char *string)
{
	struct log_capture_state *state = privData;
	Jim_AppendString(state->interp, state->output, string, strlen(string));
}

static struct log_capture_state *command_log_capture_start(Jim_Interp *interp)
{
	/* capture log output and return it. A garbage collect can
	 * happen, so we need a reference count to this object */
	Jim_Obj *tclOutput = Jim_NewStringObj(interp, "", 0);
	if (NULL == tclOutput)
		return NULL;

	struct log_capture_state *state = malloc(sizeof(*state));
	if (NULL == state)
		return NULL;

	state->interp = interp;
	Jim_IncrRefCount(tclOutput);
	state->output = tclOutput;

	log_add_callback(tcl_output, state);

	return state;
}
int parse_long(const char *str, long *ul)
{
	char * end;
	*ul = strtol(str, &end, 0);
	if (end && *end) {
		return -1;
	}
	return 0;
}
int parse_ulong(const char *str, unsigned long *ul)
{
	char * end;
	*ul = strtoul(str, &end, 0);
	if (end && *end) {
		return -1;
	}
	return 0;
}
int parse_ullong(const char *str, unsigned long long *ul)
{
	char * end;
	*ul = strtoull(str, &end, 0);
	if (end && *end) {
		return -1;
	}
	return 0;
}
int parse_llong(const char *str, long long *ul)
{
	char * end;
	*ul = strtoll(str, &end, 0);
	if (end && *end) {
		return -1;
	}
	return 0;
}
/* Classic openocd commands provide progress output which we
 * will capture and return as a Tcl return value.
 *
 * However, if a non-openocd command has been invoked, then it
 * makes sense to return the tcl return value from that command.
 *
 * The tcl return value is empty for openocd commands that provide
 * progress output.
 *
 * Therefore we set the tcl return value only if we actually
 * captured output.
 */
static void command_log_capture_finish(struct log_capture_state *state)
{
	if (NULL == state)
		return;

	log_remove_callback(tcl_output, state);


	free(state);
}

static int command_retval_set(Jim_Interp *interp, int retval)
{
	int *return_retval = Jim_GetAssocData(interp, "retval");
	if (return_retval != NULL)
		*return_retval = retval;

	return (retval == ERROR_OK) ? JIM_OK : retval;
}

extern struct command_context *global_cmd_ctx;

/* dump a single line to the log for the command.
 * Do nothing in case we are not at debug level 3 */
void script_debug(Jim_Interp *interp, const char *name,
	unsigned argc, Jim_Obj * const *argv)
{
	if (debug_level < LOG_LVL_DEBUG)
		return;

	char *dbg = alloc_printf("command - %s", name);
	for (unsigned i = 0; i < argc; i++) {
		int len;
		const char *w = Jim_GetString(argv[i], &len);
		char *t = alloc_printf("%s %s", dbg, w);
		free(dbg);
		dbg = t;
	}
	LOG_DEBUG("%s", dbg);
	free(dbg);
}

static void script_command_args_free(char **words, unsigned nwords)
{
	for (unsigned i = 0; i < nwords; i++)
		free(words[i]);
	free(words);
}

static char **script_command_args_alloc(
	unsigned argc, Jim_Obj * const *argv, unsigned *nwords)
{
	char **words = malloc(argc * sizeof(char *));
	if (NULL == words)
		return NULL;

	unsigned i;
	for (i = 0; i < argc; i++) {
		int len;
		const char *w = Jim_GetString(argv[i], &len);
		words[i] = strdup(w);
		if (words[i] == NULL) {
			script_command_args_free(words, i);
			return NULL;
		}
	}
	*nwords = i;
	return words;
}

struct command_context *current_command_context(Jim_Interp *interp)
{
	/* grab the command context from the associated data */
	struct command_context *cmd_ctx = Jim_GetAssocData(interp, "context");

	return cmd_ctx;
}

static int script_command_run(Jim_Interp *interp,
	int argc, Jim_Obj * const *argv, struct command *c, bool capture)
{
	target_call_timer_callbacks_now();
	LOG_USER_N("%s", "");	/* Keep GDB connection alive*/

	unsigned nwords;
	char **words = script_command_args_alloc(argc, argv, &nwords);
	if (NULL == words)
		return JIM_ERR;

	struct log_capture_state *state = NULL;
	if (capture)
		state = command_log_capture_start(interp);

	struct command_context *cmd_ctx = current_command_context(interp);
	int retval = run_command(cmd_ctx, c, (const char **)words, nwords);

	command_log_capture_finish(state);

	script_command_args_free(words, nwords);
	return command_retval_set(interp, retval);
}

static int script_command(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
	/* the private data is stashed in the interp structure */

	struct command *c = interp->cmdPrivData;
	assert(c);
	script_debug(interp, c->name, argc, argv);
	return script_command_run(interp, argc, argv, c, true);
}

static struct command *command_root(struct command *c)
{
	while (NULL != c->parent)
		c = c->parent;
	return c;
}

/**
 * Find a command by name from a list of commands.
 * @returns Returns the named command if it exists in the list.
 * Returns NULL otherwise.
 */
static struct command *command_find(struct command *head, const char *name)
{
	for (struct command *cc = head; cc; cc = cc->next) {
		if (strcmp(cc->name, name) == 0)
			return cc;
	}
	return NULL;
}

struct command *command_find_in_context(struct command_context *cmd_ctx,
	const char *name)
{
	return command_find(cmd_ctx->commands, name);
}
struct command *command_find_in_parent(struct command *parent,
	const char *name)
{
	return command_find(parent->children, name);
}

/**
 * Add the command into the linked list, sorted by name.
 * @param head Address to head of command list pointer, which may be
 * updated if @c c gets inserted at the beginning of the list.
 * @param c The command to add to the list pointed to by @c head.
 */
static void command_add_child(struct command **head, struct command *c)
{
	assert(head);
	if (NULL == *head) {
		*head = c;
		return;
	}

	while ((*head)->next && (strcmp(c->name, (*head)->name) > 0))
		head = &(*head)->next;

	if (strcmp(c->name, (*head)->name) > 0) {
		c->next = (*head)->next;
		(*head)->next = c;
	} else {
		c->next = *head;
		*head = c;
	}
}

static struct command **command_list_for_parent(
	struct command_context *cmd_ctx, struct command *parent)
{
	return parent ? &parent->children : &cmd_ctx->commands;
}

static void command_free(struct command *c)
{
	/** @todo if command has a handler, unregister its jim command! */

	while (NULL != c->children) {
		struct command *tmp = c->children;
		c->children = tmp->next;
		command_free(tmp);
	}

	free(c->name);
	free(c->help);
	free(c->usage);
	free(c);
}

static struct command *command_new(struct command_context *cmd_ctx,
	struct command *parent, const struct command_registration *cr)
{
	assert(cr->name);

	/*
	 * If it is a non-jim command with no .usage specified,
	 * log an error.
	 *
	 * strlen(.usage) == 0 means that the command takes no
	 * arguments.
	*/
	if ((cr->jim_handler == NULL) && (cr->usage == NULL)) {
		LOG_DEBUG("BUG: command '%s%s%s' does not have the "
			"'.usage' field filled out",
			parent && parent->name ? parent->name : "",
			parent && parent->name ? " " : "",
			cr->name);
	}

	struct command *c = calloc(1, sizeof(struct command));
	if (NULL == c)
		return NULL;

	c->name = strdup(cr->name);
	if (cr->help)
		c->help = strdup(cr->help);
	if (cr->usage)
		c->usage = strdup(cr->usage);

	if (!c->name || (cr->help && !c->help) || (cr->usage && !c->usage))
		goto command_new_error;

	c->parent = parent;
	c->handler = cr->handler;
	c->jim_handler = cr->jim_handler;
	c->jim_handler_data = cr->jim_handler_data;
	c->mode = cr->mode;

	command_add_child(command_list_for_parent(cmd_ctx, parent), c);

	return c;

command_new_error:
	command_free(c);
	return NULL;
}

static int command_unknown(Jim_Interp *interp, int argc, Jim_Obj *const *argv);

static int register_command_handler(struct command_context *cmd_ctx,
	struct command *c)
{
	Jim_Interp *interp = cmd_ctx->interp;
	char *ocd_name = alloc_printf("ocd_%s", c->name);
	if (NULL == ocd_name)
		return JIM_ERR;

	LOG_DEBUG("registering '%s'...", ocd_name);

	Jim_CmdProc *func = c->handler ? &script_command : &command_unknown;
	int retval = Jim_CreateCommand(interp, ocd_name, func, c, NULL);
	free(ocd_name);
	if (JIM_OK != retval)
		return retval;

	/* we now need to add an overrideable proc */
	char *override_name = alloc_printf(
			"proc %s {args} {eval ocd_bouncer %s $args}",
			c->name, c->name);
	if (NULL == override_name)
		return JIM_ERR;

	retval = Jim_Eval_Named(interp, override_name, 0, 0);
	free(override_name);

	return retval;
}

struct command *register_command(struct command_context *context,
	struct command *parent, const struct command_registration *cr)
{
	if (!context || !cr->name)
		return NULL;

	const char *name = cr->name;
	struct command **head = command_list_for_parent(context, parent);
	struct command *c = command_find(*head, name);
	if (NULL != c) {
		/* TODO: originally we treated attempting to register a cmd twice as an error
		 * Sometimes we need this behaviour, such as with flash banks.
		 * http://www.mail-archive.com/openocd-development@lists.berlios.de/msg11152.html */
		LOG_DEBUG("command '%s' is already registered in '%s' context",
			name, parent ? parent->name : "<global>");
		return c;
	}

	c = command_new(context, parent, cr);
	if (NULL == c)
		return NULL;

	int retval = ERROR_OK;
	if (NULL != cr->jim_handler && NULL == parent) {
		retval = Jim_CreateCommand(context->interp, cr->name,
				cr->jim_handler, cr->jim_handler_data, NULL);
	} else if (NULL != cr->handler || NULL != parent)
		retval = register_command_handler(context, command_root(c));

	if (ERROR_OK != retval) {
		unregister_command(context, parent, name);
		c = NULL;
	}
	return c;
}

int register_commands(struct command_context *cmd_ctx, struct command *parent,
	const struct command_registration *cmds)
{
	int retval = ERROR_OK;
	unsigned i;
	for (i = 0; cmds[i].name || cmds[i].chain; i++) {
		const struct command_registration *cr = cmds + i;

		struct command *c = NULL;
		if (NULL != cr->name) {
			c = register_command(cmd_ctx, parent, cr);
			if (NULL == c) {
				retval = ERROR_FAIL;
				break;
			}
		}
		if (NULL != cr->chain) {
			struct command *p = c ? : parent;
			retval = register_commands(cmd_ctx, p, cr->chain);
			if (ERROR_OK != retval)
				break;
		}
	}
	if (ERROR_OK != retval) {
		for (unsigned j = 0; j < i; j++)
			unregister_command(cmd_ctx, parent, cmds[j].name);
	}
	return retval;
}

int unregister_all_commands(struct command_context *context,
	struct command *parent)
{
	if (context == NULL)
		return ERROR_OK;

	struct command **head = command_list_for_parent(context, parent);
	while (NULL != *head) {
		struct command *tmp = *head;
		*head = tmp->next;
		command_free(tmp);
	}

	return ERROR_OK;
}

int unregister_command(struct command_context *context,
	struct command *parent, const char *name)
{
	if ((!context) || (!name))
		return ERROR_COMMAND_SYNTAX_ERROR;

	struct command *p = NULL;
	struct command **head = command_list_for_parent(context, parent);
	for (struct command *c = *head; NULL != c; p = c, c = c->next) {
		if (strcmp(name, c->name) != 0)
			continue;

		if (p)
			p->next = c->next;
		else
			*head = c->next;

		command_free(c);
		return ERROR_OK;
	}

	return ERROR_OK;
}

void command_set_handler_data(struct command *c, void *p)
{
	if (NULL != c->handler || NULL != c->jim_handler)
		c->jim_handler_data = p;
	for (struct command *cc = c->children; NULL != cc; cc = cc->next)
		command_set_handler_data(cc, p);
}

void command_output_text(struct command_context *context, const char *data)
{
	if (context && context->output_handler && data)
		context->output_handler(context, data);
}

void command_print_sameline(struct command_context *context, const char *format, ...)
{
	char *string;

	va_list ap;
	va_start(ap, format);

	string = alloc_vprintf(format, ap);
	if (string != NULL) {
		/* we want this collected in the log + we also want to pick it up as a tcl return
		 * value.
		 *
		 * The latter bit isn't precisely neat, but will do for now.
		 */
		LOG_USER_N("%s", string);
		/* We already printed it above
		 * command_output_text(context, string); */
		free(string);
	}

	va_end(ap);
}

void command_print(struct command_context *context, const char *format, ...)
{
	char *string;

	va_list ap;
	va_start(ap, format);

	string = alloc_vprintf(format, ap);
	if (string != NULL) {
		strcat(string, "\n");	/* alloc_vprintf guaranteed the buffer to be at least one
					 *char longer */
		/* we want this collected in the log + we also want to pick it up as a tcl return
		 * value.
		 *
		 * The latter bit isn't precisely neat, but will do for now.
		 */
		LOG_USER_N("%s", string);
		/* We already printed it above
		 * command_output_text(context, string); */
		free(string);
	}

	va_end(ap);
}

static char *__command_name(struct command *c, char delim, unsigned extra)
{
	char *name;
	unsigned len = strlen(c->name);
	if (NULL == c->parent) {
		/* allocate enough for the name, child names, and '\0' */
		name = malloc(len + extra + 1);
		strcpy(name, c->name);
	} else {
		/* parent's extra must include both the space and name */
		name = __command_name(c->parent, delim, 1 + len + extra);
		char dstr[2] = { delim, 0 };
		strcat(name, dstr);
		strcat(name, c->name);
	}
	return name;
}

char *command_name(struct command *c, char delim)
{
	return __command_name(c, delim, 0);
}

static bool command_can_run(struct command_context *cmd_ctx, struct command *c)
{
	return c->mode == COMMAND_ANY || c->mode == cmd_ctx->mode;
}

static int run_command(struct command_context *context,
	struct command *c, const char *words[], unsigned num_words)
{
	if (!command_can_run(context, c)) {
		/* Many commands may be run only before/after 'init' */
		const char *when;
		switch (c->mode) {
			case COMMAND_CONFIG:
				when = "before";
				break;
			case COMMAND_EXEC:
				when = "after";
				break;
			/* handle the impossible with humor; it guarantees a bug report! */
			default:
				when = "if Cthulhu is summoned by";
				break;
		}
		LOG_ERROR("The '%s' command must be used %s 'init'.",
			c->name, when);
		return ERROR_FAIL;
	}

	struct command_invocation cmd = {
		.ctx = context,
		.current = c,
		.name = c->name,
		.argc = num_words - 1,
		.argv = words + 1,
	};
	int retval = c->handler(&cmd);
	if (retval == ERROR_COMMAND_SYNTAX_ERROR) {
		/* Print help for command */
		char *full_name = command_name(c, ' ');
		if (NULL != full_name) {
			command_run_linef(context, "usage %s", full_name);
			free(full_name);
		} else
			retval = -ENOMEM;
	} else if (retval == ERROR_COMMAND_CLOSE_CONNECTION) {
		/* just fall through for a shutdown request */
	} else if (retval != ERROR_OK) {
		/* we do not print out an error message because the command *should*
		 * have printed out an error
		 */
		LOG_DEBUG("Command failed with error code %d", retval);
	}

	return retval;
}

int command_run_line(struct command_context *context, char *line)
{
	/* all the parent commands have been registered with the interpreter
	 * so, can just evaluate the line as a script and check for
	 * results
	 */
	/* run the line thru a script engine */
	int retval = ERROR_FAIL;
	int retcode;
	/* Beware! This code needs to be reentrant. It is also possible
	 * for OpenOCD commands to be invoked directly from Tcl. This would
	 * happen when the Jim Tcl interpreter is provided by eCos for
	 * instance.
	 */
	Jim_Interp *interp = context->interp;
	Jim_DeleteAssocData(interp, "context");
	retcode = Jim_SetAssocData(interp, "context", NULL, context);
	if (retcode == JIM_OK) {
		/* associated the return value */
		Jim_DeleteAssocData(interp, "retval");
		retcode = Jim_SetAssocData(interp, "retval", NULL, &retval);
		if (retcode == JIM_OK) {
			retcode = Jim_Eval_Named(interp, line, 0, 0);

			Jim_DeleteAssocData(interp, "retval");
		}
		Jim_DeleteAssocData(interp, "context");
	}
	if (retcode == JIM_OK) {
		const char *result;
		int reslen;

		result = Jim_GetString(Jim_GetResult(interp), &reslen);
		if (reslen > 0) {
			int i;
			char buff[256 + 1];
			for (i = 0; i < reslen; i += 256) {
				int chunk;
				chunk = reslen - i;
				if (chunk > 256)
					chunk = 256;
				strncpy(buff, result + i, chunk);
				buff[chunk] = 0;
				LOG_USER_N("%s", buff);
			}
			LOG_USER_N("\n");
		}
		retval = ERROR_OK;
	} else if (retcode == JIM_EXIT) {
		/* ignore.
		 * exit(Jim_GetExitCode(interp)); */
	} else if (retcode == ERROR_COMMAND_CLOSE_CONNECTION) {
		return retcode;
	} else {
		Jim_MakeErrorMessage(interp);
		LOG_USER("%s", Jim_GetString(Jim_GetResult(interp), NULL));

		if (retval == ERROR_OK) {
			/* It wasn't a low level OpenOCD command that failed */
			return ERROR_FAIL;
		}
		return retval;
	}

	return retval;
}

int command_run_linef(struct command_context *context, const char *format, ...)
{
	int retval = ERROR_FAIL;
	char *string;
	va_list ap;
	va_start(ap, format);
	string = alloc_vprintf(format, ap);
	if (string != NULL) {
		retval = command_run_line(context, string);
		free(string);
	}
	va_end(ap);
	return retval;
}

void command_set_output_handler(struct command_context *context,
	command_output_handler_t output_handler, void *priv)
{
	context->output_handler = output_handler;
	context->output_handler_priv = priv;
}

struct command_context *copy_command_context(struct command_context *context)
{
	struct command_context *copy_context = malloc(sizeof(struct command_context));

	*copy_context = *context;

	return copy_context;
}

void command_done(struct command_context *cmd_ctx)
{
	if (NULL == cmd_ctx)
		return;

	free(cmd_ctx);
}

/* find full path to file */
static int jim_find(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
	if (argc != 2)
		return JIM_ERR;
	return JIM_OK;
}







#define HELP_LINE_WIDTH(_n) (int)(76 - (2 * _n))

static void command_help_show_indent(unsigned n)
{
	for (unsigned i = 0; i < n; i++)
		LOG_USER_N("  ");
}







