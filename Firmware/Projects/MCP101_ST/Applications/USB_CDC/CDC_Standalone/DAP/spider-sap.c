#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "string.h"
#include "spider-sap.h" 
#include "serialprog.h"
#include "common.h"
#include "dap.h"
#include "nrf51.h"
#ifdef __RTX_FreeRTOS__
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "spider-sap.h" 
#include "spider-funs.h"
#include "spider-i2c.h"
#include "spider-ser.h"
#include "spider-ver.h"
#include "semphr.h"
#include "../DAP/spider-spi.h"
#include "spider-adc.h"
#include "stm32f4xx_spi.h"
#include "sdio_stm32f4.h"
#include "usbd_conf.h"
#include "flash_if.h"
#include "usb_core.h"
#include "lwip/netif.h"


#include "ff.h"
portTASK_FUNCTION_PROTO(prvDAPCMDPktTask, pvParameters );

#endif


#define     SER_TYPE_GENERAL    0x00
#define     SER_TYPE_CRC32      0x21    // maybe not used
#define     SER_TYPE_DUMMY      0x30    // maybe not used

#define MC101_BOARD 1
//#undef MC101_BOARD
int gChkRomtable=0;
int gUp=0;
//__align(4)
	uint8_t	sapcache[0x10];	//////??????? XAP_LOAD_SIZE
//__align(4)
	uint8_t	linecache[0x40];	
//__align(4)
     xDAPPort pPort; // __attribute__( ( section( "CCM")) ) ;
//__align(4)
     xSapFile xSapfile; // __attribute__( ( section( "CCM")) ) ;
//__align(4)
__IO uint16_t adcbuf0[PORT_COUNT];
//__IO uint16_t adcbuf1[4];

//__align(4)
    static xSAPTable xst[MAX_SAP_SECTION+1];// __attribute__( ( section( "CCM")) ) ;
//__align(4)
    static uint32_t enckeys[]  __attribute__( ( section( "CCM")) )  = {0x16896168, 0x47617279,0x4A657365,0x526F6D65};

//__align(4)
    static xDAPBuffer xBuffer;
    static xDAPBuffer *xBuff;

//__align(4)
    static xSerFile xSF;// __attribute__( ( section( "CCM")) );
    xSapFile *xap;
    xSerFile *xsf = NULL;
    /*---------------------------------------------------------------------------*/
//    FATFS fs;
//    FATFS fsusb;
//    static xDAPCfg xCfg;// __attribute__( ( section( "CCM")) );
    U8	respNULL[] = {0xFF, 0xFF, 0xFF, 0xFF};
/*__align(4)
    static char logbuf[128];// __attribute__( ( section( "CCM")) ) ;
    static char logcache[100]; // __attribute__( ( section( "CCM")) ) ;
  */  
		//static int logs = 0;
static uint8_t currcmd = 0;

extern uint16_t build_number;
extern uint16_t firmware_version;

int clk_flag;//if clk_flag is 1,try different reset mode
int rst_flag;//if rst_flag is 1,try different reset mode
int gAtmelHasErase=0;
int gAtmelHasPlug=0;		
int		gAtmelNeedLock=0;
int runrc;

int gNeedLockChip=0;
int	gNeedErasePin=0;
		
int	gNrfUICRExtra=0;
NRF_UICR_Type pnrf_uicr_t;		
		
int runrc;


//int MsgOutput(MsgType type,const char fmt[], ...);
void printxPacket(U8 io, xDAPPacket *xPkt);
uint32_t uint32(uint8_t *strno);
int		(*runfunc)(xDAPPacket *xPacket);	
static void run_SAPSequence(xDAPPacket *xPacket,xSAPSeq *Seq,uint8_t SEQCnt);
char *ErrMsg(uint8_t errco, uint8_t opt);
#define SPIP	4


/*
extern int dbg_deep;
#ifndef USE_SYSLOG
#define  uP(...)       if(dbg_deep>0)MsgOutput(USERMSG,__VA_ARGS__)
#else
#define  uP(...)       if(dbg_deep>0)mtk_sf_printf(__VA_ARGS__,-1)
#endif

#ifndef USE_SYSLOG
#define  cP(...)       if(dbg_deep>1)MsgOutput(USERMSG,__VA_ARGS__)
#else
#define  cP(...)       if(dbg_deep>1)mtk_sf_printf(USERMSG,__VA_ARGS__,-1)
#endif

#ifndef USE_SYSLOG
#define  eP(...)       if(dbg_deep>0) MsgOutput(ERRMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // Error
#else
#define  eP(...)       if(dbg_deep>0) mtk_sf_printf(__VA_ARGS__,-1)//mtk_sf_printf(__VA_ARGS__,-1) // Error
#endif

#define  wP(...)       if(dbg_deep>1) MsgOutput(WARNINGMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)	    // Warning
#define  iP(...)       if(dbg_deep>2) MsgOutput(INFOMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)	    // Info

#define  dP(...)       	  if(dbg_deep>1) MsgOutput(DEBUGMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)  // debug
#define  DP(...)       	  if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)  // full print
#define  vP(...)       	   if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // full print
#define  VP(...)       	   if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // full print
#define  printPacket(x,y)  if(dbg_deep>2) printf_memory(y,x) // 

*/
uint8_t xI2CMsgPacketOut(uint8_t type, uint32_t len, uint32_t addr, char *data,uint8_t flag);
#if 0
#define  IcP(a,b,c,d)    
#define  IeP(a,b,c,d)    
#define  IwP(a,b,c,d)    
#define  IiP(a,b,c,d)    
#define  IdP(a,b,c,d)    
#define  IfP(a,b,c,d)    
#else
#define  IcP(a,b,c,d)    if (xap->CICO & DAP_INTF_I2C) xI2CMsgPacketOut(a,b,c,d,0)
#define  IeP(a,b,c,d)    /*if ((xap->CICO & DAP_INTF_I2C) && (xap->tDebug >= OL_ERROR))*/    xI2CMsgPacketOut(a,b,c,d,0)
#define  IwP(a,b,c,d)    if ((xap->CICO & DAP_INTF_I2C) && (xap->tDebug >= OL_WARNING))  xI2CMsgPacketOut(a,b,c,d,0)
#define  IiP(a,b,c,d)    /*if ((xap->CICO & DAP_INTF_I2C) && (xap->tDebug >= OL_INFO))*/     xI2CMsgPacketOut(a,b,c,d,0)
#define  IdP(a,b,c,d)    if ((xap->CICO & DAP_INTF_I2C) && (xap->tDebug >= OL_DEBUG))    xI2CMsgPacketOut(a,b,c,d,0)
#define  IfP(a,b,c,d)    if ((xap->CICO & DAP_INTF_I2C) && (xap->tDebug >= OL_FULL))     xI2CMsgPacketOut(a,b,c,d,0)
#endif
void dump_targetmem(xDAPPacket *xPacket,uint32_t addr);
void ReportMsgByAck(xDAPPort *dap,uint8_t ack)
{
	if(ack>SWD_ACK_WAIT)
	{
		if(ack==SWD_ACK_FATAL)
			IeP(dap->manstate, OPT_NF|OPT_PORT|OPT_CODE|OPT_ERR, NULL, ErrMsg(TE_NO_ACK, OPT_ERR));
		else
			IeP(dap->manstate, OPT_NF|OPT_PORT|OPT_CODE|OPT_ERR, NULL, ErrMsg(TE_INVALID_ACK, OPT_ERR));
	}
}
#ifdef STM32F10X_MD
static void Reinit_RST(int type)
{
	
	
}
#else
static void Reinit_RST(int type)
{
	if(!type)
		  /* Configure I/O pin nRESET */
	{
		PIN_nRESET_PORT->PCR[PIN_nRESET_BIT]       = PORT_PCR_MUX(1);  /* GPIO */
																		
		PIN_nRESET_GPIO->PSOR  = 1 << PIN_nRESET_BIT;                    /* High level */
		PIN_nRESET_GPIO->PDDR |= 1 << PIN_nRESET_BIT;                    /* Output */
	}else
	{
	  PIN_nRESET_PORT->PCR[PIN_nRESET_BIT]       = PORT_PCR_MUX(1)	|  /* GPIO */
                                               PORT_PCR_PE_MASK 	|  /* Pull enable */
                                               PORT_PCR_PS_MASK 	|  /* Pull-up */
                                               PORT_PCR_ODE_MASK;  /* Open-drain */
  PIN_nRESET_GPIO->PSOR  = 1 << PIN_nRESET_BIT;                    /* High level */
  PIN_nRESET_GPIO->PDDR |= 1 << PIN_nRESET_BIT;                    /* Output */
	}
}
#endif
void DAP_SetLed(int a, int b,int c)
{
}
void xUIMsg(uint8_t cmd, uint32_t opt, uint8_t port, uint8_t errco)
{
		xI2CMsgPacketOut(cmd, opt, port, MSGSTR[errco],0);
}
void vTaskDelay(int ms)
{
	Delayms(ms);
}

void xI2CMsgQPut(void *xPkt, uint8_t type)
{
}
void xI2CMsgQOptPut(uint8_t cmd, uint8_t opt, uint8_t code, uint8_t port, uint32_t addr, uint8_t *data)
{
}
uint32_t getTick()
{
	return HAL_GetTick();
}
int xTaskGetTickCount()
{
	return getTick();
}
//#define ADCx ADC0
 void ADC_SoftwareStartConv(void* ADC)
 {
#ifndef STM32F10X_MD
	 adcbuf0[0]=Read_ADC((ADC_Type*)ADC,0);
#else
	 adcbuf0[0]= get_targe_voltage();
#endif
	
 }	 
int  hw_version(int n)
{
	return 2;
}	
void power_control(uint8_t voltage, uint8_t port)
{
	switch(voltage)
	{
		case 0:
				output_0v_io();
				break;
		case 1:
				output_1v8_io();
				break;
		case 2:
				output_2v5_io();
				break;
		case 4:
				output_3v3_io();
				break;
		case 8:
				output_5v_io();
				break;
		default:
			break;
	}
		
}
char *ErrMsg(uint8_t errco, uint8_t opt)
{
	return MSGSTR[errco];
}
void bootloader(uint32_t fidx)
{}
int serial_log(xSerFile *xsf, uint32_t action)
{return 0;}
int serial_readlines(xSerFile *xsf, int port)
{return 0;}		
	

/*
 *
 * xConfig()
 *  id:
 *  data
 *  action: 
 *      0: get
 *      1: set
 *      2: save
 */
int xConfig(uint32_t id, uint8_t *data, uint8_t action)
{
    if (action == 0) {
    }
    if (action & 0x01) {
    }
    if (action & 0x02) {
    }
    return 0;
}
void printports(char *msgstr)
{
    U8 i,x=0;

    xDAPPort *dap = &pPort;
    if (xap->CICO & (DAP_INTF_UART|DAP_INTF_USB)) {
        iP("\nPort{");
        for(i=0;i<PORT_COUNT;i++) {
            if(dap->rport & (1<<i)) {
                iP("%s%d", (x++>0)?",":"", i+1);
            }
        }

        iP("} %s, elapsed %dms.", msgstr, xTaskGetTickCount() - dap->startTick);
    }
}
/*
 * check_alg_result()
 *      fco: fail code
 *      pco: pass code
 */
void check_alg_result(uint8_t fco, uint8_t pco)
{
    xDAPPort *dap = &pPort;
    int i;
		volatile  char *fmsg, *pmsg;
 ///   if (fco)
 ///       fmsg = ErrMsg(fco, OPT_ERR);
    if (pco)
        pmsg = (char *)MSGSTR[pco];

    dap->vport = 0;
    dap->rport = 0;
    for (i=0;i<PORT_COUNT;i++) {
        if (dap->xport & (1<<i)) {
            dap->port = (1<<i);
            if (dap->rets[i] > 0) {
                dap->vport |= (1<<i);
                dap->xport &= ~(1<<i);
                DAP_SetLed((1<<i), REDLED, LEDON);
                if (fco) {
                    //IwP(dap->manstate, OPT_NF|OPT_PORT|OPT_A2C|OPT_ERR, fco, fmsg);
                    wP("\ncheck_alg_result[%1X], %s", dap->manstate, fmsg);
                }
            } else {
                dap->rport |= (1<<i);
                if (pco) {
                    //IwP(dap->manstate, OPT_NF|OPT_PORT, NULL, pmsg);
                    wP("\ncheck_alg_result[%1X], %s", dap->manstate, pmsg);
                }
            }
        }
    }
}
void swd_calctime(void)
{
    int i = 0, j;
    uint32_t ti=0;
    uint32_t tEr, tPg, tBc, tVr, acc;

    if (xap->funToDo & DO_ERASESECTOR) {
#if 0        
        j = 0;
        for (i=1;i<MAX_SECTOR_COUNT;i++) {
            if ((xap->sectors[i].szSector == 0xFFFFFFFF) || (xap->sectors[i].startSector == 0xFFFFFFFF) || (xap->szData < xap->sectors[i].startSector)) {
                j += ((xap->szData - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
                break;
            } else {
                j += ((xap->sectors[i].startSector - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
            }
        }
        tEr = xap->toErase * j;
#else
        if (xap->szData > xap->szFlash)
            tEr = xap->toProg * xap->szFlash / xap->szProg;
        else
            tEr = xap->toProg * (xap->szData + xap->szProg)/xap->szProg;
#endif
        ti += tEr;
    }
    if (xap->funToDo & DO_ERASECHIP) {
#if 0        
        j = 0;
        for (i=1;j<MAX_SECTOR_COUNT;i++) {
            if (xap->sectors[i].szSector == 0xFFFFFFFF || xap->sectors[i].startSector == 0xFFFFFFFF) {
                j +=  ((xap->szFlash - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
                break;
            } else {
                j += ((xap->sectors[i].startSector - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
            }
        }
        tEr = j * xap->toErase;
#else
        tEr = xap->toProg * xap->szFlash/xap->szProg;
#endif        
        ti += tEr;
    }
    if (xap->funToDo & DO_BLANKCHECK) {
        tBc = xap->toErase >> 1;
        ti += tBc;
    }

    if (xap->funToDo & DO_PROGRAM) {
        if (xap->szData > xap->szFlash)
            tPg = (xap->szFlash * xap->toProg) / xap->szProg;
        else
            tPg = (xap->szData * xap->toProg) / xap->szProg;
        ti += tPg;
    }

    if (xap->funToDo & DO_CRC32) {
        if (xap->szData > xap->szFlash)
            tVr = (xap->szFlash * xap->toProg) / xap->szProg;
        else
            tVr = (xap->szData * xap->toProg) / xap->szProg;
        tVr = 1+tVr>>3;

        //tVr = (2 * xap->toErase * xap->szData)/xap->szFlash;  
        ti += tVr;
    }
    i = 0;
    /*
       xap->estimate[i++] = SM_CONNECT;
       xap->estimate[i++] = 100 * 255 / ti;
       xap->estimate[i++] = SM_DAPRESET;
       xap->estimate[i++] = 100 * 255 / ti;
       xap->estimate[i++] = SM_RESET_HALT;
       xap->estimate[i++] = 100 * 255 / ti;
       xap->estimate[i++] = SM_LOAD_CODE;
       xap->estimate[i++] = 100 * 255 / ti;
       */

    acc = 0;
    if (xap->funToDo & DO_BLANKCHECK) {
        xap->estimate[i++] = SM_BLANKCHECK;
        acc += 255 * tBc / ti;
        xap->estimate[i++] = acc;
        dP("\nBlankcheck ESTI:%d", acc);
    }

    if (xap->funToDo & DO_ERASESECTOR) {
        xap->estimate[i++] = SM_ERASESECTOR;
        j = 255 * tEr / ti;
        //j = (j > 200)?150:j;
        acc += j;
        xap->estimate[i++] = acc;
        dP("\nEraseSector ESTI:%d", acc);
    }

    if (xap->funToDo & DO_ERASECHIP) {
        xap->estimate[i++] = SM_ERASECHIP;
        j = 255 * tEr / ti;
        //j = (j > 200)?150:j;
        acc += j;
        xap->estimate[i++] = acc;
        dP("\nEraseChip ESTI:%d", acc);
    }

    if (xap->funToDo & DO_PROGRAM) {
        xap->estimate[i++] = SM_PROGPAGE;
        j = 255 * tPg / ti;
        //j = (j > 200)?150:j;
        acc += j;
        xap->estimate[i++] = acc;
        dP("\nProgram ESTI:%d", acc);
    }

    if (xap->funToDo & DO_CRC32) {
        xap->estimate[i++] = SM_CRC32;
        acc = 0xFF;
        xap->estimate[i++] = acc; //255 * tVr / ti;
        dP("\nVerify ESTI:%d", acc);
    }

    if (xap->estimate[i-1] < 0xFF)
        xap->estimate[i-1] = 0xFF;

    xap->estimate[i++] = 0xFF;
    xap->estimate[i++] = 0xFF;

}

extern int gProgOnLine;
extern int gStartProgI2C;
extern uint8_t aTxBuffer[I2CRXBUFFERSIZE];
	/* Buffers used for displaying Time and Date */
//uint8_t aShowTime[50] = {0};
uint8_t aShowDate[50] = {0};
char Timetemp[50];
int MsgOutput(MsgType type,const char fmt[], ...)
{
    va_list arp;
    static int rc;
    uint32_t szRW = 0;
    int res;
	  char logbuf[128];
		char buf[128];
	
	//char Timetemp[32];
		//int Ticktemp = xTaskGetTickCount();
	

		//RTC_GetCalendar(Timetemp,aShowDate);
		//sprintf(Timetemp,"%02d:%02d:%02d.%04d",(Ticktemp/1000)/3600,((Ticktemp/1000)/60)%60,((Ticktemp/1000)%3600)%60,Ticktemp%1000);
#ifdef __ONLINE__	
    if (gProgOnLine)//xap->logging > 0) 
		{
        va_start(arp, fmt);
        rc = vsprintf(buf, fmt, arp);
        va_end(arp);
			switch(type)
			{
				case USERMSG:
					sprintf(logbuf,"%s",buf);
					break;
				case ERRMSG:
					sprintf(logbuf,"[%s]Error:%s",Timetemp,buf);
					break;
				case WARNINGMSG:
					sprintf(logbuf,"[%s]Warning:%s",Timetemp,buf);
					break;
				case INFOMSG:
					sprintf(logbuf,"%s",buf);
					break;
				case DEBUGMSG:
					sprintf(logbuf,"[%s]Debug:%s",Timetemp,buf);
					break;
				case FULLMSG:
					sprintf(logbuf,"[%s]%s",Timetemp,buf);
					break;
				case IICMSG:
					sprintf(aTxBuffer,"[%s]%s",Timetemp,buf);
				default:
					sprintf(logbuf,"%s",buf);
					break;
				
			}
			if(type!=IICMSG)
					mtk_printf_usbser(logbuf,-1);
			//else
				//mtk_printf_iic(buf,-1);	
			#if 0
        if ((rc + logs) > 1024) {  // write logcache
            FIL logfile;
            if ((res = f_open(&logfile, (const TCHAR *)xap->logname, FA_OPEN_ALWAYS|FA_WRITE)) != 0) {
                printf("\nCan not Open Log file:%s, %s!", xap->logname, MSGSTR[res]);
                return -1;
            }
            /* Seek */
            res = f_lseek(&logfile, f_size(&logfile));
            if (res) {
                printf("\nSeek EOF failed, %s!", MSGSTR[res]);
                f_close(&logfile);
                return -1;
            }	
            res = f_write(&logfile, logcache, logs, &szRW);
            if (res) {
                printf("\nWrite Logging failed, %s!", MSGSTR[res]);
            }
			f_close(&logfile);
            logs = 0;
        }
        // copy logbuf to logcache
        for (szRW=0;szRW<rc;szRW++) 
            logcache[logs++] = logbuf[szRW];
				#endif
    }
#endif		
#ifndef __ONLINE__		
    if (xap->tDebug) {
        va_start(arp, fmt);
        rc = vprintf(fmt, arp);
        va_end(arp);
    }
#endif
    return rc;
}
int MsgErrOutput(const char fmt[], ...)
{
    va_list arp;
    static int rc;
    uint32_t szRW = 0;
    int res;
	  static char logbuf[128];
#ifdef __ONLINE__	
    if (gProgOnLine)//xap->logging > 0) 
		{
        va_start(arp, fmt);
        rc = vsprintf(logbuf, fmt, arp);
        va_end(arp);
			mtk_printf_usbser("\r\nError:",-1);
				mtk_printf_usbser(logbuf,-1);
			#if 0
        if ((rc + logs) > 1024) {  // write logcache
            FIL logfile;
            if ((res = f_open(&logfile, (const TCHAR *)xap->logname, FA_OPEN_ALWAYS|FA_WRITE)) != 0) {
                printf("\nCan not Open Log file:%s, %s!", xap->logname, MSGSTR[res]);
                return -1;
            }
            /* Seek */
            res = f_lseek(&logfile, f_size(&logfile));
            if (res) {
                printf("\nSeek EOF failed, %s!", MSGSTR[res]);
                f_close(&logfile);
                return -1;
            }	
            res = f_write(&logfile, logcache, logs, &szRW);
            if (res) {
                printf("\nWrite Logging failed, %s!", MSGSTR[res]);
            }
			f_close(&logfile);
            logs = 0;
        }
        // copy logbuf to logcache
        for (szRW=0;szRW<rc;szRW++) 
            logcache[logs++] = logbuf[szRW];
				#endif
    }
#endif		
#ifndef __ONLINE__		
    if (xap->tDebug) {
        va_start(arp, fmt);
        rc = vprintf(fmt, arp);
        va_end(arp);
    }
#endif
    return rc;
}
int iDAPOpen(void)
{
	extern uint8_t FlashBuf[];
	xDAPPort *dap = &pPort;
	dap->xport=(1<<PORT_COUNT)-1;
  //  FRESULT fres;
    xBuff = &xBuffer;
	xBuff->buffo = FlashBuf;
    xap = &xSapfile;
    xap->cache = sapcache;
    //xap->tDebug = 16;
    xap->tDebug = OL_INFO;
    xap->swj = SWJ_SWD;
    xap->dapspeed = SWJ_CLOCK_SPEED;
	
    xsf = (xSerFile *)&xSF;
    xsf->line = linecache;

    return 0;
}


/*---------------------------------------------------------------------------*/
#ifndef __SPIDER__
U32 SWD_FLUSH_CMD(xDAPPort *dap) 
{
	uint8_t *request = dap->bufo;
	//uint8_t response[512]={0};
	memset(xBuff->buffi,0,sizeof(xBuff->buffi));
//	disable_usb_irq();
	DAP_ProcessCommand(request, xBuff->buffi /*response*/);
//	enable_usb_irq();
	dap->cmdoff = 0;
	dap->cmdidx = 0;
	return *((U32*)&xBuff->buffi[4]);
}

#else
#endif

/*---------------------------------------------------------------------------*/
/*
weib:if SWD_FLUSH,flush;else get a long buf include one !SPS+multi data
*/
U32 swd_buff_cmd(U16 port, U8 req, U8 *data)
{
    xDAPPort *dap = &pPort;

    if ((port & SWD_NOBUFFER)==0) {
        if (dap->cmdidx++ == 0) {
            dap->cmdoff = 0;
            spi_preamble();
            dap->bufo[dap->cmdoff++] = 0x05;
            dap->bufo[dap->cmdoff++] = 0x00;
            dap->bufo[dap->cmdoff++] = dap->cmdidx;
        } else { 
            dap->bufo[2+SPIP] = dap->cmdidx;		// dap->cmdidx should be updated to result of ++
        }

        dap->bufo[dap->cmdoff++] = req;

        if ((req & SWD_RW_MASK)==0 || (req & SWD_RD_MASK) || (req & SWD_WR_MASK))	{	// Write or read value match
            dap->bufo[dap->cmdoff++] = data[0];
            dap->bufo[dap->cmdoff++] = data[1];
            dap->bufo[dap->cmdoff++] = data[2];
            dap->bufo[dap->cmdoff++] = data[3];
            if (req & SWD_WR_MASK) {
                dap->writemask = (data[0] | (data[1]<<8) | (data[2]<<16) | (data[3]<<24));
            }
        }
    }

/*
            dap->cmdoff = 0;
            spi_preamble();
            dap->bufo[dap->cmdoff++] = 0x05;
            dap->bufo[dap->cmdoff++] = 0x00;
			dap->bufo[dap->cmdoff++] = req;

        if ((req & SWD_RW_MASK)==0 || (req & SWD_RD_MASK) || (req & SWD_WR_MASK))	{	// Write or read value match
            dap->bufo[dap->cmdoff++] = data[0];
            dap->bufo[dap->cmdoff++] = data[1];
            dap->bufo[dap->cmdoff++] = data[2];
            dap->bufo[dap->cmdoff++] = data[3];
            if (req & SWD_WR_MASK) {
                dap->writemask = (data[0] | (data[1]<<8) | (data[2]<<16) | (data[3]<<24));
            }
        }
*/
    //if ((port & SWD_FLUSH)) {
        return SWD_FLUSH_CMD(dap);
    //}
//    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
#define swd_readDP(port, adr, outreg) dap->cmdidx=0;swd_buff_cmd(port, SWD(SWD_DP, SWD_READ, adr), outreg)
//#define swd_writeDP(port, adr, outreg) dap->cmdidx=0;swd_buff_cmd(port, SWD(SWD_DP, SWD_WRITE, adr), outreg)
U32 swd_writeDP(U16 port, U8 adr, U8 *outreg)
{
	xDAPPort *dap = &pPort;

	dap->cmdidx=0;
	if (adr == DP_SELECT)
		dap->dp_sel_value = *(U32*)outreg;
	
	swd_buff_cmd(port, SWD(SWD_DP, SWD_WRITE, adr), outreg);
	return 0;
}
/*---------------------------------------------------------------------------*/
#define swd_readAP(port, ap, adr, outreg) dap->cmdidx=0;*outreg=0;swd_accAP(port, ap, (adr|SWD_READ), outreg)
#define swd_writeAP(port, ap, adr, outreg) dap->cmdidx=0;swd_accAP(port, ap, (adr|SWD_WRITE), outreg)

U32 swd_memapWAP(U16 port, U8 adr, U32 *outreg) {
    U32 sel;
    xDAPPort *dap = &pPort;

    // Set up SELECT register for AP access
    sel = MEM_AP << 24;
    if (sel != dap->dp_sel_value) {
        swd_writeDP(swd_flush(port), DP_SELECT, (U8 *)&sel);	// write DP
        dap->dp_sel_value = sel;
    }
    return swd_buff_cmd(port, SWD(SWD_AP,0, adr|SWD_WRITE), (U8 *)outreg);
}
/*---------------------------------------------------------------------------*/
U32 swd_accAP(U16 port, U8 ap, U8 adr, U32 *outreg) {//AP registers
    U32 sel;
    xDAPPort *dap = &pPort;

    // Set up SELECT register for AP access
    sel = ap << 24 | (adr & 0xf0);
	
	
    if (sel != dap->dp_sel_value) {
        swd_writeDP(swd_flush(port), DP_SELECT, (U8 *)&sel);	// write DP
        dap->dp_sel_value = sel;
    }
		if(ap==MEM_AP)
			adr&=0xE;
    return swd_buff_cmd(port, SWD(SWD_AP,0, adr), (U8 *)outreg);
}
/*---------------------------------------------------------------------------*/
U32 memap_readBD0(U16 port, U32 address, U32 *value)
{
    xDAPPort *dap = &pPort;
    U32 temp;

    /* setup TAR */
    if ((dap->ap_tar_value & 0xfffffff0) != (address & 0xFFFFFFF0)) {
        temp = address & 0x0FFFFFFF0;
        swd_writeAP(port & 0x03ff, MEM_AP, MEM_AP_REG_TAR, &temp);
        dap->ap_tar_value = address & 0xFFFFFFF0;
    }

    /* 
     * Setup DP-SELECT 
    */
    if (dap->dp_sel_value != 0x00000010) {
        temp = 0x00000010;
        swd_writeDP(port, DP_SELECT, (U8 *)&temp);
        dap->dp_sel_value = temp;
    }

    return swd_buff_cmd(swd_flush(port), SWD(SWD_READ,SWD_AP,(address&0xC)), (U8 *)value);
}
/*---------------------------------------------------------------------------*/
U32 read_DHCSR(U16 port)
{
#if 1	
    return memap_readBD0(port, DCB_DHCSR, NULL);
#else
    xDAPPort *dap = &pPort;
    U32 temp = DCB_DHCSR;	 
    swd_writeAP(port & 0x03ff, MEM_AP, MEM_AP_REG_TAR, &temp);
    dap->ap_tar_value = 0xFFFFFFFF;

    return swd_readAP(swd_flush(port), MEM_AP, MEM_AP_REG_DRW, NULL);
#endif
}
/*---------------------------------------------------------------------------*/
U32 swd_WAP(U16 port, U8 ap, U32 address, U32 *value)//access mem
{

	xDAPPort *dap = &pPort;
   U32 temp = ap << 24;
    if (dap->dp_sel_value != temp) {
        swd_buff_cmd(swd_flush(port), DP_SELECT, (U8 *)&temp);
        dap->dp_sel_value = temp;
    }
	dap->cmdidx=0;
    swd_buff_cmd(port, SWD_AP|MEM_AP_REG_TAR, (U8 *)&address);
    return swd_buff_cmd(swd_flush(port), SWD_AP|MEM_AP_REG_DRW, (U8 *)value);
}
/*---------------------------------------------------------------------------*/
U32 swd_RAP(U16 port, U8 ap, U32 address)//access mem
{

	xDAPPort *dap = &pPort;
    U32 temp = ap << 24;
    if (dap->dp_sel_value != temp) {
        swd_buff_cmd(swd_flush(port), DP_SELECT, (U8 *)&temp);
        dap->dp_sel_value = temp;
    }
    swd_buff_cmd(port, SWD_AP|MEM_AP_REG_TAR, (U8 *)&address);
    return swd_buff_cmd(swd_flush(port), SWD_READ|SWD_AP|MEM_AP_REG_DRW, NULL);
}
/*---------------------------------------------------------------------------*/
U32 memap_writeBD0(U16 port, U32 address, U32 *value)
{
    xDAPPort *dap = &pPort;
    U32 temp;

    /* setup TAR */
    if ((dap->ap_tar_value & 0xfffffff0) != (address & 0xFFFFFFF0)) {
        temp = address & 0x0FFFFFFF0;
        swd_writeAP(port & 0x03ff, MEM_AP, MEM_AP_REG_TAR, &temp);
        dap->ap_tar_value = address & 0xFFFFFFF0;
    }

    /* Setup DP-SELECT */
    temp = 0x00000010;
    if (dap->dp_sel_value != temp) {
        swd_writeDP(port, DP_SELECT, (U8 *)&temp);
        dap->dp_sel_value = temp;
    }

    return swd_buff_cmd(swd_flush(port), SWD(SWD_WRITE,SWD_AP,(address&0xC)), (U8 *)value);
}
/*---------------------------------------------------------------------------*/
U32 prepCRegAccX(U16 port)
{
    xDAPPort *dap = &pPort;
    U32 temp;
    // set read mask
    if (dap->writemask != 0x00010000) {
        temp = 0x00010000;
        swd_writeDP((port & 0x03ff), SWD_WR_MASK, (U8 *)&temp);// write mask
        //swd_writeDP(swd_flush(port), SWD_WR_MASK, (U8 *)&temp);// write mask
    }
    if (dap->ap_tar_value != DCB_DHCSR) {
        // setup DHCSR as bank 0, write DHCSR to TAR
        temp = DCB_DHCSR;
        dap->ap_tar_value = temp;
        //swd_writeAP((port & 0x03ff), MEM_AP_REG_TAR, (U8 *)&temp);// write TAR as DCH_DHCSR
        swd_buff_cmd((port&0x3ff), SWD(SWD_AP,SWD_WRITE, MEM_AP_REG_TAR), (U8 *)&temp);
    }

    if (dap->dp_sel_value != MEM_AP_REG_BD0) {
        temp = MEM_AP_REG_BD0;
        dap->dp_sel_value = temp;
       return swd_writeDP(swd_flush(port), DP_SELECT, (U8 *)&temp);	// write DP
    }
    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
/* 
 * swd_coreRegX() should used with preCRegAccX()
 */
#define swd_readCREG(port, reg) swd_coreRegX(port, 0, reg, 0)
#define swd_writeCREG(port, reg, data) swd_coreRegX(port, 1, reg, data)
U32 swd_coreRegX(U16 port, U8 RnW, U8 reg, U8 *data)
{
    U32 temp;

    temp = reg & 0x1f;
    if (RnW == 1) {	// write DCRDR
        swd_buff_cmd(port & 0x03ff, SWD(SWD_AP,SWD_WRITE, (DCB_DCRDR & 0xF)), data);
        temp |= DCRSR_WnR;
    }

    // write DCRSR
    swd_buff_cmd(port & 0x03ff, SWD(SWD_AP,SWD_WRITE, (DCB_DCRSR & 0xF)), (U8 *)&temp);

    // read match S_REGRDY
    temp = S_REGRDY;
    swd_buff_cmd(port & 0x03ff, SWD(SWD_AP,(SWD_READ|SWD_RD_MASK), (DCB_DHCSR & 0xF)), (U8 *)&temp);


    if (RnW == 0) { // read
        swd_buff_cmd(port & 0x03ff, SWD(SWD_AP,SWD_READ, (DCB_DCRDR & 0xF)), (U8 *)&data);
    }
    return DAP_OK;
}
/* ---------------------------------------------------------------------------------------------------------------------------------*/
#define swd_block_read(port, count)	App_swd_block_transfer(port, 0, count, 0)
#define swd_block_write(port, count, buffer)	App_swd_block_transfer(port, 1, count, buffer)
U32 App_swd_block_transfer(U16 port, U8 RnW, U16 count, U8 *buffer)
{
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);

    dap->ap_tar_value = 0xFFFFFFFF;
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x06;
    dap->bufo[dap->cmdoff++] = 0x00;
    dap->bufo[dap->cmdoff++] = count & 0xFF;
    dap->bufo[dap->cmdoff++] = (count>>8);
    if (RnW == 0) {	// read
        dap->bufo[dap->cmdoff++] = SWD(SWD_AP, SWD_READ,MEM_AP_REG_DRW);
    } else {			// write
        dap->bufo[dap->cmdoff++] = SWD(SWD_AP, SWD_WRITE,MEM_AP_REG_DRW);
        while(count-- > 0) {
            dap->bufo[dap->cmdoff++] = *buffer++;
            dap->bufo[dap->cmdoff++] = *buffer++;
            dap->bufo[dap->cmdoff++] = *buffer++;
            dap->bufo[dap->cmdoff++] = *buffer++;
        }
    }
    return SWD_FLUSH_CMD(dap);
}

void dump_targetmem(xDAPPacket *xPacket,uint32_t addr)
{
			U16 port = xPacket->port;
			uint32_t temp = addr;
	    xDAPPort *dap = &pPort;
if(dbg_deep>4)
{
			swd_writeAP(port, MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
			dap->ap_tar_value=temp;

      swd_block_read(swd_flush(port), 0x14);
}
}
void  dump_targetmem_kinetis(xDAPPacket *xPacket,uint32_t addr)
{

				U16 port = xPacket->port;
			uint32_t temp = addr;
	    xDAPPort *dap = &pPort;

					swd_writeAP(port, MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
					dap->ap_tar_value=temp;

					swd_block_read(swd_flush(port), 0x14);
}
void write_targetmem(xDAPPacket *xPacket,uint32_t addr,uint16_t cnt,uint8_t *buf)
{
			U16 port = xPacket->port;
			uint32_t temp = addr;
	    xDAPPort *dap = &pPort;
			U8	*resp = FUNRESPONSE(xPacket);
			swd_writeAP(port, MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
			if(resp[2] == SWD_ACK_OK) {
			//dap->ap_tar_value=temp;
				while (cnt > SHORT_BLOCK_SIZE) {
						 swd_block_write(swd_flush(port), SHORT_BLOCK_SIZE>>2, buf);	
							buf += SHORT_BLOCK_SIZE;
							cnt-=SHORT_BLOCK_SIZE;
				}
				if(cnt>0)
				{
					cnt = (cnt>4)?(cnt>>2):1;
					swd_block_write(swd_flush(port),cnt,buf);
				}
		}
}
void read_targetmem(xDAPPacket *xPacket,uint32_t addr,uint16_t cnt,uint8_t *buf)
{
		U16 port = xPacket->port;
			uint32_t temp = addr;
	    xDAPPort *dap = &pPort;
			uint32_t val,count=cnt;
	
		{	
			swd_writeAP(port, MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
			//dap->ap_tar_value=temp;
                    cnt = (cnt>4)?(cnt>>2):1;			
      val=swd_block_read(swd_flush(port), cnt);
			memcpy(buf,(uint8_t *)&(xBuff->buffi[4]),count);
		}
}
void write_targetreg(xDAPPacket *xPacket,uint32_t regaddr,uint8_t ByteCnt,uint32_t buf)
{

	write_targetmem(xPacket,regaddr,ByteCnt,(uint8_t*)&buf);

}
void read_targetreg(xDAPPacket *xPacket,uint32_t regaddr,uint8_t ByteCnt,uint8_t *buf)
{

	read_targetmem(xPacket,regaddr,ByteCnt,(uint8_t*)buf);

}
uint8_t check_AtmelD20Lock(xDAPPacket *xPacket)
{
	uint32_t addr=0;
	uint32_t rwBuf=0;
	uint8_t LockFlag=0;
	addr=0x41004018;
	read_targetreg(xPacket,addr,2,(uint8_t *)&rwBuf);
	if(rwBuf&1<<8)//sec bit
		LockFlag=1;
	return LockFlag;

}
static void run_SAPSequence(xDAPPacket *xPacket,xSAPSeq *Seq,uint8_t SEQCnt)
{
	uint32_t rwBuf=0;
	//uint32_t MaskOffset=0;
	uint32_t CheckVal=0;
	xSAPSeq *tmpSeq=Seq;
	uint32_t addr=0;
	do{
		if(tmpSeq->RW)	//READ
		{
			//MaskOffset=tmpSeq->SeqVal;
			do{
				addr=(tmpSeq->SeqAddress)&(~(uint32_t)(3<<0));
				read_targetreg(xPacket,addr,tmpSeq->SeqLength+(tmpSeq->SeqAddress-addr),(uint8_t *)&rwBuf);
				rwBuf=(rwBuf>>((tmpSeq->SeqAddress-addr)*8));
				if(!tmpSeq->IsNeedConvert)//Check 1: 0-exit;0: 1-exit
				{	
					CheckVal=(rwBuf)&(tmpSeq->SeqVal);
				}else
				{
					CheckVal=((rwBuf&(tmpSeq->SeqVal)));
					if(CheckVal!=0)
						CheckVal=0;
					else
						CheckVal=1;
				}
			}while(CheckVal==0);
		}
		else
		{
				rwBuf=tmpSeq->SeqVal;
			write_targetreg(xPacket,tmpSeq->SeqAddress,tmpSeq->SeqLength,rwBuf);
		}
		tmpSeq++;
		SEQCnt--;	
	}while(SEQCnt>0);
}
/*-----------------------------------------------------------------------------------------------------------*/
/*
 *  cmd:
 *      0x80: reset DAP
 *      0x01: get current mode:
 *          resp[3]:
 *              0x00: under bootloader
 *              0x01: normal mode.
 */
U32 App_DAP_GetMode(uint16_t port, uint8_t cmd)
{
    xDAPPort *dap = &pPort;
    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x9D;    
    dap->bufo[dap->cmdoff++] = 0x01;    // Length
    dap->bufo[dap->cmdoff++] = cmd;       // Command

    return SWD_FLUSH_CMD(dap);
}
/*-----------------------------------------------------------------------------------------------------------*/
/*
 * mode:
 *      0: bootloader
 *      1: app 
 */
U32 App_DAP_SetMode(U16 port, U8 mode)
{
    xDAPPort *dap = &pPort;
    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x9F;
    dap->bufo[dap->cmdoff++] = mode;
    dap->bufo[dap->cmdoff++] = 0;
    dap->bufo[dap->cmdoff++] = 0;
    dap->bufo[dap->cmdoff++] = 0;
    dap->bufo[dap->cmdoff++] = 0;
    dap->bufo[dap->cmdoff++] = 0;
    dap->bufo[dap->cmdoff++] = 0;

    return SWD_FLUSH_CMD(dap);
}
/*-----------------------------------------------------------------------------------------------------------*/
/*
 * mode:
 */
U32 App_DAP_Flash(U16 port, uint16_t adr, uint16_t len, uint8_t *data)
{
    xDAPPort *dap = &pPort;
    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x9E;
    dap->bufo[dap->cmdoff++] = 0;
    dap->bufo[dap->cmdoff++] = 0;
    dap->bufo[dap->cmdoff++] = 0;
    dap->bufo[dap->cmdoff++] = adr & 0xFF;
    dap->bufo[dap->cmdoff++] = adr >> 8;
    dap->bufo[dap->cmdoff++] = len & 0xFF;
    dap->bufo[dap->cmdoff++] = len >> 8;
    while (len) { 
        dap->bufo[dap->cmdoff++] = *data++;
        len --;
    }

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_Info(U16 port, U8 id)
{
    xDAPPort *dap = &pPort;
    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x00;
    dap->bufo[dap->cmdoff++] = id;

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_Led(U16 port, U8 led, U8 onoff)
{
  #if 0  
		xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    if (dap->xport == 0)
        return 0;
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x01;
    dap->bufo[dap->cmdoff++] = led;
    dap->bufo[dap->cmdoff++] = onoff;
#endif
    return DAP_OK;//SWD_FLUSH_CMD(dap);
    //return SWD_FLUSH_CMD_PORT(dap, port);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_ChangePktSize(uint8_t ls)
{
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x92;
    dap->bufo[dap->cmdoff++] = ls;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_Init(U16 port)
{
    xDAPPort *dap = &pPort;
    int i;

    dap->dp_sel_value = 0xFFFFFFFF;
    dap->ap_csw_value = 0xFFFFFFFF;
    dap->ap_tar_value = 0xFFFFFFFF;
    dap->ap_cfg_value = 0xFFFFFFFF;
    dap->writemask	  = 0xFFFFFFFF;
    dap->cmdoff = 0; dap->cmdidx = 0;
    for (i=0;i<17;i++)
        if (i != 11)
            dap->regs[i] = 0;

    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
/*
 *     // swd_jtag==>
 *     //		0 = Default mode
 *     //		1 = SWD mode
 *     //		2 = JTAG mode
 */    	
U32 App_DAP_Connect(U16 port, U8 swd_jtag)	
{								
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);

    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x02;
    dap->bufo[dap->cmdoff++] = swd_jtag;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_Disconnect(U16 port)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x03;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_TransferConfigure(U16 port, U8 idle, U16 wait, U16 match)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x04;
    dap->bufo[dap->cmdoff++] = idle;
    dap->bufo[dap->cmdoff++] = wait & 0xff;
    dap->bufo[dap->cmdoff++] = wait >> 8;
    dap->bufo[dap->cmdoff++] = match & 0xff;
    dap->bufo[dap->cmdoff++] = match >> 8;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_WriteABORT(U16 port, U8 index, U32 Abort)	
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x08;
    dap->bufo[dap->cmdoff++] = index;   // fixed by Johnwin
    dap->bufo[dap->cmdoff++] = (Abort >> 0) & 0xFF;
    dap->bufo[dap->cmdoff++] = (Abort >> 8) & 0xFF;
    dap->bufo[dap->cmdoff++] = (Abort >> 16) & 0xFF;
    dap->bufo[dap->cmdoff++] = (Abort >> 24) & 0xFF;

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_TEST_Pins(U16 port, U8 dir, U8 val, U32 wait)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x89;    // ID_DAP_Vendor9
    dap->bufo[dap->cmdoff++] = dir;
    dap->bufo[dap->cmdoff++] = val;
    dap->bufo[dap->cmdoff++] = wait & 0xff;
    dap->bufo[dap->cmdoff++] = (wait>>8) & 0xff;
    dap->bufo[dap->cmdoff++] = (wait>>16) & 0xff;
    dap->bufo[dap->cmdoff++] = (wait>>24) & 0xff;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_SWJ_Pins(U16 port, U8 output, U8 select, U32 wait)		// delay us	
{						
    xDAPPort *dap = &pPort;
    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x10;
    dap->bufo[dap->cmdoff++] = output;
    dap->bufo[dap->cmdoff++] = select;
    dap->bufo[dap->cmdoff++] = wait & 0xff;
    dap->bufo[dap->cmdoff++] = (wait>>8) & 0xff;
    dap->bufo[dap->cmdoff++] = (wait>>16) & 0xff;
    dap->bufo[dap->cmdoff++] = (wait>>24) & 0xff;

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_SWJ_Clock(U16 port, U32 clock)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x11;
    dap->bufo[dap->cmdoff++] = (clock >>  0) & 0xff;
    dap->bufo[dap->cmdoff++] = (clock >>  8) & 0xff;
    dap->bufo[dap->cmdoff++] = (clock >> 16) & 0xff;
    dap->bufo[dap->cmdoff++] = (clock >> 24) & 0xff;

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_SWJ_Sequence(U16 port, U8 count, U8 *data)
{						
    xDAPPort *dap = &pPort;
    int i = 0;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x12;
    dap->bufo[dap->cmdoff++] = count;
    while(dap->cmdoff < (3+SPIP+count/8)) {
        dap->bufo[dap->cmdoff] = data[i] & 0xff;
        dap->cmdoff ++;
        i++;
    }

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_SWD_Configure(U16 port, U8 configuration)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x13;
    dap->bufo[dap->cmdoff++] = configuration;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_JTAG_Sequence(U16 port, U8 length, U8 *data)
{						
    xDAPPort *dap = &pPort;
    int i;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);

    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x14;
    for (i=0;i<length;i++)
        dap->bufo[dap->cmdoff++] = data[i];

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_JTAG_Configure(U16 port, U8 count, U8 IR)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x15;
    dap->bufo[dap->cmdoff++] = count;
    dap->bufo[dap->cmdoff++] = IR;

    return SWD_FLUSH_CMD(dap);
}

/*---------------------------------------------------------------------------*/
U32 App_DAP_JTAG_IDCODE(U16 port, U8 index)
{						
    xDAPPort *dap = &pPort;

    if (dap->cmdoff)
        SWD_FLUSH_CMD(dap);
    spi_preamble();
    dap->bufo[dap->cmdoff++] = 0x16;
    dap->bufo[dap->cmdoff++] = index;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 App_DAP_TransferAbort(U16 port)
{						
    xDAPPort *dap = &pPort;

    dap->cmdoff = 0;

    dap->bufo[dap->cmdoff++] = 0x07;

    return SWD_FLUSH_CMD(dap);
}
/*---------------------------------------------------------------------------*/
U32 dap_swj_sequence(U16 port, U8 swj)
{
    /* More than 50 TCK/SWCLK cycles with TMS/SWDIO high,
     * putting both JTAG and SWD logic into reset state.
     */

    U8 obu[8];
    U32 ret;
    int i;

    /* Switching sequence enables SWD and disables JTAG
     * NOTE: bits in the DP's IDCODE may expose the need for
     * an old/obsolete/deprecated sequence (0xb6 0xed).
     */

    for (i=0;i<7;i++)
        obu[i] = 0xFF;

    App_DAP_SWJ_Sequence(port, 51, obu);

    if (swj == SWJ_SWD) 
        obu[0] = 0x9E;
    else
        obu[0] = 0x3C;

    obu[1] = 0xe7;
   App_DAP_SWJ_Sequence(port, 16, obu);

    obu[0] = 0xFF;  obu[1] = 0xFF;
    ret =App_DAP_SWJ_Sequence(port, 51, obu);
    if (swj == SWJ_SWD) {
        obu[0] = 0;  obu[1] = 0;
        ret = App_DAP_SWJ_Sequence(port, 8, obu);
    }
    return ret;
}
/*---------------------------------------------------------------------------*/
void swd_printinfo(uint16_t port, uint8_t *resp, uint8_t *header)
{
    switch(resp[1]) {
        case 0:
            break;
        case 1:
            iP("\nPort[%1x]:%s [%d]", port, header, resp[2]); break;
        case 2:
            iP("\nPort[%1x]:%s [%d]", port, header, resp[2] + (resp[3]<<8)); break;
        default:
            iP("\nPort[%1x]:%s [%s]", port, header, &resp[2]); break;
    }
}

/*---------------------------------------------------------------------------*/
int swd_led(xDAPPacket *xPacket)
{
  #ifdef MC101_BOARD  
//	static uint8_t xport;
//    U16 port = xPacket->port;
 //   xDAPPort *dap = &pPort;
 //   uint16_t ledold;
    
	return DAP_GONEXT;
#if 0
    switch (dap->funstate) {
        case 0:
            xport = dap->xport;
            if (dap->led[0] == dap->led[1]) {
                dP("\nswd_led same[%08X/%08X]", dap->led[0], dap->led[1]);
                goto LED_RED;
            }
            dP("\nswd_led[%08X/%08X]", dap->led[0], dap->led[1]);
            port    = dap->led[0] & ((1<<PORT_COUNT) - 1);
            ledold = dap->led[1] & ((1<<PORT_COUNT) - 1);
            if (port == ledold) {
                goto LED_RED;
            }
            dP("\nswd_led[%1X] green on", port);

            if (port) {
                SYNCSEQ();
                JUMPFUN(1);
                dap->xport = port;
                dap->exp = App_DAP_Led(port, GREENLED, LEDON); 
                return DAP_OK;
            }
            dap->rport = dap->xport;

        case 1:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if (PORTSSYNC(dap)) {
                port = ~(dap->led[0]) & ((1<<PORT_COUNT) - 1);
                dP("\nswd_led[%1X] green off", port);
                if (port) {
                    SYNCSEQ();
                    JUMPFUN(2);
                    dap->xport = port;
                    dap->exp = App_DAP_Led(port,GREENLED, LEDOFF); 
                    return DAP_OK;
                }
                dap->rport = dap->xport;
            } else {
                return DAP_OK;
            }
        case 2:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if (PORTSSYNC(dap)) {
LED_RED:
                port   = (dap->led[0]>>16) & ((1<<PORT_COUNT) - 1);
                ledold = (dap->led[1]>>16) & ((1<<PORT_COUNT) - 1);
                if (port == ledold) {
                    goto LED_REDOFF;
                }
                dP("\nswd_led[%1X] red on", port);
                if (port) {
                    SYNCSEQ();
                    JUMPFUN(3);
                    dap->xport = port;
                    dap->exp = App_DAP_Led(port,REDLED, LEDON); 
                    return DAP_OK;
                }
                dap->rport = dap->xport;
            } else {
                return DAP_OK;
            }

        case 3:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if (PORTSSYNC(dap)) {
LED_REDOFF:
                SYNCSEQ();
                JUMPFUN(4);
                port = (~(dap->led[0]>>16)) & ((1<<PORT_COUNT) - 1);
                dP("\nswd_led[%1X] red off", port);
                if (port) {
                    dap->xport = port;
                    dap->exp = App_DAP_Led(port,REDLED, LEDOFF); 
                } else {
                    port   = (dap->led[0]>>16) & ((1<<PORT_COUNT) - 1);
                    dap->xport = port;
                    dap->exp = App_DAP_Led(port, REDLED, LEDON); 
                }
            }
            return DAP_OK;
            
        case 4:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if (PORTSSYNC(dap)) {
                dap->xport = xport;
                dap->led[1] = dap->led[0];
                return DAP_GONEXT;
            }

        default:
            break;
    }
		#endif
	#endif
    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
int swd_showled(xDAPPacket *xPacket)
{
    #ifndef MC101_BOARD
	U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    //U8	*resp = SUBRESPONSE(xPacket);

    switch (dap->substate) {
        case 0:
            SYNCSEQ();
            nextstate(dap);
            cP("Led: %d, OnOff:%d, Port:%02X", dap->regs[0], dap->regs[1], dap->regs[2]);
            dap->exp = App_DAP_Led(dap->regs[2], dap->regs[0], dap->regs[1]); 
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:
            break;
    }
		#endif
    return DAP_OK;
}


/*---------------------------------------------------------------------------*/
int swd_connect(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    int i;
    xDAPPort *dap = &pPort;
    U8	*resp = SUBRESPONSE(xPacket);
    uint32_t temp;
    switch (dap->substate) {
        case 0:
            INITSEQ(0);
            dap->timeout = 1000;
            //IiP(SM_CONNECT, OPT_NF|OPT_MSG, NULL, MSGSTR[ALG_CONN_START]);
            if (currcmd==SM_AUTO) {
                xUIMsg(SM_CONNECT, OPT_NF, 0, ALG_CONN_START);
            }
            iP("\n%s Port[%1x]", MSGSTR[ALG_CONN_START], dap->xport);
            App_DAP_Init(port); 
            nextstate(dap);
            dap->exp = App_DAP_Connect(swd_flush(dap->xport), xap->swj);
						/*weib add for ATMEL UNSEC 2015-07-07*/
				if (xap->vendor == (ATMEL << 24)) 
				{
					if(!gNeedErasePin)
							{
								if(!xap->SapExtraFun)
										gAtmelHasPlug=1;
								if(!gAtmelHasPlug)//Cold Plugging
								{
									uint32_t resp_t=resp[1];
									App_DAP_SWJ_Pins(port, 0x00, 0x01, 0x0); // SWCLK assert low
									App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0); // SWRST assert low
									Delayms(200);
									App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0); // SWRST assert high
									Delayms(200);
									if(0)//xap->SapExtraFun)
									{
										run_SAPSequence(xPacket,xap->SapExtraFun,xap->ExtraFunCnt);
									}
									gAtmelHasPlug=1;
									//return DAP_AGAIN;
									resp[1]=resp_t;
								}
							}		
					}							
/*weib add end*/
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[1] == xap->swj) {  // SWD, others failed.
                    dap->rport |= port;
                }else
									return DAP_AGAIN;
            }
            if (PORTSSYNC(dap)) {
                nextstate(dap);
                dap->exp = App_DAP_SWJ_Clock(swd_flush(dap->xport), xap->dapspeed);
            }
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    dap->rport |= port;
                } else {
                    // TODO
                    wP("\nDAP_SWJ_Clock Setting Fault(%1x) Clock[%d] Port[%1x] Err[%1x]", resp[1], xap->dapspeed, port, resp[1]);
                }
            }
            if (PORTSSYNC(dap)) {
                nextstate(dap);
                dap->exp = App_DAP_TransferConfigure(swd_flush(dap->xport), 0,0x64,0x64);
            }
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    dap->rport |= port;
                }
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                if (xap->swj == SWJ_SWD) {
                    JUMPSUB(4);
                    dap->exp = App_DAP_SWD_Configure(swd_flush(dap->xport), 0);
                } else {    // JTAG
                    JUMPSUB(10);
                    for (i=0;i<PORT_COUNT;i++) {
                        dap->rets[i] = 0;
                        dap->idcode[i] = 0;
                    }
                    dap->exp = dap_swj_sequence(swd_flush(dap->xport), xap->swj);
                }
            }
            return DAP_OK;
        case 10:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    dap->rport |= port;
                }
            }
            if (PORTSSYNC(dap)) {
                dap->retries = 0;
                JUMPSUB(11);
                /* get device JTAG device ID */
                dap->exp = App_DAP_JTAG_Sequence(port, 5, (U8 *)"\x02\x46\xFF\x01\xFF");
            }
            return DAP_OK;

        case 11:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    dap->rport |= port;
                }
            }
            if (PORTSSYNC(dap)) {
                JUMPSUB(12);
                /* get IR length? TODO: not sure whether it correct or not */
                dap->exp = App_DAP_JTAG_Sequence(port, 14, (U8 *)"\x03\x41\xFF\x02\xFF\x80\xFF\xFF\x00\xFF\xFF\xFF\x00\xFF`");
            }
            return DAP_OK;

        case 12:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    for(i=0;i<2;i++) {
                        temp = (resp[2+i*4] << 0) | (resp[3+i*4] << 8) | (resp[4+i*4] << 16) | (resp[5+i*4] << 24);
                        cP("\nJTAG ID response port[%1X], [%d] 0x%08X", port, i, temp);
                        if (temp != 0xFF00FFFF) {
                            if (temp == 0xFFFFFFFF || temp == 0) {  // remove PORTS
                                dap->xport &= ~port;
                                dap->port = port;
                                IwP(dap->manstate, OPT_NF|OPT_PORT|OPT_CODE|OPT_ERR, NULL, ErrMsg(TE_NO_ACK, OPT_ERR));
                                DAP_SetLed(port, REDLED, LEDON);
                            }
                            if (temp & 1) {
                                if (dap->rets[P2I(port)] == 0)
                                    dap->idcode[P2I(port)] = temp;  // keep first ID code
                                dap->rets[P2I(port)] += 1;          // save device count
                            }
                            dap->vport |= port;
                        } else {
                            dap->rport |= port;
                            dap->vport &= ~port;
                        }
                    }
                }
            }
            if (dap->retries ++ > 4) {  // max 8 devices
                dap->xport = dap->rport;
            } else
            if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp = App_DAP_JTAG_Sequence(port, 10, (U8 *)"\x01\x80\xFF\xFF\x00\xFF\xFF\xFF\x00\xFF`");
            }
            if (PORTSSYNC(dap)) {
                int err = 0;
                temp = 0;
                for (i=0;i<PORT_COUNT;i++) {    // idcode identical check
                    if (dap->xport & (1<<i)) {
                        if (temp > 0 && temp != dap->idcode[i])
                            err = 1;
                        temp = dap->idcode[i];
                    }
                }
                temp = 0;
                for (i=0;i<PORT_COUNT;i++) {    // device count identical check
                    if (dap->xport & (1<<i)) {
                        if (temp > 0 && temp != dap->rets[i])
                            err = 1;
                        temp = dap->rets[i];
                    }
                }
                if (err) {
                    for (i=0;i<PORT_COUNT;i++) {    // device count identical check
                        if (dap->xport & (1<<i)) {
                            xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, (1<<port), TE_TARGET_DIFF);
                            DAP_SetLed((1<<i), REDLED, LEDON);
                            dap->xport &= ~(1<<i);
                        }
                    }
                    return DAP_ERROR;
                }
                SYNCSEQ();
                JUMPSUB(13);
                /* reset */
                dap->exp = App_DAP_JTAG_Sequence(port, 5, (U8 *)"\x02\x46\xFF\x01\xFF");
            }
            return DAP_OK;

        case 13:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    dap->rport |= port;
                }
            }
            if (PORTSSYNC(dap)) {
                JUMPSUB(14);
                /* get IR length? TODO: not sure whether it correct or not */
                dap->exp = App_DAP_JTAG_Sequence(port, 14, (U8 *)"\x03\x42\xFF\x02\xFF\x80\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF`");
            }
            return DAP_OK;
            
        case 14:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    int j, k=0;
                    for(i=0;i<2;i++) {
                        temp = (resp[2+i*4] << 0) | (resp[3+i*4] << 8) | (resp[4+i*4] << 16) | (resp[5+i*4] << 24);
                        cP("\nJTAG IR response port[%1X], [%d] 0x%08X", port, i, temp);
                        for (j=0;j<32;j++) {
                            if (temp & (1<<j)) {
                                k++;
                            } else {
                                k = 0;
                            }
                            if (k > 1)
                                break;
                        }
                        if (k > 1)
                            break;

                    }
                    dap->regs[P2I(port)] = i * 32 + j - 1;
                    dap->rport |= port;
                }
            }

            if (PORTSSYNC(dap)) {
                int err = 0, dc = 0, irl = 0;
                for (i=0;i<PORT_COUNT;i++) {    // ir length identical check
                    if (dap->xport & (1<<i)) {
                        if (irl > 0 && irl != dap->regs[i])
                            err = 1;
                        irl = dap->regs[i];
                        dc = dap->rets[i];
                    }
                }
                if (err) {
                    for (i=0;i<PORT_COUNT;i++) {    
                        if (dap->xport & (1<<i)) {
                            xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, (1<<port), TE_TARGET_DIFF);
                            DAP_SetLed((1<<i), REDLED, LEDON);
                            dap->xport &= ~(1<<i);
                        }
                    }
                    return DAP_ERROR;
                }
                cP("\nDevice Count = %d, IR length = %d.", dc, irl);
                SYNCSEQ();
                JUMPSUB(15);
                dap->exp = App_DAP_JTAG_Sequence(port, 5, (U8 *)"\x02\x46\xFF\x01\xFF");
                dap->exp = App_DAP_JTAG_Configure(port, dc, irl);
            }
            return DAP_OK;
        case 15:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    dap->rport |= port;
                }
            }
            if (PORTSSYNC(dap)) {
                JUMPSUB(7);
                /* get IR length? TODO: not sure whether it correct or not */
                dap->regs[16] = DAP_GONEXT;
                dap->funstate = 0;
                return swd_led(xPacket);
            }
            return DAP_OK;


        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    dap->rport |= port;
                }
            }
            if (PORTSSYNC(dap)) {
                nextstate(dap);
                if ((xap->resett & C_UNDER_RST) &&  (xap->resett & RST_HW)) {
                    dP("\nUnder Reset: Assert Reset Low...");
                    App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0); // assert low
                //} else {
                //   App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);  // set as input
                }
                dap->exp = dap_swj_sequence(swd_flush(dap->xport), xap->swj);
            }
            return DAP_OK;

        case 5:
            if (PORTSEQEQ(port)) {
                if (resp[1] == DAP_OK) {
                    dap->rport |= port;
                }
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                nextstate(dap);
                dap->exp = swd_readDP(swd_flush(dap->xport), DP_IDCODE, NULL);
            }
            return DAP_OK;

        case 6:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    dap->port = port;
                    dap->idcode[P2I(port)] = (resp[3] << 0) | (resp[4] << 8) | (resp[5] << 16) | (resp[6] << 24);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            } 

            if (PORTSSYNC(dap)) {
                uint32_t temp = 0, errdapid = 0;
                for(port=0;port<PORT_COUNT;port++) {
                    if(dap->xport & (1<<port)) {
                        uP("IDCODE: 0x%08X\r\n",dap->idcode[port]);
                        if (currcmd==SM_AUTO) {
                            dap->port = (1<<port);
                            IiP(SM_CONNECT, OPT_PORT|OPT_NF|OPT_ADDR, dap->idcode[port], NULL);
                        }
                        if (temp > 0 && temp != dap->idcode[port]) {
                            errdapid = 1;
                            eP("\n%s", ErrMsg(TE_TARGET_DIFF, OPT_ERR));   
                        }
                        temp = dap->idcode[port];
                    }
                }

                DAP_SetLed(dap->xport, GREENLED, LEDON);
                if (errdapid) {
                    for(port=0;port<PORT_COUNT;port++) {
                        if(dap->rport & (1<<port)) {
                            xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, (1<<port), TE_TARGET_DIFF);
                        }
                    }
                    DAP_SetLed(dap->rport, REDLED, LEDON);
                    xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, NULL, TE_TARGET_DIFF);
                    dap->regs[16] = DAP_ERROR;
                    dap->funstate = 0;
                    JUMPSUB(7);
                    dap->xport = 0;
                    return swd_led(xPacket);
                    //return DAP_ERROR;
                }

                if (xap->dapid > 0 && temp > 0 && xap->dapid != temp) {
                    eP("\n%s", ErrMsg(TE_TARGET_DAPID_MISMATCH, OPT_ERR));   
                    DAP_SetLed(dap->rport, REDLED, LEDON);
                    xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, 0, TE_TARGET_DAPID_MISMATCH);
                    dap->regs[16] = DAP_ERROR;
                    dap->funstate = 0;
                    JUMPSUB(7);
                    dap->xport = 0;
                    return swd_led(xPacket);
                    //return DAP_ERROR;
                }
#if 0
                xap->dapstate |= PREQ_CONNECTED;
                return DAP_GONEXT;
#else
                dap->regs[16] = DAP_GONEXT;
                dap->funstate = 0;
                JUMPSUB(7);
                return swd_led(xPacket);
                //return DAP_GONEXT;
#endif

            }
			return DAP_OK;
        case 7:
            if (DAP_GONEXT != swd_led(xPacket))
                return DAP_OK;

            xap->dapstate |= PREQ_CONNECTED;
            return dap->regs[16];

        default:
            break;
    }
    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
/*
 * select ap 0
 *  read AP_REG_BASE
 *  read AP_REG_IDR(apid)
 *
 */
int swd_getdbgbase(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:		
            SYNCSEQ();
            temp = 0x000000F0;
            JUMPFUN(1);
            dap->exp = swd_writeDP(swd_flush(port), DP_SELECT, (U8 *) &temp);
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(2);
                dap->exp = swd_readAP(swd_flush(port), MEM_AP, MEM_AP_REG_BASE, &temp);//weib mofify 01-09-2015
                //dap->exp = swd_buff_cmd(swd_flush(port), SWD_READ|SWD_AP|0x08, NULL); 
                //dap->exp = swd_RAP(port, MEM_AP, 0x08);
            }
            return DAP_OK;
        case 2:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    dap->rets[P2I(port)] = temp;
                    dP("\nPort[%1x] debug base = 0x%08x", port, temp);
                }else
									return DAP_AGAIN;
            }
#if 0      // skip read APID
            if (PORTSSYNC(dap)) {
                JUMPFUN(3);
                dap->exp = swd_readAP(swd_flush(port), MEM_AP, 0x0C, &temp);
                //dap->exp = swd_RAP(port, MEM_AP, 0x0C);
                //return DAP_GONEXT;
            }
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    dP("\nPort[%1x] APID = 0x%08x", port, temp);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
#endif
            if (PORTSSYNC(dap)) {
                int i, errstop = 0;
                temp = 0;
                for (i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                        if (temp > 0 && dap->rets[i] != temp) { // error, dbgbase not matched
                            errstop = 1;
                            break;
                        }
                        temp = dap->rets[i];
                    }
                }
                if (errstop == 1) {
                    for (i=0;i<PORT_COUNT;i++) {
                        if (dap->xport & (1<<i)) {
                            xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_TARGET_DIFF);
                            cP("\n%s", MSGSTR[TE_TARGET_DIFF]);
                            DAP_SetLed((1<<i), REDLED, LEDON);
                        }
                    }
                    dap->xport = 0;
                    return DAP_ERROR;
                }
                dap->regs[0] = temp & 0xFFFFF000;   // keep debug base address for swd_readROM
                dap->regs[1] = temp & 0xFFFFF000;
                dap->regs[2] = temp & 0xFFFFF000;   
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:				
            break;
    }
    return DAP_OK;
}

/*---------------------------------------------------------------------------*/
int swd_MDMAP_RESET(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:		
            SYNCSEQ();
            JUMPFUN(1);
 ////????           dap->dp_sel_value = 0;
            temp = 0x01000000;
            dap->exp = memap_writeBD0(port, DCB_DEMCR, &temp);
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(2);
                temp = 0x00000001;
                dap->exp = memap_writeBD0(port, DCB_DEMCR, &temp);
            }
            return DAP_OK;
        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(3);
                temp = DBGKEY | C_DEBUGEN | C_HALT;
                dap->exp = memap_writeBD0(port, DCB_DHCSR, &temp);
            }
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}////weib add for unstable error
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(4);
                temp = 0;
                dP("\nMDMAP CTRL = 0");
                dap->exp = swd_writeAP(swd_flush(port), MDM_AP, MDM_AP_REG_CTRL, &temp);
            }
            return DAP_OK;

        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(5);
                dap->retries = 0;
                dap->exp = read_DHCSR(port);
            }
            return DAP_OK;

        case 5:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(6);
                temp = DBGKEY | C_DEBUGEN | C_HALT;
                dap->exp = memap_writeBD0(port, DCB_DHCSR, &temp);
            }
            return DAP_OK;

        case 6:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(7);
                temp = 0x00000001;
                dap->exp = memap_writeBD0(port, DCB_DEMCR, &temp);
            }
            return DAP_OK;

        case 7:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(8);
                dap->exp = read_DHCSR(port);
            }
            return DAP_OK;

        case 8:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:				// runUnInit
            break;
    }
    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
int swd_MDMAP(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:		
            SYNCSEQ();
            JUMPFUN(2);
            temp = 0;
            dap->exp = swd_readAP(swd_flush(port), MDM_AP, MDM_AP_REG_STS, &temp);
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    dap->rets[P2I(port)] = temp;
                    cP("Port [%X] MDMAP_STATUS: %08X", port, temp);
                    dap->rport |= port;
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, port, TE_UNKNOWN_ERR);
                }
            }

            if (PORTSSYNC(dap)) {
                int i;
                dap->rport = 0;
                dap->vport = 0;
                for (i=0;i<PORT_COUNT;i++) {
										dap->rets[i] |= KINETIS_SECURED;//always mass erase!! weib add 2015-0715 
                    if (dap->xport & (1<<i)){
                        if (dap->rets[i] & KINETIS_SECURED) {
                            dap->vport |= (1<<i);
                            cP("\nPort [%X], MCU Secured!", (1<<i));
                            xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_TARGET_SECURED);
                        } else {
                            dap->rport |= (1<<i);
                        }
                    }
                }

                SYNCSEQ();
                if (dap->vport) {
                    //if (currcmd == SM_AUTO || currcmd == SM_ERASECHIP) { //(xap->funToDo & DO_KINETIS_ME)) {
                    if (xap->vendor == (KINETIS << 24))
										{
												dap->regs[10] = dap->rport;     // store ports that not need mass erase
                        cP("\nKinetis Mass Erasing... Port[%X]", dap->vport);
                        dap->vport = 0;
                        dap->funstate = 3;
                        dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST Low
                        dap->retries = 0;
                        temp = 0x00000008;
                        dap->exp = swd_writeAP(swd_flush(port), MDM_AP, MDM_AP_REG_CTRL, &temp);
                        return DAP_OK;
                    } else {    // release under reset
                        dap->xport = dap->vport;
                        if (xap->resett & C_UNDER_RST) {
                            if (xap->resett & RST_HW) {
                                dP("\nUnder Reset: Release Under Reset[%1X]", dap->xport);
                                App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0); // assert high
                            }
                        }
                        DAP_SetLed(dap->vport, REDLED, LEDON);
                    }
                }

                dap->xport = dap->rport;

                if (dap->xport == 0) {
                    return DAP_ERROR;
                }

                if (xap->swj == SWJ_JTAG) {
                    JUMPFUN(8);
                } else {
                    JUMPFUN(6);
                }
                temp = 0x14;    // enable debug.
                dap->exp = swd_writeAP(swd_flush(port), MDM_AP, MDM_AP_REG_CTRL, &temp);
            }
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, port, TE_UNKNOWN_ERR);
                }
            }

            if (PORTSSYNC(dap)) {
                if ((dap->regs[10] == dap->xport)) { // cannot not continue
                    SYNCSEQ();
                    JUMPFUN(6);
                    temp = 0x14;    // enable debug.
                    dap->exp = swd_writeAP(swd_flush(port), MDM_AP, MDM_AP_REG_CTRL, &temp);
                    return DAP_OK;
                }
                JUMPFUN(4);
                dap->rport = dap->regs[10];
                temp = 0x00000009;
                dap->exp = swd_writeAP(swd_flush(port), MDM_AP, MDM_AP_REG_CTRL, &temp);
            }
            return DAP_OK;
        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, port, TE_UNKNOWN_ERR);
                }
            }

            if (PORTSSYNC(dap)) {
                if ((dap->regs[10] == dap->xport)) { // cannot not continue
                    SYNCSEQ();
                    JUMPFUN(6);
                    temp = 0x14;    // enable debug.
                    dap->exp = swd_writeAP(swd_flush(port), MDM_AP, MDM_AP_REG_CTRL, &temp);
                    return DAP_OK;
                }

                JUMPFUN(5);
                dap->rport = dap->regs[10];
                dap->exp = swd_readAP(swd_flush(port), MDM_AP, MDM_AP_REG_STS, &temp);
            }
            return DAP_OK;

        case 5:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    dP("\nPort[%X]: MDMAP_STS: %08X", port, temp);
                    if (temp & 0x04)
                        dap->vport |= port;     // security on
                    else
                        dap->rport |= port;
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, port, TE_UNKNOWN_ERR);
                }
            }

            if (dap->retries ++ > 1000) {
                int i;
                for(i=0;i<PORT_COUNT;i++) {
                    if ((dap->xport ^ dap->rport) & (1<<i)) {
                        xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_UNKNOWN_ERR);
                        DAP_SetLed((1<<i), REDLED, LEDON);
                    }
                }
                dap->xport = dap->rport;
                //dap->regs[10] = dap->rport & dap->regs[10];
            } else
            if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp = swd_readAP(swd_flush(port), MDM_AP, MDM_AP_REG_STS, &temp);
                return DAP_OK;
            }

            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                //dap->xport |= dap->regs[10]; // restore PORT settings
                dap->xport = dap->rport;
                if (xap->swj == SWJ_JTAG) {
                    JUMPFUN(8);
                } else {
                    JUMPFUN(6);
                }
                temp = 0x14;    // enable debug.
                dap->exp = swd_writeAP(swd_flush(port), MDM_AP, MDM_AP_REG_CTRL, &temp);
            }
            return DAP_OK;
        case 6:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, port, TE_UNKNOWN_ERR);
                }
            }

            if (PORTSSYNC(dap)) {
                dP("\nUnder Reset: Assert RESET High...");
                JUMPFUN(7);
                dap->retries = 0;
                dap->exp = App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);		// assert nRST high
            }
            return DAP_OK;

        case 7:
            if (PORTSEQEQ(port)) {
							#ifdef MC101_BOARD
								if (1) 
							#else
                if ((resp[1]&0x80))             // wait reset high 
              #endif   
									dap->rport |= port;
                else
                    dap->vport |= port;
            }

            if (dap->retries++ > 30) {
                int i;
                for(i=0;i<PORT_COUNT;i++) {
                    if ((dap->xport ^ dap->rport) & (1<<i)) {
                        xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_UNKNOWN_ERR);
                        DAP_SetLed((1<<i), REDLED, LEDON);
                    }
                }
                dap->rport = dap->xport;
            } else
            if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp = App_DAP_SWJ_Pins(port, 0x0, 0x0, 0x0);		// read only
                return DAP_OK;
            }

            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                JUMPFUN(8);
                dap->exp = App_DAP_SWJ_Pins(port, 0x10, 0x10, 0x0);		// set reset as input
            }
            return DAP_OK;

        case 8:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }

        default:				// runUnInit
            break;
    }
    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
/**
 * swd_chkSTM32
 *  1. READ STM32 DBGMCU_IDCODE
 *  2. Read Security Level
 *
 */
int swd_chkSTM32(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    int i, err;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:		
            SYNCSEQ();
            JUMPFUN(1);
            temp = 0;
            swd_writeDP(port, DP_SELECT, (U8 *) &temp);

            temp = MCU_IDCODE;
            for(i=0;i<PORT_COUNT;i++) {
                if (dap->xport & (1<<i)) {
                    if (dap->idcode[i] == Cortex_M0)
                        temp = MCU_IDCODE_M0;
                    break;
                }
            }
            dap->exp = swd_RAP(port, 0, temp);
            return DAP_OK;


        case 1:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {    // MCU_IDCODE readed
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    dap->rets[P2I(port)] = temp;
                    dap->rport |= port;
                    cP("\nPort[%1X] STM32 DBGMCU_IDCODE = 0x%08X", port, temp);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                temp = 0;
                err= 0;
                for (i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                        if (temp && temp != dap->rets[i]) { // MCU_IDCODE not matched
                            err = 1;
                        }
                        temp = dap->rets[i];
                    }
                }
                if (err == 1) {
                    return DAP_ERROR;
                }
                switch (temp & 0xFFF) {
                    case 0x411:         // STM32F205/207/215/217
                    case 0x413:         // STM32F405/407/415/417
                    case 0x419:         // STM32F42xx/43xx
                        dap->regs[0] = FLASH_OPTCR;
                        break;
                    default:            // F0,F1,F3
                        dap->regs[0] = FLASH_OBR;
                }
                JUMPFUN(2);
                dap->exp = swd_RAP(port, 0, dap->regs[0]);
                return DAP_OK;
            }
            return DAP_OK;
        case 2:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {    // MCU_IDCODE readed
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    dap->rets[P2I(port)] = temp;
                    dP("\nPort[%1X] FLASH_OBR = 0x%08X", port, temp);
                    if (((dap->regs[0] == FLASH_OBR)   && ( temp & 2)) ||
                        ((dap->regs[0] == FLASH_OPTCR) && ((temp & 0xFF00) != 0xAA00))) {
                        cP("\nPort[%1X] Target Secured.", port);
                        xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, port, TE_TARGET_SECURED);
                        if (currcmd == SM_AUTO) {
                            dap->rport |= port;     // continue
                        } else {
                            dap->xport &= ~port;
                            DAP_SetLed(port, REDLED, LEDON);
                        }
                    } else {
                        dap->rport |= port;
                    }
                }
            }
            if (PORTSSYNC(dap)) {
                if (dap->xport == 0)
                    return DAP_ERROR;
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:				// runUnInit
            break;
    }
    return DAP_OK;
}
/*---------------------------------------------------------------------------*/

int swd_readROM(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    int i;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:		
            SYNCSEQ();
            JUMPFUN(1);
            temp = 0;
            swd_writeDP(port, DP_SELECT, (U8 *) &temp);

            temp = dap->regs[1] | 0xFD0;
            dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                JUMPFUN(2);
 //               swd_block_read(port, 12);
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
            }
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    if ((temp & 0xF0000040) == 0xF0000040)
                        dap->rport |= port;
										else
										{
											ReportMsgByAck(dap,resp[2]);
											return DAP_AGAIN;
										}
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (resp[0] == 0x06) {  // ROM Table
                if (resp[3] == SWD_ACK_OK) {
                    int j=resp[1] | resp[2] << 8;
                    uint32_t CID, PID0, PID1;

                    if (j == 12) {
                        PID1 = resp[ 4] |  resp[ 8]<<8 | resp[12]<<16 | resp[16]<<24;
                        PID0 = resp[20] |  resp[24]<<8 | resp[28]<<16 | resp[32]<<24;
                        CID  = resp[36] |  resp[40]<<8 | resp[44]<<16 | resp[48]<<24;
                        cP("\nPort[%1x] offset[%08X], CID[%08X], PID[%08X], PID1[%08X]", port, dap->regs[1] | 0xFD0, CID, PID0, PID1);
                        dap->regs[P2I(port)+12] = CID;

                        switch ((CID>>12) & 0xF) {
                            case 0x00:  
                                cP("\n\tGeneric verification component.");
                                break;
                            case 0x01:  // ROM table
                                cP("\n\tROM Table.");
                                if (xap->deviceid[1] && xap->deviceid[1] != PID1) {
                                    xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, port, TE_TARGET_ID_MISMATCH);
                                    dap->xport &= ~port;
                                    DAP_SetLed(port, REDLED, LEDON);
                                    cP("\nPort[%1x] %s", port, ErrMsg(TE_TARGET_ID_MISMATCH, OPT_ERR));
                                }
                                if (xap->deviceid[0] && xap->deviceid[0] != PID0) {
                                    xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, port, TE_TARGET_ID_MISMATCH);
                                    dap->xport &= ~port;
                                    DAP_SetLed(port, REDLED, LEDON);
                                    cP("\nPort[%1x] %s", port, ErrMsg(TE_TARGET_ID_MISMATCH, OPT_ERR));
                                }
                                dap->regs[P2I(port)+4] = PID1;
                                dap->regs[P2I(port)+8] = PID0;
                                break;
                            case 0x09:  // Debug component
                                cP("\n\tDebug component.");
                                break;
                            case 0x0B:  // Peripheral Test Block (PTB)
                                cP("\n\tPeripheral Test Block (PTB).");
                                break;
                            case 0x0D:  // OptimoDE Data Engine SubSystem (DESS) component
                                cP("\n\tOptimoDE Data Engine SubSystem (DESS) component.");
                                break;
                            case 0x0E:  // Generic IP component
                                cP("\n\tGeneric IP component.");
                                break;
                            case 0x0F:  // PrimeCell peripheral
                                cP("\n\tPrimeCell peripheral.");
                                break;
                            default:
                                cP("\n\tReserved.");
                                break;
                        }

                    }
                }else{
                    dP("\nswd_readROM()::Block Transfer ERROR(%02x)", resp[3]);
                }
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                JUMPFUN(3);
                if (dap->regs[15] == 0) {
                    for (i=0;i<PORT_COUNT;i++) {
                        if (dap->xport & (1<<i)) {
                            if (dap->regs[12+i] == 0xB105E00D) {
                                dap->regs[15] = 1;
                                temp = 0x01000000;
                                memap_writeBD0(port, DCB_DEMCR, &temp);
                                break;
                            }
                        }
                    }
                }

                temp = dap->regs[2];
                dap->regs[2] += 4;
                swd_writeAP(port, MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
                dap->exp = swd_readAP(swd_flush(port), MEM_AP, MEM_AP_REG_DRW, &temp);		// read DRW
            }
            return DAP_OK;
            
        case 3:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    dap->rets[P2I(port)] = temp;
                    dP("\nPort[%1x] ROM Table Offset=0x%08x", port, temp);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                int errstop=0;
                uint32_t cid = 0;
                temp = 0;
                for (i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                        if (temp > 0 && dap->rets[i] != temp) { // error, ROM table offset not matched
                            errstop = 1;
                        }
                        cid = dap->regs[i+12];
                        temp = dap->rets[i];
                    }
                }
                if (errstop == 1) {
                    for (i=0;i<PORT_COUNT;i++) {
                        if (dap->xport & (1<<i)) {
                            cP("\n%s", ErrMsg(TE_TARGET_DIFF, OPT_ERR));   
                            xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_TARGET_DIFF);
                        }
                    }
                    DAP_SetLed(dap->xport, REDLED, LEDON);
                    dap->xport = 0;
                    return DAP_ERROR;
                }
                SYNCSEQ();
                if (temp > 0 && cid > 0) {
                    dap->regs[1] = dap->regs[0] + temp & 0xFFFFF000;
                    return DAP_GOON;
                } else {
                    JUMPFUN(4);
                    temp = SCB_CPUID;
                    swd_writeAP(port, MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
                    dap->exp = swd_readAP(swd_flush(port), MEM_AP, MEM_AP_REG_DRW, &temp);		// read DRW
                    return DAP_OK;
                }
            }

        case 4:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    dap->rets[P2I(port)] = temp;
                    cP("\nPort[%1x] CPUID=0x%08x", port, temp);
                }else
								{
																{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}//weib add for unstable error
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(5);
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
            }
            return DAP_OK;

        case 5:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    if ((temp & 0xf0000040) != 0xf0000040)
                        cP("\nPort[%1x] ERROR:0x%08x", port, temp);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                int errstop=0;
                temp = 0;
                for (i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                        if (temp > 0 && dap->rets[i] != temp) { // error, CPUID not the same
                            errstop = 1;
                        }
                        temp = dap->rets[i];
                    }
                }
                if (errstop == 1) {
                    for (i=0;i<PORT_COUNT;i++) {
                        if (dap->xport & (1<<i)) {
                            cP("\n%s", ErrMsg(TE_TARGET_DIFF, OPT_ERR));   
                            xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_TARGET_DIFF);
                        }
                    }
                    cP("\nCPUID Mismatched!");
                    DAP_SetLed(dap->xport, REDLED, LEDON);
                    dap->xport = 0;
                    return DAP_ERROR;
                }
                if (xap->cpuid > 0 && temp > 0 && xap->cpuid != temp) {
                    cP("\n%s", ErrMsg(TE_TARGET_CPUID_MISMATCH, OPT_ERR));   
                    xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_TARGET_CPUID_MISMATCH);
                    return DAP_ERROR;
                } else {
                    return DAP_GONEXT;
                }
            }
            return DAP_OK;

        case 6:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }

        default:				
            break;
    }
    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
int swd_cfgTarget(xDAPPacket *xPacket) 
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp, val;
    xFun *sub = (xFun *)dap->pvFun;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:		
            SYNCSEQ();
            sub->i = 0;

CFG_TGT_NEXT:            
            JUMPFUN(1);
            temp = xap->cfgregs[sub->i];
            if (temp == 0xFFFFFFFF) {
                return DAP_GONEXT;
            }
            val = xap->cfgregs[sub->i+1];
            //dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            dap->exp = memap_writeBD0(port, temp, &val);
            //dap->exp = swd_memapWAP(port, temp, &val);
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                sub->i += 2;
                goto CFG_TGT_NEXT;
            }
            return DAP_OK;

        default:				
            break;
    }
    return DAP_OK;
}
/*---------------------------------------------------------------------------*/
int swd_powerup_reset(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    int i;
    U8	*resp = SUBRESPONSE(xPacket);

    switch (dap->substate) {
        case 0:
            SYNCSEQ();
            dap->timeout = 3000;
            //IiP(SM_DAPRESET, OPT_NF, NULL, MS_CMD_RESETDAP);
            //iP("\n%s", MS_CMD_RESETDAP);
            dP("Powerup Reset P[%1x]", dap->xport);
            dap->retries = 0;
            temp = 0;
            nextstate(dap);
            dap->exp = swd_writeDP(swd_flush(dap->xport), DP_SELECT, (U8 *)&temp);
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                } else if (resp[2] == SWD_ACK_WAIT) {
                    dap->vport |= port;
                    cP("\nPort [%X]: Target Wait!", port);
                } else if (resp[2] == SWD_ACK_FAULT) {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    cP("\nPort [%X]: Target Fault!", port);
                    xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, port, TE_UNKNOWN_ERR);
                }
            } 
            if (dap->retries ++ > 10) {
                for(i=0;i<PORT_COUNT;i++) {
                    if ((dap->xport ^ dap->rport) & (1<<i)) {
                        xUIMsg(SM_CONNECT, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_UNKNOWN_ERR);
                    }
                }
                DAP_SetLed((dap->xport ^ dap->rport), REDLED, LEDON);
                dap->xport = dap->rport;
            } else if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp = swd_writeDP(swd_flush(dap->xport), DP_SELECT, (U8 *)&temp);
                return DAP_OK;
            }

            if (PORTSSYNC(dap)) {
                if(dap->xport == 0)
                    return DAP_ERROR;
                SYNCSEQ();
                nextstate(dap);
                temp = 0x50000000;
                dap->exp = swd_writeDP(swd_flush(dap->xport), DP_CTRL_STAT, (U8 *)&temp);
            }
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    cP("[%s]", "Write DP_CTRL_STAT ERROR!");
                }
            } 

            if (PORTSSYNC(dap)) {
                nextstate(dap);
                dap->exp = swd_readDP(swd_flush(dap->xport), DP_CTRL_STAT, (U8 *)&temp);
            }

            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    if ((temp & 0xF0000040) == 0xF0000040) {
                        dap->rport |= port;
                    }else if ((temp & 0xF0000040) == 0xD0000040) {
                        dap->rport |= port;
                    }else if ((temp & 0xF0000040) == 0xF0000000) {
                        dap->rport |= port;
                    } else {
                        dap->xport &= ~port;    // remove port
                        DAP_SetLed(port, REDLED, LEDON);
                        dap->port = port;
                        //IiP(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_PORT, NULL, ErrMsg(TE_DBGPWR_FAIL, OPT_ERR));
                        xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, 0, TE_DBGPWR_FAIL);
                        wP("\nDEBUGGER POWER UP Failed(0x%08x)", temp);
                    }
                }
            } 
            if (PORTSSYNC(dap)) {
                nextstate(dap);
                dap->exp = App_DAP_WriteABORT(swd_flush(dap->xport), 0, 0x0000001e);
            }

            return DAP_OK;


        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[1] == SWD_STS_OK) {
                    dap->rport |= port;
                }
            } 
            if (PORTSSYNC(dap)) {
                nextstate(dap);
                temp = 0x54000000;
                dap->exp = swd_writeDP(swd_flush(dap->xport), DP_CTRL_STAT, (U8 *)&temp);
            }

            return DAP_OK;

        case 5:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            } 
            if (PORTSSYNC(dap)) {
                nextstate(dap);
                if (xap->swj == SWJ_JTAG)
                    temp = 0x50000f32;
                else
                    temp = 0x50000f00;
                dap->exp = swd_writeDP(swd_flush(dap->xport), DP_CTRL_STAT, (U8 *)&temp);
            }

            return DAP_OK;

        case 6:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                if ((xap->vendor>>24) == KINETIS ) {
                    dap->funstate = 0;
                    nextstate(dap);
                     // return  DAP_GONEXT;
									Delayms(500);
       			   return swd_MDMAP(xPacket);
                } else {
                    //JUMPSUB(9);
                    //temp = 0x23000052;
                    ////dap->exp = swd_writeAP(swd_flush(dap->xport), MEM_AP, MEM_AP_REG_CSW, &temp);
                    //dap->exp = swd_buff_cmd(swd_flush(port), SWD_WRITE|SWD_AP|MEM_AP_REG_CSW, (U8 *)&temp);
                    //dap->ap_csw_value = temp;
                    JUMPSUB(8);
                    temp = 0;
                    dap->exp = swd_writeDP(swd_flush(port), DP_SELECT, (U8 *)&temp);	// write DP SEL 0x00000000
                    dap->dp_sel_value = temp;
                    return DAP_OK;
                }
            }
            return DAP_OK;

        case 7:
            i = swd_MDMAP(xPacket);
            if (i != DAP_GONEXT)
                return i;
#if 0
            JUMPSUB(9);
            temp = 0x23000052;
            dap->exp = swd_writeAP(swd_flush(dap->xport), MEM_AP, MEM_AP_REG_CSW, &temp);
            //dap->exp = swd_buff_cmd(swd_flush(port), SWD_WRITE|SWD_AP|MEM_AP_REG_CSW, (U8 *)&temp);
            dap->ap_csw_value = temp;
#else
            //AFTER_MDMAP:
            JUMPSUB(8);
            temp = 0;
            dap->exp = swd_writeDP(swd_flush(port), DP_SELECT, (U8 *)&temp);	// write DP SEL 0x00000000
            dap->dp_sel_value = temp;
            return DAP_OK;

        case 8:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                if (xap->swj == SWJ_JTAG) {
                    dap->funstate = 0;
                    JUMPSUB(15);
                    return swd_led(xPacket);
                }
                JUMPSUB(9);
                temp = 0x23000052;
                //dap->exp = swd_buff_cmd(swd_flush(port), SWD_WRITE|SWD_AP|MEM_AP_REG_CSW, (U8 *)&temp);
                dap->exp = swd_writeAP(swd_flush(dap->xport), MEM_AP, MEM_AP_REG_CSW, &temp);
                dap->ap_csw_value = temp;
            }
#endif
            return DAP_OK;

        case 9:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) { 
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_ERROR;
								}
            }
            if (PORTSSYNC(dap)) {
                //IiP(SM_DAPRESET, OPT_NF, NULL, MS_CMD_DAPRESETTED);
                printports("Target Debugger Resetted");
						if ((xap->vendor>>24) == KINETIS ) {//weib add,because chkstm32 maybe need up
                dap->funstate = 0;
                nextstate(dap);
                swd_getdbgbase(xPacket);
						}
							else {             
                xap->dapstate |= PREQ_INITED;
                return DAP_GONEXT;
							}              
            }
            return DAP_OK;

        case 10:
            i = swd_getdbgbase(xPacket);
            if (i == DAP_ERROR) {
                dap->regs[16] = i;
                goto DAPINIT_LED;
            } else if (i != DAP_GONEXT) {
                return DAP_OK;
            }
            nextstate(dap);
            dap->funstate = 0;
            dap->regs[15] = 0;  // SET DEMCR=0x01000000 in swd_readROM()

        case 11:
						Delayms(50);  // wait for hardware stable
						if(gChkRomtable)
							i = swd_readROM(xPacket);
						else
							i=DAP_GONEXT;
						if (i == DAP_ERROR) {
                dap->regs[16] = i;
                return DAP_ERROR;
            } else if (i == DAP_GONEXT) {
                for (i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                        break;
                    }
                }
                if (xap->resett ==0 && dap->regs[4+i] == 0x00000004 && dap->regs[8+i] == 0x000bb471) {
                    cP("\nNvuoton board found, change to SYS RESET!");
                    xap->resett &= ~(0xF);
                    xap->resett |= RST_SYS;
                }

                JUMPSUB(12);
                dap->funstate = 0;
                if ((xap->vendor>>24) == KINETIS) {
                    dP("\nMDMAP_RESET...");
                    runfunc = &swd_MDMAP_RESET;
                    return runfunc(xPacket);
                } else if ((xap->vendor>>24) == STM32) {
                    dP("\nCheck STM32...");
                    runfunc = &swd_chkSTM32;
                    return runfunc(xPacket);
                }
                dap->regs[16] = DAP_GONEXT;
                goto DAPINIT_LED;

            } else if (i == DAP_GOON) {
                dap->funstate = 0;
                i = swd_readROM(xPacket);
            }else if(i ==DAP_AGAIN)
							return i;
						
            return DAP_OK;

        case 12:
            i = runfunc(xPacket);
            if (i == DAP_GONEXT) {
                dap->regs[16] = DAP_GONEXT;
                goto DAPINIT_LED;
            } else if (i == DAP_ERROR) {
                dap->regs[16] = i;
                goto DAPINIT_LED;
						}else if(i == DAP_AGAIN)
						{
							return i;//weib add for unstable error
            }else 
                return DAP_OK;
DAPINIT_LED:
#if 1
            dap->funstate = 0;
            JUMPSUB(15);
            return swd_led(xPacket);

        case 15:
            if (DAP_GONEXT != swd_led(xPacket))
                return DAP_OK;
#endif

            xap->dapstate |= PREQ_INITED;
            return dap->regs[16];

        default:
            break;
    }
    return DAP_OK;
}
/*----------------------------------------------------------------------------*/
/* 
 * loadSapFileSeg()
 *	 Using a buffer to cache SAP file segment to avoid read file segment again and again.
 *		Return request buffer address if requested buffer inside cache range
 *		otherwise, read file segment into cache and return new address of buffer
 */
U8 *loadSapFile(uint8_t *fname, uint32_t offset, uint32_t sz)
{

	   extern int read_times;
	   int len;
	#if 0
	   uint32_t abs_offset = (read_times-1) * 4096;

	   if(offset < 4096  && read_times == 1) 
	   {
			xap->cache = xBuff->buffo + offset;   
			xap->offCache = offset;
			if(offset+sz > 4096)
				xap->szCache = 4096 - offset;
			else
				xap->szCache = sz;
		read_times++;
			return xap->cache;
	   }else{
			//len = GetSAPFile(xap->sapfname,xBuff->buffo,offset);
			 len = GetSAPFile(xap->sapfname,rBuf,offset);
			if(len < 0)
				return NULL;
			
			//xap->cache = xBuff->buffo;   	
			xap->offCache = 0;
			xap->szCache = len;
			return rBuf;//xap->cache;


	   }
		 #endif
	  len = GetSAPFile(xap->sapfname,xBuff->buffo,sz,offset);
		if(len < 0)
		{
			CloseSAPFile();
			return NULL;
			
		}
			
		xap->cache = xBuff->buffo;   	
		xap->offCache = 0;
		xap->szCache = len;
		CloseSAPFile();
		return xap->cache;	 


}
U8 *loadSapFileSeg(uint8_t *fname, uint32_t offset, uint32_t sz, uint32_t *SZ)
{
  return NULL;
}

/* --------------------------------------------------------------------------*/
/* 
 * swd_halt()
 *		Halt target CPU.
 *			write 0xA05F0003 to DHCSR
 *			check if halted then done
 *
 */
int swd_halt(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    U32 temp;
    xDAPPort *dap = &pPort;
    U8	*resp = SUBRESPONSE(xPacket);

    switch(dap->substate) {
        case 0:			// write DHCSR with 0xA05F0003
            SYNCSEQ();
            iP("\nHALTing TARGET CPU...");
            temp = DBGKEY | C_DEBUGEN | C_HALT;
            JUMPSUB(1);
            dap->exp = memap_writeBD0(port, DCB_DHCSR, &temp);
            break;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPSUB(2);
                dap->retries = 0;
                dap->exp = read_DHCSR(port);
            }
            break;

        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    if ((temp & 0x00030003) == 0x00030003) {
                        dap->rport |= port;
                    } else {
                        dap->vport |= port;
                    }
                } 
            }
            if (dap->retries++ > 20) {
                DAP_SetLed((dap->xport ^ dap->rport), REDLED, LEDON);
                dap->xport = dap->rport;
            } else if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp = read_DHCSR(port);
            }
            if (PORTSSYNC(dap)) {
                printports("Halted.");
                return DAP_GONEXT;	
            }
            break;
        default:
            cP("\nswd_halt()::nunknown state(%d)", dap->substate);
            break;
    }
    return DAP_OK;
}



/* --------------------------------------------------------------------------*/
int swd_reset_halt(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    U32 temp;
    int i, err=0;
    xDAPPort *dap = &pPort;
    U8	*resp = SUBRESPONSE(xPacket);

    switch(dap->substate) {          
        case 0:			// read DHCSR with 0xA05F0003
            SYNCSEQ();
            dap->retries = 0;
            dap->timeout = 2000;
            dap->dp_sel_value = 0xFFFFFFFF;
	        dap->ap_tar_value = 0xFFFFFFFF;
#if 1
            JUMPSUB(0x21);
            temp = 0x00000001;
		//	dap->cmdidx = 0;
            dap->exp = swd_WAP(port, 0, DCB_DEMCR, &temp);
#else
            JUMPSUB(0x1);
            dap->exp = swd_RAP(port, 0, DCB_DHCSR);
            return DAP_OK;
#endif

        case 0x21:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                JUMPSUB(0x22);
                temp = DBGKEY | C_DEBUGEN | C_HALT;
                //memap_writeBD0(port, DCB_DHCSR, &temp);

                dap->exp = swd_WAP(port, 0, DCB_DHCSR, &temp);
            }
            return DAP_OK;
        case 0x22:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                JUMPSUB(0x23);
                //dap->exp = read_DHCSR(port);
                if (0 && xap->swj == SWJ_JTAG) {
                    temp = 0x01000000;
                    swd_writeDP(port, DP_SELECT, (U8 *)&temp);
                    temp = 0;
                    swd_buff_cmd(port, SWD_AP|MEM_AP_REG_TAR, (U8 *)&temp);
                    temp = 0x00000000;
                    swd_writeDP(swd_flush(port), DP_SELECT, (U8 *)&temp);
                }
                dap->exp = swd_RAP(port, 0, DCB_DHCSR);
            }

            return DAP_OK;

        case 0x23:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                JUMPSUB(0x24);
                temp = DBGKEY | C_DEBUGEN | C_HALT;
                //memap_writeBD0(port, DCB_DHCSR, &temp);
                dap->exp = swd_WAP(port, 0, DCB_DHCSR, &temp);
            }
        case 0x24:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPSUB(0x25);
                temp = 0x01000000;
                dap->exp = swd_WAP(port, 0, DCB_DEMCR, &temp);
                //dap->exp = read_DHCSR(port);
            }

            return DAP_OK;
        case 0x25:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPSUB(0x26);
                temp = 0x00000001;
                dap->exp = swd_WAP(port, 0, DCB_DEMCR, &temp);
                //dap->exp = read_DHCSR(port);
            }

            return DAP_OK;
        case 0x26:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPSUB(0x1);
                dap->exp = swd_RAP(port, 0, DCB_DHCSR);
            }

            return DAP_OK;
        case 0x11:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    dP("\nswd_reset_halt():: Port[%X]::DHCSR: %08X ", port, temp);
                    dap->rets[P2I(port)] = temp;
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                JUMPSUB(0x12);
                temp = DBGKEY | C_DEBUGEN | C_HALT;
                dap->exp = swd_WAP(port, 0, DCB_DHCSR, &temp);
                //dap->exp = memap_writeBD0(port, DCB_DHCSR, &temp);
            }
            return DAP_OK;
        case 0x12:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                JUMPSUB(0x13);
                temp = 0x01000000;
                dap->exp = swd_WAP(port, 0, DCB_DEMCR,  &temp);
            }
            return DAP_OK;

        case 0x13:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                JUMPSUB(0x14);
                //memap_writeBD0(port, DCB_DEMCR, &temp);
                dap->exp = swd_RAP(port, 0, 0xE0002000);
                //dap->exp = memap_readBD0(port, 0xe0002000, &temp);
            }
            return DAP_OK;
        case 0x14:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                JUMPSUB(0x15);
                //dap->exp = memap_readBD0(port, 0xe0001000, &temp);    // read number of comparator
                dap->exp = swd_RAP(port, 0, 0xE0001000);
            }
            return DAP_OK;

        case 0x15:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    dP("\nswd_reset_halt():: Port[%X]::DWT NUMCOMP: %d ", port, temp>>28);
                    dap->rets[P2I(port)] = temp;
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                dap->regs[7] = (temp>>28);
                dap->regs[8] = 0;
                SYNCSEQ();
                JUMPSUB(0x16);
                temp = 0x00000002;
                dap->exp = swd_WAP(port, 0, 0xe0002000, &temp);
                //memap_writeBD0(port, 0xe0002000, &temp);
            }
            return DAP_OK;
        case 0x16:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                if (dap->regs[8] < dap->regs[7]) {
                    dap->rport = 0;
                    temp = 0x00000000;
                    dap->exp = swd_WAP(port, 0, (0xe0001028 + dap->regs[8]*0x10),&temp);
                    dap->regs[8] += 1;
                    //memap_writeBD0(port, (0xe0001028 + i*0x10), &temp);
                } else {
                    JUMPSUB(0x17);
                    temp = 0x00000001;
                    dap->exp = swd_WAP(port, 0, DCB_DEMCR, &temp);
                //dap->exp = memap_writeBD0(port, DCB_DEMCR, &temp);
                }
            }
            return DAP_OK;
        case 0x17:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                JUMPSUB(1);
                dap->exp = swd_RAP(port, 0, DCB_DHCSR);
                //dap->exp = read_DHCSR(port);
            }
            return DAP_OK;
        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    dP("\nswd_reset_halt():: Port[%X]::DHCSR: %08X ", port, temp);
                    dap->rets[P2I(port)] = temp;
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                temp = 0;
                for (i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                        if (temp != 0 && temp != dap->rets[i]) {
                            cP("\nDHCSR not SYNC: %08X, %08X ", temp, dap->rets[i]);
                            //err = 1;
                        }
                        temp = dap->rets[i];
                    }
                }
                if (err) {
                    IiP(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_PORT, NULL, ErrMsg(TE_SWRST_FAIL, OPT_ERR));
                    dap->regs[16] = DAP_ERROR;
                    goto RESETHALT_LED;
                    //return DAP_ERROR;
                }
                if (temp & S_RESET_ST){
                    DAP_SetLed((dap->xport ^ dap->rport), REDLED, LEDON);
                    dap->xport = dap->rport;
                    JUMPSUB(1);
#if 0                    
                    if (temp & S_HALT) {
                        dap->regs[16] = DAP_GONEXT;
                        goto RESETHALT_LED;
                    }
#endif
                    if (dap->retries++ > 1) {
                        cP("\nSeems under reset?");
                        temp = DBGKEY | C_DEBUGEN | C_HALT;
                        memap_writeBD0(port, DCB_DHCSR, &temp);
                        if (xap->resett & C_UNDER_RST) {
                            App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);
                        }
                    }
                    dap->exp = swd_RAP(port, 0, DCB_DHCSR);
                } else {
                    if (temp & S_HALT) {
                        SYNCSEQ();
                        if (xap->resett & RST_HW) {
                            dap->retries = 0;   // HW RESET 
                            JUMPSUB(4);
														if(xap->vendor==ATMEL<<24)
															dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x01, 0x0);//	// assert nCLK low
                            dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
                            vTaskDelay(5);
                            return DAP_OK;
                        } else if (xap->resett & RST_VECT) {
                            JUMPSUB(7);
                            App_DAP_SWJ_Pins(port, 0x10, 0x10, 0x0);		// set as input again
                            temp = 0x05fa0001;	//temp = 0x05fa0001;	// VECTRESET
                            dap->exp = swd_WAP(port, 0, SCB_AIRCR, &temp);
                            vTaskDelay(5);
                            return DAP_OK;
                        } else if (xap->resett & RST_SYS){ //RST_AUTO or RST_SYS
                            JUMPSUB(7);
                            temp = 0x05fa0004;	// SYSRESETREQ 
 //                           App_DAP_SWJ_Pins(port, 0x10, 0x10, 0x0);		// set as input again

                            dap->exp = swd_WAP(port, 0, SCB_AIRCR, &temp);
                            //if (xap->vendor == (STM32 << 24))
                            //    dap->exp =App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
                            vTaskDelay(5);
                            //if (xap->vendor == (STM32 << 24))
                            //    dap->exp =App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);		// assert nRST high 
                            return DAP_OK;
                        } else {
                            dap->retries = 0;   // HW RESET 
                            JUMPSUB(4);
                            dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
                            return DAP_OK;
                        }
                        //nextstate(dap);
                        //temp = 0x01000000;
                        //dap->exp = memap_writeBD0(port, DCB_DEMCR, &temp);
                        //temp = 0x00000001;
                        //dap->exp = memap_writeBD0(port, DCB_DEMCR, &temp);
                    } else {
                        FUNJUMP(dap, 0);
                        JUMPSUB(0);
                        //swd_readDP(port, DP_CTRL_STAT, NULL); // seems no need
                        temp = DBGKEY | C_DEBUGEN | C_HALT;
                        //memap_writeBD0(port, DCB_DHCSR, &temp);
                        swd_WAP(port, 0, DCB_DHCSR, &temp);
                        temp = 0x01000000;
                        swd_WAP(port, 0, DCB_DEMCR, &temp);
                        temp = 0x00000001;
                        swd_WAP(port, 0, DCB_DEMCR, &temp);

                        if (xap->resett & C_UNDER_RST) {
                            App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);
                        }
                        //dap->exp = read_DHCSR(port);
                        dap->exp = swd_RAP(port, 0, DCB_DHCSR);
                    }
                }
            }
            return DAP_OK;

        case 2: 
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                if (xap->resett & RST_HW) {
                    dap->retries = 0;   // HW RESET 
                    JUMPSUB(4);
										if(xap->vendor==ATMEL<<24)
											dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x01, 0x0);		// assert nRST low
                    dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
                    return DAP_OK;
                } else if (xap->resett & RST_VECT) {
                    JUMPSUB(7);
                    temp = 0x05fa0001;	//temp = 0x05fa0001;	// VECTRESET
                    //dap->exp = memap_writeBD0(port, SCB_AIRCR, &temp);
                    dap->exp = swd_WAP(port, 0, SCB_AIRCR, &temp);
                    vTaskDelay(5);
                    dap->exp = App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);		// assert nRST high 
                    return DAP_OK;
                } else if (xap->resett & RST_SYS){ //RST_AUTO or RST_SYS
                    JUMPSUB(7);
                    temp = 0x05fa0004;	// SYSRESETREQ 
                    dap->exp = memap_writeBD0(port, SCB_AIRCR, &temp);
                    //dap->exp = swd_WAP(swd_flush(port), 0, SCB_AIRCR, &temp);
                    vTaskDelay(5);
                    dap->exp = App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);		// assert nRST high 
                    return DAP_OK;
                } else {
                    dap->retries = 0;   // HW RESET 
                    JUMPSUB(4);
                    dap->exp = App_DAP_SWJ_Pins(swd_flush(port), 0x00, 0x80, 0x0);		// assert nRST low
                    return DAP_OK;
                }
            }
            return DAP_OK;

        case 4: // wait nRST low
            if (PORTSEQEQ(port)) {
							#ifdef MC101_BOARD
								if (1) 
							#else
                if ((resp[1]&0x80) == 0)
							#endif
                    dap->rport |= port;
                else
                    dap->vport |= port;
            }
            if (dap->retries++ > 10) {
                for(i=0;i<PORT_COUNT;i++) {
                    if ((dap->xport ^ dap->rport) & (1<<i)) {
                        xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_UNKNOWN_ERR);
                    }
                }
                DAP_SetLed((dap->xport ^ dap->rport), REDLED, LEDON);
                dap->xport = dap->rport;
            } else if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp =App_DAP_SWJ_Pins(port, 0x00, 0x0, 0x0);
            }

            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                dap->retries = 0;
                nextstate(dap);
                dap->exp = App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);		// assert nRST Hi again
            }
            return DAP_OK;

        case 5: // wait nRST hi
            if (PORTSEQEQ(port)) {
							#ifdef MC101_BOARD
								if (1) 
							#else
                if ((resp[1]&0x80))//weib add
							#endif
                    dap->rport |= port;
                else
                    dap->vport |= port;
            }

            if (dap->retries++ > 10) {
                for(i=0;i<PORT_COUNT;i++) {
                    if ((dap->xport ^ dap->rport) & (1<<i)) {
                        xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_UNKNOWN_ERR);
                    }
                }
                DAP_SetLed((dap->xport ^ dap->rport), REDLED, LEDON);
                dap->xport = dap->rport;
            } else if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x0, 0x0);
            }

            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                nextstate(dap);
                //dap->exp = read_DHCSR(port);
                //dap->exp = App_DAP_SWJ_Pins(port, 0x10, 0x10, 0x0);  // set reset as input
							vTaskDelay(50);
                dap->exp = swd_RAP(port, 0, DCB_DHCSR);
							
            }
            return DAP_OK;

        case 6:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    dap->rets[P2I(port)] = temp;
                    dap->port = port;
                    if (temp & S_HALT) {
                        dap->rport |= port;
                        if (currcmd == SM_RESET_HALT)  {
                            IiP(SM_RESET_HALT, OPT_NF|OPT_PORT, NULL, MSGSTR[ALG_CONN_HALT]);
                        }
                        if ((temp & S_RESET_ST)==0) {
                            cP("\nAttached targets [%X] HW reset: Halted without S_RESET_ST[%08X].", port, temp);
                        }
                    } else {
                        cP("\nAttached targets [%X] HW reset: Not Halted[%08X]!", port, temp);
                        if (currcmd != SM_ABORT) {
                            xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, 0, TE_HALT_FAIL);
                        }
                    }
                } 
            }

            if (PORTSSYNC(dap)) {
                printports("Target halted(HW RESET)");
                dap->regs[16] = DAP_GONEXT;
                goto RESETHALT_LED;
                //return DAP_GONEXT;
            }

            return DAP_OK;

        case 7:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if PORTSSYNC(dap) {
                if (xap->cfgregs[0] != 0xFFFFFFFF) {
                    nextstate(dap);
                    dap->funstate = 0;
                    return swd_cfgTarget(xPacket);
                }
                goto NO_AFTERRST_CFG; 
            }
            return DAP_OK;

        case 8:
            i = swd_cfgTarget(xPacket);
            if (i != DAP_GONEXT)
                return DAP_OK;

NO_AFTERRST_CFG:
            JUMPSUB(10);
            for (port=0;port<PORT_COUNT;port++) {
                dap->rets[port] = 0;
            }
            // dap->exp = read_DHCSR(port);
            dap->exp = swd_RAP(port, 0, DCB_DHCSR);
            return DAP_OK;

        case 10:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    /////dap->rport |= port;
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<<24;
                    if (temp & S_RESET_ST) {
                        dap->rets[P2I(port)] = S_RESET_ST;
                        dap->rport |= port;
                    } else
                        dap->vport |= port;

                } else
                    dap->vport |= port;
            }
            if (dap->retries++ > 3) {
                dap->vport = dap->xport ^ dap->rport;
                for(port=0;port <PORT_COUNT;port++) {
                    if (dap->vport & (1<<port)) {
                        iP("\nTargets halt failed.");
                        IiP(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_PORT, NULL, ErrMsg(TE_HALT_FAIL, OPT_ERR));
                        if (currcmd != SM_ABORT)
                            xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, 0, TE_HALT_FAIL);
                    }
                }
                //DAP_SetLed((dap->xport ^ dap->rport), REDLED, LEDON);
                //dap->xport = dap->rport;
                dap->rport = dap->xport;
            } else if PORTSASYNC(dap) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp = swd_RAP(port, 0, DCB_DHCSR);
                //dap->exp = read_DHCSR(port);
            } 

            if (PORTSSYNC(dap)) {
                dap->rport = 0;
                dap->vport = 0;
                for (port=0;port<PORT_COUNT;port++) {
                    if (dap->xport & (1<<port)) {
                        temp = dap->rets[port];
                        dap->port = (1<<port);
                        dap->rport |= (1<<port);
                        if (temp & S_RESET_ST) {
                        } else {
                            dap->vport = (1<<port);
                            wP("\nPort[%1X], Targets halted without reset flag.", (1<<port));
                            //dap->xport &= ~(1<<port);
                            IiP(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_PORT, NULL, ErrMsg(TE_SWRST_FAIL, OPT_ERR));
                            //if (currcmd != SM_ABORT)
                            //    xUIMsg(SM_DAPRESET, OPT_NF|OPT_ERR|OPT_CODE, 0, TE_SWRST_FAIL);
                        }
                        if (currcmd == SM_RESET_HALT)  {
                            IiP(SM_RESET_HALT, OPT_NF|OPT_PORT, NULL, MSGSTR[ALG_CONN_HALT]);
                        }
                    }
                }
                //if (dap->vport)
                //    goto RESET_AGAIN;

                DAP_SetLed((dap->xport ^ dap->rport), REDLED, LEDON);
                dap->xport = dap->rport;
                if (dap->xport) {
                    dap->regs[16] = DAP_GONEXT;
                    if (xap->resett & RST_VECT)
                        printports("Target(VECTRST) halted");
                    else
                        printports("Target(SYSRST) halted");
                } else {
                    dap->regs[16] = DAP_ERROR;
                }
                goto RESETHALT_LED;
                //return DAP_GONEXT;
            }
            return DAP_OK;
RESETHALT_LED:
#if 1
            dap->funstate = 0;
            JUMPSUB(11);
            return swd_led(xPacket);
        case 11:
            if (DAP_GONEXT != swd_led(xPacket))
                return DAP_OK;
#endif
            if (dap->xport) 
                xap->dapstate |= PREQ_HALTED;

            return dap->regs[16];

        default:
            cP("\nReset detect::nunknown state(%d)", dap->substate);
            break;
    }
    return DAP_OK;
}

/* --------------------------------------------------------------------------*/
/* 
 * swd_resetgo()
 *		Halt target CPU.
 *			write 0xA05F0003 to DHCSR
 *			check if halted then done
 *			if not halted then
 *			    write 0x00000001 to DEMCR and reset target CPU using nRST pin
 *
 */
int swd_resetgo(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = SUBRESPONSE(xPacket);

    switch(dap->substate) {
        case 0:	
            SYNCSEQ();
            dap->regs[10] = dap->xport;
            dP("swd_resetgo()...");
            dap->timeout = 1500;
            dap->funstate = 0;
            JUMPSUB(1);
            temp = 0;
            dap->exp = swd_WAP(port, 0, DCB_DEMCR, &temp);

        case 1:
            if (PORTSEQEQ(port)) {
                //if (resp[2] == SWD_ACK_OK) {
                dap->rport |= port;
                //} 
            }

            if PORTSSYNC(dap) {
                if (xap->vendor == (XMC<<24)) {
                    JUMPSUB(4); // XMC-4500 failed to response
                } else {
                    JUMPSUB(3);
				}
                dap->retries = 0;
                temp = 0xA05F0000;
                dap->exp = swd_WAP(port, 0, DCB_DHCSR, &temp);
                //dap->exp = memap_writeBD0(port, DCB_DHCSR, &temp);
            }
            return DAP_OK;
        case 3:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if PORTSSYNC(dap) {
                SYNCSEQ();
                if (dap->xport == 0) {
                    dap->xport = dap->regs[10];
                    JUMPSUB(5);
                    dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
                    return DAP_OK;
                }
                dap->retries = 0;
                if (xap->resett & RST_HW) {
                    JUMPSUB(5);
                    dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
                    vTaskDelay(5);
                    return DAP_OK;
                }else if (xap->resett & RST_VECT) {
                    JUMPSUB(4);
                    App_DAP_SWJ_Pins(port, 0x10, 0x10, 0x0);		// set as input
                    temp = 0x05fa0001;	// VECTRESETREQ 
                    dap->exp = swd_WAP(port, 0, SCB_AIRCR, &temp);
                    vTaskDelay(5);
                    //dap->exp = memap_writeBD0(port, SCB_AIRCR, &temp);
                    return DAP_OK;
                } else if (xap->resett & RST_SYS){
                    JUMPSUB(4);
                    App_DAP_SWJ_Pins(port, 0x10, 0x10, 0x0);		// set as input
                    temp = 0x05fa0004;	// SYSRESETREQ 
                    dap->exp = memap_writeBD0(port, SCB_AIRCR, &temp);
                    vTaskDelay(5);
                    return DAP_OK;
                } else {
                    JUMPSUB(5);
                    dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
                }
            }
            return DAP_OK;

        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    if (currcmd != SM_AUTO) {
                        dap->port = port;
                        IiP(SM_RESET_GO, OPT_NF|OPT_PORT, NULL, MSGSTR[ALG_RR_RUNNING]);
                    }
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if PORTSSYNC(dap) {
                JUMPSUB(5);
                dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
                vTaskDelay(5);
            }
            return DAP_OK;

        case 5: // wait low
            if (PORTSEQEQ(port)) {
                if ((resp[1] & 0x80) == 0) {	// nRST low, go next
                    dap->rport |= port;
                } else {
                    dap->vport |= port;
                }
            }

            if (dap->retries++ > 10) {
                dap->xport = dap->rport;
            } else if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp = App_DAP_SWJ_Pins(port, 0x00, 0x0, 0x0);
            }

            if PORTSSYNC(dap) {
                SYNCSEQ();
                dap->retries = 0;
                JUMPSUB(6);
                vTaskDelay(5);  // wait 5 ms
                dap->exp = App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);	// assert nRST high
            }
            return DAP_OK;

        case 6:
            if (PORTSEQEQ(port)) {
                if (resp[1] & 0x80) {	// wait nRST pin high
                    dap->rport |= port;
                    if (currcmd != SM_AUTO) {
                        dap->port = port;
                        IiP(SM_RESET_GO, OPT_NF|OPT_PORT, NULL, MSGSTR[ALG_RR_RUNNING]);
                    }
                } else {
                    dap->vport |= port;
                }
            }
            if (dap->retries++ > 10) {
                dap->vport = dap->xport ^ dap->rport;
                dap->xport = dap->rport;
            } else if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->vport = 0;
                dap->exp =App_DAP_SWJ_Pins(port, 0x00, 0x0, 0x0);
            }

            if PORTSSYNC(dap) {
                SYNCSEQ();
                printports("Target Resetted and Running");
                xUIMsg(SM_RESET_GO, OPT_NF, 0, ALG_RR_START);
                temp = 0;
                dap->rport = 0;

                switch(xap->vendor>>24) {
                    case XMC:
                    case ATMEL:
                        break;
                    default:
                        swd_writeDP(swd_flush(port), DP_CTRL_STAT, (U8 *)&temp);
                }

                xap->dapstate &= ~(PREQ_CONNECTED | PREQ_INITED | PREQ_HALTED | PREQ_CODELOADED);
                vTaskDelay(5);
                dap->exp =App_DAP_SWJ_Pins(port, 0x10, 0x10, 0x0);  // setinput
                return DAP_GONEXT;
            }

            return DAP_OK;


        default:
            cP("\nswd_resetgo()::unknown substate");
            break;
    }
    return DAP_OK;
}

/* --------------------------------------------------------------------------*/
int swd_reset(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = SUBRESPONSE(xPacket);

    switch(dap->substate) {
        case 0:	
            dap->timeout = 500;
            dap->funstate = 0;
            nextstate(dap);
            temp = DBGKEY; 
            SYNCSEQ();
            dap->exp = memap_writeBD0(port, DCB_DHCSR, &temp);
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if PORTSSYNC(dap) {
                nextstate(dap);
                SYNCSEQ();
                temp = 0; 
                dap->exp = memap_writeBD0(port, DCB_DEMCR, &temp);
            }
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if PORTSSYNC(dap) {
                dap->retries = 0;
                nextstate(dap);
                dap->exp =App_DAP_SWJ_Pins(port, 0x00, 0x80, 0x0);		// assert nRST low
            }
            return DAP_OK;

        case 3: // wait low
            if (PORTSEQEQ(port)) {
                if ((resp[1] & 0x80) == 0) {	// nRST low, go next
                    dap->rport |= port;
                } else {
                    dap->vport |= port;
                }
            }
            if (dap->retries > 10) {
                dap->xport = dap->rport;
            } else if (PORTSASYNC(dap)) {
                dap->retries ++;
                dap->vport = 0;
                SYNCSEQ();
                dap->exp =App_DAP_SWJ_Pins(port, 0x00, 0x0, 0x0);
            }
            if PORTSSYNC(dap) {
                SYNCSEQ();
                nextstate(dap);
                dap->exp =App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);	// assert nRST high
            }

            return DAP_OK;

        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[1] & 0x80) {	// wait nRST pin high
                    dap->rport |= port;
                } else {
                    dap->vport |= port;
                }
            }
            if (dap->retries > 10) {
                //dap->vport = dap->xport ^ dap->rport;
                dap->xport = dap->rport;
            } else if (PORTSASYNC(dap)) {
                SYNCSEQ();
                dap->retries ++;
                dap->vport = 0;
                dap->exp =App_DAP_SWJ_Pins(port, 0x00, 0x0, 0x0);
            }
            if PORTSSYNC(dap) {
                dap->exp =App_DAP_SWJ_Pins(port, 0x10, 0x10, 0x0); // set reset as input
                xap->dapstate &= ~(PREQ_CONNECTED | PREQ_INITED | PREQ_HALTED | PREQ_CODELOADED);
                printports("Target Resetted and Running");
                temp = 0;
                dap->rport = 0;
                swd_writeDP(swd_flush(port), DP_CTRL_STAT, (U8 *)&temp);
                return DAP_GONEXT;
            }

            return DAP_OK;

        default:
            cP("\nswd_reset()::unknown substate");
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------*/
/* 
 * swd_disconnect()
 *
 */
int swd_disconnect(xDAPPacket *xPacket)
{
	U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    
    switch(dap->substate) {
        default:
            cP("\nDAP_Disconnect...unknow state(%02X)", dap->substate);
        case 0:			
            //dP("\nDAP_Disconnect...");
            if (xap->xport == 0)
                return DAP_GONEXT;
            SYNCSEQ();
            JUMPSUB(1);
            dap->funstate = 0;
            return swd_led(xPacket);

        case 1:
            if (DAP_GONEXT != swd_led(xPacket))
                return DAP_OK;

            SYNCSEQ();
            xap->dapstate &= ~(PREQ_CONNECTED | PREQ_INITED | PREQ_HALTED | PREQ_CODELOADED);
            dap->xport = (1<<PORT_COUNT) - 1;
            JUMPSUB(2);
            dap->exp = App_DAP_Disconnect(NULL);
            dP("\nDAP_Disconnect...");
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) {
                dap->rport |= port;
            }

            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------*/
U32 toint(U8 *buf, U8 sz)
{
    U32 retval = 0;
    U8 i;
    for (i=0;i<=sz;i++)
        retval |= (buf[i] << (i*8));
    return retval;
}
/* --------------------------------------------------------------------------*/
uint8_t swd_gettime(uint8_t cmd)
{
    int i;
    for (i=0;xap->estimate[i]<0xFF;i+=2) {
        if (xap->estimate[i] == cmd)
            return xap->estimate[i+1];
    }
    return 0;
}
/*----------------------------------------------------------------------------*/
int swd_calc_crc32(char *fname, xDAPPort *xdap, xSapFile *xxap, xSerFile *xxsf)
{
    int i; 
    int sync = 1;
    uint32_t offset, tas, tae;
    uint32_t sz, SZ;
    uint8_t *buff;
    xSerial *xs;
    
    for (i=0;i<PORT_COUNT;i++)
        xdap->crcs[i] = 0;

    tas = xdap->regs[1];    // start address
    tae = xdap->regs[2];    // end address

    while (tas < tae) {
        xs = NULL;
        sz = tae - tas;      // data to read
        if (sz > XAP_LOAD_SIZE)
            sz = XAP_LOAD_SIZE;

        //if (xdap->program_serial && xxsf && xi < xxsf->cntSer) {
        if (xdap->program_serial) {
            for(i=0;i<xxsf->cntSer;i++) {
                xs = (xSerial *) (xxsf->sbuff + i * sizeof(xSerial));
                if ((xs->addr >= tas) && (xs->addr + xs->size) <= (tas + sz)) {
                    sz = xs->addr - tas;
                    break;
                } else {
                    xs = NULL;
                }
            }
        }

        SZ = 0;
        if (sz > 0) {
            uint32_t noff, nsz;
            offset = xxap->offData + tas - xxap->startFlash;
            noff = offset;
            nsz = sz;

            /** if sap file userdata were encrypted, need to be 8 alignment */
            if (xxap->sap_version & SAP_ENCRYPTED) {
                noff &= 0xFFFFFFF8;
                nsz = ((offset+sz+7) & 0xFFFFFFF8) - noff;
            }

            buff = loadSapFileSeg((uint8_t *)fname, noff, nsz, &SZ);

            if (buff == NULL) {
                cP("\nswd_calc_crc32(): failed to read file!");
                return -1;
            }

            /** if sap file userdata were encrypted, need to descrypt it */
            if (xxap->sap_version & SAP_ENCRYPTED) {
                uint32_t *ramsource = (uint32_t *)buff;

                for(i=0;i<SZ>>2;) {
                    decTEA(&ramsource[i], enckeys);
                    i += 2;
                }
                xxap->szCache = 0;  // invalidate cache for the data in cache was decrypted.
                buff += (offset - noff);

                SZ = SZ - (offset - noff);
                if (SZ > sz)
                    SZ = sz;
            }
            //cP("\nswd_calc_crc32(): to read:%d, read:%d", sz, SZ);

            if (sync) {
                xdap->crcs[0] = calcCRC32(buff, SZ, xdap->crcs[0]);
            } else {
                for (i=0;i<PORT_COUNT;i++) {
                    if (xdap->xport & (1<<i)) {
                        xdap->crcs[i] = calcCRC32(buff, SZ, xdap->crcs[i]);
                    }
                }
            }
        }

        tas += SZ;
        if (xs != NULL) {
            if (xs->addr == tas && (xs->addr < tae)) {
                SZ = xs->size;
                if ( xs->type == SER_TYPE_CRC32)
                    SZ = 4;

                if ((xs->addr + SZ) < tae) {
                    if (sync == 1) {
                        for (i=1;i<PORT_COUNT;i++) {
                            if (xdap->xport & (1<<i)) {
                                xdap->crcs[i] = xdap->crcs[0];
                            }
                        }
                        sync = 0;
                    }
                    for (i=0;i<PORT_COUNT;i++) {
                        if (xdap->xport & (1<<i)) {
                            xdap->crcs[i] = calcCRC32((uint8_t *)xs->data[i], SZ, xdap->crcs[i]);
                        }
                    }
                    tas += SZ; 
                }
            }
        }
    }
    if (sync) {
        for (i=1;i<PORT_COUNT;i++) {
            if (xdap->xport & (1<<i)) {
                xdap->crcs[i] = xdap->crcs[0];
            }
        }
    }
    return 0;
}

/* ------------------------------------------------------------------------------------------------ */
/*
weib:Get and check SAP Header (new and old).
*/
int parseSAPH(uint8_t *fname)
{
 	#if 0 ////////????????????  
	// check magic, 
    uint8_t *ch, fid;
    int i;
    uint32_t cksum = 0, cks, *ul;
    xDAPPort *dap = &pPort;

    for(i=0;i<=MAX_SAP_SECTION;i++) {
        xst[i].id = 0;
        xst[i].offset = 0;
    }

    xap->szCache = 0;
    if ((ch = loadSapFileSeg(fname, 0, 512, NULL)) == NULL) {
        return -1;
    }
    ul = (uint32_t *)ch;
    if (*ul != FT_MAGIC) {
        i = 0;
        do {
            dP("\n[%d] %08x", i, *ul);
            fid = (*ul) & 0xFF;
            xst[i].id = fid;
            cksum += *ul++;

            cksum += *ul;
            dP("#%08x", *ul);
            xst[i].offset = *ul++;

            cksum += *ul;
            dP("#%08x", *ul);
            if (fid == 0)
                cks = *ul;
            //xst[i].size = *ul ++; // not used
            ul++;

            dP("#%08x", cksum);
            i++;
            if (i > MAX_SAP_SECTION && fid != 0) {
                wP("\nSAP file header error.");
                return ERROR_NOT_SAP_FILE;
            }

        } while (fid > 0);

        if (cksum != (cks<<1)) {    // checksum should be zero, otherwise raise error.
            wP("\nSAP file header check sum error. %08X", cksum);
            return ERROR_NOT_SAP_FILE;
        }
    } else {
        xst[0].id = 1;
        xst[0].offset = 0;
        xst[1].id = 0;
        dP("\nOld SAP: [%d] %08X", xst[0].id, xst[0].offset);
    }

    for (i=0;xst[i].id!=0;i++) {
        dP("\nid[%d] offset[%08X].", xst[i].id, xst[i].offset);
        ch = loadSapFileSeg(fname, xst[i].offset, 4, NULL);
        ul = (uint32_t *)ch;
        if (*ul != FT_MAGIC) {
            wP("\nNot SAP file, %08X vs %08x \n", *ul, FT_MAGIC);
            return ERROR_NOT_SAP_FILE;
        }
    }
    dP("\nid[%d] offset[%08X].", xst[i].id, xst[i].offset);
    xap->sapid = 0;
    xap->dapstate &= ~PREQ_SAPLOADED;
    xap->dapstate |= PREQ_SAPSELECTED;
/////??????    dap->program_serial = SerFileInit(xsf, (char *)xap->sapfname); 
    // 0: no SER file existed
    // 1: do serial programming
    // 2: SER file problematic, prevent programming with bad serial file.

    dP("\nparseSAP Header() Finished");
#endif
    return DAP_OK;
}


/*--parseSAPBody()--------------------------------------------------------------------------*/
int parseSAPB(U8 sid)
{
    uint32_t temp;
    int i = 0, j= 0;
    U8 *ch, *buff;
    U8 sz;
   #ifndef MC101_BOARD   
    /* TODO...*/
    xap->speedClock = 8000000;

    dP("\nParsing SAP file: %s, SAPID:%d.", xap->sapfname, sid);
    buff = loadSapFileSeg(xap->sapfname, xst[sid].offset, 512, NULL);   //xap->sapfname, 0, 512);
    if (buff == NULL) {
        wP("\nLoad SAP File Segment failed!");
        return -1;
    }
	#else
    buff = loadSapFile(xap->sapfname, xst[sid].offset, 512);   //xap->sapfname, 0, 512);
    ch = buff;
    buff += 512;
    //if (ch[0] != (FT_MAGIC & 0xff) || ch[1] != ((FT_MAGIC>>8)&0xff) || ch[2] != ((FT_MAGIC>>16)&0xff) || ch[3] != ((FT_MAGIC>>24)&0xff)) {
    if (FT_MAGIC != *(uint32_t *)ch) {
        wP("Not SAP file, %08X vs %08X \n", *(uint32_t *)ch, FT_MAGIC);
        return ERROR_NOT_SAP_FILE;
    }

    i = 2 * FATNAMELEN + 4;
    //memset((void*)((char *)(xap)+ i), 0, sizeof(xSapFile) -i); 

    xap->patErase = 0;
    xap->resett= 0;
    xap->funCRC32  = 0;
    xap->funVerify = 0;
    xap->sap_version= 0;
    xap->funInit = 0;
    xap->funUnInit =  0;
    xap->funEraseChip = 0;
    xap->funBlankCheck =  0;
    xap->funEraseSector = 0;
    xap->funProgPage =  0;
    xap->szProg= 0;
    xap->toProg = 0;
    xap->toErase = 0;
    xap->szFlash = 0;

    xap->offCode = 0;
    xap->offCode = 0;
    xap->szCode = 0;
    xap->offData = 0;
    xap->offData = 0;
    xap->szData = 0;
    xap->szRam = 0;
    xap->vendor = 0;
    xap->dapid  = 0;
    xap->cpuid  = 0;
    xap->szPrgCode = 0;
    xap->szPrgData = 0;
    xap->startPC =   0;
    xap->startSP=    0;
    xap->startBF=    0;
    xap->startFlash = 0;
    xap->crcCode =  0;
    xap->crcData =  0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->funToDo =  0;
    xap->voltage =  0;
    xap->logging =  0;
    xap->swj     =  0x01;   // default to SWJ
		xap->ProgOffset=0;
		xap->NrfLock=0;

    i = 0;

    ch += 4;

    while(ch < buff) {
        // printf("SAP(%02x), %08x\n", *ch, (unsigned)ch);
        sz = *ch >> 6;//weib:flag group ensure the following data number.
        switch (*ch) {
            case FT_FLASH_TYPE:		   break;
            case FT_ERASE_PATTERN:	   xap->patErase =       toint(ch+1, sz); break;
            case FT_RAM_TYPE:		   break;
            case FT_RESET_TT:	       xap->resett=          toint(ch+1, sz); break;
            case FT_VOLTAGE:	       xap->voltage=         toint(ch+1, sz); break;
            case FT_FUN_CRC32:	       xap->funCRC32  =      toint(ch+1, sz); break;
            case FT_FUN_VERIFY:	       xap->funVerify =      toint(ch+1, sz); break;
            case FT_SAP_VERSION:       xap->sap_version=     toint(ch+1, sz); break;
            case FT_FUN_INIT:	       xap->funInit =        toint(ch+1, sz); break;
            case FT_FUN_UNINIT:        xap->funUnInit =      toint(ch+1, sz); break;
            case FT_FUN_ERASECHIP:     xap->funEraseChip =   toint(ch+1, sz); break;
            case FT_FUN_BLANKCHECK:    xap->funBlankCheck =  toint(ch+1, sz); break;
            case FT_FUN_ERASESECTOR:   xap->funEraseSector = toint(ch+1, sz); break;
            case FT_FUN_PROGPAGE:      xap->funProgPage =    toint(ch+1, sz); break;
            case FT_DRIVER_VER:		   break;
            case FT_PROG_SIZE:	       xap->szProg=          toint(ch+1, sz); break;	/* TODO */
            case FT_PP_TIMEOUT:        xap->toProg =         toint(ch+1, sz); break;
            case FT_ES_TIMEOUT:        xap->toErase =        toint(ch+1, sz); break;
            case FT_FLASH_SIZE:	       xap->szFlash =        toint(ch+1, sz); break;

            case FT_CODE_OFFSET:       
                xap->offCode =        toint(ch+1, sz); 
                xap->offCode +=       xst[sid].offset;
                break;
            case FT_CODE_SIZE:         xap->szCode =         toint(ch+1, sz); break;

            case FT_DATA_OFFSET:       
                xap->offData =        toint(ch+1, sz); 
                xap->offData +=       xst[sid].offset;
                break;
            case FT_DATA_SIZE:	       xap->szData =         toint(ch+1, sz); break;
            case FT_RAM_SIZE:          xap->szRam =          toint(ch+1, sz); break;
            case FT_SECTOR_SIZE:       xap->sectors[i++>>1].szSector =    toint(ch+1, sz); break;
            case FT_SECTOR_START_ADDR: xap->sectors[i++>>1].startSector = toint(ch+1, sz); break;
            case FT_CMDAV:             xap->cfgregs[j++] =     toint(ch+1, sz); break;
            case FT_VENDOR:            xap->vendor =        toint(ch+1, sz); break;
            case FT_DAPID:             xap->dapid  =        toint(ch+1, sz); break;
            case FT_CPUID:             xap->cpuid  =        toint(ch+1, sz); break;
            case FT_DAPSPEED:          xap->dapspeed  =     toint(ch+1, sz); break;
            case FT_TARGET_SECTION:	   break;
            case FT_RAM_SECTION:       break;
            case FT_FLASH_SECTION:     break;
            case FT_CODE_SECTION:      break;
            case FT_DATA_SECTION:      break;
            case FT_SCRIPT_SECTION:	   break;
            case FT_PRGCODE_SIZE:      xap->szPrgCode =      toint(ch+1, sz); break;
            case FT_PRGDATA_SIZE:      xap->szPrgData =      toint(ch+1, sz); break;
            case FT_RAM_START_ADDR:    xap->startPC =        toint(ch+1, sz); break;
            case FT_STACK_START_ADDR:  xap->startSP=         toint(ch+1, sz); break;
            case FT_BUFFER_ADDR:       xap->startBF=         toint(ch+1, sz); break;
            case FT_SERIAL_SIG:        xap->serialsig=       toint(ch+1, sz); break;
            case FT_FLASH_START_ADDR:  xap->startFlash =     toint(ch+1, sz); break;
            case FT_CODE_CRC32:        xap->crcCode =        toint(ch+1, sz); break;
            case FT_DATA_CRC32:        xap->crcData =        toint(ch+1, sz); break;
            case FT_ID0:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<<24; break;
            case FT_ID1:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<<16; break;
            case FT_ID2:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<< 8; break;
            case FT_ID3:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<< 0; break;
            case FT_ID4:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<<24; break;
            case FT_ID5:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<<16; break;
            case FT_ID6:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<< 8; break;
            case FT_ID7:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<< 0; break;
            case FT_FUNTODOS:	       xap->funToDo =        toint(ch+1, sz); break;
            case FT_LOGGING:	       xap->logging =        toint(ch+1, sz); break;
            case FT_SWJ:	           xap->swj     =        toint(ch+1, sz); break;
						case FT_FUN_EXTRA:
						{
							uint32_t SeqCnt=toint(ch+1, sz);
							xSAPSeq *tmpDapExtraFun=NULL;	
							xap->ExtraFunCnt=SeqCnt;
							ch += sz + 2;
							xap->SapExtraFun=(xSAPSeq *)malloc(sizeof(xSAPSeq)*SeqCnt);
							if(xap->SapExtraFun)
							{				
								tmpDapExtraFun=xap->SapExtraFun;
								do{
									sz=4;
									tmpDapExtraFun->SeqAddress=toint(ch, sz);
									ch+=sz;
									tmpDapExtraFun->IsNeedConvert=(toint(ch, sz)>>16)&0xFF;
									tmpDapExtraFun->RW=(toint(ch, sz)>>8)&0xFF;
									tmpDapExtraFun->SeqLength=toint(ch, sz)&0xFF;
									ch+=sz;
									tmpDapExtraFun->SeqVal=toint(ch, sz);
									SeqCnt--;
								if(SeqCnt)
								{
									tmpDapExtraFun++;
									ch+=4;
								}
								else
									ch-=2;//should be 4,it is for the old fun
								}while(SeqCnt>0);
 							}
						}
							break;						
						case FT_SEQUENCE:	
						{
							uint32_t SeqCnt=toint(ch+1, sz);
							xSAPSeq *tmpSeq=NULL;
							xap->SEQCnt=SeqCnt;
							xap->SapSequence=(xSAPSeq *)malloc(sizeof(xSAPSeq)*SeqCnt);
							ch += sz + 2;
							if(xap->SapSequence)
							{
								tmpSeq=xap->SapSequence;
								do{
								sz=4;

								tmpSeq->SeqAddress=toint(ch, sz);
								ch+=sz;
								tmpSeq->IsNeedConvert=(toint(ch, sz)>>16)&0xFF;
								tmpSeq->RW=(toint(ch, sz)>>8)&0xFF;	
								tmpSeq->SeqLength=toint(ch, sz);
								ch+=sz;
								tmpSeq->SeqVal=toint(ch, sz);
								SeqCnt--;
								if(SeqCnt)
								{
									ch+=4;
									tmpSeq++;
								}
								else
									ch-=2;//should be 4,it is for the old fun
								}while(SeqCnt>0);
							}
						}
						break;
						case 	FT_AT_ERASEPIN:
						{
							sz=1;
							gNeedErasePin=toint(ch+1, sz);
						}
						break;
						case FT_NRF_EXTRAFUN:
						{
							
							int cnt;
							sz=4;
							if(NRF_UICR_BASE==toint(++ch, sz))//UICR
							{
								gNrfUICRExtra=1;
								ch+=sz;
								cnt=toint(ch, sz);
								ch+=sz;
								pnrf_uicr_t.CLENR0=toint(ch+UICR_CLENR0_OFFSET, sz);

								pnrf_uicr_t.RBPCONF=toint(ch+UICR_RBPCONF_OFFSET, sz);
		
								pnrf_uicr_t.XTALFREQ=toint(ch+UICR_XTALFREQ_OFFSET, sz);

								pnrf_uicr_t.FWID=toint(ch+UICR_FWID_OFFSET, sz);

								pnrf_uicr_t.BOOTLOADERADDR=toint(ch+UICR_BOOTLOADERADDR_OFFSET, sz);
								if(cnt)
									ch+=cnt;
								ch-=sz;
								ch-=2;
								
							}
						
						
						}
							
							break;	
						case FT_PROG_OFFSET:  	 xap->ProgOffset  = 	 toint(ch+1, sz); break;
						case FT_NRF_LOCK:  	 			xap->NrfLock  = 	 toint(ch+1, sz); break;
            case FT_END_SYMBOL:       ch = buff;                             break;

            default:		           break;
        }
        ch += sz + 2;
    }

    if (xap->vendor == (KINETIS << 24)) {
        if (xap->startBF == 0) {
            if ((xap->szRam + (xap->startPC & 0xFFFFFFFE))  > (((xap->startSP+0x3FF)&0xFFFFFC00) + xap->szProg)) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else {
                temp = (xap->startPC & 0xFFFFFFFE) + xap->szPrgCode + xap->szPrgData;
                temp = (temp + 0x0F) & 0xFFFFFFF0;   // buffer address
                xap->startBF = temp;
            }
        }
    } else {
        if (xap->startBF == 0) {
            if (xap->szRam == 0) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else if (xap->szRam > 0 && ((xap->szRam + (xap->startPC & 0xFFFFFFFE))  > (((xap->startSP+0x3FF)&0xFFFFFC00) + xap->szProg))) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else {
                temp = (xap->startPC & 0xFFFFFFFE) + xap->szPrgCode + xap->szPrgData;
                temp = (temp + 0x0F) & 0xFFFFFFF0;   // buffer address
                xap->startBF = temp;
            }
        }
    }
		if (xap->vendor == (NODRIC << 24)) {
			if(xap->NrfLock)
				gNeedLockChip=1;
		}
    iP("\n RAM Size=%d, PC=0x%08X, SP=0x%08X, BF=%08X", xap->szRam, xap->startPC, xap->startSP, xap->startBF);

    xap->sectors[i>>1].szSector = 0xFFFFFFFF;
    xap->sectors[i>>1].startSector = 0xFFFFFFFF;
    xap->cfgregs[j++] = 0xFFFFFFFF;
    xap->cfgregs[j++] = 0xFFFFFFFF;

   // if (xap->vendor == (KINETIS << 24))
    //    xap->resett = 0x41;

    swd_calctime();

    xap->dapstate |= PREQ_SAPLOADED;
    dP("\nparseSAP Body()::Finished");
    if (xap->sreset)
        xap->resett = xap->sreset;

    if (xap->svoltage)
        xap->voltage = xap->svoltage;

    if (xap->sswj) 
        xap->swj = xap->sswj;

    if (xap->swj != SWJ_JTAG)
        xap->swj = SWJ_SWD;
    else
        xap->dapspeed = 100000;

    if (xap->dapspeed == 0)
        xap->dapspeed = SWJ_CLOCK_SPEED;
		if(clk_flag)
			xap->dapspeed = SWJ_CLOCK_SPEED/2;//try the half clock
    if (xap->slogging)
        xap->logging = xap->slogging;

    if (xap->logging > 0) {
        xap->logging <<= 2;
///////??????        file_ext_replace((char *)xap->sapfname, (char *)xap->logname, "LOG");
    }
#endif
//		xap->szProg=xap->szProg*2;
    return DAP_OK;
}

/*----------------------------------------------------------------------------*/
/* fileoff -> ram address, size
 * R0      -> R1,          R2
 * xap->offCode, xap->startPC & (0xFFFFFFFE), xap->szCode 
 * */
static	U32 rest=0;
static	U32 off=0;
static	U32 read_time=0;
volatile static	U32 read_cnt=0;
static	U32 start_add=0;
static	U32 CodeRest=0;
static	U32 CodeRead=0;
#define MIN(a,b) (a<b?a:b)
int swd_load_ram(xDAPPacket *xPacket)
{		// load flash algorithm
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    U32 temp;
    int i;
    U8	*resp = FUNRESPONSE(xPacket);
	
    switch(dap->funstate) {
        case 0:			// put register
			if(dbg_deep > 0)
            cP("\nswd_load_ram(file offset=%08x, target address=%08x, size=%08x)", dap->regs[0], dap->regs[1], dap->regs[2]);
            SYNCSEQ();

					  sub->adr = 0;
						dap->xport = 1;
						CodeRest=dap->regs[2];
						rest=dap->regs[2];
						start_add=dap->regs[1];
READ_SAP_NEXT:
            start_add=dap->regs[1]+off;
						if(rest>0x400)
						{
								sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+off, 0x400);
							  read_time++;
								rest=rest-0x400;
								sub->adr = 0;
								off+=0x400;
							CodeRead=0x400;
							CodeRest=0x400;
						}else if(rest>0)
						{						
							sub->buf = loadSapFile(xap->sapfname, dap->regs[0]+off, rest);	// load algorithm
							sub->adr = 0;
							CodeRead=rest;
							CodeRest=rest;
							rest=0;
							
						}
						

						if (sub->buf == NULL) {
                DAP_SetLed(dap->xport, REDLED, LEDON);
                dap->xport = 0;

                //IiP(currcmd, OPT_NF|OPT_ERR|OPT_A2C, FR_READ_FAIL, ErrMsg(FR_READ_FAIL, OPT_ERR));
                xUIMsg(0, OPT_NF|OPT_ERR|OPT_CODE, 0, FR_READ_FAIL);
									rest=0;
									off=0;
									read_time=0;
									read_cnt=0;
                return DAP_ERROR;
            }
			//printf_memory(dap->regs[2],sub->buf);

LOADRAM_LOAD:
						temp = start_add+sub->adr;//dap->regs[1] + sub->adr;		// target address
            if (temp & 0x03)
                dP("\n!!!Target address not word aligned!!!");

		     dap->regs[7]  = CodeRest;//dap->regs[2]; 
					if(dap->regs[7]>=0x400)	
					{
           dap->regs[7] = 0x400;  // size to be transfer, never larger than 1K.
            //if (temp & 0x3FF) {     // buffer cross 1k boundary
             //   dap->regs[7] -= (temp & 0x3FF);  // size to be transfer current transfer, avoid 1k boundary
            //} 
					}
						if(temp & 0x3FF)
							dap->regs[7] =MIN(dap->regs[7],(0x400-(temp & 0x3FF)));
					
            dP("\nWrite AP TAR %X", temp);
            FUNJUMP(dap, 1);
									dump_targetmem(xPacket,0x8000000);
            dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									rest=0;
									off=0;
									read_time=0;
									read_cnt=0;
								
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								
								}
            }

            if (PORTSSYNC(dap)) {
                //dap->regs[7] = SWD_BOUNDARY;
                dap->regs[8] = 0; 

                if ((sub->adr + dap->regs[7]) > dap->regs[2])
                    dap->regs[7] = dap->regs[2] - sub->adr;
            } else {
                return DAP_OK;
            }


        case 2:
TRANSFER_NEXTBLOCK:
			FUNJUMP(dap, 3);
            i = dap->regs[7] - dap->regs[8];
            if (0)//i >= LONG_BLOCK_SIZE) {
						{
							App_DAP_ChangePktSize(1);
                dap->exp = swd_block_write(swd_flush(port), LONG_BLOCK_SIZE>>2, (U8 *)(sub->buf + sub->adr + dap->regs[8]));	
                dap->regs[8] += LONG_BLOCK_SIZE;
            } else {
                if (i > SHORT_BLOCK_SIZE) {
                    dap->exp = swd_block_write(swd_flush(port), SHORT_BLOCK_SIZE>>2, (U8 *)(sub->buf +sub->adr+dap->regs[8]));	
                    dap->regs[8] += SHORT_BLOCK_SIZE;
                } else if (i > 0) {
#if 1                    
                    i = (i>4)?(i>>2):1;
                    dap->exp = swd_block_write(swd_flush(port), i, (U8 *)(sub->buf +sub->adr+dap->regs[8]));	 
#else
                    dap->exp = swd_block_write(swd_flush(port), i>>2, (U8 *)(sub->buf +sub->adr+dap->regs[8]));	 
#endif
                    dap->regs[8] += i<<2;

                    dap->regs[8] = dap->regs[7];
                } 
            }
			 if(dap->regs[7] - dap->regs[8] > 0)
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }
            }
            if (resp[0] == 0x06) {
                if (resp[3] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									rest=0;
									off=0;
									read_time=0;
									read_cnt=0;
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                if(dap->regs[8] < dap->regs[7]) {
                    goto TRANSFER_NEXTBLOCK;
                }

                sub->adr += dap->regs[8];
                FUNJUMP(dap, 4);
                SYNCSEQ();
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
            }
            return DAP_OK;

        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if ((temp & 0xf0000040) == 0xf0000040) {
                        dap->rport |= port;
                    }
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    cP("\n!!! swd_load_ram()::command transfer error(%02x), port[%1x]", resp[2], port);
                }
            }
            if (resp[0] == 0x06) {
                if (resp[3] != SWD_ACK_OK) {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    cP("\n!!! swd_load_ram()::block transfer error(%02x), port[%1x]", resp[2], port);
                }
            }

            if (PORTSSYNC(dap)) {
                if((sub->adr < MIN(CodeRead,0x400))) {
								{
                  if(CodeRest>dap->regs[7])
									CodeRest-=dap->regs[7];
									goto LOADRAM_LOAD;
								}
                }else if(rest)
								{
									if(CodeRest>dap->regs[7])
									CodeRest-=dap->regs[7];
									goto READ_SAP_NEXT;
								}
								else {
									rest=0;
									off=0;
									read_time=0;
									read_cnt=0;
                  return DAP_GONEXT;
                }
            }
            return DAP_OK;

#if 0 /////????? !defined(SPIDER)
LOADRAM_READBACK:   // no readback in multiport version 
            temp = dap->regs[1] + sub->adr;
            dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            dap->funstate = 3;
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    int k, j;
                    k = 0x400;
                    j = 0;
                    if ((sub->adr + 0x400) > dap->regs[2])
                        k = dap->regs[2] - sub->adr;

                    while(j<k) {
                        if ((j + SHORT_BLOCK_SIZE) > k) {
                            swd_block_read(port, (k-j)>>2);
                            j = k;
                        } else {
                            swd_block_read(port, SHORT_BLOCK_SIZE>>2);
                            j += SHORT_BLOCK_SIZE;
                        }

                    }
                    sub->adr += j;
                    dap->funstate = 4;
                    dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
                }else
									return DAP_AGAIN;
            }
            return DAP_OK;

        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if ((temp & 0xf0000040) == 0xf0000040) {
                        if(sub->adr < dap->regs[2])
                            goto LOADRAM_READBACK;
                        else
                            return DAP_GONEXT;
                    }
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (resp[0] == 0x06) {
                if (resp[3] == SWD_ACK_OK) {
                    int i, j=resp[1] | resp[2] << 8;

                    for(i=0;i<j;i++) {
                        temp  = resp[4 + i*4];
                        temp |= resp[5 + i*4] << 8;
                        temp |= resp[6 + i*4] << 8 * 2;
                        temp |= resp[7 + i*4] << 8 * 3;
                        if ((i&0x07)==0)
                            cP("\n    >>>");
                        cP("%08x ", (unsigned) temp);
                    }
                }else{
                    cP("\nswd_load_ram::Block Transfer ERROR(%02x)", resp[3]);
                }
            }
            return DAP_OK;
#endif
        default:
            break;
    }
    return DAP_OK;
}

/* --------------------------------------------------------------------------- */
int swd_read_dummy(xDAPPacket *xPacket)
{	
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    //int i;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:			// put register
            SYNCSEQ();
            dap->regs[0] = 0;
DUMMY_READ_NEXT:            
            if (dap->regs[0] >= xap->szData || dap->regs[0] >= 0x400)
                goto DUMMY_READ_END;
            temp = xap->startFlash + dap->regs[0];
            JUMPFUN(1);
            dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                JUMPFUN(2);
                temp = xap->szData - dap->regs[0];
                if (temp > 56)
                    temp = 56;

                dap->exp = swd_block_read(port, temp >> 2);
                dap->regs[0] += temp;
            } 
            return DAP_OK;
        case 2:
            if (PORTSEQEQ(port)) { 
                if (resp[3] == SWD_ACK_OK) {
                    dap->rport |= port;
                }
            }

            if (PORTSSYNC(dap)) {
                JUMPFUN(3);
                goto DUMMY_READ_NEXT;
                //dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
            } 
            return DAP_OK;
        case 3:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    if ((temp & 0xF0000040) == 0xF0000040)
                        dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
DUMMY_READ_END:            
                return DAP_GONEXT;
            }
            return DAP_OK;
        default:
            ;
    }
    return DAP_OK;
}

/* --------------------------------------------------------------------------- */
/* 
 * swd_read_serial()
 * */
int swd_read_serial(xDAPPacket *xPacket)
{	
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    int i;
    U8	*resp = FUNRESPONSE(xPacket);
    xSerial *xs = (xSerial *) (xsf->sbuff + dap->regs[11] * sizeof(xSerial));

    switch(dap->funstate) {
        case 0:			// put register
            SYNCSEQ();
            temp = xs->addr;		// target ram address
            if (temp & 0x03)
                dP("\n!!!Target address not word aligned!!!");

            JUMPFUN(1);
            dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                dP("\nswd_read_serial(target address=%08x, size=%d, type=%02X)", xs->addr, xs->size, xs->type);
                JUMPFUN(2);
                if (xs->type == SER_TYPE_CRC32)
                    temp = 1;
                else
                    temp = xs->size >> 2;
                swd_block_read(port, temp);
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
            } 
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    if ((temp & 0xF0000040) == 0xF0000040)
                        dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (resp[0] == 0x06) { 
                if (resp[3] == SWD_ACK_OK) {
                    temp = resp[1] | resp[2] << 8;    // length
                    if (xs->type == SER_TYPE_CRC32)
                        temp = 1;
                    dP("\n\tData==>");
                    for (i=0;i<temp;i++) {
                        xs->data[P2I(port)][4*i+0] = resp[4*i+4];
                        xs->data[P2I(port)][4*i+1] = resp[4*i+5];
                        xs->data[P2I(port)][4*i+2] = resp[4*i+6];
                        xs->data[P2I(port)][4*i+3] = resp[4*i+7];
                        dP("%02X %02X %02X %02X ", resp[4*i+4], resp[4*i+5], resp[4*i+6], resp[4*i+7]);
                    }
                }else{
                    wP("\nswd_read_serial::Block READ ERROR(%02x)", resp[3]);
                }
                return DAP_OK;
            }

            if (PORTSSYNC(dap)) {
                JUMPFUN(3);
                SYNCSEQ();
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
            }
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if ((temp & 0xf0000040) == 0xf0000040) {
                        dap->rport |= port;
                    }
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    wP("\n!!! swd_read_serial()::command transfer error(%02x), port[%1x]", resp[2], port);
                }
            }
            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */
/* 
 * swd_load_serial()
 *     m4 ram address -> target ram address, size 
 *     rets[i],       -> r1,                 r2
 * */
int swd_load_serial(xDAPPacket *xPacket)
{	
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    int i;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:			// put register
            SYNCSEQ();
            temp = dap->regs[1];		// target ram address
            if (temp & 0x03)
                dP("\n!!!Target address not word aligned!!!");

            FUNJUMP(dap, 1);
            dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                xSerial *xSer = (xSerial *)dap->regs[2];
                cP("\nswd_load_serial(target address=%08x, size=%d)", dap->regs[1], xSer->size);
                FUNJUMP(dap, 2);
                if (xSer->type == SER_TYPE_CRC32)
                    temp = 4;
                else 
                    temp = xSer->size;   // size of serial, and 4 alighment
                for (i=0;i<PORT_COUNT;i++) {
                    if(dap->xport & (1<<i)) {
                        //sub->buf = dap->rets[i];
                        dap->rport = dap->xport & ~(1<<i);  // exclude other port
                        swd_block_write(swd_flush(port), temp>>2, (U8 *)(xSer->data[i]));	
                    }
                }
                dap->rport = 0;
            } 
            return DAP_OK;

        case 2:
            if (resp[0] == 0x06) {
                if (resp[3] == SWD_ACK_OK) {
                    dap->rport |= port;
                }
            }

            if (PORTSSYNC(dap)) {
                FUNJUMP(dap, 3);
                SYNCSEQ();
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
            }
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if ((temp & 0xf0000040) == 0xf0000040) {
                        dap->rport |= port;
                    }
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    wP("\n!!! swd_load_serial()::command transfer error(%02x), port[%1x]", resp[2], port);
                }
            }
            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:
            break;
    }
    return DAP_OK;
}
/* ---------------------------------------------------------------------------` */
/* ram address, size
 * R1,          R2
 * 
 * */
int swd_read_ram(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    U32 temp;
    int i;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:			// put register
            //cP("\nswd_read_ram(target address=%08x, size=%08x)", dap->regs[1], dap->regs[2]);
            sub->adr = 0;
            if(sub->adr >= dap->regs[2])
                return DAP_GONEXT;

READRAM_LOAD:   // no readback in multiport version 
            JUMPFUN(1);
            temp = dap->regs[1] + sub->adr;
            if (temp & 0x03)
                dP("\n!!!Target address not word aligned!!!");

            dap->regs[7] = 0x400 - (temp & 0x3FF);
            if (dap->regs[7] == 0)
                dap->regs[7] = 0x400;

            dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }

            if (PORTSSYNC(dap)) {
                //dap->regs[7] = SWD_BOUNDARY;
                dap->regs[8] = 0; 

                if ((sub->adr + dap->regs[7]) > dap->regs[2])
                    dap->regs[7] = dap->regs[2] - sub->adr;
            } else {
                return DAP_OK;
            }
READRAM_NEXTBLOCK:            
        case 2:
            JUMPFUN(3);
            i = dap->regs[7] - dap->regs[8];
            if (i > SHORT_BLOCK_SIZE) {
                dap->exp = swd_block_read(swd_flush(port), SHORT_BLOCK_SIZE>>2);	
                dap->regs[8] += SHORT_BLOCK_SIZE;
            } else {
                dap->exp = swd_block_read(swd_flush(port), i>>2); 
                dap->regs[8] = dap->regs[7];
            } 
            return DAP_OK;

        case 3:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }
            }
            if (resp[0] == 0x06) {
                if (resp[3] == SWD_ACK_OK) {
                    dap->rport |= port;
                }
            }

            if (PORTSSYNC(dap)) {
                if(dap->regs[8] < dap->regs[7]) {
                    goto READRAM_NEXTBLOCK;
                }

                sub->adr += dap->regs[8];
                JUMPFUN(4);
                SYNCSEQ();
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);
            }
            return DAP_OK;

        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if ((temp & 0xf0000040) == 0xf0000040) {
                        dap->rport |= port;
                    }
                } else {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    wP("\n!!! swd_read_ram()::command transfer error(%02x), port[%1x]", resp[2], port);
                }
            }
            if (resp[0] == 0x06) {
                if (resp[3] != SWD_ACK_OK) {
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    wP("\n!!! swd_read_ram()::block transfer error(%02x), port[%1x]", resp[2], port);
                }
            }

            if (PORTSSYNC(dap)) {
                if(sub->adr < dap->regs[2]) {
                    goto READRAM_LOAD;
                } else {
                    return DAP_GONEXT;
                }
            }
            return DAP_OK;

        default:
            break;
    }
    return DAP_OK;
}
/* ---------------------------------------------------------------------------` */
/* address, size, buffer
 *      R0,   R1, R2
 * */
int swd_readmem(xDAPPacket *xPacket)
{		// load flash algorithm
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    xFun *sub = (xFun *)dap->pvFun;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:
            SYNCSEQ();
            sub->adr = dap->regs[0];
            sub->buf = (uint8_t *)dap->regs[2];
            dap->regs[2] = dap->regs[0] + dap->regs[1];

            if (dap->regs[1] == 0)
                return DAP_GONEXT;

//LOADRAM_READBACK:
            JUMPFUN(1);
            temp = sub->adr;
            dap->exp = swd_writeAP(swd_flush(port), MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                JUMPFUN(2);
                temp = dap->regs[1];
                if (temp > SHORT_BLOCK_SIZE)
                    temp = SHORT_BLOCK_SIZE;
                swd_block_read(port, temp>>2);
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, resp);
                dap->regs[1] -= temp;
            }
            return DAP_OK;


        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if ((temp & 0xf0000040) == 0xf0000040) {
                        dap->rport |= port;
                    }
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (resp[0] == SWD_BLOCK) {
                if (resp[3] == SWD_ACK_OK) {
                    int i, j=resp[1] | resp[2] << 8;

                    for(i=0;i<j;i++) {
                        *sub->buf++ = resp[4 + i*4];
                        *sub->buf++ = resp[5 + i*4];
                        *sub->buf++ = resp[6 + i*4];
                        *sub->buf++ = resp[7 + i*4];
                    }
                }else{
                    cP("\nswd_readmem::Block Transfer ERROR(%02x)", resp[3]);
                }
            }

            if (PORTSSYNC(dap)) {
                if (dap->regs[1] == 0)
                    return DAP_GONEXT;

                JUMPFUN(2);
                temp = dap->regs[1];
                if (temp > SHORT_BLOCK_SIZE)
                    temp = SHORT_BLOCK_SIZE;
                swd_block_read(port, temp>>2);
                dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, resp);
                dap->regs[1] -= temp;
            }
            return DAP_OK;


        default:
            break;
    }
    return DAP_OK;
}

/* --------------------------------------------------------------------------- */
int swd_rdp(xDAPPacket *xPacket)
{		// load flash algorithm
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    volatile U32 temp;
    U8	*resp = SUBRESPONSE(xPacket);

    switch(dap->substate) {
        case 0:
            nextstate(dap);
            dap->exp = swd_buff_cmd(swd_flush(port), SWD(SWD_DP,SWD_READ, dap->regs[0] & 0xFF), NULL);
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    cP("\nRead Port [%1x], DP(0x%02x) Value(0x%08x)", port, dap->regs[0]&0xff, temp);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;


        default:
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */
int swd_rap(xDAPPacket *xPacket)
{		// load flash algorithm
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
volatile    U32 temp;
    U8	*resp = SUBRESPONSE(xPacket);

    switch(dap->substate) {
        case 0:
            dap->exp = swd_buff_cmd(swd_flush(port), SWD(SWD_AP,SWD_READ, dap->regs[0] & 0xFF), NULL);
            dap->substate ++;
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    cP("\nRead Port[%1x], AP(0x%02x) Value(0x%08x)", port, dap->regs[0]&0xff, temp);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */
int swd_wdp(xDAPPacket *xPacket)
{		// load flash algorithm
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = SUBRESPONSE(xPacket);

    switch(dap->substate) {
        case 0:
            temp = dap->regs[1];
            dap->exp = swd_buff_cmd(swd_flush(port), SWD(SWD_DP,SWD_WRITE, dap->regs[0] & 0xFF), (U8 *)&temp);
            dap->substate ++;
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    cP("\nWrite PORT[%1x], DP(0x%02x) Value(0x%08x)", port, dap->regs[0]&0xff, dap->regs[1]);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */
int swd_wap(xDAPPacket *xPacket)
{		// load flash algorithm
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = SUBRESPONSE(xPacket);

    switch(dap->substate) {
        case 0:
            temp = dap->regs[1];
            dap->exp = swd_buff_cmd(swd_flush(port), SWD(SWD_AP,SWD_WRITE, dap->regs[0] & 0xFF), (U8 *)&temp);
            dap->substate ++;
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    cP("\nWrite PORT[%1x], AP(0x%02x) Value(0x%08x)", dap->regs[0]&0xff, port, dap->regs[1]);
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                return DAP_GONEXT;
            }
            return DAP_OK;

        default:
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */
int swd_polling_result(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    int i;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:			// put register
            dap->retries = 0;
            FUNJUMP(dap, 1);
            dap->timestamp = xTaskGetTickCount();
            INITSEQ(dap->outseq);
            dap->exp = read_DHCSR(port);
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if (temp & S_HALT) {
                        dap->rport |= port;
                    } else {
                        dap->vport |= port;
                    }
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            temp = xTaskGetTickCount() - dap->timestamp;
            if (dap->xport != dap->rport && temp > dap->reto) {
                dap->vport = dap->xport ^ dap->rport;
                for(i=0;i<PORT_COUNT;i++) {
                    if (dap->vport & (1<<i)) {
                        // send out error msg
                    }
                }
                DAP_SetLed(dap->vport, REDLED, LEDON);
                dap->xport = dap->rport;
                cP("\nTarget Timeout(%d)[%1x/%1x]!", dap->reto, dap->vport, dap->rport) ;
                temp = xTaskGetTickCount();
                cP("\nTarget Timeout[%d/%d]!", temp, dap->timestamp);
            } else 
                if (PORTSASYNC(dap)) {
                    dap->vport = 0;
                    INITSEQ(dap->outseq);
                    dap->exp = read_DHCSR(port);
                }
            if (PORTSSYNC(dap)) {
                FUNJUMP(dap, 2);
                prepCRegAccX(port);
                swd_readCREG(port, 0);
                INITSEQ(dap->outseq);
                //dap->exp = SWD_FLUSH_CMD(dap);//weib move
            }
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    dap->rets[P2I(port)] = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                }else {
                    /* TODO */
                    cP("\nrun()::Target read R0 failed(%02x)!!!", resp[2]);
                }
            }
            if (PORTSSYNC(dap)) {
                temp = xTaskGetTickCount() - dap->timestamp;
                dP("\nElapsed Time[%d], Since[%d]!", temp, dap->timestamp);
                return DAP_GONEXT;
            }
            break;
        default:
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */
int swd_runtarget(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    U32 temp;
    U8	*resp = FUNRESPONSE(xPacket);

    switch(dap->funstate) {
        case 0:			// put register
            SYNCSEQ();
            if (dap->reto < 1000)
                dap->reto = 1000;
            FUNJUMP(dap, 1);
            dap->exp = prepCRegAccX(port);
            //if(dap->exp == DAP_OK)
            dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL); 
            return DAP_OK;

        case 1:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                dap->regs[12] = 0;
                goto RUNTARGET_LOADREG;
            }
            return DAP_OK;

        case 2:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                }else
								{
									ReportMsgByAck(dap,resp[2]);
									return DAP_AGAIN;
								}
            }
            if (PORTSSYNC(dap)) {
                goto RUNTARGET_LOADREG;
            } else
                return DAP_OK;

RUNTARGET_LOADREG:
            SYNCSEQ();
            FUNJUMP(dap, 2);
            while(dap->regs[12] < 17) {
                if (dap->regacc & (1<<dap->regs[12])) {
                    swd_writeCREG(port, dap->regs[12], (U8 *)&dap->regs[dap->regs[12]]);
                } 
                dap->regs[12]++;
                if (dap->cmdidx > 9) {
                    dap->exp = SWD_FLUSH_CMD(dap);
                    return DAP_OK;
                }
            }

            if (dap->cmdidx > 0) {
                dap->exp = SWD_FLUSH_CMD(dap);
                return DAP_OK;
            }
						temp=0;
						while(temp<16)
             swd_readCREG(port, temp++);
						
						
            FUNJUMP(dap, 3);
            temp = DBGKEY | C_DEBUGEN;
            swd_buff_cmd(port & 0x03ff, SWD(SWD_AP,SWD_WRITE, (DCB_DHCSR & 0xF)), (U8 *)&temp);
            dap->exp = swd_readDP(swd_flush(port), DP_CTRL_STAT, NULL);		// 
            dap->dp_sel_value = 0xFFFFFFFF; // reflash
            dap->ap_tar_value = 0xFFFFFFFF;
            dap->timestamp = xTaskGetTickCount();
            return DAP_OK;

        case 3:
           if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {	// read 
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if (temp & SWD_DP_READOK) {
                        dap->rport |= port;
                    }
										else
										{
											ReportMsgByAck(dap,resp[2]);
											return DAP_AGAIN;
										}
                } else {
                    /* TODO */
									if(xap->vendor == (ATMEL<<24))
									{
										 if(dap->regs[0]==0x1FFFFFF0)//NVM Sec Bit
										 {
												return DAP_GONEXT;
										 }
									}
                    cP("\nTarget resume error!!!");
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, port, ALG_RUN_FAIL);
									return DAP_AGAIN;
                }
            }
            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                FUNJUMP(dap, 4);
                dap->exp = read_DHCSR(port);
            }
            return DAP_OK;

        case 4:
            if (PORTSEQEQ(port)) {
                if (resp[2] == SWD_ACK_OK) {
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if (temp & S_RETIRE_ST) {
											  if (temp & S_HALT) {
                            dap->rport |= port;
												}else
                        dap->vport |= port;
                    } else {
                        if (temp & S_HALT) {
                            dap->rport |= port;
                        } else {
                            dap->vport |= port;
                        }
                    }
                }else if (resp[2] == SWD_ACK_WAIT) {
                    /* TODO */
                    cP("\nWait target halt error(%02x)!!!", resp[2]);
                    //dap->vport |= port;
                    temp = resp[3] | resp[4]<<8 | resp[5]<<16 | resp[6]<< 24;
                    if (temp & S_HALT) {
                        dap->rport |= port;
                    } else {
                        dap->vport |= port;
                    }
                }else {
                    /* TODO */
									if(xap->vendor == (ATMEL<<24))
									{
										 if(dap->regs[0]==0x1FFFFFF0)//NVM Sec Bit
										 {
												return DAP_GONEXT;
										 }
									}
                    cP("\nWait target halt error(%02x)!!!", resp[2]);
                    DAP_SetLed(port, REDLED, LEDON);
                    dap->xport &= ~port;
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, port, ALG_RUN_FAIL);
									return DAP_AGAIN;
                }
            }
						
            temp = xTaskGetTickCount() - dap->timestamp;
				//if(dap->reto<0x1000)
				//		dap->reto=0xa000;
						if(dap->timetick>temp)
							dap->timetick=0;
						else if((temp-dap->timetick)>1000)
						{
							dap->timetick=temp;
							tP(".");//print a '.'every 2s
						}
            if (1!= dap->rport && temp > dap->reto) {
                dap->vport = dap->xport ^ dap->rport;
                for(port=0;port<PORT_COUNT;port++) {
                    if (dap->vport & (1<<port)) {
                        //dap->port = (1<<port);
                        //IiP(dap->manstate, OPT_NF|OPT_ERR|OPT_PORT|OPT_ADDR, dap->adr, ErrMsg(ALG_TIMEOUT, OPT_ERR));
                        xUIMsg(0, OPT_NF|OPT_ERR|OPT_CODE, (1<<port), ALG_TIMEOUT);
                    }
                }
                DAP_SetLed(dap->vport, REDLED, LEDON);
                dap->xport = dap->rport;
                eP("\nTarget Timeout(%d), Port[%1x]!", dap->reto, dap->vport) ;
                temp = xTaskGetTickCount();
                eP("\nTarget Timeout[%d/%d]!", temp, dap->timestamp);
								return DAP_AGAIN;
            } else {
                if (PORTSASYNC(dap)) {
                    dap->vport = 0;
                    SYNCSEQ();
                    dap->exp = read_DHCSR(port);
                }
            }

            if (PORTSSYNC(dap)) {
                SYNCSEQ();
                FUNJUMP(dap, 5);
                prepCRegAccX(port);

                swd_readCREG(port, 0);
							
							//swd_readCREG(port, 15);
               // dap->exp = SWD_FLUSH_CMD(dap);//weib move
            }
            return DAP_OK;

        case 5:
            if (PORTSEQEQ(port)) { 
                if (resp[2] == SWD_ACK_OK) {
                    dap->rport |= port;
                    temp = resp[3] | resp[4] << 8 | resp[5] << 16 | resp[6] << 24;
                    dap->rets[P2I(port)] = temp;
                    //if (temp)
                    dP("\nrun(%08x)::Target R0(%08x)!!!", dap->adr, temp);

                }else {
                    /* TODO */
                    cP("\nrun()::Target read R0 failed(%02x)!!!", resp[2]);
                    dap->xport &= ~port;
                    DAP_SetLed(port, REDLED, LEDON);
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, port, ALG_RUN_FAIL);
									
                }
            }
            if (PORTSSYNC(dap)) {
                temp = xTaskGetTickCount() - dap->timestamp;
                dP("\nElapsed Time[%d], Since[%d]!", temp, dap->timestamp);
                return DAP_GONEXT;
            }

            return DAP_OK;
        default:
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */

int swd_check_power(void)
{
	return 0;
	#if 0 ///////////????????????
    int i, j;
    uint8_t vv, pp, vw[] = {183,132,100,66};
    uint8_t res = 0;

    power_control(0, 0);    // power off all port

    for(i=0;i<8;i++) {                  /// Sampling 8 times
        ADC_SoftwareStartConv(ADCx);
        vTaskDelay(10);

        cP("\nChecking Voltage...");
        if (i<4) continue;      // skip unstable measurings
        for(j=0;j<PORT_COUNT;j++) {
            if (adcbuf0[j] > 0x100)
                res |= (1<<j);          // voltage detected on this port, mark it as failed!
        }
    }

    if (res) {
        cP("\nTarget already powered.");
        return (res<<PORT_COUNT);
    }

    for (i=0;i<10;i++) {    // charging in advance to avoid overcurrent
        power_control(1, 0);    
        vTaskDelay(1);
    }
    
    power_control(0, 0);    vTaskDelay(10);

    /* vv = 0, 1V8
     *      1, 2V5
     *      2, 3V3
     *      3, 5V
     *  start from 2, 3V3
     */
    for (vv=2;vv<4;vv++) {
        uint16_t hh, ll;
        hh = (0xFFF * 115/vw[vv]);
        ll = (0xFFF * 85/vw[vv]);
        if (ll >= 0xFFF)
            ll = 0xF00;

        for (pp=0;pp<PORT_COUNT;pp++) {
            power_control((1<<vv), (1<<pp));  

            for (i=0;i<8;i++) {
                ADC_SoftwareStartConv(ADCx);
                vTaskDelay(10);
                if (i<4)    // skip unstable measurings
                    continue;

                cP("\nChecking Voltage...");
                for(j=0;j<PORT_COUNT;j++) {
                    if (j==pp) { // check range
                        if (adcbuf0[j] < ll || adcbuf0[j] > hh) {
                            // voltage out of range
                            res = 1<<(pp+PORT_COUNT) | (1 << vv);
                            return res;
                        }
                    } else {    // check if no power, should be no power detected
                        if (adcbuf0[j] > 0x100) {
                            // voltage leaked
                            res = 1<<(pp+PORT_COUNT) | (1 << vv);
                            return res;
                        }
                    }
                }
            }
            power_control(0, 0); // turn off
            vTaskDelay(10);
        }
    }

return 0xFF;
		#endif
}

/* --------------------------------------------------------------------------- */
int swd_power_control(xDAPPacket *xPacket)
{
	//return DAP_GONEXT;
	#if 1 /////////??????????
    int i, j, k;
    uint8_t pwrsts[PORT_COUNT];
		xDAPPort *dap = &pPort;
    uint8_t VCHECK = PWR_3V3;
		uint16_t port=xPacket->port;
		App_DAP_SWJ_Pins(port, 0x00, 0x01, 0x0);		
		App_DAP_SWJ_Pins(port, 0x80, 0x80, 0x0);	
    switch (dap->substate) {
        default:
            eP("\nswd_power_control() unknown state(%d)?", dap->substate);
        case 0:
            dap->led[0] = 0;
            dap->led[1] = 0;
            if ((hw_version(0) < 2)) {
                dap->regs[16] = DAP_GONEXT;
                goto POWER_LED;
            }
            dap->CTRL |= CTRL_NOQUERY;  // stop query

            /** Check if power ready */
            for(i=0;i<PORT_COUNT;i++)
                pwrsts[i] = 0;
                ///cP("\nChecking Voltage...");
            for(i=0;i<8;i++) {                  /// Sampling 8 times
                ADC_SoftwareStartConv(ADCx);
                vTaskDelay(10);

                for(j=0;j<PORT_COUNT;j++) {
                    pwrsts[j] = pwrsts[j]<<1;
                    if (adcbuf0[j] > 0x400)
                        pwrsts[j] |= 1;
                }
            }

            dap->xport = 0;
            for(i=0;i<PORT_COUNT;i++) {
                if ((pwrsts[i] & 0x7) == 0x7) {
                    dap->xport |= (1<<i);
                }
            }

            if (dap->xport) {
                for(i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                    } else {
                        xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_NO_POWER);
                        DAP_SetLed((1<<i), REDLED, LEDON);
                        cP("\nTarget No Power[%1X]!", (1<<i));
                    }
                }
                cP("\nTarget already powered.");
                dap->regs[16] = DAP_GONEXT;
                goto POWER_LED;
            } else if (xap->voltage == 0) {
                for(i=0;i<PORT_COUNT;i++) {
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_NO_POWER);
                    DAP_SetLed((1<<i), REDLED, LEDON);
                    eP("\nTarget No Power[%1X]!", (1<<i));
                }
                dap->regs[16] = DAP_GONEXT;
                goto POWER_LED;
            }
#if 0
            EXTI->IMR &= ~EXTI_Line0;    // disable overcurrent interrupt 
            EXTI->IMR &= ~EXTI_Line3;    // disable overcurrent interrupt 
            EXTI->IMR &= ~EXTI_Line10;    // disable overcurrent interrupt 
            EXTI->IMR &= ~EXTI_Line14;    // disable overcurrent interrupt 
#endif
             cP("\nPower ON[T:%d] ", xTaskGetTickCount());          
						for (k=0;k<5;k++) {
                /** Check if short */

                power_control(VCHECK, 0xF);  // 3V3, should be 1.8V final, power on
                dP("[T:%d] ", xTaskGetTickCount());
                for (j=0;j<PORT_COUNT;j++)
                    pwrsts[j] = 0;

                for (i=0;i<6;i++) {
                    ADC_SoftwareStartConv(ADCx);
                    vTaskDelay(10);

  ///                  cP("\nChecking Short...");
                    for(j=0;j<PORT_COUNT;j++) {
                        pwrsts[j] = pwrsts[j]<<1;
                        if (adcbuf0[j] < 0x200)
                            pwrsts[j] |= 1; // short
                    }
                }

                dap->xport = (1<<PORT_COUNT) - 1;
                cP("[T:%d] ", xTaskGetTickCount());
                for(i=0;i<PORT_COUNT;i++) {
                    if (pwrsts[i] & 0x07) {
                        dap->xport &= ~(1<<i); 
                    }
                }

                dap->vport = 0;
                for(i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) {
                    } else {
                        //xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_PWR_SHORT);
                        //DAP_SetLed((1<<i), REDLED, LEDON);
                        cP("\nPort short[%1X]!", (1<<i));
                        dap->vport |= (1<<i);
                    }
                }
                if (dap->vport == 0)
                    break;
                else {
                    power_control(0,0);  // power off
                    cP("Re-Check Short...");
                    vTaskDelay(30);
                }
            }
 //           EXTI->IMR |= VDDFAULT_EXTI_Pins;    // enable overcurrent interrupt

            for(i=0;i<PORT_COUNT;i++) {
                if (dap->xport & (1<<i)) {
                } else {
                    xUIMsg(dap->manstate, OPT_NF|OPT_ERR|OPT_CODE, (1<<i), TE_PWR_SHORT);
                    DAP_SetLed((1<<i), REDLED, LEDON);
                    eP("\nPort short[%1X]!", (1<<i));
                }
            }

            if (dap->xport == 0) {
                dap->regs[16] = DAP_ERROR;
            } else {
                if (xap->voltage != VCHECK) {
                    power_control(PWR_0V,0);  // power off
                    vTaskDelay(10);
                    power_control(xap->voltage, dap->xport);
                    dP("[T:%d] ", xTaskGetTickCount());
                }
                dap->regs[16] = DAP_GONEXT;
            }
POWER_LED:
            dap->CTRL &= ~CTRL_NOQUERY;
            dap->funstate = 0;
            JUMPSUB(1);
            return swd_led(xPacket);            

        case 1:
            if (DAP_GONEXT != swd_led(xPacket))
                return DAP_OK;
            xap->dapstate |= PREQ_PWR_READY;
            return dap->regs[16];
    }
#endif	
}

/*----------------------------------------------------------------------------*/

int swd_erasechip(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
								#if 0
						if (xap->vendor == (ATMEL << 24)) 
						{
							if(!gAtmelHasErase)
							{
							#ifndef STM32F10X_MD
								PORTC->PCR[3] = PORT_PCR_MUX(1);					
								PTC->PSOR =  (1 << 3); 
								vTaskDelay(500);
								PTC->PCOR =  (1 << 3); 
							#else
							
							#endif
								gAtmelHasErase=1;
								return DAP_GONEXT;
							}else
								return DAP_GONEXT;
						}
						#endif
														#if 1
						if (xap->vendor == (NODRIC << 24)) 
						{
							//USE Erase ALL!!
							//uint32_t val=0x0;
							Nrf51EraseAll(xPacket,xap->ProgOffset);
							return DAP_GONEXT;
						//	read_targetreg(xPacket,0x4001E504,&val);//nvm config
						//	val=0x2;
						//	write_targetreg(xPacket,0x4001E504,&val);
					//		read_targetreg(xPacket,0x4001E504,&val);//nvm config
						}
						#endif
    switch(dap->substate) {
        case 0:				// runInit
            if (currcmd == SM_AUTO) 
                dap->port = swd_gettime(SM_ERASECHIP);
            else
                dap->port = 0xFF;
            IiP(SM_ERASECHIP, OPT_NF|OPT_ESTI, xap->szFlash|xap->startFlash, MSGSTR[ALG_EC_START]);
            iP("\n%s", MSGSTR[ALG_EC_START]);
            dap->funstate = 0;
            nextstate(dap);
            dap->timeout = 1000;
            dap->adr = xap->startFlash;
            dap->regs[0] = xap->startFlash;
            dap->regs[1] = xap->speedClock;
            dap->regs[2] = 1;	/* function, 1: Erase */
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E207;	
            return swd_runtarget(xPacket);

        case 1:				// Init() Done.
					runrc=swd_runtarget(xPacket);
            if (DAP_GONEXT == runrc) {
                check_alg_result(ALG_INIT_FAIL, 0);
                // dP("\nEraseChip()::Init OK[%1x]/Failed[%1x]", dap->rport, dap->vport);
                // dap->regs[10] = dap->xport;  // store Init OK port, no need
                goto ERASECHIP;
            }else
							return runrc;
            //return DAP_OK;

ERASECHIP:					// run EraseSector
            dap->adr = xap->startFlash;
            dap->regs[0] = xap->toErase;
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funEraseChip+ (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E207;	
#if 1            
            dap->reto = xap->toErase * (xap->szFlash/0x1000);///15;
						if(dap->reto==0)
									dap->reto=xap->toErase;				
            dP("\nSet Erase timeout %d ms", dap->reto);
#else
            dap->reto = 0;
            for (i=1;i<MAX_SECTOR_COUNT;i++) {
                if (xap->sectors[i].szSector == 0xFFFFFFFF || xap->sectors[i].startSector == 0xFFFFFFFF) {
                    dap->reto += xap->toErase * ((xap->szFlash - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
                    break;
                } else {
                    dap->reto += xap->toErase * ((xap->sectors[i].startSector - xap->sectors[i-1].startSector)/xap->sectors[i-1].szSector);
                }
            }
            if (xap->tDebug > 0x10)
                dap->reto <<= 2;
#endif            
            FUNJUMP(dap, 0);
            swd_runtarget(xPacket);
            dap->substate = 2; 
            return DAP_OK;

        case 2:
						runrc=swd_runtarget(xPacket);
            if (DAP_GONEXT == runrc) {
                check_alg_result(ALG_EC_FAIL, ALG_EC_PASS);
                //dP("\nEraseChip()::Erase OK[%1x]/Failed[%1x]", dap->rport, dap->vport);
                //if (dap->rport) { // EraseChip OK, next
                //    dap->port = dap->rport;
                //    IiP(SM_ERASECHIP, OPT_NF, NULL, MS_CMD_EC_OK);
                //} 
                //if (dap->vport) {				
                //    dap->port = dap->vport;
                //    IiP(SM_ERASECHIP, OPT_NF|OPT_ERR, NULL, MS_CMD_EC_FAIL);
                //}
                goto ERASECHIP_DONE;
            }else
							return runrc;
            //return DAP_OK;

ERASECHIP_DONE:
            dap->substate = 3;
        case 3:				// runUnInit
            dap->regs[0] = 1;	// function, 1: Erase
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funUnInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E201;	
            FUNJUMP(dap, 0);
            dap->substate = 4;
            return swd_runtarget(xPacket);

        case 4:
           runrc=swd_runtarget(xPacket); 
					if (DAP_GONEXT == runrc) {
                check_alg_result(ALG_UNINIT_FAIL, NULL);
                printports("Target Flash Erased");
                dap->funstate = 0;
                JUMPSUB(5);
							vTaskDelay(200);
								tP("\r\n");
									uP("Full Chip Erase Done.\r\n\r\n");
								if (xap->funToDo & DO_PROGRAM)
								{
									vTaskDelay(100);
									//uP("Programmimg...\r\n");
								}
								
                return swd_led(xPacket);
            }else
							return runrc;
            //return DAP_OK;
        case 5:
            if (DAP_GONEXT != swd_led(xPacket))
						{
  
							return DAP_OK;
						}
            return DAP_GONEXT;

        default:				// runUnInit
            break;
    }
    return DAP_OK;
}
/* --------------------------------------------------------------------------- */
int swd_erasesector(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    int i;

    switch(dap->substate) {
        case 0:				// runInit
            if (currcmd == SM_AUTO) 
                dap->port = swd_gettime(SM_ERASESECTOR);
            else
                dap->port = 0xFF;

            if (xap->doChipEraseBySector == 1) {
                if (currcmd == SM_AUTO)
                    dap->port = swd_gettime(SM_ERASECHIP);
                IiP(SM_ERASESECTOR, OPT_NF|OPT_ESTI, xap->szFlash|xap->startFlash, MSGSTR[ALG_EC_START]);
            } else {
                if (xap->szFlash >= xap->szData){
                    IiP(SM_ERASESECTOR, OPT_NF|OPT_ESTI, xap->szData |xap->startFlash, MSGSTR[ALG_ES_START]);
                } else {
                    IiP(SM_ERASESECTOR, OPT_NF|OPT_ESTI, xap->szFlash|xap->startFlash, MSGSTR[ALG_ES_START]);
                }
            }
            dP("\n%s", MSGSTR[ALG_ES_START]);
            dap->regs[0] = xap->startFlash;
            dap->regs[1] = xap->speedClock;
            dap->regs[2] = 1;	/* function, 1:Erase */
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E207;	
            dap->timeout = 200; //xap->toErase;
            dap->reto = xap->toErase;
            if (xap->tDebug > 0x10)
                dap->reto <<= 2;
            dap->funstate = 0;
            nextstate(dap);
            return swd_runtarget(xPacket);

        case 1:				// Init() Done.
           runrc=swd_runtarget(xPacket); 					
					if (DAP_GONEXT == runrc) {
                check_alg_result(ALG_INIT_FAIL, NULL);

                dP("\nErasing Sector, result of Init(): ");
                if (dap->rport) dP("OK[%1x]", dap->rport);
                if (dap->vport) dP("Failed[%1x]", dap->vport);
                // dap->regs[10] = dap->rport; // any port Init() ok should run UnInit()

                // blankcheck(adr, sz, pat, timeout)
                //	start from each sector and stop at (dap->startFlash + dap->szData)
                //	adr = dap->sectors[0], sz=dap->sectors[i].szSector
                sub->i = 0;		// sector index
                sub->j = 0;
                goto ERASESECTOR_NEXT;
            }else
							return runrc;
            //return DAP_OK;

ERASESECTOR_START:
            dP("\nBlank Checking (%08x)...", (unsigned)dap->adr);
            dap->regs[0] = dap->adr;
            dap->regs[1] = xap->sectors[sub->i].szSector;
            dap->regs[2] = xap->patErase;
            dap->regs[3] = 0;
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funBlankCheck + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E20f;	
            dap->timeout = 1000;
            JUMPSUB(2);
            dap->funstate = 0;
            //swd_runtarget(xPacket);
            return DAP_OK;

        case 2:
            runrc= swd_runtarget(xPacket);
						if (DAP_GONEXT == runrc) {
                dap->rport = 0;
                dap->vport = 0;
								dap->vport |= (1);//weib add
                for (i=0;i<PORT_COUNT;i++) {
                    if ((dap->xport & (1<<i))) {
                        if (dap->rets[i] > 0) {
                            dap->vport |= (1<<i);   // not blank
                        } else {
                            dap->rport |= (1<<i);   // already blank
                        }
                    }
                }

  //              if(dap->vport == 0) { // all blank
  //                  sub->j += 1;	// next block
 //                   dap->regs[10] = 0;
  //                  goto ERASESECTOR_NEXT;
  //              }
                dap->regs[10] = dap->rport; // keep the already blank port.
                dap->xport = dap->vport;
								tP(".");
                cP("\nErasing Sector (%08x)...", (unsigned)dap->adr);
                //IiP(SM_ERASESECTOR, OPT_NF|OPT_ADDR, dap->adr, NULL);
                dap->timeout = 1000;
                dap->regs[0] = dap->adr;
                //dap->regs[1] = xap->toErase;
                dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
                dap->regs[13] = xap->startSP;
                dap->regs[14] = xap->startPC;
                dap->regs[15] = xap->funEraseSector + (xap->startPC & 0xFFFFFFFE);
                dap->regs[16] = 0x01000000;
                dap->regacc = 0x1E207;	
                dap->funstate = 0;
                JUMPSUB(3);
                swd_runtarget(xPacket);
            }else
							return runrc;
            //return DAP_OK;

        case 3: // ok?
						runrc= swd_runtarget(xPacket);
            if (DAP_GONEXT == runrc) {
                dap->rport = 0;
                dap->vport = 0;
                for (i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) { 
                        if (dap->rets[i] > 0) {
                            dap->port = (1<<i);
                            IiP(SM_ERASESECTOR, OPT_NF|OPT_ERR|OPT_A2C|OPT_PORT, ALG_ES_FAIL, ErrMsg(ALG_ES_FAIL, OPT_ERR));
                            dap->vport |= (1<<i);
                            dap->xport &= ~(1<<i);    // remove erase failed port.
                        } else {
                            dap->rport |= (1<<i);
                        }
                    }
                }
                dP("\nswd_erasesector(%08x)::", dap->adr);
                if (dap->rport) dP("OK[%1x] ", dap->rport);
                if (dap->vport) dP("Failed[%1x]!!!", dap->vport);
                DAP_SetLed(dap->vport, REDLED, LEDON);

                ///////////////////dap->xport = dap->regs[10] & ~(dap->vport); // restore running port.
                dap->xport |= dap->regs[10];    // add back already blank sector
                sub->j += 1;	// next block
                goto ERASESECTOR_NEXT;
            }else
							return runrc;
            //return DAP_OK;

ERASESECTOR_NEXT:
            if (( xap->sectors[sub->i].startSector + sub->j * xap->sectors[sub->i].szSector) >= xap->sectors[sub->i + 1].startSector) {
                sub->j = 0;
                sub->i += 1;
            }

            if(xap->sectors[sub->i].szSector == 0xFFFFFFFF && xap->sectors[sub->i].startSector == 0xFFFFFFFF) {
                goto ERASESECTOR_END;
            }

            dap->adr = xap->startFlash + xap->sectors[sub->i].startSector + sub->j * xap->sectors[sub->i].szSector;

            /*
             * DO_ERASECHIP without xap->funEraseChip()
             */
            if (xap->doChipEraseBySector && dap->adr < (xap->startFlash + xap->szFlash)) {
                goto ERASESECTOR_START;
            }

            if(dap->adr >= (xap->startFlash + xap->szData)) {
                goto ERASESECTOR_END;
            }
            if(dap->adr >= (xap->startFlash + xap->szFlash)) {
                goto ERASESECTOR_END;
            }
            goto ERASESECTOR_START;

ERASESECTOR_END:
            JUMPSUB(4);
            for (i=0;i<PORT_COUNT;i++) {    // for any port not been excluded, it passed the erase sector operation
                if (dap->xport & (1<<i)) {  
                    dap->port = 1<<i;
                    dap->rport |= (1<<i);
                    if (xap->doChipEraseBySector)
                        IeP(SM_ERASESECTOR, OPT_NF|OPT_PORT, NULL, MSGSTR[ALG_EC_PASS]);
                    else
                        IeP(SM_ERASESECTOR, OPT_NF|OPT_PORT, NULL, MSGSTR[ALG_ES_PASS]);
                }
            }
            if (xap->doChipEraseBySector)
                xap->doChipEraseBySector = 0;

            printports((char *)MSGSTR[ALG_ES_PASS]);
        case 4:				// runUnInit
            // dap->xport = dap->regs[10];  // removed ==> no need to restore init.
            dap->regs[0] = 1;
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funUnInit+ (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E201;	
            dap->timeout = 1000;
            FUNJUMP(dap, 0);
            JUMPSUB(5);
            dap->funstate = 0;
            return swd_runtarget(xPacket);

        case 5:
          runrc= swd_runtarget(xPacket);  
					if (DAP_GONEXT == runrc) {
                check_alg_result(ALG_INIT_FAIL, NULL);
                dap->funstate = 0 ;
                JUMPSUB(6);
						    uP("Erase Sector Done.");  
                return swd_led(xPacket);
                //return DAP_GONEXT;
            }else
							return runrc;
            //return DAP_OK;
        case 6:
            if (DAP_GONEXT == swd_led(xPacket))
						{

							return DAP_GONEXT;
						}
            return DAP_OK;


        default:				// runUnInit
            break;
    }
    return DAP_OK;
}
/*----------------------------------------------------------------------------*/
int swd_blankcheck(xDAPPacket *xPacket)
{
    U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    int i;
    uint32_t temp;

    switch(dap->substate) {
        case 0:				// runInit
            if (currcmd == SM_AUTO) 
                dap->port = swd_gettime(SM_BLANKCHECK);
            else
                dap->port = 0xFF;
            IiP(SM_BLANKCHECK, OPT_NF|OPT_ESTI, xap->szFlash|xap->startFlash, MSGSTR[ALG_BC_START]);
            dP("\n%s", MSGSTR[ALG_BC_START]);

            // invalidated flash cache
            temp = xap->startFlash + xap->szFlash - 64;
            swd_writeAP(port, MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            dap->ap_tar_value = temp;
            swd_block_read(swd_flush(port), 14);

            dap->regs[0] = xap->startFlash;
            dap->regs[1] = xap->speedClock;
            dap->regs[2] = 1;	/* function, 1:Erase */
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E207;	
            dap->timeout = 200; //xap->toErase;
            dap->reto = xap->toErase;
            if (xap->tDebug > 0x10)
                dap->reto <<= 2;
            dap->funstate = 0;
            nextstate(dap);
            swd_runtarget(xPacket);
            return DAP_OK;

        case 1:				// Init() Done.
            runrc=swd_runtarget(xPacket);
						if (DAP_GONEXT == runrc) {
                check_alg_result(ALG_INIT_FAIL, NULL);

                dP("\nBlankCheck()::Init OK[%1x]/Failed[%1x]", dap->rport, dap->vport);
                // dap->regs[10] = dap->rport;
                // dap->xport = dap->rport;

                // blankcheck(adr, sz, pat, timeout)
                //	start from each sector and stop at (dap->startFlash + dap->szData)
                //	adr = dap->sectors[0], sz=dap->sectors[i].szSector
                sub->i = 0;		// sector index
                sub->j = 0;

                if (xap->vendor == (KINETIS << 24)) {   // avoid memory cache issue
                    JUMPSUB(0x10);
                    dap->funstate = 0;
                    return swd_read_dummy(xPacket);
                } else {
                    goto BLANKCHECK_NEXT;
                }
            } else {
                return runrc;
            }
        case 0x10:
            if (DAP_GONEXT != swd_read_dummy(xPacket)) {
                return DAP_OK;
            }
            goto BLANKCHECK_NEXT;

BLANKCHECK_START:
            dap->substate = 1;
            dap->regs[0] = dap->adr;
            //IiP(SM_BLANKCHECK, OPT_NF|OPT_ADDR, dap->adr, NULL);
						tP(".");
            cP("\nBlank Checking (%08x)...", (unsigned)dap->regs[0]);
            dap->regs[1] = xap->sectors[sub->i].szSector;
            dap->regs[2] = xap->patErase;
            dap->regs[3] = 0;
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funBlankCheck + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E20f;	
            dap->timeout = 1000;
            dap->funstate = 0;
            swd_runtarget(xPacket);
            dap->substate = 2;
            return DAP_OK;

        case 2:
							runrc=swd_runtarget(xPacket);
						if (DAP_GONEXT == runrc) {

                for (i=0;i<PORT_COUNT;i++) {
                    if ((dap->xport & (1<<i)) && (dap->rets[i] > 0)) {
                        //dap->xport &= ~(1<<i);  // exclude failed port.
                        dap->port = (1<<i);
                        //@address Not Blank @XXXXXXXX
                        DAP_SetLed((1<<i), REDLED, LEDON);
                        IiP(SM_BLANKCHECK, OPT_NF|OPT_ERR|OPT_PORT|OPT_A2C, ALG_BC_FAIL, ErrMsg(ALG_BC_FAIL, OPT_ERR));
                        uP("\r\n Flash Address (0x%08x) not Blank\r\n",dap->adr);
                    }
                }

                if(dap->xport) { // blankcheck OK, next
                    sub->j += 1;	// next block
                    goto BLANKCHECK_NEXT;
                } else {				
                    goto BLANKCHECK_END;
                }
            }else
							return runrc;
            //return DAP_OK;

        case 3: // ok?
					runrc=swd_runtarget(xPacket);
            if (DAP_GONEXT == runrc) {
                dap->vport = 0;
                for (i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i)) { 
                        if (dap->rets[i] > 0) {
                            IiP(SM_BLANKCHECK, OPT_NF|OPT_ERR|OPT_A2C|OPT_PORT, ALG_BC_FAIL, ErrMsg(ALG_BC_FAIL,OPT_ERR));
                            dap->vport |= (1<<i);
                            dap->xport &= ~(1<<i);  // removed blankcheck failed port.
                            DAP_SetLed((1<<i), REDLED, LEDON);
                        } else {
                            dap->rport |= (1<<i);
                        }
                    }
                }
                dP("\nswd_blankcheck(%08x)::EraseSector::OK(%1x)/Failed(%1x)", dap->adr, dap->rport, dap->vport);

                sub->j += 1;	// next block
                goto BLANKCHECK_NEXT;
            }else
							return runrc;
            //return DAP_OK;

BLANKCHECK_NEXT:
            if ((xap->sectors[sub->i].startSector + sub->j * xap->sectors[sub->i].szSector) >= xap->sectors[sub->i + 1].startSector) {
                sub->j = 0;
                sub->i += 1;
            }
            if(xap->sectors[sub->i].szSector == 0xFFFFFFFF && xap->sectors[sub->i].startSector == 0xFFFFFFFF) {
                goto BLANKCHECK_END;
            }
            dap->adr = xap->startFlash + xap->sectors[sub->i].startSector + sub->j * xap->sectors[sub->i].szSector;
#if 0            
            if(dap->adr >= (xap->startFlash + xap->szData)) {
                goto BLANKCHECK_END;
            }
#endif            
            if(dap->adr >= (xap->startFlash + xap->szFlash)) {
                goto BLANKCHECK_END;
            }
            goto BLANKCHECK_START;

BLANKCHECK_END:
            dap->substate = 4;
            // message out blankcheck status...
            dap->rport = 0;
            for (i=0;i<PORT_COUNT;i++) {
                if (dap->xport & (1<<i)) {
                    dap->port = 1<<i;
                    dap->rport |= (1<<i);
                    IiP(SM_BLANKCHECK, OPT_NF|OPT_PORT, NULL, MSGSTR[ALG_BC_PASS]);
                    uP("\r\n Blank Check Done.\r\n");
                }
            }
            if (dap->rport == 0)
                goto ERASESECTOR_LED;
                //return DAP_GONEXT;

        case 4:				// runUnInit
            // temp = dap->xport;
            // dap->xport = dap->regs[10];     // any port init() ok, should be uninit()
            // dap->regs[10] = temp;

            dap->regs[0] = 1;
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funUnInit+ (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E201;	
            dap->timeout = 1000;
            FUNJUMP(dap, 0);
            dap->substate = 5;
            return swd_runtarget(xPacket);

        case 5:
						runrc=swd_runtarget(xPacket);
            if (DAP_GONEXT == runrc) {
                check_alg_result(ALG_UNINIT_FAIL, NULL);
                printports("Blank Check Uninit() OK");
                // dap->xport = dap->regs[10];     // any port init() ok, should be uninit()
                goto ERASESECTOR_LED;
                //return DAP_GONEXT;
            }else
							return runrc;
            //return DAP_OK;
ERASESECTOR_LED:
            dap->funstate = 0;
            JUMPSUB(6);
            return swd_led(xPacket);
        case 6:
            if (DAP_GONEXT == swd_led(xPacket))
						{

							return DAP_GONEXT;
						}
            return DAP_OK;


        default:				// runUnInit
            break;
    }
    return DAP_OK;
}
/*----------------------------------------------------------------------------*/

int swd_progpage(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    xFun *sub = (xFun *)dap->pvFun;
    int i;

    switch(dap->substate) {
        case 0:				// runInit

            dap->funstate = 0;
            if (currcmd == SM_AUTO) { 
                dap->port = swd_gettime(SM_PROGPAGE);
            } else
                dap->port = 0xFF;
            IiP(SM_PROGPAGE, OPT_NF|OPT_ESTI, xap->szData|xap->startFlash, MSGSTR[ALG_PP_START]);
            //temp = xTaskGetTickCount();
            iP("\n%s", MSGSTR[ALG_PP_START]);
            dap->regs[0] = xap->startFlash;
            dap->regs[1] = xap->speedClock;
            dap->regs[2] = 2;	/* function, 2: Program  */
            dap->regs[9] = xap->szPrgCode+ (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E207;	
            dap->timeout = 3000;
            JUMPSUB(1);
						dump_targetmem(xPacket,0x8000000);
            return swd_runtarget(xPacket);

        case 1:				// Init() Done.
            i = swd_runtarget(xPacket);
            if (i != DAP_GONEXT) {
                return i;
            }
						dump_targetmem(xPacket,0x8000000);
            check_alg_result(ALG_INIT_FAIL, NULL);
            DP("\nswd_progpage::Init()::OK(%1x)Failed(%1x)", dap->rport, dap->vport);
            dap->regs[4] =  0;       //xap->startFlash;	

PROGPAGE_NEXT:
            if ((dap->regs[4] <xap->szData) && (dap->regs[4] < xap->szFlash)) {
                //IiP(SM_PROGPAGE, OPT_NF|OPT_ADDR, dap->regs[4], NULL);
                dap->regs[0] = xap->offData + dap->regs[4];	// file offset
                dap->regs[1] = xap->startBF;        // target memory address
							if((dap->regs[4] +xap->szProg)<xap->szData)
                dap->regs[2] = xap->szProg;					// size
							else
								dap->regs[2] = xap->szData-dap->regs[4];					// size
                dap->regs[5] = dap->regs[1];				// data buffer starting address
                dap->funstate = 0;
                dap->substate = 2;
                i = swd_load_ram(xPacket);
							 if (xap->sap_version & SAP_ENCRYPTED) {    // data encrypted
                    int j;
                    uint32_t *ramsource = (uint32_t *)sub->buf;

                    for(j=0;j<dap->regs[2]>>2;) {
                      decTEA(&ramsource[j], enckeys);
                        j += 2;
                    }
                    xap->szCache = 0;
                }
                return DAP_OK;
            } else {	// Program Page Done.
                goto PROGPAGE_END;
            }

        case 2:
					dump_targetmem(xPacket,0x8000000);
           if ((i=swd_load_ram(xPacket)) != DAP_GONEXT) {
                return i;
            }
            dap->adr = dap->regs[4] + xap->startFlash+xap->ProgOffset;	// flash address to write
						if((dap->regs[4]%(50*(xap->szProg)))==0)
							tP(".");//print a '.' ervey 50 packet
							
LOAD_SERIAL:         ///// program serial, load serial data to target memory
            if ((currcmd == SM_AUTO) && (dap->program_serial) && (xsf->toW > 0)) {
                xSerial *xs;
                for (i=0;i<xsf->cntSer;i++) {
                    if (xsf->toW & (1<<i)) {
                        xs = (xSerial *) (xsf->sbuff + i * sizeof(xSerial));
                        /*
                         * If serial entity inside current programming page(address + program page).
                         */
                        if ((xs->addr >= dap->adr) && ((xs->addr+xs->size) <= (dap->adr + xap->szProg))) {
                            xsf->toW &= ~(1<<i);
                            JUMPSUB(3);
                            dap->funstate = 0;
                            if (xs->type == SER_TYPE_GENERAL) {
                                dap->regs[1] = xs->addr - dap->adr + dap->regs[5]; // target memory address
                                dap->regs[2] = (uint32_t)xs;
                                return swd_load_serial(xPacket);
                            } else if (xs->type == SER_TYPE_CRC32) {
                                /* calculate the CRC32 value */
                                    // get port with start and end address.
                                for (i=0;i<PORT_COUNT;i++)
                                    if(dap->xport & (1<<i))
                                        break;
                                dap->regs[1] = xs->data[i][4]<<24 | xs->data[i][5]<<16 | xs->data[i][6]  << 8 | xs->data[i][7];     // start address
                                dap->regs[2] = xs->data[i][8]<<24 | xs->data[i][9]<<16 | xs->data[i][10] << 8 | xs->data[i][11];    // end address
                                cP("\nSerial index=%d, CRC32 start=%08X, end=%08X", dap->regs[11], dap->regs[1], dap->regs[2]);
                                    // calculate CRC32 value
                                swd_calc_crc32((char *)xap->sapfname, dap, xap, xsf);
                                for(i=0;i<PORT_COUNT;i++) {
                                    if (dap->xport & (1<<i)) {
                                        xs->data[i][0] = (dap->crcs[i]>>24) & 0xFF;
                                        xs->data[i][1] = (dap->crcs[i]>>16) & 0xFF;
                                        xs->data[i][2] = (dap->crcs[i]>> 8) & 0xFF;
                                        xs->data[i][3] = (dap->crcs[i]>> 0) & 0xFF;
                                        cP("\nPort[%d] CRC32 = %08X", i, dap->crcs[i]);
                                    }
                                }
                                dap->regs[1] = xs->addr - dap->adr + dap->regs[5]; // target memory address
                                cP("\nCRC32 addr=%08X, dap->adr=%08X, regs5=%08X, target=%08X", dap->regs[1], dap->adr, dap->regs[5], xs->addr);
                                dap->regs[2] = (uint32_t)xs;
                                return swd_load_serial(xPacket);
                            }
                        } 
                    }
                }
            }

            goto DO_PROGPAGE;
        case 3:
            if ((i=swd_load_serial(xPacket)) != DAP_GONEXT) {
                return i;
            }

            goto LOAD_SERIAL;

DO_PROGPAGE:            
            dap->writemask = 0xFFFFFFFF;
            dap->ap_tar_value = 0xFFFFFFFF;
            dap->dp_sel_value = 0xFFFFFFFF;

            dap->regs[0] = dap->adr;
            if ((dap->regs[4] + xap->szProg) < xap->szData)
                dap->regs[1] = xap->szProg;						// program size
            else
                dap->regs[1] = xap->szData - dap->regs[4];						// program size
			if(dbg_deep>1)
            cP("\nPROGRAM PAGE::Addr[%08x], Size[%04x]", dap->adr, dap->regs[1]);
            dap->regs[2] = dap->regs[5];					// address of data to write to flash
            dap->regs[3] = xap->toProg;						// timeout
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funProgPage + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E20f;
					dap->reto = xap->toProg * (xap->szFlash/0x1000);			
            if (xap->tDebug > 0x10)
                dap->reto <<= 2;
            dap->funstate = 0;
            JUMPSUB(4);

            return swd_runtarget(xPacket);
        case 4:
            i = swd_runtarget(xPacket);
            if (i == DAP_GONEXT) {
                check_alg_result(ALG_PP_FAIL, NULL);
                //if (dap->xport == 0) { // no port to continue
                //    cP("\nAll port failed!");
                //    return DAP_ERROR;
               // }

                dap->regs[4] += xap->szProg;

                goto PROGPAGE_NEXT;
            }
            return i;

        case 5:
            i = swd_read_serial(xPacket);
            if (i != DAP_GONEXT) {
                return i;
            }
            dap->regs[11] ++;
            goto PROGPAGE_NEXT;
PROGPAGE_END:		
						if(xap->vendor == (ATMEL<<24))
						{
								if(dap->regs[0]==0x1FFFFFF0)//NVM Sec Bit
								{
									return DAP_GONEXT;
								}
						}
            dap->regs[0] = 2;	/* function 2:program */
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funUnInit+ (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E201;	
            dap->funstate = 0;
            dap->substate = 7;
            return swd_runtarget(xPacket);

        case 7:
            runrc=swd_runtarget(xPacket);
						if (DAP_GONEXT == runrc) {
                check_alg_result(ALG_UNINIT_FAIL, ALG_PP_PASS);
                printports((char *)MSGSTR[ALG_PP_PASS]);
                dap->funstate = 0;
                JUMPSUB(8);
								tP("\r\n");
								vTaskDelay(200);	
								uP("Programming Done.\r\n\r\n"); 
								vTaskDelay(200);	
                return swd_led(xPacket);
                //return DAP_GONEXT;
            }else
							return runrc;
            //return DAP_OK;
        case 8:
            if (DAP_GONEXT == swd_led(xPacket))
						{
							return DAP_GONEXT;
						}
            return DAP_OK;

        default:				// runUnInit
            break;
    }
    return DAP_OK;
}

/*----------------------------------------------------------------------------*/
int swd_loadcode(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    int	rc;

    switch(dap->substate) {
        case 0:		
            dap->regs[0] = xap->offCode;
            dap->regs[1] = xap->startPC & 0xFFFFFFFE;
            dap->regs[2] = xap->szCode;
            dP("\nLoad Code::offset(%08x)start(%08x)size(%08x)", xap->offCode, xap->startPC&0xFFFFFFFE, xap->szCode);
            JUMPSUB(1);
            dap->funstate = 0;
            return swd_load_ram(xPacket);

        case 1:
            rc = swd_load_ram(xPacket);
            if (rc == DAP_GONEXT) {
                printports("Target Algorithm Loaded");
                //xap->dapstate |= PREQ_CODELOADED;
                dap->regs[1] = xap->startPC & 0xFFFFFFFE;
                dap->regs[2] = xap->szCode;
                JUMPSUB(2);
                dap->funstate = 0;
                return swd_read_ram(xPacket);
            }
            return rc;
        case 2:
            rc = swd_read_ram(xPacket);
            if (rc == DAP_GONEXT) {
                xap->dapstate |= PREQ_CODELOADED;
                return DAP_GONEXT;
            }
            return rc;

        default:				// runUnInit
            break;
    }
    return DAP_OK;
}
int swd_loadbin(xDAPPacket *xPacket)
{
 
    return DAP_OK;
}
/*----------------------------------------------------------------------------*/
/*
 * swd_CRC32()
 *      read back serial
 *          call swd_read_serial
 *      run funCRC
 *      calc crc32 from sapfile and xsf structure
 *
 */
int swd_CRC32(xDAPPacket *xPacket)
{
    //U16 port = xPacket->port;
    xDAPPort *dap = &pPort;
    //U32 temp;
    int i;

    switch(dap->substate) {
        case 0:             // read back
            SYNCSEQ();
            dap->writemask = 0xFFFFFFFF;
            dap->ap_tar_value = 0xFFFFFFFF;
            dap->dp_sel_value = 0xFFFFFFFF;
            if (currcmd == SM_AUTO) 
                dap->port = swd_gettime(SM_CRC32);
            else
                dap->port = 0xFF;
            IiP(SM_CRC32, OPT_NF|OPT_ESTI, xap->szData|xap->startFlash, MSGSTR[ALG_CRC_START]);
            iP("\nVerifying... ");
#if 1
            dap->regs[0] = xap->startFlash;
            dap->regs[1] = xap->speedClock;
            dap->regs[2] = 3;	/* function, 3: verify */
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E207;	
            dap->reto = (xap->szData * xap->toProg + xap->toProg) / xap->szProg;
            dP("\nswd_CRC32 set timeout(%d)", dap->reto);
            JUMPSUB(1);
            dap->funstate = 0;
            return swd_runtarget(xPacket);

        case 1:				// Init() Done.
					i= swd_runtarget(xPacket);
            if (DAP_GONEXT != i) {
                return i;
            }

            check_alg_result(ALG_INIT_FAIL, NULL);

            /* for any port init() ok, should be unInit() */
            // dap->regs[10] = dap->rport; 
            if (dap->vport)
                dP("\nswd_CRC32::Init()::OK(%1x)Failed(%1x)", dap->rport, dap->vport);

#endif
            dap->regs[11] = 0;
            if (xap->vendor == (KINETIS << 24) && (xap->szProg > 4)) {
                // only Kinetis need dummy read to avoid cache issue.
                // szProg > 4 is a workaroud to avoid eeprom issue.
                JUMPSUB(2);
                dap->funstate = 0;
                return swd_read_dummy(xPacket);
            } else {
                goto READ_SERIAL_NEXT;
            }
        case 2:
            if (DAP_GONEXT != swd_read_dummy(xPacket)) {
                return DAP_OK;
            }

            
READ_SERIAL_NEXT:
            if (dap->program_serial && (dap->xport & 0xF)) {
                if (dap->regs[11] < xsf->cntSer) {
                    dap->funstate = 0;
                    JUMPSUB(3);
                    return swd_read_serial(xPacket);
                }
            }
            dap->regs[11] = 0;
            goto CALCCRC_START;
        case 3:
            i = swd_read_serial(xPacket);
            if (i != DAP_GONEXT) {
                return i;
            }
            dap->regs[11] ++;
            goto READ_SERIAL_NEXT;

CALCCRC_START:
            // run verify
            dap->regs[0] = 0xFFFFFFFF;
            dap->regs[1] = xap->startFlash+xap->ProgOffset;			// start address
            dap->regs[2] = (xap->szData > xap->szFlash)?xap->szFlash:xap->szData;
            dap->regs[3] = 0x04c11db7;				//xap->crcData;		//  ????
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funCRC32 + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E20f;	
            dap->timeout = 200;
            JUMPSUB(5);
            dap->funstate = 0;
            return swd_runtarget(xPacket);

        case 5:
            if (DAP_GONEXT != swd_runtarget(xPacket)) {
                if (dap->funstate == 4 && dap->regs[11]==0) {   // target running
                    dap->regs[11] = 1;  // avoid repeatedly called.
                    if (dap->program_serial) {
                        dap->regs[1] = xap->startFlash;     // start address
                        dap->regs[2] = (xap->szFlash > xap->szData)?xap->szData:xap->szFlash;
                        dap->regs[2] += dap->regs[1];       // end address

                        if (swd_calc_crc32((char *)xap->sapfname, dap, xap, xsf) < 0)
                            return DAP_ERROR;
                    } else {
                        for (i=0;i<PORT_COUNT;i++) {
                            dap->crcs[i] = xap->crcData;
                        }
                    }
                }
                return DAP_OK;
            }

            dap->vport   = 0;
            dap->rport   = 0;
            for (i=0;i<PORT_COUNT;i++) {
                dap->port = 1<<i;
                if (1)/*dap->xport & (1<<i))*/ {
                    if (dap->rets[i] != dap->crcs[i]) {
												tP("\r\n");  
												eP("\nPort[%d] Verify Failed! calculated CRC32=0x%08x, expected=0x%08x", i+1, (unsigned) dap->rets[i], (unsigned) dap->crcs[i]);
                        dap->vport |= (1<<i);
                        IeP(SM_CRC32, OPT_NF|OPT_ERR|OPT_PORT|OPT_ADDR, dap->rets[i], ErrMsg(ALG_CRC_FAIL, OPT_ERR));
                        DAP_SetLed((1<<i), REDLED, LEDON);
											return  DAP_ERROR;
                    } else {
                        char crc32[18];
                        sprintf(crc32,"%s0X%08X ",MSGSTR[ALG_CRC_PASS], dap->rets[i]);
                        IeP(SM_CRC32, OPT_NF|OPT_PORT|OPT_ADDR, dap->rets[i], crc32);
                        //IeP(SM_CRC32, OPT_NF|OPT_PORT|OPT_ADDR, dap->rets[i], MS_CMD_CRC32_OK);
                        tP("\r\n");
											  uP(" Verify Passed. calculated CRC32=0x%08x, expected=0x%08x\r\n\r\n",(unsigned) dap->rets[i], (unsigned) dap->crcs[i]);
                        dap->rport |= (1<<i);
                    }
                //} else {
                //    IeP(SM_CRC32, OPT_NF|OPT_ERR|OPT_PORT, NULL, "Unknown Error!");
                }
            }
            printports("CRC32 Verified OK");    // runUnInit
            //dap->xport = dap->regs[10]; // restores ports that init() ok.
            dap->regs[0]  = 3;	                // function, 3: Verify
            dap->regs[9]  = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funUnInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc   = 0x1E201;	
            dap->timeout  = 200;
            dap->substate = 6;
            FUNJUMP(dap, 0);
            return swd_runtarget(xPacket);

        case 6:
            if (DAP_GONEXT == swd_runtarget(xPacket)) {
                check_alg_result(ALG_UNINIT_FAIL, NULL);
                dP("\nCRC32 Verify::UnInit()::OK(%1x)/Failed(%1x)", dap->rport, dap->vport);
                dap->funstate = 0;
                JUMPSUB(7);
                return swd_led(xPacket);
                //return DAP_GONEXT;
            }
            return DAP_OK;

        case 7:
            if (DAP_GONEXT == swd_led(xPacket))
                return DAP_GONEXT;
            return DAP_OK;

        default:				// runUnInit
            eP("\nswd_CRC32():: unknow state");
            break;
    }
    return DAP_OK;
}

/** @brief swd_dump() dump target memory to SD card or Console
 *  @param xPacket
 *  @return return DAP_OK or DAP_GONEXT 
 */
/* ------------------------------------------------------------------------------*/
int swd_dump(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    U32 temp;

    switch(dap->substate) {
        case 0:
            dap->regs[6] = dap->regs[0];
            dap->regs[7] = dap->regs[1];
            dap->regs[8] = dap->regs[2];
            cP("\nDump Start Address: 0x%08X", dap->regs[6]);
            cP("\n              Size: 0x%08X", dap->regs[7]);
            if (dap->regs[8])
                cP("\n  Dump To File: %s", dap->regs[8]);
            else
                cP("\n  Dump To Console.");

            SYNCSEQ();
            dap->writemask = 0xFFFFFFFF;
            dap->ap_tar_value = 0xFFFFFFFF;
            dap->dp_sel_value = 0xFFFFFFFF;
            dap->regs[0] = xap->startFlash;
            dap->regs[1] = xap->speedClock;
            dap->regs[2] = 3;	/* function, 3: verify */
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc = 0x1E207;	
            dap->reto = 10000;
            dP("\nswd_dump() set timeout(%d)", dap->reto);
            JUMPSUB(1);
            dap->funstate = 0;
            //return swd_runtarget(xPacket);
						swd_runtarget(xPacket);
        case 1:				// Init() Done.
            //if (DAP_GONEXT != swd_runtarget(xPacket)) {
             //   return DAP_OK;
           // }

            check_alg_result(ALG_INIT_FAIL, NULL);

            /* for any port init() ok, should be unInit() */
            dap->regs[10] = dap->rport; 
            if (dap->vport)
                dP("\nswd_dump()::Init()::OK(%1x)Failed(%1x)", dap->rport, dap->vport);
            xap->szCache = 0;
            xap->offCache = 0;

DUMP_NEXTBLOCK:            
            dap->regs[2] = (uint32_t)xap->cache;
            dap->regs[0] = dap->regs[6];
            temp = dap->regs[7];
            if (temp > 0x400)
                temp = 0x400;

            dap->regs[1] = temp;

            dap->regs[6] += temp;
            dap->regs[7] -= temp;
            dap->regs[9] = temp;
            JUMPSUB(2);
            dap->funstate = 0;
            // address(R0), size(R1), buffer(R2)
            //return swd_readmem(xPacket);
						swd_readmem(xPacket);
        case 2:
           // if (DAP_GONEXT != swd_readmem(xPacket)) {
            //    return DAP_OK;
           // }
				while(DAP_GONEXT != swd_readmem(xPacket));
					
				

            //if (dap->regs[8]) {
//////??????                fileAppend((char *)dap->regs[8], xap->cache, dap->regs[9]);
           /* } else*/{
                int i;
                volatile uint32_t *ul;
                ul = (uint32_t *) xap->cache;
                cP("\nADDR:", dap->regs[0]);
                for(i=0;i<dap->regs[9];i+=4) {
                    if ((i & 0x1f) == 0)
                        cP("\n\t");
                    cP(" ", *ul++);
                }
            }

            if (dap->regs[7] > 0)
                goto DUMP_NEXTBLOCK;
            
            printports("Dump Finished");    // runUnInit
            //run unInit()
            dap->xport = dap->regs[10]; // restores ports that init() ok.
            dap->regs[0]  = 3;	                // function, 3: Verify
            dap->regs[9]  = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = xap->funUnInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            dap->regacc   = 0x1E201;	
            dap->timeout  = 200;
            dap->substate = 0;//6;
            FUNJUMP(dap, 0);
            return swd_runtarget(xPacket);

        case 6:
            if (DAP_GONEXT == swd_runtarget(xPacket)) {
                check_alg_result(ALG_UNINIT_FAIL, NULL);
                dP("\nswd_dump()::UnInit()::OK(%1x)/Failed(%1x)", dap->rport, dap->vport);
							JUMPSUB(0);
                return DAP_GONEXT;
            }
						JUMPSUB(0);
            return DAP_OK;

        default:				// runUnInit
            cP("\nswd_dump():: unknow state");
            break;
    }
    return DAP_OK;
}
/* ------------------------------------------------------------------------------*/
int swd_runfun(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    int i;

    switch(dap->substate) {
        case 0:				// runInit
            //dap->regs[0] = xap->startFlash;
            //dap->regs[1] = xap->speedClock;
            //dap->regs[2] = 3;	/* function, 3: verify */
            dap->regs[9] = xap->szPrgCode + (xap->startPC & 0xFFFFFFFE);
            dap->regs[13] = xap->startSP;
            dap->regs[14] = xap->startPC;
            dap->regs[15] = dap->regs[10];		//xap->funInit + (xap->startPC & 0xFFFFFFFE);
            dap->regs[16] = 0x01000000;
            //  dap->regacc = 0x1E207;	        // TODO: should be specify previously.
            dap->timeout = 2500;
            FUNJUMP(dap, 0);
            dap->substate = 1;
            return swd_runtarget(xPacket);

        case 1:		
							runrc=swd_runtarget(xPacket);
				if (DAP_GONEXT == runrc) {
                for(i=0;i<PORT_COUNT;i++) {
                    if (dap->xport & (1<<i))
                        iP("\nswd_runfun() return(0x%08x)", dap->rets[P2I(1<<i)]);
                }
                return DAP_GONEXT;
            }else
							return runrc;
 //           return DAP_OK;

        default:
            cP("\nswd_runfun():: unknow state");
            break;
    }
    return DAP_OK;
}
/*----------------------------------------------------------------------------*/
int swd_abort(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    int rc;

    switch(dap->manstate) {
        case SM_START:
           App_DAP_Init(0xF);
            dap->manstate = SM_HALT;
            dap->substate = 0;

        case SM_HALT:
            rc = swd_halt(xPacket);
            if (rc != DAP_GONEXT) {
                return rc;
            }

            dap->substate = 0;
            dap->manstate = SM_RUNUNINIT;
            dap->regs[0] = 1;
            dap->regs[10] = xap->funUnInit + (xap->startPC & 0xFFFFFFFE);
            dap->regacc = 0x1E207;	

        case SM_RUNUNINIT:
            rc = swd_runfun(xPacket);
            if (rc != DAP_GONEXT) {
                return rc;
            }

            dap->substate = 0;
            dap->manstate = SM_CONNECT;

        case SM_CONNECT:
            rc = swd_connect(xPacket);
            if (rc != DAP_GONEXT) {
                return rc;
            }

            dap->substate = 0;
            dap->manstate = SM_DAPRESET;

        case SM_DAPRESET:
            rc = swd_powerup_reset(xPacket);
            if (rc != DAP_GONEXT) {
                return rc;
            }

            dap->substate = 0;
            dap->manstate = SM_RESET_HALT;

        case SM_RESET_HALT:
            rc = swd_reset_halt(xPacket);
            if (rc != DAP_GONEXT) {
                return rc;
            }

            dap->manstate = SM_DONE;
            dap->substate = 0;
            xUIMsg(SM_ABORT, OPT_NF, 0, ALG_CMD_ABORT);
            return DAP_GONEXT;

        default:
            cP("\nswd_abort():: unknow state");
            break;
    }
    return DAP_OK;
}   // END of swd_abort()
/* ------------------------------------------------------------------------------*/

	static	int loop_cnt=0; 
//#define WITH_TIMEOUT
int swd_auto(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    int rc, i;

    switch(dap->manstate) {
        case SM_START:
						uP("Checking Power..\r\n");
            rc = swd_power_control(xPacket);
            if (rc != DAP_GONEXT) {
                return rc;
            }
								#if 1
						if (xap->vendor == (ATMEL << 24)) 
						{
							if(gNeedErasePin)
							{
								if(!gAtmelHasErase)
								{
								#ifndef STM32F10X_MD	
	
									PORTC->PCR[3] = PORT_PCR_MUX(1);					
									PTC->PSOR =  (1 << 3); 
									vTaskDelay(500);
									PTC->PCOR =  (1 << 3); 
									PORTC->PCR[3] |=PORT_PCR_ODE_MASK;
									vTaskDelay(100);
								#else
								  GPIOB->ODR |= GPIO_PIN_10;	
										vTaskDelay(500);
									 GPIOB->ODR &= ~GPIO_PIN_10;	
								#endif
								gAtmelHasErase=1;
									output_0v_io();
									dap->substate = 0;
									return DAP_OK;

	
								}
							}
						}
						#endif
						if(xap->vendor==TOSHIBA<<24&&xap->voltage==8)
						{
							vTaskDelay(500);
						}
            dap->manstate = SM_CONNECT;
						uP("Connecting..\r\n");
            dap->substate = 0;
						loop_cnt=0;
						
        case SM_CONNECT:
            rc = swd_connect(xPacket);
            if (rc != DAP_GONEXT) {
								loop_cnt++;
							#ifdef WITH_TIMEOUT
								if(loop_cnt>0x1000)
									return DAP_ERROR;
								#endif
                return rc;
            }

            dap->substate = 0;
            dap->manstate = SM_DAPRESET;
						uP("Power reset and halt the target..\r\n");						
            //iP("\nReset DAP...");
						loop_cnt=0;
        case SM_DAPRESET:
           rc = swd_powerup_reset(xPacket);
            if (rc != DAP_GONEXT) {
								loop_cnt++;
							#ifdef WITH_TIMEOUT
								if(loop_cnt>0x1000)
									return DAP_ERROR;
								#endif
								if(rc !=DAP_OK)
									return DAP_AGAIN;
                return rc;
            }
					if(xap->vendor==ATMEL<<24)
						{
							if(!gNeedErasePin)
							{
								if(!gAtmelHasErase)
								{
									if(xap->SapExtraFun)
									{
										if(1)//check_AtmelD20Lock(xPacket))
										{
											run_SAPSequence(xPacket,xap->SapExtraFun,xap->ExtraFunCnt);
											gAtmelHasErase=1;
											App_DAP_SWJ_Pins(0, 0x01, 0x01, 0x0); // SWCLK assert high
											App_DAP_SWJ_Pins(0, 0x00, 0x80, 0x0); // SWRST assert low
											Delayms(200);
											App_DAP_SWJ_Pins(0, 0x80, 0x80, 0x0); // SWRST assert high
											Delayms(200);
											dap->substate = 0;
											dap->manstate = SM_CONNECT;
											return DAP_OK;
										}
									}
								}
							}
						}

            dap->substate = 0;
            dap->manstate = SM_RESET_HALT;					
            uP("\nReset Target...\r\n");
						loop_cnt=0;
        case SM_RESET_HALT:
			
        rc = swd_reset_halt(xPacket);
		    //rc = swd_target_set(xPacket);
            if (rc != DAP_GONEXT) {
								loop_cnt++;
							#ifdef WITH_TIMEOUT
								if(loop_cnt>0x1000)
									return DAP_ERROR;
								#endif
                return rc;
            }
						if(xap->vendor==NODRIC<<24)
						{
							int NrfLock=0;
							GetNrfUICR(xPacket);
							GetNrfFICR(xPacket);
							NrfLock=ChkNrfLock();
							if(NrfLock)
							{
								uint32_t RegionAdd=0;
								
								Nrf51EraseAll(xPacket,RegionAdd);
								NrfLock=0;
								return DAP_AGAIN;
							}
						}


            dap->manstate = SM_LOAD_CODE;
						uP("Download the algorithm...\r\n");							
						vTaskDelay(100);
            dap->substate = 0;
						loop_cnt=0;
#if 1
            if (dap->program_serial && xap->sapid == 0) {
                if (dap->program_serial > 1) {      
                    for(i=0;i<PORT_COUNT;i++) {
                        if (dap->xport & (1<<i)) {
                            dap->xport &= ~(1<<i);  // exclude port that read serial failed.
                            dap->port = (1<<i);
                            DAP_SetLed((1<<i), REDLED, LEDON);
                            IiP(SM_PROGSERIAL, OPT_NF|OPT_ERR|OPT_A2C|OPT_PORT, SERP_SIG_ERR, ErrMsg(SERP_FORMATERR, OPT_ERR));
                        }
                    }
                    cP("\nSerial related file error!");
                    return DAP_ERROR;
                }
                if (xap->serialsig != xsf->signature) {
                    for(i=0;i<PORT_COUNT;i++) {
                        if (dap->xport & (1<<i)) {
                            dap->xport &= ~(1<<i);  // exclude port that read serial failed.
                            dap->port = (1<<i);
                            DAP_SetLed((1<<i), REDLED, LEDON);
                            IiP(SM_PROGSERIAL, OPT_NF|OPT_ERR|OPT_A2C|OPT_PORT, SERP_SIG_ERR, ErrMsg(SERP_SIG_ERR, OPT_ERR));
                        }
                    }
                    cP("\nSerial Signature not matched!");
                    return DAP_ERROR;
                    
                }
                
                if ((rc = serial_readlines(xsf, dap->xport)) < 0) { 
                    for(i=0;i<PORT_COUNT;i++) {
                        if (dap->xport & (1<<i)) {
                            dap->port = (1<<i);
                            DAP_SetLed((1<<i), REDLED, LEDON);
                            IiP(SM_PROGSERIAL, OPT_NF|OPT_ERR|OPT_A2C|OPT_PORT, FR_READ_FAIL, ErrMsg(FR_READ_FAIL, OPT_ERR));
                        }
                    }
                    cP("\nRead Serial Failed!");
                    return DAP_ERROR;
                }
                dap->vport = rc;
                dap->vport = dap->xport & ~dap->vport;

                for(i=0;i<PORT_COUNT;i++) {
                    if (dap->vport & (1<<i)) {
                        dap->xport &= ~(1<<i);  // exclude port that read serial failed.
                        dap->port = (1<<i);
                        DAP_SetLed((1<<i), REDLED, LEDON);
                        IiP(SM_PROGSERIAL, OPT_NF|OPT_ERR|OPT_A2C|OPT_PORT, SERP_USEUP, ErrMsg(SERP_USEUP, OPT_ERR));
                        cP("\nPort[%1X] read serial failed!", (1<<i));
                    }
                }
                if (dap->xport == 0)
                    return DAP_ERROR;

                xsf->toW = 0;
                for (i=0;i<xsf->cntSer;i++) {
                    xsf->toW |= (1<<i);
                }
            }
#endif

        case SM_LOAD_CODE:
            dap->funToDo = 0;
            if (xap->funToDo & DO_LOADCODE) {
							if(!(xap->dapstate&PREQ_CODELOADED))
							{
								rc = swd_loadcode(xPacket);
								if (rc != DAP_GONEXT) {
										return rc;
								}else
								{
									dap->substate = 0;
									
									return rc;
								}
												
							}
						}			
            /*
             * BLANKCHECK/ERASESECTOR/ERASECHIP can only be executed one of them, or none.
             */
						if(xap->vendor==KINETIS<<24&&((xap->startBF&0Xf0000000)!=0X20000000))// Just for kl
						dump_targetmem_kinetis(xPacket,xap->startFlash);
            if (xap->funToDo & DO_BLANKCHECK) {
                dap->manstate = SM_BLANKCHECK;
            } else if (xap->funToDo & DO_ERASESECTOR) {
                dap->manstate = SM_ERASESECTOR;
            } else if (xap->funToDo & DO_ERASECHIP) {
                xap->doChipEraseBySector = 0;
                if (xap->funEraseChip == 0x00 ) 
									{
                    if (xap->funEraseSector > 0) {
                        dap->funToDo = xap->funToDo;
                        dap->manstate = SM_ERASESECTOR;
                        xap->funToDo &= ~DO_ERASECHIP;
                        xap->funToDo |= DO_ERASESECTOR;
                        xap->doChipEraseBySector = 1;
                    } else {
                        IiP(SM_ERASECHIP, OPT_NF|OPT_ERR|OPT_MSG, NULL, ErrMsg(ALG_EC_NOT_SUPPORT, OPT_ERR));
                        return DAP_ERROR;
                    }
                } else {
                    dap->manstate = SM_ERASECHIP;
                }
            }
            dap->substate = 0;
						if(dap->manstate == SM_ERASECHIP)
						{
							uP("Erase all chip.\r\n");
						}
						else if(dap->manstate == DO_ERASESECTOR)
						{
							uP("Erase sector.\r\n");
						}
						else if(dap->manstate == DO_BLANKCHECK)
						{
							uP("Blank Check.\r\n");
						}
        case SM_BLANKCHECK:
            if (dap->manstate == SM_BLANKCHECK && xap->funToDo & DO_BLANKCHECK) {
                rc = swd_blankcheck(xPacket);
                if (rc != DAP_GONEXT) {
                    return rc;
                }
                if (dap->xport == 0)
                    return rc;
            }

        case SM_ERASESECTOR:
            if (dap->manstate == SM_ERASESECTOR && xap->funToDo & DO_ERASESECTOR) {
                rc = swd_erasesector(xPacket);
                if (rc != DAP_GONEXT) {
                    return rc;
                }
            }

        case SM_ERASECHIP:
            if (dap->manstate == SM_ERASECHIP && xap->funToDo & DO_ERASECHIP) {
							if(xap->vendor!=KINETIS<<24&&!gAtmelHasErase)//MASS Erase up weib add 2015-0715
                rc = swd_erasechip(xPacket);
							else
							{
								vTaskDelay(200);
								uP("Full Chip Erase Done.\r\n\r\n"); 	
								rc=DAP_GONEXT;
							}
                if (rc != DAP_GONEXT) {
                    return rc;
                }
            }
            if (dap->funToDo ) {
               // xap->funToDo = dap->funToDo;        // restore xap->funToDo
              //  dap->funToDo = 0;
            }
            dap->manstate = SM_PROGPAGE;
						if (xap->funToDo & DO_PROGRAM)
						{
							vTaskDelay(200);	
							uP("Programmimg...\r\n");	
							vTaskDelay(200);
						}							
            dap->substate = 0;

        case SM_PROGPAGE:
            if (xap->funToDo & DO_PROGRAM) {							
              rc = swd_progpage(xPacket);
              if (rc != DAP_GONEXT) {
                return rc;
              }
            }
						#if 0
						{
						uint32_t temp;
						 U16 port = xPacket->port;	
						U8	*resp = FUNRESPONSE(xPacket);
						temp = 0x20000000;
            swd_writeAP(port, MEM_AP, MEM_AP_REG_TAR, &temp);		// set AP TAR
            dap->ap_tar_value = temp;
            swd_block_read(swd_flush(port), 0x20);
						printf_memory(resp,0x20);	
						}
						#endif
						gUp=0;
            dap->manstate = SM_CRC32;
						if (xap->funToDo & DO_CRC32)
							uP("Verify!!!\r\n");							
            dap->substate = 0;

        case SM_CRC32:
            if (xap->funToDo & DO_CRC32) {
                rc = swd_CRC32(xPacket);
                if (rc != DAP_GONEXT) {
                    return rc;
                }else{
									if(gNeedLockChip)
									LockNrfChip(xPacket);
									dP("\nCurrent SAP[%d] id = %d", xap->sapid, xst[xap->sapid].id);
									dP("\nNext    SAP[%d] id = %d", xap->sapid+1, xst[xap->sapid+1].id);

									if (xst[xap->sapid+1].id > 0) {
											return DAP_NEXTSAP;
									} else {
											xap->sapid = 0;
									}

									if (dap->program_serial) {
											dP("\nLogging serial...");
											for(rc=0;rc<PORT_COUNT;rc++) {
													if (dap->xport & (1<<rc)) 
															xsf->state[rc] = 0x88;
											}
											/* if log error, then stop */
											if(serial_log(xsf, 1) > 0) {
													dap->program_serial += 1;   
											}
									}							
									if(xap->vendor==ATMEL<<24)//LOCK
									{
										if(xap->SapSequence)
										{
											run_SAPSequence(xPacket,xap->SapSequence,xap->SEQCnt);
											dap->manstate = SM_DONE;
										dap->substate = 0;
										return DAP_GONEXT;
										}

									}
									if(xap->vendor==TOSHIBA<<24)//LOCK
									{
										if(xap->SapSequence)
										{
											run_SAPSequence(xPacket,xap->SapSequence,xap->SEQCnt);
											dap->manstate = SM_DONE;
										dap->substate = 0;
										return DAP_GONEXT;
										}

									}
									if(xap->vendor==NODRIC<<24)
									{
										if(gNrfUICRExtra)
										{
											SetNrfUICR(xPacket,pnrf_uicr_t);
											gNrfUICRExtra=0;
											dap->manstate = SM_DONE;
										dap->substate = 0;
										return DAP_GONEXT;
										}

									}								
								
							}
            }

            dap->manstate = SM_RESET_GO;
						if (xap->funToDo & DO_RESETGO) 
							uP("Reset and go!!\r\n");							
            dap->substate = 0;


        case SM_RESET_GO:
            if (xap->funToDo & DO_RESETGO) {
                rc = swd_resetgo(xPacket);
                if (rc != DAP_GONEXT) {
                    return rc;
                }
            }
            dap->manstate = SM_DONE;
            dap->substate = 0;
            return DAP_GONEXT;

        default:
            cP("\nswd_auto():: unknow state");
            break;
    }
    return DAP_OK;
}   // END of swd_auto()




/*----------------------------------------------------------------------------*/
int swd_runpreq(xDAPPacket *xPacket, uint32_t preq)
{
  #ifndef MC101_BOARD  
	xDAPPort *dap = &pPort;
    int rc;

    if (preq & PREQ_SAPSELECTED) {
        if (xap->sap_index == 0) {
            rc = DAPConfig(0);
            if (rc != DAP_OK) {
                eP("\nConfig file error:%s", MSGSTR[rc]);
                xI2CMsgQOptPut(dap->manstate, OPT_ERR|OPT_MSG, NOCODE, NOPORT, NOADDR, (uint8_t *)MSGSTR[rc]);
                xI2CMsgQPut(NULL, 0);
                return DAP_ERROR;
            }
        }

        if(DAP_SAP_SELECTED == fflistdir(NULL, xap->sap_index, xap->sapfname, NULL, SAPEXT)) {
            if (parseSAPH(xap->sapfname) < 0) {					/* parse sap file */
                eP("Parsing SAP file Failed!!!\n" );
                return DAP_ERROR;
            } else {
                xap->dapstate |= PREQ_SAPSELECTED;
            }
        } else {
            eP("\nSAP file not found(%d)", xap->sap_index);
            return DAP_ERROR;
        }
        dap->substate = 0;
        dap->funstate = 0;
    } 

    if (preq & PREQ_SAPLOADED) {
        if (parseSAPB(xap->sapid) < 0) {					/* parse sap file */
            eP("Parsing SAP file Failed!!!\n" );
            return DAP_ERROR;
        }
        dap->substate = 0;
        dap->funstate = 0;
    } 
    
    if (preq & PREQ_PWR_READY) {
        dap->manstate = SM_START;
        rc = swd_power_control(xPacket);
        if (rc != DAP_GONEXT) {
            return rc;  //DAP_GOON;
        }
        dap->substate = 0;
        dap->funstate = 0;
    }

    if (preq & PREQ_CONNECTED) {
        dap->manstate = SM_CONNECT;
        rc = swd_connect(xPacket);
        if (DAP_GONEXT != rc) {
            return rc;  //DAP_GOON;
        }
        dap->substate = 0;
        dap->funstate = 0;
    }

    if (preq & PREQ_INITED) {
        dap->manstate = SM_DAPRESET;
        rc = swd_powerup_reset(xPacket);
        if (rc == DAP_ERROR) { 
            dap->substate = 0;
            dap->funstate = 0;
            return rc;
        } else
            if (DAP_GONEXT != rc) {
                return DAP_GOON;
            }
        dap->substate = 0;
        dap->funstate = 0;
    }

    if (preq & PREQ_HALTED) {
        dap->manstate = SM_RESET_HALT;
        if (DAP_GONEXT != swd_reset_halt(xPacket)) {
            return DAP_GOON;
        }
        dap->substate = 0;
        dap->funstate = 0;
    }
    if (preq & PREQ_CODELOADED) {
        dap->manstate = SM_LOAD_CODE;
        if (DAP_GONEXT != swd_loadcode(xPacket)) {
            return DAP_GOON;
        }
        dap->substate = 0;
        dap->funstate = 0;
    }
    dap->substate = 0;
    dap->funstate = 0;
#endif
    return DAP_GONEXT;
}

/*----------------------------------------------------------------------------*/
/* 
 *
 *  */
uint32_t uint32(uint8_t *strno)
{
    int base=10;
    uint8_t *ch = strno;
    uint32_t ret = 0;
    U8 start = 0;

    while(*ch) {
        if (*ch == 'x' || *ch == 'X') {
            base = 16; ret = 0;
            ch++;
            start++;
            continue;
        }
        if (*ch >= '0' && *ch <= '9') {
            ret = ret * base + *ch - '0';
            ch++;
            start++;
            continue;
        }
        if (base==16 && *ch >= 'A' && *ch <= 'F') {
            ret = ret * base + *ch - 'A' + 10;
            ch++;
            start++;
            continue;
        }
        if (base==16 && *ch >= 'a' && *ch <= 'f') {
            ret = ret * base + *ch - 'a' + 10;
            ch++;
            start++;
            continue;
        }
        if (start==0 && (*ch==' ' ||*ch=='\t')) {
            ch++;
            continue;
        }
        break;
    }
    return ret;
}
/* ------------------------------------------------------------------------------*/
/*
 * nextarg(string)
 *      return next token
 */
uint8_t *nextarg(uint8_t *oparg)
{
    while(*oparg) {
        if (*oparg <= 32) {
            while(*oparg <= 32)
                oparg ++;
            return oparg;
        }
        oparg ++;
    }
    return oparg;
}
/*----------------------------------------------------------------------------*/
int strequ(uint8_t *pp, uint8_t *qq)	//const TCHAR *qq)
{
    while(*pp>32 || *qq>32) {
        if (*pp++ != *qq++)
            return 0;
    }
    return 1;
}

/*-----------------------------------------------------------------*/
int swd_setreset(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;
    if (dap->regs[0]) {
        xap->sreset = dap->regs[0];
        cP("\nForce reset to (%1x)", xap->sreset);
    }

    cP("\n low 4 bit:");
    cP("\n\t reset == 00 AUTO Reset.");
    cP("\n\t bit 0 ==> HW Reset.");
    cP("\n\t bit 1 ==> SYS Reset.");
    cP("\n\t bit 2 ==> VECT Reset(not tested).");
    cP("\n");
    cP("\n\t bit 4 ==> Normal connect(not supported).");
    cP("\n\t bit 5 ==> Connect with pre-reset.");
    cP("\n\t bit 6 ==> Connect with under-reset.");
    cP("\n\t bit 7 ==> reset after Connect(always and not affected by this bit.");

    return DAP_OK;
}

/*----------------------------------------------------------------------------*/

int swd_calcCRC32(xDAPPacket *xPacket)
{
    size_t sz = xap->szData, szC = 0;
    uint32_t toRead, crc = 0;
    U8 *buff;

    xap->szCache = 0;
    xap->offCache = 0;
    while(szC < sz) {
        toRead = sz - szC;
        if (toRead > 0x400)
            toRead = 0x400; 

        buff = loadSapFileSeg(xap->sapfname, (xap->offData+szC), toRead, NULL);
        crc = calcCRC32(buff, toRead, crc);
        szC += toRead;
    }
    cP("\nCRC32=%08X", crc);
    return (int)crc;
}

/*----------------------------------------------------------------------------*/
#ifdef __SPIDER__
int swd_select_sap(xDAPPacket *xPacket)
{
    xDAPPort *dap = &pPort;

    if (dap->regs[0] > 0) {
        if(DAP_SAP_SELECTED == fflistdir(NULL, dap->regs[0], xap->sapfname, NULL, SAPEXT)) {
            cP("\nSAP file:%s, selected.", xap->sapfname);
            if (parseSAPH(xap->sapfname) < 0) {
                cP("\nParsing SAP file Failed!" );
            } 

            if (parseSAPB(dap->regs[1]) < 0) {
                cP("\nParsing SAP file, id=%d Failed!", dap->regs[1]);
            } else {
                xap->sapid = dap->regs[1];
            }
        } else {
            cP("\nSAP file no not found(%d)", dap->regs[0]);
        }
    } else {
        cP("\nsap usage:\n\tsap x(x=1..n)");
    }
    return DAP_OK;
}
#else

int parseSAPHeader(uint8_t *buff)
{
    // check magic, 
    uint8_t *ch, fid;
    int i,j=0;
    uint32_t cksum = 0, cks, *ul;
	//uint32_t temp;
		 U8 sz;
//    int sid = 0; ////?????
		int ret=0;
//    xDAPPort *dap = &pPort;

    for(i=0;i<=MAX_SAP_SECTION;i++) {
        xst[i].id = 0;
        xst[i].offset = 0;
    }

    xap->szCache = 0;

	ul = (uint32_t *)buff;
    if (*ul != FT_MAGIC) {
        i = 0;
        do {
            dP("\n[%d] %08x", i, *ul);
            fid = (*ul) & 0xFF;
            xst[i].id = fid;
            cksum += *ul++;

            cksum += *ul;
            dP("#%08x", *ul);
            xst[i].offset = *ul++;

            cksum += *ul;
            dP("#%08x", *ul);
            if (fid == 0)
                cks = *ul;
            //xst[i].size = *ul ++; // not used
            ul++;

            dP("#%08x", cksum);
            i++;
            if (i > MAX_SAP_SECTION && fid != 0) {
                wP("\nSAP file header error.");
                return ERROR_NOT_SAP_FILE;
            }

        } while (fid > 0);

        if (cksum != (cks<<1)) {    // checksum should be zero, otherwise raise error.
            wP("\nSAP file header check sum error. %08X", cksum);
            return ERROR_NOT_SAP_FILE;
        }
    } else {
        xst[0].id = 1;
        xst[0].offset = 0;
        xst[1].id = 0;
        dP("\nOld SAP: [%d] %08X", xst[0].id, xst[0].offset);
    }
    for (i=0;xst[i].id!=0;i++) {
        dP("\nid[%d] offset[%08X].", xst[i].id, xst[i].offset);
        //ch = buff+xst[i].offset;
        //ul = (uint32_t *)ch;
				ret = GetSAPFile(xap->sapfname,(uint8_t *)ul,4,xst[i].offset);
			if(!ret)
				return ERROR_NOT_SAP_FILE;
        if (*ul != FT_MAGIC) {
            wP("\nNot SAP file, %08X vs %08x \n", *ul, FT_MAGIC);
            return ERROR_NOT_SAP_FILE;
        }
    }
//	xap->sapid = 0;
    xap->dapstate &= ~PREQ_SAPLOADED;
    xap->dapstate |= PREQ_SAPSELECTED;
////?????   dap->program_serial = SerFileInit(xsf, (char *)xap->sapfname); 
    // 0: no SER file existed
    // 1: do serial programming
    // 2: SER file problematic, prevent programming with bad serial file.
#if 0
	ch = buff+xst[xap->sapid)].offset;
    buff = (uint8_t *)ul+512;
	
    i = 2 * FATNAMELEN + 4;


    xap->patErase = 0;
    xap->resett= 0;
    xap->funCRC32  = 0;
    xap->funVerify = 0;
    xap->sap_version= 0;
    xap->funInit = 0;
    xap->funUnInit =  0;
    xap->funEraseChip = 0;
    xap->funBlankCheck =  0;
    xap->funEraseSector = 0;
    xap->funProgPage =  0;
    xap->szProg= 0;
    xap->toProg = 0;
    xap->toErase = 0;
    xap->szFlash = 0;

    xap->offCode = 0;
    xap->offCode = 0;
    xap->szCode = 0;
    xap->offData = 0;
    xap->offData = 0;
    xap->szData = 0;
    xap->szRam = 0;
    xap->vendor = 0;
    xap->dapid  = 0;
    xap->cpuid  = 0;
    xap->szPrgCode = 0;
    xap->szPrgData = 0;
    xap->startPC =   0;
    xap->startSP=    0;
    xap->startBF=    0;
    xap->startFlash = 0;
    xap->crcCode =  0;
    xap->crcData =  0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[1] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->deviceid[0] = 0;
    xap->funToDo =  0;
    xap->voltage =  0;
    xap->logging =  0;
    xap->swj     =  0x01;   // default to SWJ

    i = 0;
    ch += 4;
    while(ch < buff) {
        // printf("SAP(%02x), %08x\n", *ch, (unsigned)ch);
        sz = *ch >> 6;//weib:flag group ensure the following data number.
        switch (*ch) {
            case FT_FLASH_TYPE:		   break;
            case FT_ERASE_PATTERN:	   xap->patErase =       toint(ch+1, sz); break;
            case FT_RAM_TYPE:		   break;
            case FT_RESET_TT:	       xap->resett=          toint(ch+1, sz); break;
            case FT_VOLTAGE:	       xap->voltage=         toint(ch+1, sz); break;
            case FT_FUN_CRC32:	       xap->funCRC32  =      toint(ch+1, sz); break;
            case FT_FUN_VERIFY:	       xap->funVerify =      toint(ch+1, sz); break;
            case FT_SAP_VERSION:       xap->sap_version=     toint(ch+1, sz); break;
            case FT_FUN_INIT:	       xap->funInit =        toint(ch+1, sz); break;
            case FT_FUN_UNINIT:        xap->funUnInit =      toint(ch+1, sz); break;
            case FT_FUN_ERASECHIP:     xap->funEraseChip =   toint(ch+1, sz); break;
            case FT_FUN_BLANKCHECK:    xap->funBlankCheck =  toint(ch+1, sz); break;
            case FT_FUN_ERASESECTOR:   xap->funEraseSector = toint(ch+1, sz); break;
            case FT_FUN_PROGPAGE:      xap->funProgPage =    toint(ch+1, sz); break;
            case FT_DRIVER_VER:		   break;
            case FT_PROG_SIZE:	       xap->szProg=          toint(ch+1, sz); break;	/* TODO */
            case FT_PP_TIMEOUT:        xap->toProg =         toint(ch+1, sz); break;
            case FT_ES_TIMEOUT:        xap->toErase =        toint(ch+1, sz); break;
            case FT_FLASH_SIZE:	       xap->szFlash =        toint(ch+1, sz); break;

            case FT_CODE_OFFSET:       
                xap->offCode =        toint(ch+1, sz); 
                xap->offCode +=       xst[sid].offset;
                break;
            case FT_CODE_SIZE:         xap->szCode =         toint(ch+1, sz); break;

            case FT_DATA_OFFSET:       
                xap->offData =        toint(ch+1, sz); 
                xap->offData +=       xst[sid].offset;
                break;
            case FT_DATA_SIZE:	       xap->szData =         toint(ch+1, sz); break;
            case FT_RAM_SIZE:          xap->szRam =          toint(ch+1, sz); break;
            case FT_SECTOR_SIZE:       xap->sectors[i++>>1].szSector =    toint(ch+1, sz); break;
            case FT_SECTOR_START_ADDR: xap->sectors[i++>>1].startSector = toint(ch+1, sz); break;
            case FT_CMDAV:             xap->cfgregs[j++] =     toint(ch+1, sz); break;
            case FT_VENDOR:            xap->vendor =        toint(ch+1, sz); break;
            case FT_DAPID:             xap->dapid  =        toint(ch+1, sz); break;
            case FT_CPUID:             xap->cpuid  =        toint(ch+1, sz); break;
            case FT_DAPSPEED:          xap->dapspeed  =     toint(ch+1, sz); break;
            case FT_TARGET_SECTION:	   break;
            case FT_RAM_SECTION:       break;
            case FT_FLASH_SECTION:     break;
            case FT_CODE_SECTION:      break;
            case FT_DATA_SECTION:      break;
            case FT_SCRIPT_SECTION:	   break;
            case FT_PRGCODE_SIZE:      xap->szPrgCode =      toint(ch+1, sz); break;
            case FT_PRGDATA_SIZE:      xap->szPrgData =      toint(ch+1, sz); break;
            case FT_RAM_START_ADDR:    xap->startPC =        toint(ch+1, sz); break;
            case FT_STACK_START_ADDR:  xap->startSP=         toint(ch+1, sz); break;
            case FT_BUFFER_ADDR:       xap->startBF=         toint(ch+1, sz); break;
            case FT_SERIAL_SIG:        xap->serialsig=       toint(ch+1, sz); break;
            case FT_FLASH_START_ADDR:  xap->startFlash =     toint(ch+1, sz); break;
            case FT_CODE_CRC32:        xap->crcCode =        toint(ch+1, sz); break;
            case FT_DATA_CRC32:        xap->crcData =        toint(ch+1, sz); break;
            case FT_ID0:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<<24; break;
            case FT_ID1:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<<16; break;
            case FT_ID2:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<< 8; break;
            case FT_ID3:               xap->deviceid[1] |= (toint(ch+1, sz)&0xFF)<< 0; break;
            case FT_ID4:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<<24; break;
            case FT_ID5:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<<16; break;
            case FT_ID6:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<< 8; break;
            case FT_ID7:               xap->deviceid[0] |= (toint(ch+1, sz)&0xFF)<< 0; break;
            case FT_FUNTODOS:	       xap->funToDo =        toint(ch+1, sz); break;
            case FT_LOGGING:	       xap->logging =        toint(ch+1, sz); break;
            case FT_SWJ:	           xap->swj     =        toint(ch+1, sz); break;
            case FT_END_SYMBOL:        ch = buff;                             break;
            default:		           break;
        }
        ch += sz + 2;
    }

    if (xap->vendor == (KINETIS << 24)) {
        if (xap->startBF == 0) {
            if ((xap->szRam + (xap->startPC & 0xFFFFFFFE))  > (((xap->startSP+0x3FF)&0xFFFFFC00) + xap->szProg)) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else {
                temp = (xap->startPC & 0xFFFFFFFE) + xap->szPrgCode + xap->szPrgData;
                temp = (temp + 0x0F) & 0xFFFFFFF0;   // buffer address
                xap->startBF = temp;
            }
        }
    } else {
        if (xap->startBF == 0) {
            if (xap->szRam == 0) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else if (xap->szRam > 0 && ((xap->szRam + (xap->startPC & 0xFFFFFFFE))  > (((xap->startSP+0x3FF)&0xFFFFFC00) + xap->szProg))) {
                xap->startBF = (xap->startSP + 0x3FF) & 0xFFFFFC00;
            } else {
                temp = (xap->startPC & 0xFFFFFFFE) + xap->szPrgCode + xap->szPrgData;
                temp = (temp + 0x0F) & 0xFFFFFFF0;   // buffer address
                xap->startBF = temp;
            }
        }
    }

    iP("\n RAM Size=%d, PC=0x%08X, SP=0x%08X, BF=%08X", xap->szRam, xap->startPC, xap->startSP, xap->startBF);

    xap->sectors[i>>1].szSector = 0xFFFFFFFF;
    xap->sectors[i>>1].startSector = 0xFFFFFFFF;
    xap->cfgregs[j++] = 0xFFFFFFFF;
    xap->cfgregs[j++] = 0xFFFFFFFF;

    if (xap->vendor == (KINETIS << 24))
        xap->resett = 0x41;

    swd_calctime();

    xap->dapstate |= PREQ_SAPLOADED;
    dP("\nparseSAP Body()::Finished");
    if (xap->sreset)
        xap->resett = xap->sreset;

    if (xap->svoltage)
        xap->voltage = xap->svoltage;

    if (xap->sswj) 
        xap->swj = xap->sswj;

    if (xap->swj != SWJ_JTAG)
        xap->swj = SWJ_SWD;
    else
        xap->dapspeed = 100000;

    if (xap->dapspeed == 0)
        xap->dapspeed = SWJ_CLOCK_SPEED;

/*    if (xap->slogging)
        xap->logging = xap->slogging;

    if (xap->logging > 0) {
        xap->logging <<= 2;
        file_ext_replace((char *)xap->sapfname, (char *)xap->logname, "LOG");
    }
*/
#endif
    return DAP_OK;
}

int swd_select_sap(xDAPPacket *xPacket)
{
	
    xDAPPort *dap = &pPort;
    int ret;
		int RstMode=0;
		uint32_t *vendor_rev=(uint32_t *)0x8002400;
		uint32_t	DEFAULT_VENDOR=(*vendor_rev)&0xFFFF;
	cP("swd_select_sap\r\n");
	if(xap->SapExtraFun)
			free(xap->SapExtraFun);
	if(xap->SapSequence)
			free(xap->SapSequence);
	memset((void *)xap, 0, sizeof(xSapFile)); 
	
	ret = GetSAPFileName((char *)xap->sapfname);
	if (ret) return ret;
	
	ret = GetSAPFile(xap->sapfname,xBuff->buffo,0x400,0);
	if(ret<=0) 
	{	
		ReportMsgByAck(dap,FR_NOT_SAP_FILE);
		return ret;
	}
xap->sapid = 0;
	parseSAPHeader(xBuff->buffo);
	parseSAPB(xap->sapid);
	if (xap->vendor == (STM32 << 24))
	{
		RstMode=1;	
		//Reinit_RST(1);
	}
	else if(xap->vendor == (KINETIS << 24))
	{
		RstMode=0;		
		//Reinit_RST(0);
	}else if(xap->vendor == (NVC << 24))
	{
		RstMode=1;
		//Reinit_RST(1);
	}else if(xap->vendor == (ATMEL<<24))
	{
		RstMode=0;
		//Reinit_RST(1);
	}else
	{
		RstMode=1;
		//Reinit_RST(0);
	}
	if(rst_flag)
	{
		RstMode=((~RstMode)&0x01);
	}
	Reinit_RST(RstMode);
#ifdef USE_VENDOR_LIMIT
	{

		unencrypt(3,0,4,(uint8_t *)&DEFAULT_VENDOR);
		if(xap->vendor!=(DEFAULT_VENDOR&0xFFFF)<<24)
			return DAP_ERROR;
	}
#endif	
	return DAP_OK;
}
int swd_close_sap(xDAPPacket *xPacket)
{
	CloseSAPFile(xap->sapfname);
	return 0;
}
#endif

#ifdef __VERBOSE_HELP__
/*----------------------------------------------------------------------------*/
#define HLPSAP		"sap\t\tSelect SAP file in SD card, please use <ls 0> command to list the SAP file.\n\t\t(eg. sap [1..n])."
#define HLPCSAP		"close\t\tClose opened file."
#define HLPBIN		"bin\t\tSelect BIN file in SD card, please use <ls 1> command to list the BIN file.\n\t\t(eg. bin [1..n])."
#define HLPSSAP		"ssap\t\tShow current SAP configs."
#define HLPSFUN		"sfun\t\tSet Function Todos.\n\t\t(eg. sfun 0x35)"
#define HLPCONN		"connect\t\tConnect to target."
#define HLPPWRON    "powerauto\tpower control."
#define HLPDISC     "disconn\t\tDisconnect target."
#define HLPTPIN     "testpin\t\tJTAG Pins loopback test, TCK-TMS, TDI-TDO, nRESET-nTRST."
#define HLPSPIN     "swjpin\t\tSet PIN (select state)."
#define HLPSLED     "dapled\t\tSet LED (pin onoff)."
#define HLPINFO		"dapinfo\t\tRead DAP Infos."
#define HLPINIT 	"init\t\tInitialize target debugger."
#define HLPSROM 	"scanrom\t\tScan ROM Table."
#define HLPRHALT 	"resethalt\tReset and Halt target."
#define HLPRSTG 	"resetgo\t\tReset target and go."
#define HLPRSTT 	"reset\t\tReset target without halt."
#define HLPHALT 	"halt\t\tHalt target."
#define HLPRSUM 	"resume\t\tResume target."
#define HLPAUTO 	"auto\t\tExecute auto run procedure, need to select SAP first."
#define HLPERASE	"erasechip\tErase target flash chip."
#define HLPBLANK	"blankcheck\tBlank Check, erase if not blank.(according SAP file)"
#define HLPESSEC	"erasesector\tErasesector(according SAP file)"
#define HLPPROG		"program\t\tProgram flash.(according SAP file)"
#define HLPVERIFY	"verify\t\tVerify CRC32.(according SAP file)"
#define HLPLOAD		"loadcode\tLoad flash algorithm inside SAP file."
#define HLPDEBUG	"debug\t\tSet debug output level.\n\t\t(eg. debug 0x10)."
#define HLPSPWR     "setpower\tSET Power Output."
#define HLPSSPD     "setspeed\tSET DAP Speed."
#define HLPSSET     "setreset\tSET Target RESET."
#define HLPSLOG     "setlog\t\tSET log to file level(0==>No log)."
#define HLPSSWJ     "setswj\t\tSET Target to SWD or JTAG(0x02) Mode."
#define HLPSSTS     "setstate\tSET DAP state."
#define HLPLIST		"ls\t\tList SAP files in SD card."
#define HLPCCRC     "crc\t\tCalculate CRC32 according binary in SAP."
#define HLPLISTU	"lsusb\t\tList files in USB MSC."
#define HLPVER		"version\t\tList SAP files in SD card."
#define HLPBOOT		"bootloader\tReset to bootloader."
#define HLPTXT		"txtest\t\tSPI Packet Transfer Test\n\t\t(eg. txtest 0x400)."
#define HLPRUNF		"runfun\t\tRun a function(eg. runfun R0 R1 R2 PC, in hex)."
#define HLPSDRT		"sdrtest\t\tTest SD read file."
#define HLPDUMP     "dump\t\tDump to SD file.\n\t\t(eg. dump 0x0 0x400 abc.bin"
#define HLPHELP		"help\t\tShow this help."
#define HLPADC		"adc\t\tShow ADC readings."
#define HLPPWR      "power\t\tpower control(eg. power 1 0xf)."
#define HLPCPWR     "chkpwr\t\tpower check."
#else
#define HLPSAP				"sap\t\tSelect SAP file in spiflash."
#define HLPCSAP				NULL
#define HLPBIN				NULL
#define HLPSSAP				NULL
#define HLPSFUN				NULL
#define HLPCONN				"connect\t\tConnect to target."
#define HLPPWRON  		"poweron\t\tpower on the target."
#define HLPDISC          NULL
#define HLPTPIN          NULL
#define HLPSPIN          NULL
#define HLPSLED          NULL
#define HLPINFO		       NULL
#define HLPINIT 	      "reset\t\treset the target."
#define HLPSROM 	       NULL
#define HLPRHALT 	       "halt\t\thalt the target."
#define HLPRSTG 	       NULL
#define HLPRSTT 	       NULL
#define HLPHALT 	       NULL
#define HLPRSUM 	       NULL
#define HLPAUTO 	       "auto\t\tExecute auto run procedure, need to select SAP first."
#define HLPERASE	       "erasechip\tErase target flash chip."
#define HLPBLANK	       "blankcheck\tBlank Check, erase if not blank.(according SAP file)"
#define HLPESSEC	       NULL
#define HLPPROG		       "program\t\tProgram flash.(according SAP file)"
#define HLPVERIFY	       "verify\t\tVerify CRC32.(according SAP file)"
#define HLPLOAD		       NULL
#define HLPDEBUG	       NULL
#define HLPSPWR          NULL
#define HLPSSPD          NULL
#define HLPSSET          NULL
#define HLPSLOG          NULL
#define HLPSSWJ          NULL
#define HLPSSTS          NULL
#define HLPLIST		       NULL
#define HLPCCRC          NULL
#define HLPLISTU	       NULL
#define HLPVER		NULL
#define HLPBOOT		NULL
#define HLPTXT		NULL
#define HLPRUNF		NULL
#define HLPSDRT		NULL
#define HLPDUMP   NULL
#define HLPHELP		NULL
#define HLPADC		NULL
#define HLPPWR    NULL
#define HLPCPWR   NULL
#endif
int swd_usage(xDAPPacket *xPacket);
static xDAPCmd xCmds[] /*__attribute__( ( section( "CCM")) )*/ = {
    /*command,		function,		argc,  preq, name & help message */
    { SM_SWD,		&swd_reset,			0, 0x00, HLPRSTT},
    { SM_NONSWD,	&swd_setreset,		1, 0x00, HLPSSET},
    { SM_NONSWD,	&swd_calcCRC32,		0, 0x00, HLPCCRC},
    { SM_NONSWD,	&swd_usage,			0, 0x00, HLPHELP},
    //{ SM_DUMP,		&swd_dump,		 0x47, 0x3F, HLPDUMP},
		{ SM_NONSWD,		&swd_dump,		 0x47, 0x3F, HLPDUMP},
    { SM_NONSWD,	&swd_select_sap, 0x03, 0x00, HLPSAP},
    { SM_NONSWD,	&swd_close_sap,  0x03, 0x00, HLPCSAP},
    { SM_CONNECT,	&swd_connect,		0, 0x00, HLPCONN},
    { SM_START,	    &swd_power_control,	0, 0x03, HLPPWRON},
    { SM_DISCONNECT,&swd_disconnect,	0, 0x00, HLPDISC},
    { SM_SWD,	    &swd_showled,    	7, 0x00, HLPSLED},
    { SM_AUTO,		&swd_auto,		    0, 0x03, HLPAUTO},
    { SM_DAPRESET,	&swd_powerup_reset,	0, 0x0F, HLPINIT},
    { SM_RESET_HALT,&swd_reset_halt,	1, 0x1F, HLPRHALT},
    { SM_RESET_GO,	&swd_resetgo,		0, 0x3F, HLPRSTG},
    { SM_SWD,		&swd_halt,			0, 0x18, HLPHALT},
    { SM_LOAD_CODE,	&swd_loadcode,		0, 0x3F, HLPLOAD},
    { SM_ERASECHIP,	&swd_erasechip,		0, 0x7F, HLPERASE},
    { SM_BLANKCHECK,&swd_blankcheck,	0, 0x7F, HLPBLANK},	
    { SM_ERASESECTOR,&swd_erasesector,	0, 0x7F, HLPESSEC},	
    { SM_PROGPAGE  ,&swd_progpage,		0, 0x7F, HLPPROG},
    { SM_CRC32,		&swd_CRC32,			0, 0x7F, HLPVERIFY},
    { SM_SWD,		&swd_runfun,		0, 0x7F, HLPRUNF},	
    { 0,		    NULL,       		0, 0,    NULL},	
    NULL
} ;
/* ------------------------------------------------------------------------------*/
int swd_usage(xDAPPacket *xPacket)
{
    int i, j;

    for(i=0;xCmds[i].op;i++) {
        if (xCmds[i].name_hlp != NULL) {
            mtk_printf(xCmds[i].name_hlp,-1);
            if (xCmds[i].argc > 0) {
                mtk_printf("\n\t\targuments:",-1);
                for (j=0;j<4;j++) {
                    if (xCmds[i].argc & (1<<j)) {
                        if (xCmds[i].argc & (1<<(j+4))) {
                            mtk_printf(" <string>",-1);
                        } else {
                            mtk_printf(" <hex>",-1);
                        }
                    }
                }
                cP(".");
            }
			mtk_printf("\n",-1);	
        }
    }
    mtk_printf("\n\n",-1);
    return DAP_OK;
}

/* ------------------------------------------------------------------------------*/
xDAPCmd *parsecmd(uint8_t *command)
{
    int i, j;
    uint8_t *oparg;
    xDAPPort *dap = &pPort;

    xap->doChipEraseBySector = 0;
    if (strequ(command, "erasechip") &&  xap->funEraseChip == 0x00 && xap->funEraseSector > 0) {
        strcpy((char *)command, "erasesector");
        xap->doChipEraseBySector = 1;
    }

    for(i=0;xCmds[i].op;i++) {
        if (strequ(command, xCmds[i].name_hlp)) {
            oparg = command;
            for (j=0;j<4;j++) {
                if (xCmds[i].argc & (1<<j)) {
                    oparg = nextarg(oparg);
                    if (xCmds[i].argc & (1<<(4+j))) {         // string
                        if (*oparg) {
                            dap->regs[j] = (uint32_t)oparg;
                        } else {
                            dap->regs[j] = 0;
                        }
                    } else {
                        dap->regs[j] = uint32(oparg);       // integer
                    }
                }
            }
            return &xCmds[i];
        }
    }
    return NULL;
}
/* ------------------------------------------------------------------------------*/
xDAPCmd *parsecmdPkt(xCMDPacket *xPkt)
{
    int i;

    if (xPkt->cmd == SM_ERASECHIP && (xap->dapstate & PREQ_SAPLOADED) == 0) {
        if ((xap->dapstate & PREQ_SAPSELECTED) == 0) {
            if (parseSAPH(xap->sapfname) < 0) {					/* parse sap file */
                eP("Parsing SAP file Failed!!!\n" );
                return NULL;
            } 
        } 
        if (parseSAPB(xap->sapid) < 0) {					/* parse sap file */
            eP("Parsing SAP file Failed!!!\n" );
            return NULL;
        }
    }
    xap->doChipEraseBySector = 0;
    if (xPkt->cmd == SM_ERASECHIP && xap->funEraseChip == 0x00 && xap->funEraseSector > 0) { /* DO_ERASECHIP without xap->funEraseChip() */
        xPkt->cmd = SM_ERASESECTOR;
        xap->doChipEraseBySector = 1;
    }

    for(i=0;xCmds[i].op;i++) {
        if (xPkt->cmd == xCmds[i].op) {
            return &xCmds[i];
        }
    }
    return NULL;
}
/* ------------------------------------------------------------------------------*/
int cmdLine(uint8_t *command)
{
	int MCP_DAPCMDLineTask (xDAPCmd *cmd ); 
     xDAPCmd *cmd;
     xDAPPacket xPacket = {0,0,0};
    xDAPPort *dap = &pPort;
	int ret = DAP_CMD_NOT_SUPPORT;

    xap->rrport = 0;
    dap->xport = xap->xport;   //((SPIx_RDY_GPIO_PORT->IDR) >> SPIx_RDY_SHIFT) & SPIx_RDY_MASK;
    xPacket.port = dap->xport;
   // xap->CICO |= DAP_INTF_I2C;
    if (command != NULL && NULL != (cmd = parsecmd(command))) {
        if (cmd->op < SM_NONSWD) {
            if ((cmd->preq & xap->dapstate) == cmd->preq) {
               ret = MCP_DAPCMDLineTask(cmd);
            } else {
                uint32_t preq;

                preq = (xap->dapstate ^ cmd->preq) & cmd->preq;

                cP("\nPlease check dap state(%02X / %02X) before run this command!!!", xap->dapstate, cmd->preq);
                cP("\n\tNeed issue following commands first:");
                if (preq & (PREQ_SAPSELECTED|PREQ_SAPLOADED)) cP("\n\t\tsap <n> <id>\t\t==> select SAP file.");
                if (preq & PREQ_CONNECTED  ) cP("\n\t\tconnect\t\t==> connect target.");
                if (preq & PREQ_INITED	   ) cP("\n\t\tinit\t\t==> Init Target DAP.");
                if (preq & PREQ_HALTED     ) cP("\n\t\tresethalt\t\t==> Reset and Halt Target.");
                if (preq & PREQ_CODELOADED ) cP("\n\t\tloadcode\t\t==> Load Algorithm.");
            }
        } else
           ret = cmd->func(&xPacket);
		
		if(dbg_deep>0) mtk_printf("cmdline return",ret);
        return ret;
    } 
    return ret;
}


/* ------------------------------------------------------------------------------*/
int MCP_DAPCMDLineTask (xDAPCmd *cmd ) 
{
    xDAPPacket xPacket = {0,0,0};
    xDAPPort *dap = &pPort;
  //   portCHAR cDMA = 0xFF;
   // xTaskHandle hDAPTask;
  //  xDAPCmd *cmd = (xDAPCmd *)pvParameters;
volatile    U32 temp;
    int rc = DAP_ERROR;

    if (cmd->func == NULL)
        return rc;

    dap->startTick = getTick();
    cP("\nStart Tick(%d)", dap->startTick);
  
    INITSEQ(0);
	if(!gProgOnLine)
	{
    if (cmd->op == SM_AUTO)
        dap->manstate = SM_START;
    else
        dap->manstate = cmd->op;
	}else
	{
		
	}
		dap->substate = 0;
    dap->funstate = 0;
    dap->timeout = 2500;
    dap->xport = (1<<PORT_COUNT) - 1;
    xPacket.port = 0; xPacket.offset = 0; xPacket.length = 0;

	for (;;) {

            if (xPacket.port) {
                xap->portstate[P2I(xPacket.port)] = (dap->manstate<<16)|(dap->substate<<8)|(dap->funstate);
                xap->portaddr[P2I(xPacket.port)] = dap->adr;
                dap->inseq[P2I(xPacket.port)]++;
            }
            //printPacket(0, &xPacket);
			xPacket.port=1;
            rc = cmd->func(&xPacket);
			
           if (rc == DAP_ERROR) {
						 {
              
							 if(gAtmelHasErase)
								 gAtmelHasErase=0;
							if(gAtmelHasPlug)
								gAtmelHasPlug=0;		
							if(gAtmelNeedLock)
								gAtmelNeedLock=0;
							 break;
						 }
            }
						if(rc==DAP_NEXTSAP)
						{
								int ret=0;
								//ret = GetSAPFile(xap->sapfname,xBuff->buffo,0x400,0);
								//if(ret<0) return ret;
                xPacket.port = 0; xPacket.offset = 0; xPacket.length = 0;							
								xap->sapid += 1;
								output_0v_io(); // cut off power to target
                	Delayms(1000);  // wait for hardware stable
							dap->manstate = SM_START;
                //xap->dapstate &= ~(PREQ_SAPLOADED | PREQ_CODELOADED);
							xap->dapstate = 0;
								ret=parseSAPB(xap->sapid);
								if(ret)
									return ret;
						}
						if(rc==DAP_AGAIN)
						{
							if(gAtmelHasErase)
								 gAtmelHasErase=0;
							if(gAtmelHasPlug)
								gAtmelHasPlug=0;		
							if(gAtmelNeedLock)
								gAtmelNeedLock=0;
							return rc;
						}
						if(rc==DAP_CONTINUE)
						{
							return rc;
						
						}
			if (dap->manstate == SM_DONE)
			{
				rc = DAP_OK;
				gAtmelHasErase=0;
				gAtmelHasPlug=0;		
				gAtmelNeedLock=0;
				break;
			}	
				
	}
    temp = getTick();
    cP("\nFinished Elapse(%d)ms, now(%d)ms.", temp - dap->startTick, temp);
    dap->manstate = SM_DONE;
    dap->substate = 0;
    dap->funstate = 0;
    dap->adr = 0;
	return rc;
}
int32_t get_sap_vendor()
{
	return xap->vendor;
}
static U16	funToDoSav=0;
int SetFunTodo(uint8_t action)
{
	xDAPPort *dap = &pPort;
	if(!xap)
		return -1;

	switch(action)
	{
		case DO_CONNECT:
				funToDoSav=xap->funToDo;
				xap->funToDo&=~DO_LOADCODE;
				xap->funToDo&=~DO_PROGRAM;
				xap->funToDo&=~DO_ERASECHIP;
				xap->funToDo&=~DO_BLANKCHECK;
				xap->funToDo&=~DO_CRC32;
				xap->funToDo&=~DO_RESETGO;	
				dap->manstate=SM_START;
				break;
		case DO_ERASESECTOR:
				xap->funToDo=funToDoSav;
				xap->funToDo&=~DO_PROGRAM;
				xap->funToDo&=~DO_ERASECHIP;
				xap->funToDo&=~DO_BLANKCHECK;
				xap->funToDo&=~DO_CRC32;
				xap->funToDo&=~DO_RESETGO;		
				xap->funToDo|=DO_ERASESECTOR;

				dap->manstate=SM_LOAD_CODE;
				break;
			case DO_ERASECHIP:
				xap->funToDo=funToDoSav;
				xap->funToDo&=~DO_PROGRAM;
				xap->funToDo&=~DO_ERASESECTOR;
				xap->funToDo&=~DO_BLANKCHECK;
				xap->funToDo&=~DO_CRC32;
				xap->funToDo&=~DO_RESETGO;
				xap->funToDo|=DO_ERASECHIP;
				dap->manstate=SM_LOAD_CODE;
				break;
			case DO_PROGRAM:
				//xap->funToDo&=~DO_ERASECHIP;
				///xap->funToDo&=~DO_ERASESECTOR;
				//xap->funToDo&=~DO_BLANKCHECK;
				xap->funToDo=funToDoSav;
				xap->funToDo|=DO_ERASECHIP;
				xap->funToDo|=DO_PROGRAM;	
				xap->funToDo&=~DO_CRC32;
				xap->funToDo&=~DO_RESETGO;
				dap->manstate=SM_LOAD_CODE;			
				break;
			case DO_BLANKCHECK:
				xap->funToDo=funToDoSav;
				//xap->funToDo&=~DO_ERASECHIP;
				xap->funToDo&=~DO_ERASESECTOR;
				xap->funToDo&=~DO_PROGRAM;
				xap->funToDo&=~DO_CRC32;
				xap->funToDo&=~DO_RESETGO;
				xap->funToDo|=DO_BLANKCHECK;
				dap->manstate=SM_LOAD_CODE;					
				break;
			case DO_CRC32:
				xap->funToDo=funToDoSav;
				xap->funToDo&=~DO_ERASECHIP;
				xap->funToDo&=~DO_ERASESECTOR;
				xap->funToDo&=~DO_PROGRAM;
				xap->funToDo&=~DO_BLANKCHECK;
			xap->funToDo&=~DO_RESETGO;
				xap->funToDo|=DO_CRC32;	
				dap->manstate=SM_LOAD_CODE;					
				break;
			default:
				break;
	}
}
void ResetDapStatus()
{
	int ret=0;	
	xDAPPort *dap = &pPort;
			//ret = GetSAPFile(xap->sapfname,xBuff->buffo,0x400,0);
			//if(ret<0) return ret;					
			xap->sapid += 1;
			output_0v_io(); // cut off power to target
				Delayms(1000);  // wait for hardware stable
		dap->manstate = SM_START;
			//xap->dapstate &= ~(PREQ_SAPLOADED | PREQ_CODELOADED);
		xap->dapstate = 0;
			ret=parseSAPB(xap->sapid);
}
void SendErrMsg(char *msg)
{
	eP(msg);
}

xSapFile* GetSapFile(void)
{
	return xap;
}