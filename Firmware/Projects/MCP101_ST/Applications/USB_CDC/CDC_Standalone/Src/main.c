/**
  ******************************************************************************
  * @file    USB_Device/CDC_Standalone/Src/main.c 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    17-December-2014
  * @brief   USB device CDC application main file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "common.h"

#include <limits.h>
/** @addtogroup STM32F1xx_HAL_Validation
  * @{
  */

/** @addtogroup STANDARD_CHECK
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/ 
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef I2cHandle;
uint32_t led_count = 0;
uint32_t work_mode = 0;
int reset_request = 0;
int button_pressed=0;
int gProgOnGoing=0;
int gProgOnLine=0;
int gStartProg=0;
int gLoopExit=0;
int gButtonPressed=0;
int gStartProgI2C=0;
extern int clk_flag;//if clk_flag is 1,try different reset mode
extern int rst_flag;//if rst_flag is 1,try different reset mode
extern uint8_t usbrxbuf[128*2];//buffer for usb receive
extern int gRecvCplt;
int Device_Addr;

I2COPStage_t I2COperationStage;
UART_HandleTypeDef UartHandle;

int gYellowBlink=0;
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
extern void usbd_hid_process (void);
#define Buzzer_Start(a) do{}while(0) 
#define Buzzer_Stop()  do{}while(0)	
	
#define DEFINE_PARSE_NUM_TYPE(name, type, func, min, max) \
	int parse ## name(const char *str, type * ul) \
	{ \
		if (!*str) { \
			return -1; \
		} \
		char *end; \
		*ul = func(str, &end, 0); \
		if (*end) { \
			return -1; \
		} \
		if ((max == *ul) ) { \
			return -1;	\
		} \
		if (min && (min == *ul) ) { \
			return -1; \
		} \
		return 0; \
	}
DEFINE_PARSE_NUM_TYPE(_ulong, unsigned long, strtoul, 0, ULONG_MAX)
DEFINE_PARSE_NUM_TYPE(_ullong, unsigned long long, strtoull, 0, ULLONG_MAX)
DEFINE_PARSE_NUM_TYPE(_long, long, strtol, LONG_MIN, LONG_MAX)
DEFINE_PARSE_NUM_TYPE(_llong, long long, strtoll, LLONG_MIN, LLONG_MAX)

#define DEFINE_PARSE_WRAPPER(name, type, min, max, functype, funcname) \
	int parse ## name(const char *str, type * ul) \
	{ \
		functype n; \
		int retval = parse ## funcname(str, &n); \
		if (0 != retval)	\
			return retval; \
		if (n > max) \
			return -1;	\
		if (min) \
			return -1; \
		*ul = n; \
		return 0; \
	}

#define DEFINE_PARSE_ULONGLONG(name, type, min, max) \
	DEFINE_PARSE_WRAPPER(name, type, min, max, unsigned long long, _ullong)
DEFINE_PARSE_ULONGLONG(_uint, unsigned, 0, UINT_MAX)
DEFINE_PARSE_ULONGLONG(_u64,  uint64_t, 0, UINT64_MAX)
DEFINE_PARSE_ULONGLONG(_u32,  uint32_t, 0, UINT32_MAX)
DEFINE_PARSE_ULONGLONG(_u16,  uint16_t, 0, UINT16_MAX)
DEFINE_PARSE_ULONGLONG(_u8,   uint8_t,  0, UINT8_MAX)
/* Private functions ---------------------------------------------------------*/
void run_led(int step,int led_mode)
{
	switch(step)
	{
		case 0://start
			led_yellow_on();	
			led_green_on();
		#if __WITH_BUZZ
				Buzzer_Start(0);
				Delayms(1000);
				 Buzzer_Stop();
		#endif
			break;
		case 1://init
			led_yellow_off();
			led_green_on();
			led_red_off();
			break;
		case 2://busy
		{
				
			if(led_mode==0)
			 {
				led_green_off();
				led_red_off();
				 led_yellow_blink();

			 }else if(led_mode==1)
			 {					
				 led_green_off();
				led_red_off();
				 led_yellow_blink();

			 }else
			 {}
		}
		break;
		case 3://finish
		{
			if(led_mode==0)
			{
				led_yellow_blink_off();
			}else if(led_mode==1)
			{
				led_yellow_blink_off();
			}else
			{}
			//inc prog statistics	
		}
			break;
		case 4://success
		{
			if(led_mode==0)
			 {
					led_red_off();
				 led_green_blink();
				#if __WITH_BUZZ
				 Buzzer_Start(4);
				Delayms(500);
				 Buzzer_Stop();
				 #endif
				led_green_blink_off();
				led_green_on();
			 }else if(led_mode==1)
			 {
				 led_green_on();				
	
				#if __WITH_BUZZ
				 Buzzer_Start(1);
				Delayms(100);
				Buzzer_Stop();
				#endif
			 }else
			 {}
			#if __WITH_BUZZ
			Buzzer_Stop();
			#endif	 
		}
			break;
		case 5://fail
		led_red_on();
		#if __WITH_BUZZ
		Buzzer_Start(3);
		Delayms(500);
		Buzzer_Stop();
		#endif
		//inc prog fail statistics	
		
				break;
		default:
			break;
	}
}

/**
  * @brief  Configures EXTI line 0 (connected to PA.00 pin) in interrupt mode
  * @param  None
  * @retval None
  */
void EXTI3_IRQHandler_Config(void)
{
  GPIO_InitTypeDef   GPIO_InitStructure;

  /* Enable GPIOA clock */
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /* Configure PA.00 pin as input floating */
  GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  GPIO_InitStructure.Pin = GPIO_PIN_3;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Enable and set EXTI line 0 Interrupt to the lowest priority */
  HAL_NVIC_SetPriority(EXTI3_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);
}
void SetCS(int flag)
{
		HAL_GPIO_WritePin(GPIOB,CS_BIT,flag);
}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
extern uint32_t g_dwTimeout;
int main(void)
{
  int retry = 0,ret = 0;
	int i = 0;
		uint8_t aRxBuffer[0x4];		
	int NeedContinue=0;
	uint32_t ChipIndex=*((uint8_t *)0x8002503);
	uint8_t *tHeap;
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();
  
  /* Configure the system clock to 72 MHz */
  SystemClock_Config();
  
	/*Init the LL Device*/
	Init_GPIO();
	/*##-1- Configure the I2C peripheral ######################################*/
  I2cHandle.Instance             = I2Cx;
  I2cHandle.Init.ClockSpeed      = I2C_SPEEDCLOCK;
  I2cHandle.Init.DutyCycle       = I2C_DUTYCYCLE;
  I2cHandle.Init.OwnAddress1     = (I2C_ADDRESS+(ChipIndex-1)*2)&0x7F;
  I2cHandle.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
  I2cHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  I2cHandle.Init.OwnAddress2     = 0xFF;
  I2cHandle.Init.GeneralCallMode = I2C_GENERALCALL_ENABLE;//I2C_GENERALCALL_DISABLE;
  I2cHandle.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;  
  
  if(HAL_I2C_Init(&I2cHandle) != HAL_OK)
  {
    /* Initialization Error */
    //Error_Handler();
  }
		
	UartHandle.Instance        = USART3;

  UartHandle.Init.BaudRate   = 115200;
  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits   = UART_STOPBITS_1;
  UartHandle.Init.Parity     = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode       = UART_MODE_TX_RX;
  if(HAL_UART_DeInit(&UartHandle) != HAL_OK)
  {
    Error_Handler();
  }  
	if(HAL_UART_Init(&UartHandle) != HAL_OK)
	{
		Error_Handler();
	}

 
	EXTI3_IRQHandler_Config();

	ADC_Config();
	DAC_Config();
	//output_3v3_io();
	//RTC_Config();
	//output_3v3_io();
#if 0
		for(i=0;i<(sizeof(aRxBuffer)/sizeof(aRxBuffer[0]));i++)
		aRxBuffer[i] = i+0x30+1;
	
	
	//calcCRC32(aRxBuffer,8,0);
	ret = SerialEraseTargetFlash(USART_SERIAL,0x40030000,1);
	ret = SerialSendBuf(USART_SERIAL,0x40030000,aRxBuffer,(sizeof(aRxBuffer)/sizeof(aRxBuffer[0])));


	//ret = SerialWriteTargetFlash(USART_SERIAL,0x181800,aRxBuffer,(sizeof(aRxBuffer)/sizeof(aRxBuffer[0])));
//	ret = SerialSendBuf(USART_SERIAL,aRxBuffer,(sizeof(aRxBuffer)/sizeof(aRxBuffer[0])));
	//ret = SerialWriteTargetFlash(USART_SERIAL,0x181C00,aRxBuffer,(sizeof(aRxBuffer)/sizeof(aRxBuffer[0])));	
	//SerialVerifyTargetFlash(USART_SERIAL,0x181800,16,1);
#endif	
	SW_CDC_Init(usbrxbuf,USB_BUF_SIZE);
	
	spiflash_probe(1,0);
	
	InitStatistics();
				
	DAP_Setup();
	iDAPOpen();

	HAL_Delay(200);				// Wait for 200ms

	led_green_on();


	SetCS(1);
  /* Infinite loop */
	I2CServiceStart();	
  while (1)
  {
		
		gLoopExit=0;//the flag to tell USB thread that the Infinite loop had been shutup;	
			
#ifdef AUTO_TEST		
		if(retry==0&&gStartTest==1)
			retry=1;
#endif

		while(gStartProg==0)
		{
			g_dwTimeout = 200;
			UartServiceEntry(0);
			if(!gProgOnLine)
				I2CServiceEntry(0);
			if(!gProgOnLine&&!gStartProgI2C)
			{	
				if(gButtonPressed)
				{
					gStartProg=1;
					SetCS(0);//give a alarm to UI
					//Delayms(200);
					//SetCS(1);
					gButtonPressed=0;
					button_pressed=1;
				}
		
			}else if(gProgOnLine)
			{
				gLoopExit=1;
				gStartProg=0;
				g_dwTimeout = 2000;
				continue;
			}else if(gStartProgI2C)
			{
				gStartProg=1;
				gButtonPressed=0;				
	//			gStartProgI2C=0;
			}


		}
		if(gStartProg||retry>0||NeedContinue)
	  {	
			int ret;
			uint8_t led_mode=0;
	#ifdef AUTO_TEST
		if(gStartTest==0)
			gStartTest=1;
	#endif
		
		I2COperationStage.IsBusy=1;
		gProgOnGoing=1;
		 //Buzzer_Stop();

//		GetLedMode(&led_mode);
		run_led(2,led_mode);
RETRY:		
		ret = program_process();
		if(ret ==0xF2)//if unstable ,or need to prog again
		{
			HAL_Delay(1000);
			goto RETRY;
//			retry++;
//			if(retry==1)
//			{
//				rst_flag=1;
//				clk_flag=1;
//			}	
//			if(retry<2)
//			{
	//			Delayms(100);
//				continue;
//			}
		}
		if(ret ==0xF3)// need to prog continue
		{
			NeedContinue=1;
			Delayms(200);
				continue;
		}
		if(retry>0)
			retry=0;
		
		if(NeedContinue)
			NeedContinue=0;
		
		rst_flag=0;
		clk_flag=0;
		gProgOnGoing=0;
		gStartProg=0;
		run_led(3,led_mode);

		if(ret ==0)
		{	  
			I2COperationStage.IsBusy=0;
			I2COperationStage.IsOK=1;			
			run_led(4,led_mode);
		}	  
		else{
						SendLastErrCodeI2C();	
			I2COperationStage.IsBusy=0;
			I2COperationStage.IsOK=0;			
			run_led(5,led_mode);
		}
		if(!gProgOnLine&&button_pressed)
		{
			SetCS(1);//give a alarm to UI
			I2CServiceEntry(0);
			//SetCS(1);
		}
	 }
	 if(gStartProgI2C)
	 {
			gStartProgI2C=0;
	 }
		if(button_pressed)
		{			
			button_pressed=0;			
			//enable_usb_irq();			
		}	
	 	if(reset_request)
		{	  
			mtk_printf("System reset now...\n\r",-1);		
			HAL_Delay(500);			
			SystemReset();
			
		} 
	}
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 72000000
  *            HCLK(Hz)                       = 72000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 2
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            HSE PREDIV1                    = 1
  *            PLLMUL                         = 9
  *            Flash Latency(WS)              = 2
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};
	  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef rccperiphclkinit = {0};
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSI;
  oscinitstruct.HSEState        = RCC_HSE_ON;
  oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV1;
  oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL9;
  oscinitstruct.LSIState = RCC_LSI_ON;  
  oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  {
    /* Start Conversation Error */
    Error_Handler(); 
  }
  
  /* USB clock selection */
  rccperiphclkinit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  rccperiphclkinit.UsbClockSelection = RCC_USBPLLCLK_DIV1_5;
  HAL_RCCEx_PeriphCLKConfig(&rccperiphclkinit);
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
  clocks dividers */
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
	
	
  rccperiphclkinit.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  rccperiphclkinit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	  HAL_RCCEx_PeriphCLKConfig(&rccperiphclkinit);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
	
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_2)!= HAL_OK)
  {
    /* Start Conversation Error */
    Error_Handler(); 
  }
}
I2COPStage_t* GetI2CStage()
{
	return &I2COperationStage;
}
uint8_t GetI2CSlaveAddress()
{
	return I2cHandle.Init.OwnAddress1;
}
/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* Turn LED3 on */
  //BSP_LED_On(LED3);
  while (1)
  {
  }
}

/**
  * @brief  Toggle LEDs to shows user input state.
  * @param  None
  * @retval None
  */
void Toggle_Leds(void)
{
  static uint32_t ticks,ToggleFlag;
	if(gYellowBlink)
	{
		if (ticks++ == 100)
		{
			if(ToggleFlag)	
				led_yellow_on();
			else
				led_yellow_off();
			ToggleFlag=(~ToggleFlag)&0x01;
			ticks = 0;
		}
	}else
	{
			ticks=0;
			ToggleFlag=0;
	}
}


#if 0
char * strdup(const char * src)
{
	char *dest = malloc( strlen( src ) + 1 );
	if(dest==NULL)
		return NULL;
	strcpy( dest, src );
	return dest;	
}

char * strndup(char * src,int len)
{
	char *dest = malloc( len + 1 );
	if(dest==NULL)
		return NULL;
	strncpy( dest, src,len);
	return dest;	
}

int64_t timeval_ms()
{
	struct timeval now;
	int retval = 0;//gettimeofday(&now, NULL);
	if (retval < 0)
		return retval;
	now.tv_sec=0;
	now.tv_usec=0;
	return (int64_t)now.tv_sec * 1000 + now.tv_usec / 1000;
}




#endif
void command_print(struct command_context *context, const char *format, ...)
{

}
int gettimeofday(struct timeval *tv, void *tz)
{
    
		tv->tv_sec = 1000;
    tv->tv_usec = 100 * 1000;//for test --weib

    return 0;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
