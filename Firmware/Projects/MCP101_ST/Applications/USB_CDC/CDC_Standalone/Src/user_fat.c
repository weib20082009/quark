/* CMSIS-DAP Interface Firmware
 * Copyright (c) 2009-2013 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// #define TARGET_MK20DX
//#include <RTL.h>
//#include <rl_usb.h>
#include <string.h>
#include "common.h"
//#include "flash.h"
//#include "main.h"
//#include "tasks.h"
//#include "fsapi.h"
#if defined(TARGET_LPC11U35)
#include <LPC11Uxx.h>
#elif defined(TARGET_MK20DX)
#include <MK20D5.h>
#endif


#if defined(TARGET_LPC11U35)	
#   define WANTED_SIZE_IN_KB  		(64)
#elif defined(TARGET_MK20DX)
#   define WANTED_SIZE_IN_KB        (1000) /*(128)*/
#elif defined(STM32F10X_MD)
#   define WANTED_SIZE_IN_KB        (1000) /*(128)*/
#endif

//------------------------------------------------------------------- CONSTANTS
#define WANTED_SIZE_IN_BYTES        ((WANTED_SIZE_IN_KB + 16 + 8)*1024)
#define WANTED_SECTORS_PER_CLUSTER  (8)

#define FLASH_PROGRAM_PAGE_SIZE         (512)
#define MBR_BYTES_PER_SECTOR            (512)

#define SPIFLASH
#undef NO_FATFS
#undef RAMDISK

#ifdef RAMDISK
#define START_APP_ADDRESS  0x20000000
#define FLASH_SIZE			0x2000
#define FLASH_FAT1_ADDR 	(START_APP_ADDRESS)
#define FLASH_ROOT1_ADDR 	(START_APP_ADDRESS+MBR_BYTES_PER_SECTOR)
#define FLASH_FILE_ADDR 	(START_APP_ADDRESS+2*MBR_BYTES_PER_SECTOR)

#endif

#ifdef SPIFLASH
#define START_APP_ADDRESS   0x0
#define FLASH_SECTOR_SIZE 0x1000
#define FLASH_SIZE			0x100000
#define FLASH_FAT1_ADDR 	0x1000  			//4096 (START_APP_ADDRESS) reserved 4096 for configuration
#define FLASH_ROOT1_ADDR 	(FLASH_FAT1_ADDR+8*MBR_BYTES_PER_SECTOR)   //(START_APP_ADDRESS+MBR_BYTES_PER_SECTOR)
#define FLASH_FILE_ADDR 	(FLASH_FAT1_ADDR+16*MBR_BYTES_PER_SECTOR) //(START_APP_ADDRESS+8*MBR_BYTES_PER_SECTOR)

#define FLASH_BIN_ADDR 	MEDIA_SECTORS_NUM*4096
#define FLASH_SECTORS_NUM 	FLASH_SIZE/4096




#define SF_FILEINFO_OFF	(SF_FILESIZE_SIZE+SF_TIME_SIZE+SF_FILENAME_SIZE)//4 bytes for filesize stored;32 bytes for time;28 bytes for filename.totle 64.
#endif
extern int dbg_deep;
static int gIsFormating=0;
static uint8_t mbrbuf[512];
static int loopcnt=0;
int gNeedErase=0,gHasErase;
SFHandle *pHandle=NULL;
SFHandle SHandle[1];
/*******************SPI Flash Map******************************
					0		:	statistics
					1		: fail statistics
					2-4	:	log
					5		: Config File
					6-	:	SAP File
******************************************************************/
#define STATIS_BUF_ADD  0
#define STATIS_BUF_SIZE  FLASH_SECTOR_SIZE*1

#define STATIS_FAIL_BUF_ADD  (STATIS_BUF_ADD+STATIS_BUF_SIZE)
#define STATIS_FAIL_BUF_SIZE  FLASH_SECTOR_SIZE*1

#define LOG_BUF_ADD  (STATIS_FAIL_BUF_ADD+STATIS_FAIL_BUF_SIZE)
#define LOG_BUF_SIZE  FLASH_SECTOR_SIZE*3

#define CONFIG_BUF_ADD  (LOG_BUF_ADD+LOG_BUF_SIZE)
#define CONFIG_BUF_SIZE  FLASH_SECTOR_SIZE*1

#define QUARKROM_BUF_ADD  (CONFIG_BUF_ADD+CONFIG_BUF_SIZE)
#define QUARKROM_BUF_SIZE  FLASH_SECTOR_SIZE

#define SAPFILE_BUF_ADD  (QUARKROM_BUF_ADD+QUARKROM_BUF_SIZE)
#define SAPFILE_BUF_SIZE  (FLASH_SIZE-SAPFILE_BUF_ADD)
//--------------------------------------------------------------------- DERIVED



#define MBR_NUM_NEEDED_SECTORS  (WANTED_SIZE_IN_BYTES / MBR_BYTES_PER_SECTOR)
#define MBR_NUM_NEEDED_CLUSTERS (MBR_NUM_NEEDED_SECTORS / WANTED_SECTORS_PER_CLUSTER)

/* Need 3 sectors/FAT for every 1024 clusters */
#define MBR_SECTORS_PER_FAT     (3*((MBR_NUM_NEEDED_CLUSTERS + 1023)/1024))

/* Macro to help fill the two FAT tables with the empty sectors without
   adding a lot of test #ifs inside the sectors[] declaration below */
#if (MBR_SECTORS_PER_FAT == 1)
#   define EMPTY_FAT_SECTORS
#elif (MBR_SECTORS_PER_FAT == 2)
#   define EMPTY_FAT_SECTORS  {fat2,0},
#elif (MBR_SECTORS_PER_FAT == 3)
#   define EMPTY_FAT_SECTORS  {fat2,0},{fat2,0},
#elif (MBR_SECTORS_PER_FAT == 6)
#   define EMPTY_FAT_SECTORS  {fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},
#elif (MBR_SECTORS_PER_FAT == 9)
#   define EMPTY_FAT_SECTORS  {fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},
#elif (MBR_SECTORS_PER_FAT == 12)
#   define EMPTY_FAT_SECTORS  {fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},{fat2,0},
#else
#   error "Unsupported number of sectors per FAT table"
#endif

#define DIRENTS_PER_SECTOR  (MBR_BYTES_PER_SECTOR / sizeof(FatDirectoryEntry_t))

#define SECTORS_ROOT_IDX        (1 + mbr.num_fats*MBR_SECTORS_PER_FAT)   // 7
#define SECTORS_FIRST_FILE_IDX  (SECTORS_ROOT_IDX + 2)					// 9	
#define SECTORS_SYSTEM_VOLUME_INFORMATION (SECTORS_FIRST_FILE_IDX  + WANTED_SECTORS_PER_CLUSTER)
#define SECTORS_INDEXER_VOLUME_GUID       (SECTORS_SYSTEM_VOLUME_INFORMATION + WANTED_SECTORS_PER_CLUSTER)
#define SECTORS_MBED_HTML_IDX   (SECTORS_INDEXER_VOLUME_GUID + WANTED_SECTORS_PER_CLUSTER)
#define SECTORS_ERROR_FILE_IDX  (SECTORS_MBED_HTML_IDX + WANTED_SECTORS_PER_CLUSTER)

//---------------------------------------------------------------- VERIFICATION

/* Sanity check */
#if (MBR_NUM_NEEDED_CLUSTERS > 4084)
  /* Limited by 12 bit cluster addresses, i.e. 2^12 but only 0x002..0xff5 can be used */
#   error Too many needed clusters, increase WANTED_SECTORS_PER_CLUSTER
#endif

#if ((WANTED_SECTORS_PER_CLUSTER * MBR_BYTES_PER_SECTOR) > 32768)
#   error Cluster size too large, must be <= 32KB
#endif

//-------------------------------------------------------------------- TYPEDEFS

__packed typedef struct {
    uint8_t boot_sector[11];

    /* DOS 2.0 BPB - Bios Parameter Block, 13 bytes */
    uint16_t bytes_per_sector;
    uint8_t  sectors_per_cluster;
    uint16_t reserved_logical_sectors;
    uint8_t  num_fats;
    uint16_t max_root_dir_entries;
    uint16_t total_logical_sectors;            /* If num is too large for 16 bits, set to 0 and use big_sectors_on_drive */
    uint8_t  media_descriptor;
    uint16_t logical_sectors_per_fat;          /* Need 3 sectors/FAT for every 1024 clusters */

    /* DOS 3.31 BPB - Bios Parameter Block, 12 bytes */
    uint16_t physical_sectors_per_track;
    uint16_t heads;
    uint32_t hidden_sectors;
    uint32_t big_sectors_on_drive;             /* Use if total_logical_sectors=0, needed for large number of sectors */

    /* Extended BIOS Parameter Block, 26 bytes */
    uint8_t  physical_drive_number;
    uint8_t  not_used;
    uint8_t  boot_record_signature;
    uint32_t volume_id;
    char     volume_label[11];
    char     file_system_type[8];

    /* bootstrap data in bytes 62-509 */
    uint8_t  bootstrap[448];

    /* Mandatory value at bytes 510-511, must be 0xaa55 */
    uint16_t signature;
} mbr_t;

typedef enum {
    BIN_FILE,
    PAR_FILE,
    DOW_FILE,
    CRD_FILE,
    SPI_FILE,
    UNSUP_FILE, /* Valid extension, but not supported */
    SKIP_FILE,  /* Unknown extension, typically Long File Name entries */
} FILE_TYPE;

typedef struct {
    FILE_TYPE type;
    char extension[3];
    uint32_t flash_offset;
} FILE_TYPE_MAPPING;

__packed typedef struct FatDirectoryEntry {
    uint8_t filename[11];
    uint8_t attributes;
    uint8_t reserved;
    uint8_t creation_time_ms;
    uint16_t creation_time;
    uint16_t creation_date;
    uint16_t accessed_date;
    uint16_t first_cluster_high_16;
    uint16_t modification_time;
    uint16_t modification_date;
    uint16_t first_cluster_low_16;
    uint32_t filesize;
} FatDirectoryEntry_t;

//------------------------------------------------------------------------- END

uint8_t BlockBuf[512];
//uint8_t FlashBuf[0x440];

int Statistics_Offset=0;
int Statistics_CNT=0;

int FailStatistics_Offset=0;
int FailStatistics_CNT=0;
typedef struct sector {
    const uint8_t * sect;
    unsigned int length;
} SECTOR;

#define MEDIA_DESCRIPTOR        (0xF0)

static const mbr_t mbr = {
    { 							//boot_sector
        0xEB,0x3C, // x86 Jump
        0x90,      // NOP
        'M','S','W','I','N','4','.','1' // OEM Name in text
    },

    MBR_BYTES_PER_SECTOR, 	 	//bytes_per_sector         = 
    WANTED_SECTORS_PER_CLUSTER, //sectors_per_cluster      = 
    1,							//reserved_logical_sectors = 
    2,							//.num_fats                 = 
    32,							//.max_root_dir_entries     = 
    ((MBR_NUM_NEEDED_SECTORS > 32768) ? 0 : MBR_NUM_NEEDED_SECTORS), //.total_logical_sectors    = 
     MEDIA_DESCRIPTOR,			//.media_descriptor         =
     MBR_SECTORS_PER_FAT, 		//logical_sectors_per_fat  =/* Need 3 sectors/FAT for every 1024 clusters */

    1,							//.physical_sectors_per_track = 
    1,							//.heads = 
    0,							//.hidden_sectors = 
    ((MBR_NUM_NEEDED_SECTORS > 32768) ? MBR_NUM_NEEDED_SECTORS : 0),	//.big_sectors_on_drive = 

    0,							//.physical_drive_number = 
    0,							//.not_used = 
    0x29,						//.boot_record_signature = 
    0x27021974,					//.volume_id = 
    {'M','b','e','d',' ','U','S','B',' ',' ',' '},	//.volume_label = 
    {'F','A','T','1','2',' ',' ',' '},	//.file_system_type = 

    /* Executable boot code that starts the operating system */
    {							//.bootstrap = 
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
    },
    0xAA55,				//.signature = 
};

static uint8_t fat2[512] = {0};
/*
static const uint8_t fail[512] = {
    'F','A','I','L',' ',' ',' ',' ',                   // Filename
    'T','X','T',                                       // Filename extension
    0x20,                                              // File attributes
    0x18,0xB1,0x74,0x76,0x8E,0x41,0x8E,0x41,0x00,0x00, // Reserved
    0x8E,0x76,                                         // Time created or last updated
    0x8E,0x41,                                         // Date created or last updated
    0x06,0x00,                                         // Starting cluster number for file
    0x07,0x00,0x00,0x0                                 // File size in bytes
};*/

static  uint8_t fat1[512] = {
    MEDIA_DESCRIPTOR, 0xFF,
    0xFF, 0xFF,
    0xFF, 0xFF,
    0xFF, 0xFF,
    0xFF, 0x00,
    0x00, 0x00,
    0x00, 0x00,
    0x00, 0x00,
};

// first 16 of the max 32 (mbr.max_root_dir_entries) root dir entries
static  uint8_t root_dir1[512] = {
    // volume label "BOOTLOADER"
    'M', 'C', 'P', '1', '0', '1', '-', MAR_VER, '_', MIN_VER, DBG_VER, 0x28, 0x0, 0x0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x85, 0x75, 0x8E, 0x41, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,

    // Hidden files to keep mac happy
/*
    // .fseventsd (LFN + normal entry)  (folder, size 0, cluster 2)
    0x41, 0x2E, 0x0, 0x66, 0x0, 0x73, 0x0, 0x65, 0x0, 0x76, 0x0, 0xF, 0x0, 0xDA, 0x65, 0x0,
    0x6E, 0x0, 0x74, 0x0, 0x73, 0x0, 0x64, 0x0, 0x0, 0x0, 0x0, 0x0, 0xFF, 0xFF, 0xFF, 0xFF,

    0x46, 0x53, 0x45, 0x56, 0x45, 0x4E, 0x7E, 0x31, 0x20, 0x20, 0x20, 0x12, 0x0, 0x47, 0x7D, 0x75,
    0x8E, 0x41, 0x8E, 0x41, 0x0, 0x0, 0x7D, 0x75, 0x8E, 0x41, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0,

    // .metadata_never_index (LFN + LFN + normal entry)  (size 0, cluster 0)
    0x42, 0x65, 0x0, 0x72, 0x0, 0x5F, 0x0, 0x69, 0x0, 0x6E, 0x0, 0xF, 0x0, 0xA8, 0x64, 0x0,
    0x65, 0x0, 0x78, 0x0, 0x0, 0x0, 0xFF, 0xFF, 0xFF, 0xFF, 0x0, 0x0, 0xFF, 0xFF, 0xFF, 0xFF,

    0x1, 0x2E, 0x0, 0x6D, 0x0, 0x65, 0x0, 0x74, 0x0, 0x61, 0x0, 0xF, 0x0, 0xA8, 0x64, 0x0,
    0x61, 0x0, 0x74, 0x0, 0x61, 0x0, 0x5F, 0x0, 0x6E, 0x0, 0x0, 0x0, 0x65, 0x0, 0x76, 0x0,

    0x4D, 0x45, 0x54, 0x41, 0x44, 0x41, 0x7E, 0x31, 0x20, 0x20, 0x20, 0x22, 0x0, 0x32, 0x85, 0x75,
    0x8E, 0x41, 0x8E, 0x41, 0x0, 0x0, 0x85, 0x75, 0x8E, 0x41, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,

    // .Trashes (LFN + normal entry)  (size 0, cluster 0)
    0x41, 0x2E, 0x0, 0x54, 0x0, 0x72, 0x0, 0x61, 0x0, 0x73, 0x0, 0xF, 0x0, 0x25, 0x68, 0x0,
    0x65, 0x0, 0x73, 0x0, 0x0, 0x0, 0xFF, 0xFF, 0xFF, 0xFF, 0x0, 0x0, 0xFF, 0xFF, 0xFF, 0xFF,

    0x54, 0x52, 0x41, 0x53, 0x48, 0x45, 0x7E, 0x31, 0x20, 0x20, 0x20, 0x22, 0x0, 0x32, 0x85, 0x75,
    0x8E, 0x41, 0x8E, 0x41, 0x0, 0x0, 0x85, 0x75, 0x8E, 0x41, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
*/
    // Hidden files to keep windows 8.1 happy
    0x42, 0x20, 0x00, 0x49, 0x00, 0x6E, 0x00, 0x66, 0x00, 0x6F, 0x00, 0x0F, 0x00, 0x72, 0x72, 0x00, 
    0x6D, 0x00, 0x61, 0x00, 0x74, 0x00, 0x69, 0x00, 0x6F, 0x00, 0x00, 0x00, 0x6E, 0x00, 0x00, 0x00,
    
    0x01, 0x53, 0x00, 0x79, 0x00, 0x73, 0x00, 0x74, 0x00, 0x65, 0x00, 0x0F, 0x00, 0x72, 0x6D, 0x00, 
    0x20, 0x00, 0x56, 0x00, 0x6F, 0x00, 0x6C, 0x00, 0x75, 0x00, 0x00, 0x00, 0x6D, 0x00, 0x65, 0x00,
    
    0x53, 0x59, 0x53, 0x54, 0x45, 0x4D, 0x7E, 0x31, 0x20, 0x20, 0x20, 0x16, 0x00, 0xA5, 0x85, 0x8A, 
    0x73, 0x43, 0x73, 0x43, 0x00, 0x00, 0x86, 0x8A, 0x73, 0x43, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00,
    
     //config file (size 512, cluster 3)
		'C', 'O', 'N', 'F', 'I', 'G', 0x20, 0x20,'M', 'C', 'P', 0x20, 0x18, 0x74, 0xD6, 0x71,
    0x6A, 0x45, 0x6A, 0x45, 0x0, 0x0, 0xD7, 0x71, 0x6A, 0x45, 0x05, 0x00, 0x9A, 0x00, 0x00, 0x00,
};

// last 16 of the max 32 (mbr.max_root_dir_entries) root dir entries
static const uint8_t root_dir2[] = {0};

static const uint8_t sect5[] = {
    // .   (folder, size 0, cluster 2)
    0x2E, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x32, 0x0, 0x47, 0x7D, 0x75,
    0x8E, 0x41, 0x8E, 0x41, 0x0, 0x0, 0x88, 0x75, 0x8E, 0x41, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0,

    // ..   (folder, size 0, cluster 0)
    0x2E, 0x2E, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x10, 0x0, 0x47, 0x7D, 0x75,
    0x8E, 0x41, 0x8E, 0x41, 0x0, 0x0, 0x7D, 0x75, 0x8E, 0x41, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,

    // NO_LOG  (size 0, cluster 0)
    0x4E, 0x4F, 0x5F, 0x4C, 0x4F, 0x47, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x8, 0x32, 0x85, 0x75,
    0x8E, 0x41, 0x8E, 0x41, 0x0, 0x0, 0x85, 0x75, 0x8E, 0x41, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
};
#if 0
static const uint8_t sect6[] = {
    0x2E, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x10, 0x00, 0xA5, 0x85, 0x8A, 
    0x73, 0x43, 0x73, 0x43, 0x00, 0x00, 0x86, 0x8A, 0x73, 0x43, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 
    
    0x2E, 0x2E, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x10, 0x00, 0xA5, 0x85, 0x8A, 
    0x73, 0x43, 0x73, 0x43, 0x00, 0x00, 0x86, 0x8A, 0x73, 0x43, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    
    // IndexerVolumeGuid (size0, cluster 0)
    0x42, 0x47, 0x00, 0x75, 0x00, 0x69, 0x00, 0x64, 0x00, 0x00, 0x00, 0x0F, 0x00, 0xFF, 0xFF, 0xFF, 
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF,
    
    0x01, 0x49, 0x00, 0x6E, 0x00, 0x64, 0x00, 0x65, 0x00, 0x78, 0x00, 0x0F, 0x00, 0xFF, 0x65, 0x00, 
    0x72, 0x00, 0x56, 0x00, 0x6F, 0x00, 0x6C, 0x00, 0x75, 0x00, 0x00, 0x00, 0x6D, 0x00, 0x65, 0x00, 
    
    0x49, 0x4E, 0x44, 0x45, 0x58, 0x45, 0x7E, 0x31, 0x20, 0x20, 0x20, 0x20, 0x00, 0xA7, 0x85, 0x8A, 
    0x73, 0x43, 0x73, 0x43, 0x00, 0x00, 0x86, 0x8A, 0x73, 0x43, 0x04, 0x00, 0x4C, 0x00, 0x00, 0x00
};

static const uint8_t sect7[] = {
	0x7B, 0x00, 0x39, 0x00, 0x36, 0x00, 0x36, 0x00, 0x31, 0x00, 0x39, 0x00, 0x38, 0x00, 0x32, 0x00, 
	0x30, 0x00, 0x2D, 0x00, 0x37, 0x00, 0x37, 0x00, 0x44, 0x00, 0x31, 0x00, 0x2D, 0x00, 0x34, 0x00, 
	0x46, 0x00, 0x38, 0x00, 0x38, 0x00, 0x2D, 0x00, 0x38, 0x00, 0x46, 0x00, 0x35, 0x00, 0x33, 0x00, 
	0x2D, 0x00, 0x36, 0x00, 0x32, 0x00, 0x44, 0x00, 0x39, 0x00, 0x37, 0x00, 0x46, 0x00, 0x35, 0x00, 
	0x46, 0x00, 0x34, 0x00, 0x46, 0x00, 0x46, 0x00, 0x39, 0x00, 0x7D, 0x00, 0x00, 0x00, 0x00, 0x00
}; 
#endif
const unsigned char DefaultConfig[512] = {"#SAP\r\n\
SAP=default.sap\r\n\
;SAP_sec\r\n\
SAP_sec=100663297\r\n\
;Firm_sec\r\n\
Firm_sec=100663297\r\n\
;LED_mode\r\n\
LED_mode=0\r\n"};
SECTOR sectors[] = {
    /* Reserved Sectors: Master Boot Record */
    {(const uint8_t *)&mbr , 512},

    /* FAT Region: FAT1 */
    {fat1, sizeof(fat1)},   // fat1, sect1
    EMPTY_FAT_SECTORS

    /* FAT Region: FAT2 */
    {fat2, 0},              // fat2, sect1
    EMPTY_FAT_SECTORS

    /* Root Directory Region */
    {root_dir1, sizeof(root_dir1)}, // first 16 of the max 32 (mbr.max_root_dir_entries) root dir entries
    {root_dir2, sizeof(root_dir2)}, // last 16 of the max 32 (mbr.max_root_dir_entries) root dir entries

    /* Data Region */

    // Section for mac compatibility
    {sect5, sizeof(sect5)},

    // contains mbed.htm
    {(const uint8_t *)BlockBuf, 512},
};

static uint8_t listen_msc_isr = 1;
//static uint8_t reason = 0;

#define SWD_ERROR               0
#define BAD_EXTENSION_FILE      1
#define NOT_CONSECUTIVE_SECTORS 2
#define SWD_PORT_IN_USE         3
#define RESERVED_BITS           4
#define BAD_START_SECTOR        5
#define TIMEOUT                 6
#if 0
static uint8_t * reason_array[] = {
    "SWD ERROR",
    "BAD EXTENSION FILE",
    "NOT CONSECUTIVE SECTORS",
    "SWD PORT IN USE",
    "RESERVED BITS",
    "BAD START SECTOR",
    "TIMEOUT",
};
#endif
  extern int reset_request;
void MySoftReset()
{
#ifndef	TARGET_MK20DX
  extern int reset_request;
  
    reset_request = 1;
#else
	int i=0;
	for(i=0;i<5;i++)
	{
		PIT_Disable(i);
	}
	NVIC_SystemReset();
#endif	
}
int file_hide = 0;


int format_spi_flash()
{
	mtk_printf("SPI Flash Begin format \n\r", -1);	
	
	//spiflash_erase(0,(uint32_t)(FLASH_FILE_ADDR+4*4096 )/*spiflash_size()*/);  //reduce format time
	spiflash_erasechip();
	spiflash_write(fat1,FLASH_FAT1_ADDR,512);
	spiflash_write(root_dir1,FLASH_ROOT1_ADDR,512);
	spiflash_write((uint8_t *)DefaultConfig,(uint32_t)(FLASH_FILE_ADDR+3*4096 ),512);  
	
    mtk_printf("SPI Flash format finished\n\r", -1);	
	return 0;
	
}


int ajust_filename(char* filename,char* newfilename)
{
	char * pch = filename;
	char * pch_new =newfilename;
	int ii;
	for(ii=0;ii<12;ii++)
	{
		if(*pch == '.' && ii < 8)
		{	
			*pch_new = 0x20; 
			pch_new++;
			continue;
		}
		if(ii==8){ pch++;continue;}
		if(96 < *pch && *pch < 123) 
			*pch_new = *pch - 32;
		else	
			*pch_new = *pch;
		pch++;pch_new++;
	}
	return 0;
}

int GetFileFlashInfo(char* filename,  uint32_t* flash_off, uint32_t* psize)
{
	int ret = 0;  // -1 not find; 
	int Context =0;
	int *pcontext =&Context;
	extern int dbg_deep;
	FatDirectoryEntry_t * pDir;
    char newfilename[12]={0};
	uint8_t *buff = fat2;     // using fat2 as buffer

	ajust_filename(filename,newfilename);

    spiflash_read(buff,FLASH_ROOT1_ADDR,512);
    pDir = (void*)buff;
	while(*(char*)pDir != 0)  
	{
		if(strncmp((void*)pDir->filename,newfilename,11) == 0)  //find file 
		{	
			*pcontext = /* pDir->first_cluster_high_16 << 16 | */ pDir->first_cluster_low_16;
			*psize = (int)pDir->filesize;
			break;
		}
		pDir++;
	}
	if(*(char*)pDir == 0)
		return -1;
		
	if(*pcontext != 0)  //read file
	{
		//int fat_offset,cluster,cluster_next;
		int cluster;
		int sect_offset;
		int cluster_size =  MBR_BYTES_PER_SECTOR * WANTED_SECTORS_PER_CLUSTER;

		 cluster = *pcontext;
	  // caculate cluster to sect
		 sect_offset = (cluster-2) * cluster_size;  
	  // copy sect to buffer
		if(dbg_deep > 0)
		{   
			 mtk_printf("\n\r rd ", (uint32_t)(FLASH_FILE_ADDR+sect_offset));	
			 mtk_printf("\n\r rd ",(uint32_t)(FLASH_FILE_ADDR+sect_offset+1024));	  
		} 
#ifdef RAMDISK
		memcpy(buf,(void*)(START_APP_ADDRESS+sect_offset),cluster_size);
		*pLeft_size -= cluster_size;
#endif
#define MAX_READ_COUNT 256
#ifdef SPIFLASH

		*flash_off = (uint32_t)(FLASH_FILE_ADDR+sect_offset);

#endif
	}
	return ret;
}
int gLogBufLock=0;

void ClrLogBuf(void)
{
//	while(gLogBufLock);
	gLogBufLock=1;
	
	//memset(ResultLogBuf,0,sizeof(ResultLogBuf));
	gLogBufLock=0;
}
int WriteLogBuf(uint8_t *val,int offset,int size)
{
	if(offset+size>512)
		return -1;
//	while(gLogBufLock);
	gLogBufLock=1;
	//memcpy(ResultLogBuf+offset,val,size);
	gLogBufLock=0;
	return 0;
}

int WriteLineFeed(int *offset)
{
	uint8_t size=2;
	uint8_t LineChar[]={0xD,0xA};
	if(*offset+size>512)
		return -1;
//	while(gLogBufLock);
	gLogBufLock=1;
	//memcpy(ResultLogBuf+*offset,LineChar,size);
	gLogBufLock=0;
	*offset+=size;
	return 0;
}
int  EraseSpiFlash(FileBufType type)
{
			int Max_Buf_Size=0,Buf_StartAdd=0,StartAdd=0;	
		switch(type)
		{	
			case STATISTICS:
				Max_Buf_Size=STATIS_BUF_SIZE;
				Buf_StartAdd=STATIS_BUF_ADD;
				break;
			case FAIL_STATISTICS:
				Max_Buf_Size=STATIS_BUF_SIZE;
				Buf_StartAdd=STATIS_FAIL_BUF_ADD;
				break;
			}
		spiflash_erase(Buf_StartAdd&(0xFFFFFF000),Max_Buf_Size);//4K		
}
int WriteBufToSpiFlash(uint8_t *val,int offset,int size,FileBufType type)
{
		int Max_Buf_Size=0,Buf_StartAdd=0,StartAdd=0;

		switch(type)
		{
			case STATISTICS:
				Max_Buf_Size=STATIS_BUF_SIZE;
				Buf_StartAdd=STATIS_BUF_ADD+offset;
				StartAdd=STATIS_BUF_ADD;
				if(size+offset>Max_Buf_Size)
				{
					Buf_StartAdd=STATIS_BUF_ADD;
					gNeedErase = 1;
					Statistics_Offset = 0;
				}
				break;
			case FAIL_STATISTICS:
				Max_Buf_Size=STATIS_BUF_SIZE;
				Buf_StartAdd=STATIS_FAIL_BUF_ADD+offset;
				StartAdd=STATIS_FAIL_BUF_ADD;
				if(size+offset>Max_Buf_Size)
				{
					Buf_StartAdd=STATIS_FAIL_BUF_ADD;
					gNeedErase = 1;
					FailStatistics_Offset = 0;
				}
				break;
			case LOGBUF:
				Max_Buf_Size=LOG_BUF_SIZE;
				Buf_StartAdd=LOG_BUF_ADD+offset;
				StartAdd=LOG_BUF_ADD;
				if(size+offset>Max_Buf_Size)
					return -1;
				gNeedErase=1;
				break;
			case CONFIGFILE:
				Max_Buf_Size=CONFIG_BUF_SIZE;
				Buf_StartAdd=CONFIG_BUF_ADD+offset;
			StartAdd=CONFIG_BUF_ADD;
				if(size+offset>Max_Buf_Size)
					return -1;
			///	if(Buf_StartAdd!=CONFIG_BUF_ADD)//0 address is for size stored 
			///		gNeedErase=1;
			///	else
				gNeedErase=0;
				break;
			case QUARKROMFILE:
				Max_Buf_Size=QUARKROM_BUF_SIZE;
				Buf_StartAdd=QUARKROM_BUF_ADD+offset;
				StartAdd=QUARKROM_BUF_ADD;
				if(size+offset>Max_Buf_Size)
					return -1;
			///	if(Buf_StartAdd!=CONFIG_BUF_ADD)//0 address is for size stored 
			///		gNeedErase=1;
			///	else
				gNeedErase=1;
				break;				
			case SAPFILE:
				Max_Buf_Size=SAPFILE_BUF_SIZE;
				Buf_StartAdd=SAPFILE_BUF_ADD+offset;
				StartAdd=Buf_StartAdd;
				if(size+offset>Max_Buf_Size)
					return -1;
			///	if(((Buf_StartAdd%FLASH_SECTOR_SIZE)==0)||(Buf_StartAdd==(SAPFILE_BUF_ADD+4)))//0 address is for size stored 
			///		gNeedErase=1;
			///	else
			///		gNeedErase=0;
			///	if(Buf_StartAdd==0)
					gNeedErase=0;
				break;
			default:
				break;
		
		
		}

	//	while(gLogBufLock);
		gLogBufLock=1;
		if(gNeedErase/*&&!gHasErase*/){
				spiflash_erase(Buf_StartAdd&(0xFFFFFF000),size);//4K
			gNeedErase=0;
			//gHasErase=1;
		}
		spiflash_write(val,Buf_StartAdd,size);
		#if 0
		{
			uint8_t val2[256];
			spiflash_read(val2,Buf_StartAdd,size);
		}
		#endif
	//memcpy(ResultLogBuf+offset,val,size);
		gLogBufLock=0;
		return size;
}
int ReadBufFromSpiFlash(uint8_t *val,int offset,int size,FileBufType type)
{
		int Max_Buf_Size=0,Buf_StartAdd=0;
		int gNeedErase=0;
		switch(type)
		{
			case STATISTICS:
				Buf_StartAdd=STATIS_BUF_ADD+offset;
			
				Max_Buf_Size=STATIS_BUF_SIZE;
				if(size+offset>Max_Buf_Size)
				{
					size=Max_Buf_Size-offset;
				}
				break;
			case FAIL_STATISTICS:
				Max_Buf_Size=STATIS_BUF_SIZE;
				Buf_StartAdd=STATIS_FAIL_BUF_ADD+offset;
				if(size+offset>Max_Buf_Size)
				{
					size=Max_Buf_Size-offset;
				}
				break;
			case LOGBUF:
				Max_Buf_Size=LOG_BUF_SIZE;
				Buf_StartAdd=LOG_BUF_ADD+offset;
				if(size+offset>Max_Buf_Size)
					size=Max_Buf_Size-offset;
				break;
			case CONFIGFILE:
				Max_Buf_Size=CONFIG_BUF_SIZE;
				Buf_StartAdd=CONFIG_BUF_ADD+offset;
				if(size+offset>Max_Buf_Size)
					size=Max_Buf_Size-offset;
				break;
				case QUARKROMFILE:
					Max_Buf_Size=QUARKROM_BUF_SIZE;
					Buf_StartAdd=QUARKROM_BUF_ADD+offset;
					if(size+offset>Max_Buf_Size)
					return -1;
			///	if(Buf_StartAdd!=CONFIG_BUF_ADD)//0 address is for size stored 
			///		gNeedErase=1;
			///	else
				gNeedErase=1;
				break;	
			case SAPFILE:
				Max_Buf_Size=SAPFILE_BUF_SIZE;
				Buf_StartAdd=SAPFILE_BUF_ADD+offset;
				if(size+offset>Max_Buf_Size)
					size=Max_Buf_Size-offset;
				break;
			default:
				break;				
		}	
		gLogBufLock=1;
		spiflash_read(val,Buf_StartAdd,size);
		gLogBufLock=0;
		return size;
}
void GetCurrentSapFile(SFHandle *hFile)
{
		hFile->offset=SF_FILEINFO_OFF;		hFile->type=SAPFILE;
		//ReadBufFromSpiFlash((uint8_t *)&(hFile->size),SF_FILESIZE_START,SF_FILESIZE_SIZE,hFile->type);
		//if(hFile->size>(SAPFILE_BUF_SIZE-SF_FILEINFO_OFF))
		//	hFile->size=0;

		ReadBufFromSpiFlash((uint8_t *)&(hFile->time[0]),SF_TIME_START,SF_TIME_SIZE,hFile->type);
		
		ReadBufFromSpiFlash((uint8_t *)&(hFile->FileName[0]),SF_FILENAME_START,SF_FILENAME_SIZE,hFile->type);	
}
void *SW_SF_Open(char *FileName,FileMode mode)
{
		if(pHandle==NULL)	
			//pHandle=malloc(sizeof(SFHandle));
			pHandle = &SHandle[0];
		pHandle->size=0;
		pHandle->mode=mode;
		if(strstr(FileName,"MCP")||strstr(FileName,"mcp"))
			pHandle->type=CONFIGFILE;
		else if(strstr(FileName,"SAP")||strstr(FileName,"sap"))
			pHandle->type=SAPFILE;
		else
			pHandle->type=0xFF;
		switch(pHandle->type)
		{
				case CONFIGFILE:
				pHandle->offset=SF_FILEINFO_OFF;
				if(mode==MODE_R)
				{
					ReadBufFromSpiFlash((uint8_t *)&(pHandle->size),SF_FILESIZE_START,SF_FILESIZE_SIZE,pHandle->type);
					if(pHandle->size>(CONFIG_BUF_SIZE-SF_FILEINFO_OFF))
						pHandle->size=0;
					ReadBufFromSpiFlash((uint8_t *)&(pHandle->time[0]),SF_TIME_START,SF_TIME_SIZE,pHandle->type);
					
					ReadBufFromSpiFlash((uint8_t *)&(pHandle->FileName[0]),SF_FILENAME_START,SF_FILENAME_SIZE,pHandle->type);					
				}
				
				break;
			case SAPFILE:
				pHandle->offset=SF_FILEINFO_OFF;
				if(mode==MODE_R)
				{
					ReadBufFromSpiFlash((uint8_t *)&(pHandle->size),SF_FILESIZE_START,SF_FILESIZE_SIZE,pHandle->type);
					if(pHandle->size>(SAPFILE_BUF_SIZE-SF_FILEINFO_OFF))
						pHandle->size=0;
					
					ReadBufFromSpiFlash((uint8_t *)&(pHandle->time[0]),SF_TIME_START,SF_TIME_SIZE,pHandle->type);
					
					ReadBufFromSpiFlash((uint8_t *)&(pHandle->FileName[0]),SF_FILENAME_START,SF_FILENAME_SIZE,pHandle->type);	
				}
				break;
			default:
				break;				
		}	
		return (void *)pHandle;	

}
int SW_SF_Write(SFHandle *handle,uint8_t *dataBuffer,int length, void *byteswritten)
{
	int size=0;	
	size = WriteBufToSpiFlash(dataBuffer,handle->offset,length,handle->type);
	if(size!=-1)
			*((int *)byteswritten)=size;
	else
		*((int *)byteswritten)=0;
	handle->offset+=*((int *)byteswritten);
	//handle->size+=*((int *)byteswritten);
	return *((int *)byteswritten);
}
int SW_SF_Read(SFHandle *handle,uint8_t *dataBuffer,int length, void *bytesread)
{
	int size=0;	
	if(handle->size==0||handle->size<=(handle->offset-SF_FILEINFO_OFF))
		return 0;
	if((((handle->offset)+length)>handle->size+SF_FILEINFO_OFF))
		length=handle->size+SF_FILEINFO_OFF-handle->offset;
	
	size=ReadBufFromSpiFlash(dataBuffer,handle->offset,length,handle->type);
	if(size!=-1)
			*((int *)bytesread)=size;
	else
		*((int *)bytesread)=0;
	handle->offset+=*((int *)bytesread);
	return *((int *)bytesread);
}	
void SW_SF_Close(SFHandle *handle)
{
	uint32_t bytew;
	uint32_t size=handle->size;
	
	handle->offset=0;
	if(handle->mode==MODE_W)
	{
		if(handle->type==CONFIGFILE||handle->type==SAPFILE)
		{
			WriteBufToSpiFlash((uint8_t *)&size,SF_FILESIZE_START,SF_FILESIZE_SIZE,handle->type);//save the file length;
			WriteBufToSpiFlash((uint8_t *)&(handle->time[0]),SF_TIME_START,SF_TIME_SIZE,handle->type);//save the file length;			
			WriteBufToSpiFlash((uint8_t *)&(handle->FileName[0]),SF_FILENAME_START,SF_FILENAME_SIZE,handle->type);//save the file length;			
		}
	}	
	handle->type=0;
	gHasErase=0;
	if(pHandle)
	{
		//free(pHandle);
		memset(pHandle,0,sizeof(SFHandle));
		pHandle=NULL;

	}
}
#if 0
int ReadFile(char* filename, int *pcontext, char* buf, int* pLeft_size)
{
	int ret = -1;  // -1 not find; 0 finish/last sect; 10 not finish/not last sect
	extern int dbg_deep;
	FatDirectoryEntry_t * pDir = (void*)root_dir1;
    char newfilename[12]={0};

	if(g_fileOpened == 0)
		return -1;

	ajust_filename(filename,newfilename);
	
	if(*pcontext == 0) //firstly find fat entry
	{
		while(*(char*)pDir != 0)  
		{
			if(strncmp((void*)pDir->filename,newfilename,11) == 0)  //find file 
			{	
				*pcontext = /* pDir->first_cluster_high_16 << 16 | */ pDir->first_cluster_low_16;
				*pLeft_size = (int)pDir->filesize;
				break;
			}
			pDir++;
		}
		if(*(char*)pDir == 0)
			return -1;
	}
		
	if(*pcontext != 0)  //read file
	{
		int fat_offset,cluster,cluster_next;
		int sect_offset;
		int cluster_size =  MBR_BYTES_PER_SECTOR * WANTED_SECTORS_PER_CLUSTER;

		 cluster = *pcontext;
	  // caculate cluster to sect
		 sect_offset = (cluster-2) * cluster_size;  
	  // copy sect to buffer
		if(dbg_deep > 0)
		{   
			 mtk_printf("\n\r rd ", (uint32_t)(FLASH_FILE_ADDR+sect_offset));	
			 mtk_printf("\n\r rd ",(uint32_t)(FLASH_FILE_ADDR+sect_offset+1024));	  
		} 
#ifdef RAMDISK
		memcpy(buf,(void*)(START_APP_ADDRESS+sect_offset),cluster_size);
		*pLeft_size -= cluster_size;
#endif
#define MAX_READ_COUNT 256
#ifdef SPIFLASH
/*
		spiflash_read(buf,(uint32_t)(FLASH_FILE_ADDR+sect_offset),1024);
		*pLeft_size -= 1024;
		if(*pLeft_size > 0)
			spiflash_read(buf+1024,(uint32_t)(FLASH_FILE_ADDR+sect_offset+1024),1024);
		*pLeft_size -= 1024;
		if(*pLeft_size > 0)
			spiflash_read(buf+2048,(uint32_t)(FLASH_FILE_ADDR+sect_offset+2048),1024);
		*pLeft_size -= 1024;
		if(*pLeft_size > 0)
			spiflash_read(buf+3092,(uint32_t)(FLASH_FILE_ADDR+sect_offset+3092),1024);
		*pLeft_size -= 1024;	
*/		
#endif

	  // chek fat_ent, if 0xff0, finish return 0; else return 10
	     if (0xff7 < cluster  )
			 return 0;
		 else
			 ret = 10;	

		// get next fat value  000- not used, ff8~fff - bad cluster or end cluster
	    
		 if(cluster % 2)
		 {
			cluster--;
			fat_offset =  cluster  + cluster/2;	
			fat_offset++;
			cluster = (*(char*)(fat1+fat_offset+1)) << 4  | ((*(char*)(fat1+fat_offset))&0xF0) >> 4 ;
			
		 }else
		 {
			fat_offset = cluster + cluster/2;	
			cluster = (*(char*)(fat1+fat_offset+1) & 0x0F) << 8 | *(char*)(fat1+fat_offset)  ;
		 }
		 *pcontext = cluster;			 
	}
	return ret;
}


void debug_print_fat16()
{
	uint8_t buf[0x100];
	 int i;
	 
     mtk_printf("\n\r SECTORS_FIRST_FILE_IDX", SECTORS_FIRST_FILE_IDX);	
     mtk_printf("\n\r SECTORS_SYSTEM_VOLUME_INFORMATION", SECTORS_SYSTEM_VOLUME_INFORMATION);	
     mtk_printf("\n\r SECTORS_INDEXER_VOLUME_GUID", SECTORS_INDEXER_VOLUME_GUID);	
     mtk_printf("\n\r SECTORS_MBED_HTML_IDX", SECTORS_MBED_HTML_IDX);	
     mtk_printf("\n\r SECTORS_ROOT_IDX", SECTORS_ROOT_IDX);	
     mtk_printf("\n\r SECTORS_ERROR_FILE_IDX", SECTORS_ERROR_FILE_IDX);	
     mtk_printf("\n\r SECTORS_FIRST_FILE_IDX", SECTORS_FIRST_FILE_IDX);	


     mtk_printf("\n\rMBR", &mbr);
	 printf_memory(&mbr, 512);

     mtk_printf("\n\rfirst sect FAT1/ROOT1 in init table", -1);	
	 printf_memory((void*)(&fat1), 1024);


		{
			
				int add;
				
				
				mtk_printf("\n\rreal FAT1/ROOT1 in flash", -1);	
				add = FLASH_FAT1_ADDR;
			    for(i=0;i<0x2;i++)
				 {
					spiflash_read(buf,add+i*0x100,0x100);
					printf_memory(buf,0x100);
				 }
				add = FLASH_ROOT1_ADDR;
			    for(i=0;i<0x2;i++)
				 {
					spiflash_read(buf,add+i*0x100,0x100);
					printf_memory(buf,0x100);
				 }
		} 	
    // read fat list
	for(i=0;i<0x2;i++)
	{
		FatDirectoryEntry_t * pDir ;
		int fat_offset,cluster;//,fat_value,cluster_next;
		//int sect_offset;
		//int cluster_size =  MBR_BYTES_PER_SECTOR * WANTED_SECTORS_PER_CLUSTER;
		
		spiflash_read(buf,FLASH_ROOT1_ADDR+i*0x100,0x100);
		
		pDir = (void*)buf;
		
		while(*(char*)pDir != 0)  
		{
			mtk_printf(pDir->filename,-1);

	        cluster = /*pDir->first_cluster_high_16 << 16 |*/ pDir->first_cluster_low_16;		
			mtk_printf("fat",-1);			
			
			pDir++;
			
	  // get next fat value  000- not used, ff8~fff - bad cluster or end cluster
	     for(;;)
		 {
			 mtk_printf(" ",cluster);			
			 if(0xff8 <= cluster || cluster == 0)
				 break;
				 
			 if(cluster % 2)
			 {
				cluster--;
				fat_offset =  cluster  + cluster/2;	
				fat_offset++;
				cluster = ((*(char*)(fat1+fat_offset))&0xF0) >> 4 | (*(char*)(fat1+fat_offset+1)) << 4 ;
				
			 }else
			 {
				fat_offset = cluster + cluster/2;	
				cluster = *(char*)(fat1+fat_offset)  | (*(char*)(fat1+fat_offset+1) & 0x0F) << 8;
			 }
	     }	
			
			
		}
	}	
}
#endif
void disable_usb_irq(void){
    NVIC_DisableIRQ(USB_HP_CAN1_TX_IRQn);	
	 NVIC_DisableIRQ(USB_LP_CAN1_RX0_IRQn);	
}
void enable_usb_irq(void){
    NVIC_EnableIRQ(USB_HP_CAN1_TX_IRQn);
	NVIC_EnableIRQ(USB_LP_CAN1_RX0_IRQn);
}
void unencrypt(int encry_mode,int exclue_value,int size,uint8_t *pbuf)
{
	uint8_t tmp1,tmp2;
	uint8_t *buf=NULL;
	
	int i=0;
	if(pbuf==NULL)
		buf=(uint8_t *)BlockBuf;
	else
		buf=pbuf;
	switch(encry_mode)
	{
		case 0:break;
		case 1://swap the first bit and last bit
			for(i=0;i<size;i++)
			{
				tmp1=buf[i];
				tmp2=tmp1&0x7E;
				tmp1=tmp2|(tmp1>>7)|((tmp1&0x1)<<7);
				buf[i]=tmp1;
			}
			break;
		case 2://XOR
			for(i=0;i<size;i++)
			{
				tmp1=buf[i];
				tmp1^=(exclue_value&0xFF);
				buf[i]=tmp1;
			}
			break;
		case 3:	//NOT
			for(i=0;i<size;i++)
			{
				tmp1=buf[i];
				buf[i]=~tmp1;
			}
			break;
		default:break;
	}	
}
//
int binary_search_iter(uint16_t *array, const int low, const int high,uint16_t key)  
{  
 int mid_index = (low+high)/2;  
	
	if(low > high)  
 {  
  return -1;  
 }  
      
if(key != array[mid_index])  
 {  
  return binary_search_iter(array, mid_index+1, high, key);
 }else  if(key == array[mid_index]&&key !=array[mid_index-1])  
 {  
  return mid_index*2;  
 }    
 else if(key == array[mid_index]&&key ==array[mid_index-1])  
 {  
  return binary_search_iter(array,low, mid_index-1, key);  
 }  
} 
int ResetStatistics(void)
{
		EraseSpiFlash(FAIL_STATISTICS);
		EraseSpiFlash(STATISTICS);
		FailStatistics_CNT=0;
		FailStatistics_Offset=0;
		Statistics_CNT=0;
		Statistics_Offset=0;	
		WriteBufToSpiFlash((uint8_t *)&Statistics_CNT,Statistics_Offset,2,STATISTICS);		
		Statistics_Offset+=2;
		WriteBufToSpiFlash((uint8_t *)&FailStatistics_CNT,FailStatistics_Offset,2,FAIL_STATISTICS);
		FailStatistics_Offset+=2;
}
 int InitStatistics(void)
 {
		uint16_t val[32];
		int i,offset=0;
	 int rc;
	 while(1){
				ReadBufFromSpiFlash((uint8_t *)val,offset,sizeof(val),STATISTICS);
				if(val[0]==0x0000&&val[1]==0x0000)
				{
					ResetStatistics();
					continue;
				}
				Statistics_Offset=binary_search_iter(val,0,sizeof(val)/2,(uint16_t)0xFFFF);
				if(Statistics_Offset!=-1)
				{
					if(Statistics_Offset!=0)
						Statistics_CNT=val[Statistics_Offset/2-1];
					break;
				}

				offset+=sizeof(val);
		}
	 if(Statistics_Offset==0)
		 ResetStatistics();
	 offset=0;
	 while(1){
			ReadBufFromSpiFlash((uint8_t *)val,offset,sizeof(val),FAIL_STATISTICS);
			FailStatistics_Offset=binary_search_iter(val,0,sizeof(val)/2,(uint16_t)0xFFFF);
		 		if(val[0]==0x0000&&val[1]==0x0000)
				{
					ResetStatistics();
					continue;
				}
			if(FailStatistics_Offset!=-1)
			{
				if(FailStatistics_Offset!=0)
					FailStatistics_CNT=val[FailStatistics_Offset/2-1];
				break;
			}

			offset+=sizeof(val);			 
	 }			 
		
	if(Statistics_Offset==0)//no statistics
	{
		ResetStatistics();
	}
 }
 int EraseForFileBuf(int type,uint32_t filesize)
 {
			if(type==CONFIGFILE)
				spiflash_erase(CONFIG_BUF_ADD,filesize);
		else if(type==SAPFILE)
				spiflash_erase(SAPFILE_BUF_ADD,filesize);
 }
 