#include <string.h>
#include "buzzer.h"
//nclude "FTM.h"


//#define FTM_PWM_init(a,b,c,d) do{}while(0)
//#define FTM_PWM_Stop(a,b) do{}while(0)


buzzer_node_s btnbeep[] =
{
/*   freq,					repeat,		on,		off,	loop */
	{BUZZER_DEFAULT_FREQ, 	0, 			10, 	0,	 	0},
	0
};

buzzer_node_s opfinish[] =
{
/*   freq,			repeat,		on,		off,	loop */
	{BUZZER_LA, 	0, 			10, 	0,	 	0},
	{BUZZER_SI, 	0, 			10, 	0,	 	0},
	{BUZZER_DO2, 	0, 			10, 	0,	 	0},
	{BUZZER_LA, 	0, 			0, 		10,	 	0},
	{BUZZER_LA, 	0, 			10, 	0,	 	0},
	{BUZZER_SI, 	0, 			10, 	0,	 	0},
	{BUZZER_DO2, 	0, 			10, 	0,	 	0},
/*
	{BUZZER_DO2, 	0, 			10, 	0,	 	0},
	{BUZZER_MI2, 	0, 			10, 	0,	 	0},
	{BUZZER_SOL2, 	0, 			10, 	0,	 	0},
	{BUZZER_DO2, 	0, 			0, 		10,	 	0},
	{BUZZER_DO2, 	0, 			10, 	0,	 	0},
	{BUZZER_MI2, 	0, 			10, 	0,	 	0},
	{BUZZER_SOL2, 	0, 			10, 	0,	 	0},
*/
	0
};
#if 1
// Symphony V
buzzer_node_s starttone [] =
{
	
	{BUZZER_LA, 	0, 			10, 	2,	 	0},
	//{BUZZER_LA, 	0, 			10, 	2,	 	0},
	//{BUZZER_LA, 	0, 			10, 	2,	 	0},
	//{BUZZER_FA, 	0, 			60, 	2,	 	0},
/*
	{BUZZER_SOL, 	0, 			15, 	2,	 	0},
	{BUZZER_SOL, 	0, 			15, 	2,	 	0},
	{BUZZER_SOL, 	0, 			15, 	2,	 	0},
	{BUZZER_MI, 	0, 			90, 	2,	 	0},
*/
	0
};
#else
// to Alice
buzzer_node_s starttone [] =
{
	{BUZZER_MI2, 	0, 			18, 	0,	 	0},
	{BUZZER_RE2_H, 	0, 		18, 	0,	 	0},
	{BUZZER_MI2, 	0, 			18, 	0,	 	0},
	{BUZZER_RE2_H, 	0, 			18, 	0,	 	0},
	{BUZZER_MI2, 	0, 			18, 	0,	 	0},
//	{BUZZER_SI, 	0, 			18, 	0,	 	0},
//	{BUZZER_DO2_H, 	0, 			12/*18*/, 	0,	 	0},
//	{BUZZER_DO2, 	0, 			12/*18*/, 	0,	 	0},
//	{BUZZER_LA, 	0, 			24/*36*/, 	0,	 	0},
	0
};
#endif

buzzer_node_s opalarm [] =
{
	{BUZZER_DEFAULT_FREQ/*BUZZER_SOL2*/, 	0, 			15, 	5,	 	0},
	{BUZZER_DEFAULT_FREQ/*BUZZER_SOL2*/, 	0, 			45, 	5,	 	0},
	{BUZZER_DEFAULT_FREQ/*BUZZER_SOL2*/, 	0, 			15,		2,	 	0},
	{BUZZER_DEFAULT_FREQ/*BUZZER_SOL2*/, 	0, 			15,		2,	 	0},
	0
};

buzzer_node_s *buzzer_type[]=
{
	btnbeep,	// BUZZER_TYPE_BTNCLICK
	opfinish,	// BUZZER_TYPE_OP_FINISH
	opalarm,	// BUZZER_TYPE_ALARM
	opalarm,	// BUZZER_TYPE_ERROR
	starttone	// BUZZER_TYPE_START
};

buzzer_train_node_s buzzer_ongoing;
//uint8_t	buzzer_go_idx = 0;

static int old_freq=0;
static int old_duty=0;
void SetBuzzer(int freq, int duty, int state){
	if (state==0)
	{
		//GPIO_LOW(GPIOA,GPIO_Pin_8);

		if(!freq)freq = BUZZER_DEFAULT_FREQ;
	if(old_duty!=duty||old_freq!=freq)
		FTM_PWM_init(TIMxx,TIM_CHANNEL_1,freq,duty);
		old_duty=duty;
		old_freq=freq;
//		PWMOutEnable(freq, duty, ENABLE);
//		GPIO_LOW(GPIOB,GPIO_Pin_4);	
	}
	else
	{
		//GPIO_HIGH(GPIOA,GPIO_Pin_8);
//		PWMOutEnable(0, duty, DISABLE);
//		GPIO_HIGH(GPIOB,GPIO_Pin_4);
		FTM_PWM_Stop(TIMxx,TIM_CHANNEL_1);
		old_duty=0;
		old_freq=0;
	}

}

/* type: BUZZER_TYPE_BTNCLICK */
int Buzzer_on(uint8_t type)
{
	if (/*g_buzzerSetting_flag==*/1)
	{
		if(type>BUZZER_TYPE_START)
			return -1;

		if(!buzzer_type[type])
			return -2;

		if(buzzer_ongoing.type && buzzer_type[type]<buzzer_ongoing.type)
			return -3;

		memcpy(&buzzer_ongoing.ctrl, buzzer_type[type], sizeof(buzzer_node_s));
		buzzer_ongoing.type = buzzer_type[type];

		return 0;
	}
	else 
		return 0;
}

void Buzzer_process(uint32_t s10ms)
{
	int i = 0;
	//buzzer_train_node_s *curr;
	buzzer_node_s	*node;

	//curr = &buzzer_ongoing[buzzer_go_idx];

	if(!buzzer_ongoing.type)
		return;

	node = &buzzer_ongoing.ctrl;
	for(i = 0;i < s10ms;i++)
	{	
		if(node->on)
		{
			node->on--;
			SetBuzzer(node->freq, 50, 0);
		}
		else if(node->off)
		{
			node->off--;
			SetBuzzer(node->freq, 50, 1);
		}
		else if(node->repeat)
		{
			node->on = buzzer_ongoing.type->on;
			node->off = buzzer_ongoing.type->off;

			node->repeat--;
		}
		else if(node->loop)
		{
			node->on = buzzer_ongoing.type->on;
			node->off = buzzer_ongoing.type->off;
			node->repeat = buzzer_ongoing.type->repeat;

			node->loop--;
		}
		else
		{
			buzzer_ongoing.type++;

			if(!buzzer_ongoing.type->freq)
			{
				SetBuzzer(node->freq, 50, 1);
				buzzer_ongoing.type = NULL;
			}
			else
			{
				memcpy(&buzzer_ongoing.ctrl, buzzer_ongoing.type, sizeof(buzzer_node_s));
			}
		}
	 }
}
//static int buzz_blink_id;
void Buzzer_Start(uint8_t type)
{	
	Buzzer_on(type);
	Buzzer_process(100);
		//HAL_GPIO_WritePin(BUZZER_GPIO,BUZZER_BIT,1);
		//HAL_Delay(50);
		//HAL_GPIO_WritePin(BUZZER_GPIO,BUZZER_BIT,0);	

}
void Buzzer_Stop(void)
{	
//	HAL_GPIO_WritePin(BUZZER_GPIO,BUZZER_BIT,0);	
	return;
}
