/**
  ******************************************************************************
  * @file    USB_Device/CDC_Standalone/Src/stm32f1xx_hal_msp.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    17-December-2014
  * @brief   HAL MSP module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/** @addtogroup USBD_USER
* @{
*/

/** @defgroup USBD_USR_MAIN
  * @brief This file is the CDC application main file
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void HAL_MspInit(void)
{
  /* USER CODE BEGIN MspInit 0 */

  /* USER CODE END MspInit 0 */

  __HAL_RCC_AFIO_CLK_ENABLE();

  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* System interrupt init*/
  /* MemoryManagement_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
  /* BusFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
  /* UsageFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
  /* SVCall_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SVCall_IRQn, 0, 0);
  /* DebugMonitor_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
  /* PendSV_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(PendSV_IRQn, 0, 0);
  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

    /**DISABLE: JTAG-DP Disabled and SW-DP Disabled 
    */
 // __HAL_AFIO_REMAP_SWJ_DISABLE();

  /* USER CODE BEGIN MspInit 1 */

  /* USER CODE END MspInit 1 */
}
/**
  * @brief UART MSP Initialization
  *        This function configures the hardware resources used in this example:
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration
  *           - DMA configuration for transmission request by peripheral
  *           - NVIC configuration for DMA interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
  static DMA_HandleTypeDef hdma_tx;
  GPIO_InitTypeDef  GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO clock */
  USARTx_TX_GPIO_CLK_ENABLE();
  USARTx_RX_GPIO_CLK_ENABLE();

  /* Enable USARTx clock */
  USARTx_CLK_ENABLE();

  /* Enable DMA clock */
//  DMAx_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = USARTx_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;

  HAL_GPIO_Init(USARTx_TX_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = USARTx_RX_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  HAL_GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStruct);
  
  /*##-3- Configure the NVIC for UART ########################################*/   
  HAL_NVIC_SetPriority(USARTx_IRQn, 4, 0);
  HAL_NVIC_EnableIRQ(USARTx_IRQn);
#if 0 
  /*##-4- Configure the DMA channels ##########################################*/
  /* Configure the DMA handler for Transmission process */
  hdma_tx.Instance                 = USARTx_TX_DMA_STREAM;
  hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
  hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
  hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hdma_tx.Init.Mode                = DMA_NORMAL;
  hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;

  HAL_DMA_Init(&hdma_tx);
  
  /* Associate the initialized DMA handle to the UART handle */
  __HAL_LINKDMA(huart, hdmatx, hdma_tx);
  
  /*##-5- Configure the NVIC for DMA #########################################*/
  /* NVIC configuration for DMA transfer complete interrupt (USARTx_TX) */
  HAL_NVIC_SetPriority(USARTx_DMA_TX_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(USARTx_DMA_TX_IRQn);
  
  /*##-6- Enable TIM peripherals Clock #######################################*/
  TIMx_CLK_ENABLE();
  
  /*##-7- Configure the NVIC for TIMx ########################################*/
  /* Set Interrupt Group Priority */ 
  HAL_NVIC_SetPriority(TIMx_IRQn, 5, 0);
  
  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIMx_IRQn);
#endif
}

/**
  * @brief UART MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO, DMA and NVIC configuration to their default state
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
  /*##-1- Reset peripherals ##################################################*/
  USARTx_FORCE_RESET();
  USARTx_RELEASE_RESET();

  /*##-2- Disable peripherals and GPIO Clocks #################################*/
 // /* Configure UART Tx as alternate function  */
 // HAL_GPIO_DeInit(USARTx_TX_GPIO_PORT, USARTx_TX_PIN);
  ///* Configure UART Rx as alternate function  */
  //HAL_GPIO_DeInit(USARTx_RX_GPIO_PORT, USARTx_RX_PIN);
	 GPIO_InitTypeDef GPIO_InitStructure;
	  GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStructure.Pull	=	GPIO_PULLDOWN;
	GPIO_InitStructure.Pin =  USARTx_TX_PIN | USARTx_RX_PIN ;
  HAL_GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStructure);		
  /*##-3- Disable the NVIC for UART ##########################################*/
  HAL_NVIC_DisableIRQ(USARTx_IRQn);
  
  /*##-4- Disable the NVIC for DMA ###########################################*/
  HAL_NVIC_DisableIRQ(USARTx_DMA_TX_IRQn);
  
  /*##-5- Reset TIM peripheral ###############################################*/
 // TIMx_FORCE_RESET();
  //TIMx_RELEASE_RESET();
}
/**
  * @brief ADC MSP initialization
  *        This function configures the hardware resources used in this example:
  *          - Enable clock of ADC peripheral
  *          - Configure the GPIO associated to the peripheral channels
  *          - Configure the DMA associated to the peripheral
  *          - Configure the NVIC associated to the peripheral interruptions
  * @param hadc: ADC handle pointer
  * @retval None
  */
void HAL_ADC_MspInit(ADC_HandleTypeDef *hadc)
{
  GPIO_InitTypeDef          GPIO_InitStruct;
  static DMA_HandleTypeDef  DmaHandle;
  RCC_PeriphCLKInitTypeDef  PeriphClkInit;
  if(hadc->Instance==ADCx) 
	{
		/*##-1- Enable peripherals and GPIO Clocks #################################*/
		/* Enable clock of GPIO associated to the peripheral channels */
		ADCx_CHANNELa_GPIO_CLK_ENABLE();
		
		/* Enable clock of ADCx peripheral */
		ADCx_CLK_ENABLE();

		/* Configure ADCx clock prescaler */
		/* Caution: On STM32F1, ADC clock frequency max is 14MHz (refer to device   */
		/*          datasheet).                                                     */
		/*          Therefore, ADC clock prescaler must be configured in function   */
		/*          of ADC clock source frequency to remain below this maximum      */
		/*          frequency.                                                      */
		PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
		PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
		HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

		/* Enable clock of DMA associated to the peripheral */
		ADCx_DMA_CLK_ENABLE();
		
		/*##-2- Configure peripheral GPIO ##########################################*/
		/* Configure GPIO pin of the selected ADC channel */
		GPIO_InitStruct.Pin = ADCx_CHANNELa_PIN;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(ADCx_CHANNELa_GPIO_PORT, &GPIO_InitStruct);
		
		/*##-3- Configure the DMA ##################################################*/
		/* Configure DMA parameters */
		DmaHandle.Instance = ADCx_DMA;

		DmaHandle.Init.Direction           = DMA_PERIPH_TO_MEMORY;
		DmaHandle.Init.PeriphInc           = DMA_PINC_DISABLE;
		DmaHandle.Init.MemInc              = DMA_MINC_ENABLE;
		DmaHandle.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;   /* Transfer from ADC by half-word to match with ADC configuration: ADC resolution 10 or 12 bits */
		DmaHandle.Init.MemDataAlignment    = DMA_MDATAALIGN_HALFWORD;   /* Transfer to memory by half-word to match with buffer variable type: half-word */
		DmaHandle.Init.Mode                = DMA_CIRCULAR;              /* DMA in circular mode to match with ADC configuration: DMA continuous requests */
		DmaHandle.Init.Priority            = DMA_PRIORITY_HIGH;
		
		/* Deinitialize  & Initialize the DMA for new transfer */
		HAL_DMA_DeInit(&DmaHandle);
		HAL_DMA_Init(&DmaHandle);

		/* Associate the initialized DMA handle to the ADC handle */
		__HAL_LINKDMA(hadc, DMA_Handle, DmaHandle);
		
		/*##-4- Configure the NVIC #################################################*/

		/* NVIC configuration for DMA interrupt (transfer completion or error) */
		/* Priority: high-priority */
		HAL_NVIC_SetPriority(ADCx_DMA_IRQn, 1, 0);
		HAL_NVIC_EnableIRQ(ADCx_DMA_IRQn);
		

		/* NVIC configuration for ADC interrupt */
		/* Priority: high-priority */
		HAL_NVIC_SetPriority(ADCx_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(ADCx_IRQn);
	}else if(hadc->Instance==ADCy)
	{
		/*##-1- Enable peripherals and GPIO Clocks #################################*/
		/* Enable clock of GPIO associated to the peripheral channels */
		ADCy_CHANNELa_GPIO_CLK_ENABLE();
		
		/* Enable clock of ADCx peripheral */
		ADCy_CLK_ENABLE();

		/* Configure ADCx clock prescaler */
		/* Caution: On STM32F1, ADC clock frequency max is 14MHz (refer to device   */
		/*          datasheet).                                                     */
		/*          Therefore, ADC clock prescaler must be configured in function   */
		/*          of ADC clock source frequency to remain below this maximum      */
		/*          frequency.                                                      */
		PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
		PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
		HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

		/* Enable clock of DMA associated to the peripheral */
		//ADCy_DMA_CLK_ENABLE();
		
		/*##-2- Configure peripheral GPIO ##########################################*/
		/* Configure GPIO pin of the selected ADC channel */
		GPIO_InitStruct.Pin = ADCy_CHANNELa_PIN;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(ADCy_CHANNELa_GPIO_PORT, &GPIO_InitStruct);
		
		/*##-3- Configure the DMA ##################################################*/
		/* Configure DMA parameters */
		//DmaHandle.Instance = ADCy_DMA;

		//DmaHandle.Init.Direction           = DMA_PERIPH_TO_MEMORY;
		//DmaHandle.Init.PeriphInc           = DMA_PINC_DISABLE;
		//DmaHandle.Init.MemInc              = DMA_MINC_ENABLE;
		//DmaHandle.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;   /* Transfer from ADC by half-word to match with ADC configuration: ADC resolution 10 or 12 bits */
		//DmaHandle.Init.MemDataAlignment    = DMA_MDATAALIGN_HALFWORD;   /* Transfer to memory by half-word to match with buffer variable type: half-word */
		//DmaHandle.Init.Mode                = DMA_CIRCULAR;              /* DMA in circular mode to match with ADC configuration: DMA continuous requests */
		//DmaHandle.Init.Priority            = DMA_PRIORITY_HIGH;
		
		/* Deinitialize  & Initialize the DMA for new transfer */
		//HAL_DMA_DeInit(&DmaHandle);
		//HAL_DMA_Init(&DmaHandle);

		/* Associate the initialized DMA handle to the ADC handle */
		//__HAL_LINKDMA(hadc, DMA_Handle, DmaHandle);
		
		/*##-4- Configure the NVIC #################################################*/

		/* NVIC configuration for DMA interrupt (transfer completion or error) */
		/* Priority: high-priority */
		//HAL_NVIC_SetPriority(ADCx_DMA_IRQn, 1, 0);
		//HAL_NVIC_EnableIRQ(ADCx_DMA_IRQn);
		

		/* NVIC configuration for ADC interrupt */
		/* Priority: high-priority */
		HAL_NVIC_SetPriority(ADCx_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(ADCx_IRQn);		
	
	}
}

/**
  * @brief ADC MSP de-initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable clock of ADC peripheral
  *          - Revert GPIO associated to the peripheral channels to their default state
  *          - Revert DMA associated to the peripheral to its default state
  *          - Revert NVIC associated to the peripheral interruptions to its default state
  * @param hadc: ADC handle pointer
  * @retval None
  */
void HAL_ADC_MspDeInit(ADC_HandleTypeDef *hadc)
{
	if(hadc->Instance==ADCx) 
	{
		/*##-1- Reset peripherals ##################################################*/
		ADCx_FORCE_RESET();
		ADCx_RELEASE_RESET();

		/*##-2- Disable peripherals and GPIO Clocks ################################*/
		/* De-initialize GPIO pin of the selected ADC channel */
		HAL_GPIO_DeInit(ADCx_CHANNELa_GPIO_PORT, ADCx_CHANNELa_PIN);

		/*##-3- Disable the DMA ####################################################*/
		/* De-Initialize the DMA associated to the peripheral */
		if(hadc->DMA_Handle != NULL)
		{
			HAL_DMA_DeInit(hadc->DMA_Handle);
		}

		/*##-4- Disable the NVIC ###################################################*/
		/* Disable the NVIC configuration for DMA interrupt */
		HAL_NVIC_DisableIRQ(ADCx_DMA_IRQn);
		
		/* Disable the NVIC configuration for ADC interrupt */
		HAL_NVIC_DisableIRQ(ADCx_IRQn);
	}else if(hadc->Instance==ADCy)
	{
		/*##-1- Reset peripherals ##################################################*/
		ADCy_FORCE_RESET();
		ADCy_RELEASE_RESET();

		/*##-2- Disable peripherals and GPIO Clocks ################################*/
		/* De-initialize GPIO pin of the selected ADC channel */
		HAL_GPIO_DeInit(ADCy_CHANNELa_GPIO_PORT, ADCx_CHANNELa_PIN);

		/*##-3- Disable the DMA ####################################################*/
		/* De-Initialize the DMA associated to the peripheral */
		if(hadc->DMA_Handle != NULL)
		{
			HAL_DMA_DeInit(hadc->DMA_Handle);
		}

		/*##-4- Disable the NVIC ###################################################*/
		/* Disable the NVIC configuration for DMA interrupt */
		//HAL_NVIC_DisableIRQ(ADCy_DMA_IRQn);
		
		/* Disable the NVIC configuration for ADC interrupt */
		HAL_NVIC_DisableIRQ(ADCy_IRQn);	

	}
}


/**
  * @brief RTC MSP Initialization 
  *        This function configures the hardware resources used in this example
  * @param hrtc: RTC handle pointer
  * 
  * @note  Care must be taken when HAL_RCCEx_PeriphCLKConfig() is used to select 
  *        the RTC clock source; in this case the Backup domain will be reset in  
  *        order to modify the RTC Clock source, as consequence RTC registers (including 
  *        the backup registers) and RCC_BDCR register are set to their reset values.
  *             
  * @retval None
  */
void HAL_RTC_MspInit(RTC_HandleTypeDef *hrtc)
{
  RCC_OscInitTypeDef        RCC_OscInitStruct;
  RCC_PeriphCLKInitTypeDef  PeriphClkInitStruct;

  /*##-1- Enables the PWR Clock and Enables access to the backup domain ###################################*/
  /* To change the source clock of the RTC feature (LSE, LSI), You have to:
     - Enable the power clock using __HAL_RCC_PWR_CLK_ENABLE()
     - Enable write access using HAL_PWR_EnableBkUpAccess() function before to 
       configure the RTC clock source (to be done once after reset).
     - Reset the Back up Domain using __HAL_RCC_BACKUPRESET_FORCE() and 
       __HAL_RCC_BACKUPRESET_RELEASE().
     - Configure the needed RTc clock source */
  __HAL_RCC_PWR_CLK_ENABLE();
  HAL_PWR_EnableBkUpAccess();
  
  /* Enable BKP CLK for backup registers */
  __HAL_RCC_BKP_CLK_ENABLE();
  
  /*##-2- Configue LSE as RTC clock soucre ###################################*/
//  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI;// RCC_OSCILLATORTYPE_LSE;
 // RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
//  RCC_OscInitStruct.LSEState = RCC_LSE_OFF;//RCC_LSE_ON;
 // if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
 // { 
 //   Error_Handler();
 // }
  
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;//RCC_RTCCLKSOURCE_LSE;
  if(HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  { 
    Error_Handler();
  }
  
  /*##-3- Enable RTC peripheral Clocks #######################################*/
  /* Enable RTC Clock */
  __HAL_RCC_RTC_ENABLE();
}

/**
  * @brief RTC MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  * @param hrtc: RTC handle pointer
  * @retval None
  */
void HAL_RTC_MspDeInit(RTC_HandleTypeDef *hrtc)
{
  /*##-1- Reset peripherals ##################################################*/
  __HAL_RCC_RTC_DISABLE();

  /*##-2- Disables the PWR Clock and Disables access to the backup domain ###################################*/
  HAL_PWR_DisableBkUpAccess();
  __HAL_RCC_PWR_CLK_DISABLE();
  
  /* Disable BKP CLK for backup registers */
  __HAL_RCC_BKP_CLK_DISABLE();
}


void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hdac->Instance==DAC)
  {
  /* USER CODE BEGIN DAC_MspInit 0 */

  /* USER CODE END DAC_MspInit 0 */
    /* Peripheral clock enable */
    __DAC_CLK_ENABLE();
  
    /**DAC GPIO Configuration    
    PA4     ------> DAC_OUT1 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_4;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN DAC_MspInit 1 */

  /* USER CODE END DAC_MspInit 1 */
  }

}

void HAL_DAC_MspDeInit(DAC_HandleTypeDef* hdac)
{

  if(hdac->Instance==DAC)
  {
  /* USER CODE BEGIN DAC_MspDeInit 0 */

  /* USER CODE END DAC_MspDeInit 0 */
    /* Peripheral clock disable */
    __DAC_CLK_DISABLE();
  
    /**DAC GPIO Configuration    
    PA4     ------> DAC_OUT1 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_4);

  /* USER CODE BEGIN DAC_MspDeInit 1 */

  /* USER CODE END DAC_MspDeInit 1 */
  }

}

/**
  * @brief I2C MSP Initialization 
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - DMA configuration for transmission request by peripheral 
  *           - NVIC configuration for DMA interrupt request enable
  * @param hi2c: I2C handle pointer
  * @retval None
  */
void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  I2Cx_SCL_GPIO_CLK_ENABLE();
  I2Cx_SDA_GPIO_CLK_ENABLE();
  /* Enable I2Cx clock */
  I2Cx_CLK_ENABLE(); 

  /*##-2- Configure peripheral GPIO ##########################################*/  
  /* I2C TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = I2Cx_SCL_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
  HAL_GPIO_Init(I2Cx_SCL_GPIO_PORT, &GPIO_InitStruct);
    
  /* I2C RX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = I2Cx_SDA_PIN;
  HAL_GPIO_Init(I2Cx_SDA_GPIO_PORT, &GPIO_InitStruct);
    
  /*##-3- Configure the NVIC for I2C ########################################*/   
  /* NVIC for I2Cx */
  HAL_NVIC_SetPriority(I2Cx_ER_IRQn, 0, 1);
  HAL_NVIC_EnableIRQ(I2Cx_ER_IRQn);
  HAL_NVIC_SetPriority(I2Cx_EV_IRQn, 0, 2);
  HAL_NVIC_EnableIRQ(I2Cx_EV_IRQn);
}
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
