#include "common.h"


/*

			1 	2 	3 	4 	5 	6 	7 
low		220 247 277 294 330 370 416 
mid		low*2
high  mid*2
	
			1 		1/2 	1/4 	1/8 	1/16
			4 		2 		1			1/2		1/4
			4800 2400 	1200 	600 	300 (ms)
*/
#define BUZZER_TYPE_BTNCLICK	0
#define BUZZER_TYPE_OP_FINISH	1
#define BUZZER_TYPE_ALARM		2
#define BUZZER_TYPE_ERROR		3
#define BUZZER_TYPE_START		4

#define BUZZER_SOL_1	1568 // 784
#define BUZZER_LA_1	1760 // 880
#define BUZZER_SI_1	1976 // 988
#define BUZZER_DO		2096 // 1048
#define BUZZER_RE		2352 // 1176
#define BUZZER_MI		2640 // 1320
#define BUZZER_FA		2792 // 1396
#define BUZZER_SOL		3136 // 1568
#define BUZZER_LA		3520 // 1760
#define BUZZER_SI		3952 // 1976
#define BUZZER_DO2		4192 // 2096
#define BUZZER_DO2_H	4442 // 2221
#define BUZZER_RE2		4704 // 2352
#define BUZZER_RE2_H	4982 // 2491
#define BUZZER_MI2		(5280) // 2640
#define BUZZER_FA2		(5584) // 2792
#define BUZZER_SOL2	(6272) // 3136
#define BUZZER_LA2		(7040) // 3520
#define BUZZER_SI2		(7904) // 3952

#define BUZZER_DEFAULT_FREQ		BUZZER_MI2 //BUZZER_FA
#define BUZZER_ARRAY_MAX			4
extern int g_buzzerSetting_flag;
/* */
typedef struct
{
	uint16_t freq;  /* 0 for stop*/
	uint8_t repeat; /* unit: times */
	uint8_t on;		/* unit: 10ms, 0xff: continue */

	uint8_t off;	/* unit: 10ms, oxff: repeat one finish */
	uint8_t loop;	/* unit: times */

}buzzer_node_s;

typedef struct
{
	buzzer_node_s *type;
	buzzer_node_s ctrl;

}buzzer_train_node_s;

void Buzzer_init(void);
void SetBuzzer(int freq, int duty, int state);
void Buzzer_process(uint32_t s10ms);

/* type: BUZZER_TYPE_BTNCLICK */
int Buzzer_on(uint8_t type);
void Buzzer_Start(uint8_t type);
void Buzzer_Stop(void);
