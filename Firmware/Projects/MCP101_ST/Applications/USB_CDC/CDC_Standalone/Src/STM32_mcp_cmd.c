
#include "common.h"
#include "serialprog.h" 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define NULL 0

//extern int mtk_printf(char *str, int value);
//extern void printf_memory( void *str, int len);

int dbg_deep = 0;

void usb_dbg_print()
{


}

void help_command()
{
	


}


//static char buf[4096]; 
static char command[30];
static int index_ch = 0;
extern int dbg_deep;// = 0;

void  mtk_command(int ch)
{
#if 0	
volatile	char input_ch[3]={0};
//    char temp[101];
	char cmd_list[4][20] ={0};
	
	input_ch[0]=ch;
	if(index_ch < 100)
	{
	  if(index_ch == 0)
		  memset(command,0,sizeof(command));
	  command[index_ch] = ch;
	  index_ch++;
	} 
	mtk_printf(input_ch,-1);
	if(ch == 0x0D)
	{
		int ii,jj=0,kk=0;
		//divide string to comand list
		for(ii=0;ii<index_ch & jj<4 && kk<20;ii++)
		{
			if(command[ii] ==' ')
			{ jj++; kk=0; continue; }	
		    if((command[ii] == 0)||(command[ii] == 0x0d))
				break;
   		    cmd_list[jj][kk]=command[ii];
			kk++;
		}
		index_ch = 0;
	}else if(ch == '?')
		;
	else	
	   return;
	index_ch = 0;	
	switch(command[0])
	{
		case '?':
			mtk_printf("help command\n\r",0);
			help_command();
			break;	
		case 'd':
			mtk_printf("USB Disk\n\r",0);
			{
				extern void debug_print_fat16();
				extern int format_spi_flash();
				if(cmd_list[1][0] == 'E')
					format_spi_flash();
				else	
				  debug_print_fat16();
				
			}
			break;
	
		case 'f':
			{
			//	int context=0,rlen=1,len =0;// getFileSize(cmd_list[1]);
//				void *fops=NULL;
			/*	
				OpenFile(cmd_list[1]);
				for(;;)
				{
				  if(0>ReadFile(cmd_list[1],&context, buf, &rlen))
					  break;
				  printf_memory(buf,4096);
				  if(rlen < 0) break;
				}
				CloseFile(cmd_list[1]);		
			*/	
				
			}
			break;		
	    case 'g':
			{
				extern int file_hide;
				dbg_deep = atoi(cmd_list[1]);
				if( cmd_list[1][0] == 'r')
					SystemReset();
				if( cmd_list[1][0] == 'p')
					print_PIT_info();	
				if(cmd_list[1][0] == 'f')
				{	
				    file_hide = atoi(cmd_list[2]);	
					mtk_printf("hide file",file_hide);
				}	
			}
			break;	
			
		case 'i':
			{  
			 char buff[10];
			 buff[0]= 0x05; buff[1]=0;
		/*	 i2c_master_write(atoi(cmd_list[1]), buff, 2);
			 buff[0]= 0x0; buff[1]=0;
			 i2c_master_read(atoi(cmd_list[1]), buff, 2);
		*/
             i2c_master_gpio_init();
             i2c_read(atoi(cmd_list[1]), (uint8_t *)buff, 2);
			 mtk_printf("I2C read:\n\r",buff[0]); 			 
			 mtk_printf("I2C read:\n\r",buff[1]); 			 
						 
			}  
			break;	
		case 'j':
			{	
				extern int JTAG_SWD_Get_IDCODE(int jtag_swd);
				JTAG_SWD_Get_IDCODE(atoi(cmd_list[1]));
			}
			break;				
		case 'l':
			{
				  char num=cmd_list[1][0];
			  int onoff=atoi(cmd_list[2]);
			  if(num == 'g')
				  if(onoff==1)
					  led_green_on();
				  else if(onoff==2)
					  led_green_blink();
				  else if(onoff==0)
				  {
					  led_green_off();	
					    led_green_blink_off();		
				  }	  
			  if(num == 'r')
				  if(onoff==1)
					  led_red_on();
				 else if(onoff==2)
					  led_red_blink();
				 else if(onoff==0)
				  {
					  led_red_off();	
					  led_red_blink_off();		
				  }		  
			  if(num == 'y')
				  if(onoff==1)
					  led_yellow_on();
				 else if(onoff==2)
					  led_yellow_blink();
				 else if(onoff==0)
				  {
					  led_yellow_off();	
					  led_yellow_blink_off();		
				  }		  	  
			}
			break;	
		case 'p':
			mtk_printf("SPI flash\n\r",0);
			if(cmd_list[1][0] == 0 )
				spiflash_probe(1,0);
			switch((cmd_list[1][0]))
			{
			  case 'r':
				   spiflash_read((uint8_t *)buf,atoi(cmd_list[2]),0x100);
				  // spiflash_write(buf,atoi(cmd_list[2]),0x100);
				   printf_memory(buf,0x100);
				  break;
			  case 'w':
				  spiflash_write((uint8_t *)buf,atoi(cmd_list[2]),0x100);
				  printf_memory(buf,0x100);
				  break;
			  case 'e':
				  spiflash_erase(atoi(cmd_list[2]),0x100);
				  memcpy(buf,(void *)0x20000000,0x100);
				  printf_memory(buf,0x100);
				  break;
			  case 'E':
				  spiflash_erase(0,spiflash_size());
				  break;	  
		    }		
			
			
			break;		
		
#ifdef PROGRAMMER
		case 's':
			{	

				if(cmd_list[1][0]==0) // help
					swd_usage(0);
				else	
				{
					if(command[2]=='d')
					{
					//	pPort.regs[0] = strtoul(&cmd_list[2][0],NULL,0);
					//	pPort.regs[1] = strtoul(&cmd_list[3][0],NULL,0);
					//	pPort.regs[2] = strtoul(&cmd_list[4][0],NULL,0);
					}
					cmdLine((uint8_t *)&command[2]);	
				}
			
			}
			break;
#endif				
		case 't':
			{	
				//extern int getTick(void);
				mtk_printf("sysTick:\n\r",getTick()); 	
			}
			break;		
			
         case 'u':
			mtk_printf("usb info\n\r",0);
			usb_dbg_print();
			break;
		case 'v':

			{

				mtk_printf("Voltage Detect:",get_targe_voltage());
				{
				
				switch(atoi(cmd_list[1]))
				{
					case 5: output_5v_io();break;
					case 3: output_3v3_io();break;
					case 2: output_2v5_io();break;
					case 1: output_1v8_io();break;
					case 0: output_0v_io();break;
					default:
					 mtk_printf("Wrong Voltage set, please input 5/3/2/1",-1);
				}
				mtk_printf("Voltage Change:",get_targe_voltage());
				}
			}
			break;	
			
	}		
	mtk_printf("\n\r>",-1);
	#endif
}


