#include "spider-sap.h" 
#include "nrf51.h"
//static NRF_NVMC_Type 	nrf_nvmc_t;		
static NRF_FICR_Type	nrf_ficr_t;	
static NRF_UICR_Type  nrf_uicr_t;	
void LockNrfChip(xDAPPacket *xPacket)
{
	uint32_t val=0;
	val=0x1;//erase en
		//write_targetreg(xPacket,0x4001E504,val);
	write_targetreg(xPacket,NRF_NVMC_BASE+NVMC_CONFIG_OFFSET,4,val);
	do{
			//read_targetreg(xPacket,0x4001E400,&val);
		read_targetreg(xPacket,NRF_NVMC_BASE+NVMC_READY_OFFSET,4,(uint8_t *)&val);
	}while(val==0);
	val=0xFFFF00FF;
	write_targetreg(xPacket,NRF_UICR_BASE+UICR_RBPCONF_OFFSET,4,val);
	do{
			//read_targetreg(xPacket,0x4001E400,&val);
		read_targetreg(xPacket,NRF_NVMC_BASE+NVMC_READY_OFFSET,4,(uint8_t *)&val);
	}while(val==0);
}
void GetNrfUICR(xDAPPacket *xPacket)
{
		
	uint32_t val=0x0;
	read_targetreg(xPacket,NRF_UICR_BASE+UICR_CLENR0_OFFSET,4,(uint8_t *)&val);
	nrf_uicr_t.CLENR0=val;
	
	read_targetreg(xPacket,NRF_UICR_BASE+UICR_RBPCONF_OFFSET,4,(uint8_t *)&val);
	nrf_uicr_t.RBPCONF=val;
	read_targetreg(xPacket,NRF_UICR_BASE+UICR_XTALFREQ_OFFSET,4,(uint8_t *)&val);
	nrf_uicr_t.XTALFREQ=val;
	read_targetreg(xPacket,NRF_UICR_BASE+UICR_FWID_OFFSET,4,(uint8_t *)&val);
	nrf_uicr_t.FWID=val;
	read_targetreg(xPacket,NRF_UICR_BASE+UICR_BOOTLOADERADDR_OFFSET,4,(uint8_t *)&val);
	nrf_uicr_t.BOOTLOADERADDR=val;
}
void SetNrfUICR(xDAPPacket *xPacket,NRF_UICR_Type pnrf_uicr_t)
{
		
	uint32_t val=0x0;
	val=pnrf_uicr_t.CLENR0;
	write_targetreg(xPacket,NRF_UICR_BASE+UICR_CLENR0_OFFSET,4,val);
	val=pnrf_uicr_t.RBPCONF;
	write_targetreg(xPacket,NRF_UICR_BASE+UICR_RBPCONF_OFFSET,4,val);
	val=pnrf_uicr_t.XTALFREQ;
	write_targetreg(xPacket,NRF_UICR_BASE+UICR_XTALFREQ_OFFSET,4,val);
		val=pnrf_uicr_t.FWID;
	write_targetreg(xPacket,NRF_UICR_BASE+UICR_FWID_OFFSET,4,val);
		val=pnrf_uicr_t.BOOTLOADERADDR;
	write_targetreg(xPacket,NRF_UICR_BASE+UICR_BOOTLOADERADDR_OFFSET,4,val);
}

void GetNrfFICR(xDAPPacket *xPacket)
{
	uint32_t val=0x0;

	read_targetreg(xPacket,NRF_FICR_BASE+FICR_CODEPAGESIZE_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.CODEPAGESIZE=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_CODESIZE_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.CODESIZE=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_CLENR0_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.CLENR0=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_PPFC_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.PPFC=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_NUMRAMBLOCK_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.NUMRAMBLOCK=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_SIZERAMBLOCK_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.SIZERAMBLOCK[0]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_SIZERAMBLOCK_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.SIZERAMBLOCK[1]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_SIZERAMBLOCK_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.SIZERAMBLOCK[2]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_SIZERAMBLOCK_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.SIZERAMBLOCK[3]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_CONFIGID_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.CONFIGID=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_DEVICEID_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.DEVICEID[0]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_DEVICEID_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.DEVICEID[1]=val;	
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_ER_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.ER[0]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_ER_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.ER[1]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_ER_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.ER[2]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_ER_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.ER[3]=val;	
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_IR_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.IR[0]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_IR_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.IR[1]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_IR_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.IR[2]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_IR_OFFSET+4,4,	(uint8_t *)&val);
	nrf_ficr_t.IR[3]=val;
	
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_DEVICEADDRTYPE_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.DEVICEADDRTYPE=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_DEVICEADDR_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.DEVICEADDR[0]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_DEVICEADDR_OFFSET+4,4,	(uint8_t *)&val);	
	nrf_ficr_t.DEVICEADDR[1]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_OVERRIDEEN_OFFSET,4,	(uint8_t *)&val);
	nrf_ficr_t.OVERRIDEEN=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_BLE_1MBIT_OFFSET,4,	(uint8_t *)&val);	
	nrf_ficr_t.BLE_1MBIT[0]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_BLE_1MBIT_OFFSET,4,	(uint8_t *)&val);	
	nrf_ficr_t.BLE_1MBIT[1]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_BLE_1MBIT_OFFSET,4,	(uint8_t *)&val);	
	nrf_ficr_t.BLE_1MBIT[2]=val;
	read_targetreg(xPacket,NRF_FICR_BASE+FICR_BLE_1MBIT_OFFSET,4,	(uint8_t *)&val);	
	nrf_ficr_t.BLE_1MBIT[3]=val;
}


int ChkNrfLock()
{
	if(((nrf_uicr_t.RBPCONF>>8)&0xFF)==0x00)
		return 1;
	else
		return 0;
}

void Nrf51ErasePage(xDAPPacket *xPacket,uint32_t add,uint32_t end)
{
	uint32_t EnDAddress=end;//nrf_ficr_t.CODESIZE*1024;//k //0x40000;
	uint32_t PageSize=nrf_ficr_t.CODEPAGESIZE;//0x400;
	uint32_t val=0x0;
	do{
		//write_targetreg(xPacket,0x4001E508,add);//nvm config	
		write_targetreg(xPacket,NRF_NVMC_BASE+NVMC_ERASEPAGE_OFFSET,4,add);
		do{
			//read_targetreg(xPacket,0x4001E400,&val);
			read_targetreg(xPacket,NRF_NVMC_BASE+NVMC_READY_OFFSET,4,(uint8_t *)&val);
		}while(val==0);
		add+=PageSize;
	}while(add<EnDAddress);
}
void Nrf51EraseSoftDevice(xDAPPacket *xPacket,uint32_t add)
{

	

}

void Nrf51EraseAll(xDAPPacket *xPacket,uint32_t add)
{
		uint32_t val=0x0;
		uint32_t cnt=0;

		val=0x2;//erase en
		//write_targetreg(xPacket,0x4001E504,val);
		write_targetreg(xPacket,NRF_NVMC_BASE+NVMC_CONFIG_OFFSET,4,val);
		do{
			//read_targetreg(xPacket,0x4001E400,&val);
			read_targetreg(xPacket,NRF_NVMC_BASE+NVMC_READY_OFFSET,4,(uint8_t *)&val);
		}while(val==0);
		//read_targetreg(xPacket,0x4001E504,&val);//nvm config
		
		if(nrf_ficr_t.PPFC==0xFF)//app
		{
			uint32_t start_addr=nrf_ficr_t.CLENR0;
			uint32_t end_addr=nrf_ficr_t.CODESIZE*1024;
			Nrf51ErasePage(xPacket,start_addr,end_addr);
		}
		val=0x0;//
		//write_targetreg(xPacket,0x4001E504,val);
		write_targetreg(xPacket,NRF_NVMC_BASE+NVMC_CONFIG_OFFSET,4,val);
		do{
			//read_targetreg(xPacket,0x4001E400,&val);
			read_targetreg(xPacket,NRF_NVMC_BASE+NVMC_READY_OFFSET,4,(uint8_t *)&val);
		}while(val==0);
		
		val=0x2;//erase en
		//write_targetreg(xPacket,0x4001E504,val);
		write_targetreg(xPacket,NRF_NVMC_BASE+NVMC_CONFIG_OFFSET,4,val);
		do{
			//read_targetreg(xPacket,0x4001E400,&val);
			read_targetreg(xPacket,NRF_NVMC_BASE+NVMC_READY_OFFSET,4,(uint8_t *)&val);
		}while(val==0);
		if(add!=0)
		{
			val=0x1;//erase all
			//write_targetreg(xPacket,0x4001E514,val);//nvm config
			write_targetreg(xPacket,NRF_NVMC_BASE+NVMC_ERASEUICR_OFFSET,4,val);		
			do{
				//read_targetreg(xPacket,0x4001E400,&val);
				read_targetreg(xPacket,NRF_NVMC_BASE+NVMC_READY_OFFSET,4,(uint8_t *)&val);
			}while(val==0);
		}else
		{
			val=0x1;//erase all
			//write_targetreg(xPacket,0x4001E514,val);//nvm config
			write_targetreg(xPacket,NRF_NVMC_BASE+NVMC_ERASEALL_OFFSET,4,val);		
			do{
				//read_targetreg(xPacket,0x4001E400,&val);
				read_targetreg(xPacket,NRF_NVMC_BASE+NVMC_READY_OFFSET,4,(uint8_t *)&val);
			}while(val==0);
		
		}
		val=0x0;//erase disable
		//write_targetreg(xPacket,0x4001E504,val);
		write_targetreg(xPacket,NRF_NVMC_BASE+NVMC_CONFIG_OFFSET,4,val);
}
