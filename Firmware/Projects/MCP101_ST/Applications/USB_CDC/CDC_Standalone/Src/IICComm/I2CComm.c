
#include "I2CComm.h"

#define IIC_PACKET_LEN 32
#define IIC_MSG_LEN 16
extern I2C_HandleTypeDef I2cHandle;

typedef enum{
	I2C_GET_STATUS,
	I2C_PROG_START,
	I2C_GET_RESULT,
	I2C_GET_OPMSG,
	I2C_ACK,
	I2C_UPFIREWARE
}I2C_Req;


/* Buffer used for reception */

uint8_t aTxBuffer[I2CRXBUFFERSIZE];
uint8_t aRxBuffer[I2CRXBUFFERSIZE];

IIC_PACKET_HEADER xPkt;
uint16_t hTxNumData = 0, hRxNumData = 0;
uint8_t bTransferRequest = 0;
int gRecvCplt=0;
extern I2C_HandleTypeDef I2cHandle;
extern int gStartProgI2C;
extern int gProgOnLine;
extern int button_pressed;
extern int Device_Addr;
extern uint8_t CalcChksum(uint8_t* pBuf, uint16_t len);
int I2C_SendAck(I2C_HandleTypeDef *Handle,PIIC_PACKET_HEADER pHeaderIn,uint8_t status);
int I2C_SendMsg(I2C_HandleTypeDef *Handle,I2C_Req dataType,uint8_t* data, uint16_t len ,uint8_t status);
static inline void SetPayloadSize(PIIC_PACKET_HEADER pHeader,uint16_t len)
{
	pHeader->lenH = len>>8;
	pHeader->lenL = len&0xFF;
}
uint8_t xI2CMsgPacketOut(uint8_t type, uint32_t len, uint32_t addr, char *data,uint8_t flag)
{
/*
    int i=0;
		uint8_t g_buffer[sizeof(PIIC_PACKET_HEADER)]={0};
		PIIC_PACKET_HEADER pHeader = (PIIC_PACKET_HEADER)g_buffer;
    uint8_t* pData = g_buffer+ sizeof(PIIC_PACKET_HEADER);
    // create header
    memcpy(pHeader->headerSig, packetHeaderSig, 1);
    pHeader->pktType = type;
    SetPayloadSize( pHeader,len);

    memcpy(pData,data,len);
    *(pData+IIC_PACKET_LEN-sizeof(PIIC_PACKET_HEADER)) = CalcChksum((uint8_t*)g_buffer, IIC_PACKET_LEN-1);
		while(HAL_I2C_Slave_Transmit(&I2cHandle, g_buffer, IIC_PACKET_LEN,1000)!= HAL_OK);	
	*/
	mtk_printf_iic(data,-1);	
	return 0;
}
static inline uint16_t GetPayloadSize(PIIC_PACKET_HEADER pHeader)
{
	uint16_t l = pHeader->lenH;
	l <<= 8;
	l+= pHeader->lenL;
	return l;
}
int I2CRecvHeader(I2C_HandleTypeDef *Handle,PIIC_PACKET_HEADER pHeader,uint8_t *buff,int bWaitInfinite)
{
    uint16_t cbRead;
    uint32_t i = 0;
    cbRead = sizeof(PIIC_PACKET_HEADER);
    {
        //Get Header From buffer
    }
		memcpy((uint8_t *)pHeader,buff,cbRead);
    return 1;
}
int I2CRecvPacket(I2C_HandleTypeDef *Handle,PIIC_PACKET_HEADER pHeader, uint8_t* pbFrame, uint16_t* pcbFrame, int bWaitInfinite)
{
    // receive header
	int payloadSize =0;
	uint8_t err = 0;
    if(!I2CRecvHeader(Handle,pHeader,pbFrame,bWaitInfinite))
    {
        return 0;
    }
    payloadSize=GetPayloadSize(pHeader);
    // make sure sufficient g_buffer is provided
    if(*pcbFrame < payloadSize)
    {
        return 0;
    }

    // receive data
    *pcbFrame = payloadSize;
    //if(!OEMSerialRecvRaw(pbFrame, pcbFrame, bWaitInfinite))
    if(1)//Get data From buffer/*SW_UartReceive(uart,pbFrame,*pcbFrame+1,g_dwTimeout)*/)
    {
    	 err = 1;
        return 0;
    }
    // verify data checksum
		uint8_t crc=I2CCalcChksum((uint8_t*)pHeader,sizeof(PIIC_PACKET_HEADER))+CalcChksum(pbFrame, *pcbFrame);
    if(crc!=pbFrame[*pcbFrame])
    {
    	 err = 1;
     

        return 0;
    }
    //SerialSendAck(pHeader, err);
    return 1;
}
void I2CServiceStart (void)
{
		while(HAL_I2C_Slave_Receive_IT(&I2cHandle, (uint8_t*)aRxBuffer, 128)!= HAL_OK);
}
void ResetToBootLoader(void)
{
	//SetUpdateMask();		
	SystemReset();
}
I2COPStage_t* GetI2CStage();
void GetCurrentSapFile(SFHandle *hFile);
void I2CServiceEntry (void* param)
{
		static uint16_t cbDataBuffer = 0;
		int i=0;
		uint8_t len=0;
		uint8_t status;
		uint8_t data[64];
		IIC_PACKET_HEADER header;
		uint8_t g_buffer[sizeof(IIC_PACKET_HEADER)+1];
		uint16_t cbPacket = sizeof(aRxBuffer);
		I2COPStage_t *I2COP;
		I2COP=GetI2CStage();
		SFHandle hFile;
		GetCurrentSapFile(&hFile);
		if(gRecvCplt)
		{
		I2CRecvPacket(&I2cHandle,&header,aRxBuffer,&cbPacket,TRUE);
		switch(header.pktType)
		{
			case I2C_GET_STATUS:
				//status=ACK_OK;
				//I2C_SendAck(&I2cHandle,&header,status);

				I2C_SendMsg(&I2cHandle,header.pktType,(uint8_t *)hFile.FileName,strlen((char *)hFile.FileName),0);
				break;
			case I2C_PROG_START:
				gStartProgI2C=1;
				status=ACK_OK;
				I2C_SendAck(&I2cHandle,&header,status);
				break;
			case I2C_GET_RESULT:
			{

				if(!I2COP->IsBusy)
					I2C_SendMsg(&I2cHandle,header.pktType,I2COP->Msg,strlen((char *)I2COP->Msg),I2COP->IsOK);
				else
			I2C_SendAck(&I2cHandle,&header,status);
			}
				break;
			case I2C_GET_OPMSG:
				I2C_SendMsg(&I2cHandle,header.pktType,I2COP->Msg,strlen((char *)I2COP->Msg),I2COP->IsOK);
			
				break;
			case I2C_UPFIREWARE:
				ResetToBootLoader();
				break;
			default:break;
		}
		gRecvCplt=0;	
		I2CServiceStart();

	}
	
}
void ParseIICPacket(uint8_t  *pBuff)
{
	
}
void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
		gRecvCplt=1;
}
int I2C_SendAck(I2C_HandleTypeDef *Handle,PIIC_PACKET_HEADER pHeaderIn,uint8_t status)
{
    uint8_t g_buffer[sizeof(PIIC_PACKET_HEADER) + 1];
		Device_Addr=GetI2CSlaveAddress();
    PIIC_PACKET_HEADER pHeader = (PIIC_PACKET_HEADER)g_buffer;
    uint8_t* pData = (g_buffer + sizeof(IIC_PACKET_HEADER));
    int len = 1;
    memcpy(pHeader, pHeaderIn , sizeof(IIC_PACKET_HEADER));
    // create header
    pHeader->pktType = I2C_ACK;
    pHeader->lenL = 1; pHeader->lenH = 0;
		pHeader->addr=Device_Addr;
    *pData = status;
    *(pData+len) = CalcChksum((uint8_t*)g_buffer, sizeof(IIC_PACKET_HEADER) + len );
   		while(HAL_I2C_Slave_Transmit_IT(Handle,g_buffer,IIC_PACKET_LEN)!=HAL_OK);	
		while (HAL_I2C_GetState(Handle) != HAL_I2C_STATE_READY)
    {
			HAL_Delay(10);
    }
		return 0;  
}
int I2C_SendMsg(I2C_HandleTypeDef *Handle,I2C_Req dataType,uint8_t* data, uint16_t len,uint8_t status)
{
    uint8_t g_buffer[sizeof(PIIC_PACKET_HEADER) + len+1];
    unsigned long written = 0;
    int i=0;
    PIIC_PACKET_HEADER pHeader = (PIIC_PACKET_HEADER)g_buffer;
    uint8_t* pData = (g_buffer + sizeof(IIC_PACKET_HEADER));
    //memcpy(pHeader, pHeaderIn , sizeof(SERIAL_PACKET_HEADER));
    // create header
		Device_Addr=GetI2CSlaveAddress();
		pHeader->headerSig[0] = packetHeaderSig[0];
		pHeader->pktType = dataType;
		pHeader->addr=Device_Addr;
	
		SetPayloadSize(pHeader, len);
		pHeader->status=status;
		memcpy(pData,data,len);

    *(pData+len) = CalcChksum((uint8_t*)g_buffer, sizeof(IIC_PACKET_HEADER) + len );
   		while(HAL_I2C_Slave_Transmit_IT(Handle,g_buffer,IIC_PACKET_LEN)!=HAL_OK);	
		while (HAL_I2C_GetState(Handle) != HAL_I2C_STATE_READY)
		{
			HAL_Delay(10);
    }
		return 0;  
}

int  mtk_printf_iic(char *str, int value)
{
	
	int len=strlen(str);
	if(gStartProgI2C||button_pressed)
	{
		char * pChar = str;
		char strBuf[25]={0};
		I2COPStage_t *I2COP;
		I2COP=GetI2CStage();
		len=MIN(len,sizeof(I2COP->Msg));
		memset(I2COP->Msg,sizeof(I2COP->Msg),0);
		if(value==-1)
			strncpy((char *)I2COP->Msg,str,len);		
		else
			strncpy((char *)I2COP->Msg,str,len-sizeof(value));
		if (value != -1)
		{
			sprintf(strBuf, "\t0x%08X\n\r", value);
			//itoa(value, strBuf,16);
			//mtk_printf_usbser(strBuf, -1);
			strcpy((char *)I2COP->Msg+strlen((char *)I2COP->Msg),strBuf);
		}	
	}
	return len;
}