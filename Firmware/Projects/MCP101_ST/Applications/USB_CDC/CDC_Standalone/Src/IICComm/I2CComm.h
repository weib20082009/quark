
#include "common.h"

static const uint8_t packetHeaderSig[] = { 0xA5 };
typedef struct tagIIC_PACKET_HEADER {
    uint8_t headerSig[1];
    uint8_t pktType;
    uint8_t addr;
    uint8_t lenH;
    uint8_t lenL;
		uint8_t status;
} __attribute__ ((packed))IIC_PACKET_HEADER, *PIIC_PACKET_HEADER;

/* Error codes */
typedef enum {
	ACK_OK = 0,
	ACK_GOON = 1,
	ACK_BUSY,
	ACK_FAIL,
	ACK_ERROR = 0xFF,
	ACK_NOT_SAP_FILE = -3,
}I2C_ACKStatus;

