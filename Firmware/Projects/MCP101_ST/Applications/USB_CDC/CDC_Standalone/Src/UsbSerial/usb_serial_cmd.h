
//  Common RS232/USB Serial routines .
//
#ifndef __OAL_BLSERIAL_CMD_H
#define __OAL_BLSERIAL_CMD_H
#include "common.h"
typedef struct
{
    uint8_t sec;
    uint8_t min;
    uint8_t hr;
    uint8_t weekday;/* 1-sunday... 7-saturday */
    uint8_t date;
    uint8_t month;
    uint8_t year;
}m_rtc_time_t;
//cmd  definition
typedef enum __USBSer_COMMAND_t
{
	USBSer_GET_DATA=0xA1,//get File,MEM
	USBSer_GET_LOG,//get LOG,Statistics

	USBSer_ACTION,//log onoff ,device operation,other CMD
	
	USBSer_SEND_DATA,//send Flle,MEM,FW
	USBSer_SEND_LOG,//send LOG,Staitistics

	USBSer_ACK,//response for CMD,request

	USBSer_Heart
}USBSer_CMD;



typedef enum __USBSer_STATUS_CODE
{
	USBSer_NO_CONNECT = 0xAA01,
	USBSer_PROFILE_OPEN,
	USBSer_PROFILE_CLOSE,
	USBSer_CHECKSUM_ERROR,
	USBSer_VAULE_ERROR,
	USBSer_SYSTEM_BUSY
}USBSer_STATUS_CODE;

typedef enum __USBSer_ERROR_CODE
{

	USBSer_ERROR_PACKET_LOSS = 0xA601,
	USBSer_ERROR_CHECKSUM_ERROR,
	USBSer_ERROR_FEAT_NOT_SUPPORT,
	USBSer_ERROR_CMD_NOT_SUPPORT
}USBSer_ERROR_CODE;

typedef enum _USBSer_RET_CODE_
{
	USBSer_RET_OK = 0,
	USBSer_RET_CMD_ERR,
	USBSer_RET_TYPE_ERR,
	USBSer_RET_CHECKSUM_ERR,
	USBSer_RET_TIME_OUT,
	USBSer_RET_BUSY,
	USBSer_RET_FLAS_CHECKSUM_ERR,
	USBSer_RET_NO_CONNECT_CLIENT,
	USBSer_RET_MISS_START,
	USBSer_RET_FILENAME_TOOLONG,
	USBSer_RET_PACKET_LOSS
} USBSer_RET_CODE;







#if __cplusplus
extern "C" {
#endif

// Use the serial bootloader in conjunction with the serial kitl transport
// Both the transport and the download service expect packets with the
// same header format. Packets not of type DLREQ, DLPKT, or DLACK are
// ignored by the serial bootloader.
// serial packet header and trailer definitions

#define __in
static const uint8_t packetHeaderSig[] = { 0xA5 };
#define HEADER_SIG_BYTES    sizeof(packetHeaderSig)

#define KS_PKT_KITL         0xAA
#define KS_PKT_DLREQ        0xBB
#define KS_PKT_DLPKT        0xCC
#define KS_PKT_DLACK        0xDD
#define KS_PKT_JUMP         0xEE
#define KITL_MTU  256+10

// packet header
int WaitAck(int uart, uint8_t type);

typedef struct tagSERIAL_PACKET_HEADER {
    uint8_t headerSig[HEADER_SIG_BYTES];
    uint8_t pktType;
    uint8_t dataType;
    uint8_t lenH;
    uint8_t lenL; // not including this header
		uint8_t PacketID;
		uint16_t FileIndex;//aviod the repeat packet
//    uint8_t crcData;
//    uint8_t crcHdr;
} __attribute__ ((packed)) SERIAL_PACKET_HEADER, *PSERIAL_PACKET_HEADER;


typedef struct SERIAL_BOOT_ACK {
    uint32_t fJumping;
} SERIAL_BOOT_ACK, *PSERIAL_BOOT_ACK;


typedef struct SERIAL_BLOCK_HEADER {
    uint32_t uBlockNum;
} SERIAL_BLOCK_HEADER, *PSERIAL_BLOCK_HEADER;

int
RecvHeader(
		int uart,
    __in PSERIAL_PACKET_HEADER pHeader, 
    int bWaitInfinite
    );

int
RecvPacket(
		int uart,
    __in PSERIAL_PACKET_HEADER pHeader, 
    __in uint8_t* pbFrame,
    __in uint16_t* pcbFrame,
    int bWaitInfinite
    );

int
SerialSendBlockAck(
    uint32_t uBlockNumber
    );

int
SerialSendBootRequest(
    __in const char * platformString
    );

int
SerialReadData(
    uint32_t cbData,
    uint8_t* pbData
    );
static inline uint16_t GetPayloadSize(PSERIAL_PACKET_HEADER pHeader)
{
	uint16_t l = pHeader->lenH;
	l <<= 8;
	l+= pHeader->lenL;
	return l;
}
static inline void SetPayloadSize(PSERIAL_PACKET_HEADER pHeader,uint16_t len)
{
	pHeader->lenH = len>>8;
	pHeader->lenL = len&0xFF;
}
//------------------------------------------------------------------------------

#if __cplusplus
}
#endif

#endif
