
#include "common.h"
typedef struct
{
	uint8_t *pdata;
	int len;
	int CurPos;
} USBSerialBuf;


USBSerialBuf g_pUSer;

static int stopRoute = 0;
int g_USBRouteTo = -1 ;
uint8_t  usb_rx_complete=0;
//uint8_t *txbuf, __txbuf[USB_BUF_SIZE*2],rxbuf[USB_BUF_SIZE];
extern UART_HandleTypeDef UartHandle;
extern uint8_t *usbtxbuf[USB_BUF_SIZE*2];
extern uint8_t usbrxbuf[USB_BUF_SIZE*2];//buffer for usb receive
int GetByteFromUSB(uint8_t * buf, uint32_t len, int* truelen, uint32_t timeout);
int SW_UartReceive(int uart, void* data, uint32_t size, uint32_t timeout )
{
	int rc;
	int truesize=0;
	uint8_t *val=(uint8_t *)data;

		while(size>0)
		{
			//truesize =size;	
			if(uart == USB_CDC_SERIAL)
			{
				rc=GetByteFromUSB(val, size ,&truesize,timeout);
			}else if(uart == USART_SERIAL)
			{
				rc = HAL_UART_Receive(&UartHandle, (uint8_t *)val, /*RXBUFFERSIZE*/size, 0x5000);
				truesize = UartHandle.RxXferSize-UartHandle.RxXferCount;
			}
			if(rc!=0)
				return 2;
			if(size>truesize)
			{
				size=size-truesize;
				val+=truesize;
			}else
			{
				val += size;
				size = 0;
			}
			

		}
	return rc;
}

int SW_UartSend(int uart, void* data, uint32_t size)
{
	int rc;
	///rc=wiced_uart_transmit_bytes(( wiced_uart_t )uart, data,  size);
	if(uart == USB_CDC_SERIAL)
	{
		memcpy(usbtxbuf,(uint8_t*)data,size);
		rc = SW_CDC_Send((uint8_t*)usbtxbuf,size);
	}else if(uart == USART_SERIAL)
	{
		rc = HAL_UART_Transmit(&UartHandle, (uint8_t *)data, /*RXBUFFERSIZE*/size, 0x5000);
	}
	
	return rc;
}
extern uint8_t * g_pRxBuffer;	
void SW_USBSer_Init()
{
		//SW_UartInit(WICED_UART_USB,115200,NULL,0);
		//txbuf = __txbuf;
///		SW_CDC_Init(usbrxbuf,USB_BUF_SIZE);
//		txbuf = &__txbuf[USB_BUF_SIZE];
}
 void  USBRouteTxEntry (void)
{
	uint32_t available=0, timeout = 0xffffffff,count=0;
	uint32_t rc=0;

				rc=SW_CDC_Send((uint8_t*)usbtxbuf, count);

}
void ResetCDCRecvBuff(void)
{
		SW_CDC_Receive(g_pRxBuffer,USB_BUF_SIZE);
}
void FreeUserBuffer(void)
 {
 		 g_pUSer.pdata=&usbrxbuf[0];
		g_pUSer.len=0;
	 g_pUSer.CurPos=0;
	 g_pRxBuffer=&usbrxbuf[0];

	 memset(&usbrxbuf[0],sizeof(usbrxbuf),0);
 }
int  usb_set_semaphore(uint8_t *flag)
{
	*flag=1;
}
int  usb_get_semaphore(uint8_t *flag,uint32_t time_out)
{
	while(1)
	{
		if(g_pUSer.CurPos<g_pUSer.len)
		{
			return 0;
		}
		else
		{
			time_out--;
			if(time_out==0)
				return -1;
			HAL_Delay(1);
		}
	}

}
int GetByteFromUSB(uint8_t * buf, uint32_t len, int* truelen, uint32_t timeout)
{
//	int len = *plen;
	int r= len;
	int rc=0;
	*truelen=0;
	///if(g_pUSer.len==0)
	if(g_pUSer.CurPos==g_pUSer.len)
	{

		rc=usb_get_semaphore( &usb_rx_complete, timeout);
		if(rc==0)
			usb_rx_complete=0;
		return rc;
	}else
	{
		if((g_pUSer.len-g_pUSer.CurPos)<len)
		{
			r = (g_pUSer.len-g_pUSer.CurPos);
		}
		if(r>0){
			memcpy(buf,(g_pUSer.pdata+g_pUSer.CurPos),r);
			*truelen=r;
			///g_pUSer.pdata+=r; g_pUSer.len-=r;
			g_pUSer.CurPos+=r;
			if(g_pUSer.len==0)
			{
				///SW_CDC_Receive( usbrxbuf,USB_BUF_SIZE);
			}
		}

			return rc;
	}
}

void USBRouteRxEntry(uint8_t* data, uint32_t Len)
{

		///g_pUSer.pdata= data;
	if(g_pUSer.len<(USB_BUF_SIZE))
	{
			g_pUSer.len+= Len;
		g_pRxBuffer+=Len;
	}
	usb_set_semaphore( &usb_rx_complete);
	///if(g_pRxBuffer==&usbrxbuf[0])
///		g_pRxBuffer=&usbrxbuf[USB_BUF_SIZE];
	///else
///		g_pRxBuffer=&usbrxbuf[0];
///

	SW_CDC_Receive( g_pRxBuffer,USB_BUF_SIZE);

	//UartServiceEntry(0);

}

#if 0
void USBRouteRxEntry(uint8_t* data, uint32_t Len)
{
	if(g_USBRouteTo!=-1)
	{
		SW_UartSend(g_USBRouteTo,data, Len);
		//data[Len]=0;
		//myprintf(data);

		SW_CDC_Receive( txbuf,USB_BUF_SIZE);
		if(txbuf!=__txbuf) txbuf = __txbuf;
		else txbuf = &__txbuf[USB_BUF_SIZE];
	}
}
#endif

extern void UartServiceEntry (void* param);
extern int SendFile(int uart,const char* filename,int mode);
static void USBSerServiceEntry (void* param)
{
	 UartServiceEntry (0);
}



void SW_USB_Receive_Callback(uint8_t* data, uint32_t Len)
{

    	USBRouteRxEntry(data, Len);
}




void SW_Clocks_Needed(void)
{
	///stm32f4xx_clocks_needed();
}
void SW_Clocks_Not_Needed(void)
{
	///stm32f4xx_clocks_not_needed();
}


