
#include "usb_serial_cmd.h"
#include "spider-sap.h"
#define BLE_BUF_LEN 128 + 16
static uint8_t rx_ble_data[BLE_BUF_LEN];
#define BLE_STK_SIZE 1024
#define BLE_PRI 8
static int ackflag=0;
static uint8_t ble_exit=0;

static uint8_t g_bHasReq= 0;
static SERIAL_PACKET_HEADER gHeadReq;
static uint8_t gDataReq[16];
uint16_t gEventID;
uint32_t gFileIndex=0;
extern int gProgOnLine;
extern int dbg_deep;
#define FILE_DATA_BUF_SZ 128
#define     IDS_CONNECT						  "Connect target "
#define     IDS_OK                  "OK!\r\n"
#define     IDS_FAIL                "Fail!\r\n"
#define     IDS_CMD_GET_FILE        "Upload File "
#define     IDS_CMD_SET_LOG         "Set Log Level "
#define     IDS_CMD_GET_STATI       "Get statistics "
#define     IDS_CMD_BLANK           "Blank Check "
#define     IDS_CMD_VERIFY          "Verify "
#define     IDS_CMD_ERASE_CHIP      "Erase Chip "
#define     IDS_CMD_GET_MEM         "Upload Target Memory "
#define     IDS_CMD_PROGRAM         "Program "
#ifndef TRUE
#define TRUE 1
#endif
	uint8_t data_t[FILE_DATA_BUF_SZ];
static uint8_t dataBuffer[KITL_MTU] = {0};
uint8_t gLogging=0;

static uint32_t D2000_Flash_Block_Add[3]={0x0,0x180000,0x00200000};
static uint32_t D2000_Flash_Block_Size[3]={0x2000,0x8000,0x1000};					
static uint32_t C1000_Flash_Block_Add[3]={0xFFFFE000,0x40000000,0x40030000};
static uint32_t C1000_Flash_Block_Size[3]={0x2000,0x30000,0x30000};
extern int Statistics_Offset;
extern int Statistics_CNT;

extern int FailStatistics_Offset;
extern int FailStatistics_CNT;

void GetMemoryFromTarget(uint8_t *buf,uint32_t addr,uint32_t size);
void ReportMsg(char *op,char *rc)
{
	char buf[32]="";
	strcat(buf,op);
	strcat(buf,rc);
	if(strstr(buf,IDS_FAIL))
	{
		eP(buf);
	}
	else if(strstr(buf,IDS_OK))
	{
		uP(buf);
	}
}
void BLEServiceEntry (void* param)
{
 //   static uint32_t dwBlockNumber = 0;
    static uint16_t cbDataBuffer = 0;
	int i=0;


    SERIAL_PACKET_HEADER header;
    uint16_t cbPacket;
    int uart=(int)param;
	while(1)
	{
		 // read from port
		//SW_SerialSendData(uart,0x00,fff,256);
		 cbPacket = sizeof(dataBuffer);
	 	if(g_bHasReq)
	 	{
	 			 memcpy( &header , &gHeadReq, sizeof(gHeadReq));
	 			 memcpy(dataBuffer, gDataReq , header.lenL);
	 			 cbPacket = header.lenL ;
	 			 g_bHasReq = 0;
	 			 BLEGetDataHandler(uart,&header, dataBuffer, cbPacket);
	 	}
	 	else if(RecvPacket(uart,&header, dataBuffer, &cbPacket, TRUE))
		 {
			 BLEGetDataHandler(uart,&header, dataBuffer, cbPacket);

		 }
	}
}
void  SW_BLE_Send(uint8_t* Ptr,uint32_t cnt)
{
  	///SW_UartSend(UART_CHANNEL_BLE,Ptr,cnt);
    
}
int SW_BLE_Receive(uint8_t*Ptr,uint32_t cnt)
{
    /// return SW_UartReceive(UART_CHANNEL_BLE,Ptr,cnt,0x1000);
    
}
#if 0
static	uint8_t  rxbuf[128];	
static  void  BLETask (ULONG data)
{
	uint32_t timeout = -1, available = 0,count = 0;
	//BLEServiceEntry((void*)UART_CHANNEL_BLE);
#if 0
	while(1)//!ble_exit)
	{     
      if(0==SW_BLE_Receive( rxbuf,1) )
      {
      		count++;
      		available = SW_Uart_Data_Available(UART_CHANNEL_BLE);
      		if(available>0)
      		{
      				if(SW_UartReceive(UART_CHANNEL_BLE, rxbuf+1,available,0))
      					count+=available;
      		}
      }
      if(count>0) 
      {
///       	 myprintf("Response From BLE(%d):\r\n",count);
///      	 myprintf(rxbuf);		
      }
  }
#endif
}
#endif





#if 0
int RecvFile(int uart,const TCHAR* filename)
{
	FIL fp;
	f_open(&fp,filename,FA_WRITE|FA_CREATE_ALWAYS);
	//int err_t=GetLastError();
	TCHAR* filename_tmp;
	BYTE r=0;
	int len, unit= 256;
	filename_tmp=filename;
	if(&fp)
	{
		f_close(&fp);
		SW_SerialSendRequest( uart,BLE_FILE_START, (uint8_t*)filename_tmp, strlen(filename_tmp)+1);

		//BLEServiceEntry((void*)f);
#define STACK_SIZE	1024
		SW_Create_Task(&hBLEHandle, 12, (CHAR*)"blemcu", BLEMcuServiceEntry, STACK_SIZE, (void*)0) ;
		//BLEServiceEntry((void*)uart);

		//fclose(f);
	}
	return 0;
}

int SendFile(int uart,const TCHAR* filename,int mode)
{
	 FIL fp;
	 int ackstatus;

	TCHAR* filename_tmp;
	BYTE r=0;
	int len, bytesread,unit= 0x64;
	char buf[128];

	f_open(&fp,filename,FA_READ);
	filename_tmp=filename;//GetFileName(filename);
	if(&fp)
	{
		//do{
		if(mode==BLEFIRM)
		{
			SW_SerialSendData(uart,BLE_DATA_SW_FIRMWARE, (uint8_t*)filename_tmp, strlen(filename_tmp)+1);
		//}while(!(r=WaitAck(BLE_FILE_START)));
			//while((r=WaitAck(uart,BLE_DATA_SW_FIRMWARE))!=1);
			do{
				ackstatus=WaitAck(uart,BLE_DATA_SW_FIRMWARE);
				if(ackstatus==BLE_RET_TIME_OUT)
				{
					SW_SerialSendData(uart,BLE_DATA_SW_FIRMWARE, (uint8_t*)filename_tmp, strlen(filename_tmp)+1);
					ackstatus=0xff;
				}
			}while(ackstatus==0xff);
		}
		else
		{
			SW_SerialSendData(uart,BLE_FILE_START, (uint8_t*)filename_tmp, strlen(filename_tmp)+1);
		//}while(!(r=WaitAck(BLE_FILE_START)));
		//	while((r=WaitAck(uart,BLE_FILE_START))!=1);
			do{
				ackstatus=WaitAck(uart,BLE_FILE_START);
				if(ackstatus==BLE_RET_TIME_OUT)
				{
					SW_SerialSendData(uart,BLE_FILE_START, (uint8_t*)filename_tmp, strlen(filename_tmp)+1);
					ackstatus=0xff;
				}
			}while(ackstatus==0xff);
		}
		do{
			f_read(&fp,buf,unit,&len);
			if(len<=0) break;
			//do{
				SW_SerialSendData(uart,BLE_FILE_DATA,(uint8_t*) buf, len);
			//}while(1);//!(r=WaitAck(BLE_FILE_DATA)));
		//while(WaitAck(uart,BLE_FILE_DATA)!=1);
				do{
					ackstatus=WaitAck(uart,BLE_FILE_START);
					if(ackstatus==BLE_RET_TIME_OUT)
					{
						SW_SerialSendData(uart,BLE_FILE_DATA,(uint8_t*) buf, len);
						ackstatus=0xff;
					}
				}while(ackstatus==0xff);
			//if(r>1) break;
		}while(1);//len<unit);
		SW_SerialSendData(uart,BLE_FILE_END, &r, 1);
		do{
			ackstatus=WaitAck(uart,BLE_FILE_START);
			if(ackstatus==BLE_RET_TIME_OUT)
			{
				SW_SerialSendData(uart,BLE_FILE_END, &r, 1);
				ackstatus=0xff;
			}
		}while(ackstatus==0xff);
		f_close(&fp);
	}
	return 0;
}

#endif

int SerialSendBuf(int uart,uint32_t addr, const uint8_t* buf,int len)
{
	 int ackstatus;
	uint8_t r=0;
	int bytessend,unit= 64;
	uint8_t data_buf[8];
	*(uint32_t *)data_buf = addr;
	*(uint32_t *)(&(data_buf[4])) = len;
	if(1)
	{
		{
			SW_SerialSendData(uart,USBSer_FILE_START, (uint8_t*)&len, sizeof(int)+1);
		//}while(!(r=WaitAck(BLE_FILE_START)));
		//	while((r=WaitAck(uart,BLE_FILE_START))!=1);
			do{
				ackstatus=WaitAck(uart,USBSer_FILE_START);
				if(ackstatus==USBSer_RET_TIME_OUT)
				{
					SW_SerialSendData(uart,USBSer_FILE_START, (uint8_t*)&len, sizeof(int)+1);
					ackstatus=0xff;
				}
			}while(ackstatus==0xff);
			gFileIndex = 0;
		}
		do{

			if(len<=0) break;
			if(len >= unit)
			{
				bytessend = unit;
				len -=unit;
			}else
			{
				bytessend = len;
				len = 0;
			}
				
			//do{
				SW_SerialSendData(uart,USBSer_FILE_DATA,(uint8_t*) buf, bytessend);
			//}while(1);//!(r=WaitAck(BLE_FILE_DATA)));
		//while(WaitAck(uart,BLE_FILE_DATA)!=1);
				do{
					ackstatus=WaitAck(uart,USBSer_FILE_START);
					if(ackstatus==USBSer_RET_TIME_OUT)
					{
						SW_SerialSendData(uart,USBSer_FILE_DATA,(uint8_t*) buf, len);
						ackstatus=0xff;
					}
				}while(ackstatus==0xff);
			gFileIndex++;
				buf += bytessend;
		}while(1);//len<unit);
		SW_SerialSendActionCMD(uart,USBSer_ACTION_PROGRAM, (uint8_t*)data_buf, sizeof(data_buf));
		do{
			ackstatus=WaitAck(uart,USBSer_ACTION_PROGRAM);
			if(ackstatus==USBSer_RET_TIME_OUT)
			{
				SW_SerialSendActionCMD(uart,USBSer_ACTION_PROGRAM, (uint8_t*)data_buf, sizeof(data_buf));
				ackstatus=0xff;
			}
		}while(ackstatus==0xff);
		SW_SerialSendData(uart,USBSer_FILE_END, &r, 1);
		do{
			ackstatus=WaitAck(uart,USBSer_FILE_START);
			if(ackstatus==USBSer_RET_TIME_OUT)
			{
				SW_SerialSendData(uart,USBSer_FILE_END, &r, 1);
				ackstatus=0xff;
			}
		}while(ackstatus==0xff);
		gFileIndex = 0;
	}
	return 0;
}

int SerialEraseTargetFlash(int uart,uint32_t addr,uint8_t pagecnt)
{
	 int ackstatus;;
	uint8_t data_buf[5];
	(*(uint32_t *)data_buf) = addr;
	data_buf[4] = pagecnt;
	SW_SerialSendActionCMD(uart,USBSer_ACTION_ERASE, (uint8_t*)&(data_buf[0]), sizeof(data_buf)+1);
	do{
		ackstatus=WaitAck(uart,USBSer_ACTION_ERASE);
		if(ackstatus==USBSer_RET_TIME_OUT)
		{
			SW_SerialSendActionCMD(uart,USBSer_ACTION_ERASE, (uint8_t*)&(data_buf[0]), sizeof(data_buf)+1);
			ackstatus=0xff;
		}
	}while(ackstatus==0xff);
	
	return ackstatus;
}
int SerialWriteTargetFlash(int uart,uint32_t addr,const char* buf,uint32_t len)
{
	 int ackstatus,rc;
	uint8_t data_buf[8];
	*(uint32_t *)data_buf = addr;
	*(uint32_t *)(&(data_buf[4])) = len;
	rc = SerialSendBuf(uart,addr,buf,len);
	if(rc != 0)
		return rc;
	//SW_SerialSendActionCMD(uart,USBSer_ACTION_PROGRAM, (uint8_t*)data_buf, sizeof(data_buf)+1);
//	do{
	//	ackstatus=WaitAck(uart,USBSer_ACTION_PROGRAM);
	//	if(ackstatus==USBSer_RET_TIME_OUT)
	//	{
	//		SW_SerialSendActionCMD(uart,USBSer_ACTION_PROGRAM, (uint8_t*)data_buf, sizeof(data_buf)+1);
	//		ackstatus=0xff;
	//	}
	//}while(ackstatus==0xff);

	return rc;
}
int SerialVerifyTargetFlash(int uart,uint32_t addr,uint32_t size,uint32_t crc)
{
	 int ackstatus;;
	uint8_t data_buf[12];
	(*(uint32_t *)data_buf) = addr;
	(*(uint32_t *)&(data_buf[4]))= size;
	(*(uint32_t *)&(data_buf[8]))= crc;
	SW_SerialSendActionCMD(uart,USBSer_ACTION_CRC, (uint8_t*)&(data_buf[0]), sizeof(data_buf)+1);
	do{
		ackstatus=WaitAck(uart,USBSer_ACTION_CRC);
		if(ackstatus==USBSer_RET_TIME_OUT)
		{
			SW_SerialSendActionCMD(uart,USBSer_ACTION_CRC, (uint8_t*)&(data_buf[0]), sizeof(data_buf)+1);
			ackstatus=0xff;
		}
	}while(ackstatus==0xff);
	if(ackstatus != 0)
		uP("Verify Failed!!!\r\n");
	else
		uP("Verify OK!!!\r\n");	
	return ackstatus;
}
int __get_fwtype(char *filename)
{
	if(strstr(filename,"STDCTFW"))
		return 0;
	else if(strstr(filename,"STAPPFW"))
		return 1;
	else if(strstr(filename,"GPSFW"))
		return 2;
	else if(strstr(filename,"BLEFW"))
		return 3;
	else
		return -1;
}

static char SPIFlashPath[4]; /* RAM disk logical drive path */

static uint8_t filename[28]={0};
static uint8_t time[32]={0};
SFHandle *filehandle=NULL;
int xyz[3];
int gHasWrite=-1;
void SendUSBSyncToLcd(int StartStop)
{
	if(StartStop)//1 start;0 stop
		CS_GPIO->ODR|=CS_BIT;
	else
		CS_GPIO->ODR&=~CS_BIT;			
	
}
extern int gHaRetried;
int BLEGetDataHandler(int uart,PSERIAL_PACKET_HEADER pheader, uint8_t * dataBuffer, uint32_t cbPacket)
{			// ignore non-download packet types
	uint8_t s=0;


	uint8_t *pdata;
	uint8_t r=0;
	uint16_t length=0;
	int ackstatus=0;
	int waitcount=0;
	uint32_t MemStart=0;
	uint32_t MemSize=0;

	USBSer_DATATYPE datatype;
	unsigned char blever[length];
	int i=0;
	int ret=DAP_OK;
	gEventID=pheader->PacketID;

	switch(pheader->pktType)
	{
		case USBSer_GET_DATA:
		{
			length=(((pheader->lenH)<<8)|(pheader->lenL));
			switch(pheader->dataType)
			{	
				case USBSer_DATA_SW_FIRMWARE:
				case USBSer_FILE_START:
				{
					memcpy(filename,dataBuffer,length+1);
					filehandle=SW_SF_Open((char *)filename,MODE_R);
					if(filehandle->size==0)
					{
						break;
					}else
					{
						gFileIndex=0;
						SW_SerialSendData(uart,pheader->dataType,filehandle,sizeof(SFHandle)+1);
						do
						{
							if(waitcount>10)
								break;
							waitcount++;
							ackstatus=WaitAck(uart,pheader->dataType);
							if(ackstatus==1)
							{

								break;
							}
							if(ackstatus==USBSer_RET_TIME_OUT||((waitcount%5)==0))
							{
								SW_SerialSendData(uart,pheader->dataType,filehandle,sizeof(SFHandle)+1);
								ackstatus=0xff;
								//waitcount=0;
							}

						}while(ackstatus==0xff);
						gFileIndex=0;
						if(ackstatus==1||waitcount>10)
							pheader->dataType=USBSer_FILE_END;
						else
							pheader->dataType=USBSer_FILE_DATA;
						waitcount=0;
				 }
				}
				case USBSer_FILE_DATA:
				{
					int bytesread;
					int i=0;
			
					do{
						SW_SF_Read(filehandle, data_t, FILE_DATA_BUF_SZ, (void *)&bytesread);
						if(bytesread)
						{
							SW_SerialSendData(uart,USBSer_FILE_DATA,data_t,bytesread);
							do
							{
								if(waitcount>20)
										break;
								waitcount++;
								ackstatus=WaitAck(uart,pheader->dataType);
								if(ackstatus==USBSer_RET_TIME_OUT||(waitcount==10))
								{
									SW_SerialSendData(uart,USBSer_FILE_DATA,data_t,bytesread);
									ackstatus=0xff;
	
								}
							}while(ackstatus==0xff);
							gFileIndex++;
							if(ackstatus==1||waitcount>20)
							{
								pheader->dataType=USBSer_FILE_END;
								break;
							}
							waitcount=0;
						}
					}while(bytesread==FILE_DATA_BUF_SZ);
					gFileIndex=0;
					pheader->dataType=USBSer_FILE_END;
				}
				case USBSer_FILE_END:
				{
					if(filehandle)
					{
						SW_SF_Close((void*)filehandle);
						filehandle=NULL;
					}
					SW_SerialSendData(uart,USBSer_FILE_END,&r,1);
					do
					{
						if(waitcount>10)
								break;
						waitcount++;
						ackstatus=WaitAck(uart,pheader->dataType);
						if(ackstatus==USBSer_RET_TIME_OUT||(waitcount==0))
						{
							SW_SerialSendData(uart,USBSer_FILE_END,&r,1);
							ackstatus=0xff;
						}
					}while(ackstatus==0xff);
					waitcount=0;
					break;
				}

				case USBSer_MEM_START:
				{
					extern xSapFile* GetSapFile(void);
					extern xSAPTable* 	GetSapTable(void);
					uint32_t ChipType =0;
					xSAPTable* xst = GetSapTable();
				//	xSapFile *xap=GetSapFile();
			//		parseSAPB(xap->sapid);
				
				
					MemStart=*((uint32_t *)dataBuffer);
					MemSize=*((uint32_t *)(dataBuffer+4));
					gFileIndex=0;
					//if(MemSize==0)
					//{
					//	MemStart=xap->ProgOffset+xap->startFlash;
					//	MemSize=xap->szData;
					//}
					memcpy(filename,((uint8_t *)&dataBuffer[0])+8,length-8+1);
					ChipType=Get_Quark_Chip();
				if(ChipType==(uint32_t)QUARK_D20XX||ChipType==QUARK_SE_C10XX)
				{
					SW_SerialSendData(uart,pheader->dataType,&ChipType,4);
				}else
				{
					SW_SerialSendData(uart,pheader->dataType,&ChipType,4);			
					return DAP_ERROR;
				}
					do
					{
							if(waitcount>10)
								break;
							waitcount++;
						ackstatus=WaitAck(uart,pheader->dataType);
						if(ackstatus==1) break;
						if(ackstatus==USBSer_RET_TIME_OUT||((waitcount%5)==0))
						{
							SW_SerialSendData(uart,pheader->dataType,filename,length);
							ackstatus=0xff;
							//waitcount=0;
						}

					}while(ackstatus==0xff);

					if(ackstatus==1||waitcount>10)
						pheader->dataType=USBSer_MEM_END;
					else
						pheader->dataType=USBSer_MEM_DATA;
					waitcount=0;
				//}
				//case USBSer_MEM_DATA:
				{
					int bytesread;
					int i=0;
					uint32_t MemOffset=0;	
					uint32_t MemLeft=0;	
					uint8_t  MemBlock = 1;
					MemOffset=MemStart;//MemStart;
					MemLeft=MemSize;
					if(MemSize==0)//dump all flash and otp
					{
						MemBlock=3;
						ChipType=Get_Quark_Chip();
						if(ChipType==(uint32_t)QUARK_D20XX)
						{

							MemOffset = D2000_Flash_Block_Add[0];
							MemLeft = D2000_Flash_Block_Size[0];
						}else
						{
							MemOffset = C1000_Flash_Block_Add[0];
							MemLeft = C1000_Flash_Block_Size[0];
						}
						//MemStart=xap->ProgOffset+xap->startFlash;
						//MemSize=xap->szData;
					}
					for(i=0;i<MemBlock;i++)
					{
					do{
						if(MemLeft==0)
							break;
						if(MemLeft>=FILE_DATA_BUF_SZ)
						{
							bytesread=FILE_DATA_BUF_SZ;
							MemLeft-=FILE_DATA_BUF_SZ;

						}else
						{
							bytesread=MemLeft;
							
						}
						GetMemoryFromTarget(data_t,MemOffset,bytesread);
						MemOffset=MemOffset+bytesread;
						SW_SerialSendData(uart,USBSer_MEM_DATA,data_t,bytesread);
						do{
								if(waitcount>20)
										break;	
								waitcount++;
								ackstatus=WaitAck(uart,pheader->dataType);
								if(ackstatus==USBSer_RET_TIME_OUT||((waitcount%10)==0))
								{
									SW_SerialSendData(uart,USBSer_MEM_DATA,data_t,bytesread);
									ackstatus=0xff;
									//waitcount=0;
								}
							}while(ackstatus==0xff);
						
							gFileIndex++;
							if(ackstatus==1||waitcount>20)
							{
								pheader->dataType=USBSer_MEM_END;
								break;
							}
							waitcount=0;		
					}while(bytesread==FILE_DATA_BUF_SZ);
						if(ChipType==(uint32_t)QUARK_D20XX)
						{

							MemOffset = D2000_Flash_Block_Add[i+1];
							MemLeft = D2000_Flash_Block_Size[i+1];
						}else
						{
							MemOffset = C1000_Flash_Block_Add[i+1];
							MemLeft = C1000_Flash_Block_Size[i+1];
						}
		
				}

							pheader->dataType=USBSer_MEM_END;
					
			}
				//case USBSer_MEM_END:
				{
					SW_SerialSendData(uart,USBSer_MEM_END,&r,1);
					do
					{
							if(waitcount>10)
								break;
							waitcount++;	
							ackstatus=WaitAck(uart,USBSer_MEM_END);
							if(ackstatus==USBSer_RET_TIME_OUT||((waitcount%5)==0))
							{
								SW_SerialSendData(uart,USBSer_MEM_END,&r,1);
								ackstatus=0xff;
							}
					}while(ackstatus==0xff);
										waitcount=0;

				}
					if(r!=0xF1)
						break;
			}				
				case USBSer_STATISTICS_FAIL:
				{
						uint32_t StatisticsFail=FailStatistics_CNT;	
						HAL_Delay(50);
						SW_SerialSendData(uart,pheader->dataType,&StatisticsFail,length);
						break;
				}
				case USBSer_STATISTICS_OK:
				{
						uint32_t StatisticsOK=Statistics_CNT;
						HAL_Delay(50);
						SW_SerialSendData(uart,pheader->dataType,&StatisticsOK,length);
						break;
				}					
						break;
				case USBSer_STATISTICS_SUM:
				{
						uint32_t StatisticsSUM=Statistics_CNT+FailStatistics_CNT;
						SW_SerialSendData(uart,pheader->dataType,&StatisticsSUM,length);
						break;
				}
						break;
				case USBSer_GET_VERSION:
				{
						uint8_t Ver_Num[16];
					#if 0
						#ifdef USE_BOOT
							uint32_t *pSWVer =(uint32_t *)0x8005020;
							uint32_t *pHWVer =(uint32_t *)0x8005024;
						#else
							uint32_t *pSWVer =(uint32_t *)0x8000020;
							uint32_t *pHWVer =(uint32_t *)0x8000024;
						#endif
					#endif
						Ver_Num[0]=MAR_VER;
						Ver_Num[1]='_';
						Ver_Num[2]=MIN_VER;
						Ver_Num[3]=DBG_VER;	
						Ver_Num[8]=DEV_VER;
						Ver_Num[9]='_';
						Ver_Num[10]='0';
						Ver_Num[11]=' ';
					
						//memcpy((char *)&Ver_Num[0],(char *)pSWVer,4);
						//memcpy((char *)&Ver_Num[0]+8,(char *)pHWVer,4);
						Ver_Num[4]=0;
						Ver_Num[8+4]=0;
//						strcpy((char *)&Ver_Num[0],(char *)"1.3");
						//strcpy((char *)&Ver_Num[0]+8,(char *)"2.0");
						SW_SerialSendData(uart,pheader->dataType,&Ver_Num[0],sizeof(Ver_Num)+1);
						gProgOnLine=1;
				}
						break;
					
				case  USBSer_DATA_LOG://response for log
						break;
				case USBSer_Data_ChipIndex:
				{
						uint32_t ChipIndex=*((uint8_t *)0x8002503);
						//   ChipIndex=1;
						SW_SerialSendData(uart,pheader->dataType,&ChipIndex,length);
						gProgOnLine=1;
						break;
				}
				case USBSer_FILE_Info:				
					memcpy(filename,dataBuffer,length+1);
					filehandle=SW_SF_Open((char *)filename,MODE_R);
					SW_SerialSendData(uart,pheader->dataType,filehandle,sizeof(SFHandle)+1);
					SW_SF_Close(filehandle);
					break;
				default:break;

			}
			break;
		}
		case USBSer_SEND_DATA:
		{
			uint32_t filesize=0;
			length=(((pheader->lenH)<<8)|(pheader->lenL));
			switch(pheader->dataType)
			{
				case USBSer_FILE_START:
				{
					{
						gFileIndex=0;
						gHasWrite=-1;
						SW_SerialSendAck(uart,pheader,0);
						SFHandle sfHandle;
						memcpy((uint8_t *)&sfHandle,(uint8_t *)dataBuffer,sizeof(sfHandle));
						
						strcpy(filename,sfHandle.FileName);
						//memcpy(filename,((uint8_t *)dataBuffer+SF_FILESIZE_SIZE),length-SF_FILESIZE_SIZE+1);
						//memcpy(filehandle->time,((uint8_t *)dataBuffer+SF_FILESIZE_SIZE+SF_FILENAME_SIZE),length-SF_FILENAME_SIZE-SF_FILESIZE_SIZE+1);
						//filesize=*((uint32_t *)dataBuffer);
						
						///if(filehandle==NULL)
						filehandle=SW_SF_Open((char *)filename,MODE_W);
						strcpy(filehandle->FileName,sfHandle.FileName);
						filehandle->size=sfHandle.size;
						strcpy(filehandle->time,sfHandle.time);
						EraseForFileBuf(filehandle->type,filehandle->size);
						
					}
					break;
				}
				case USBSer_FILE_DATA:
				{
					int byteswritten;
					m_rtc_time_t ctime;

					if(gHasWrite!=pheader->FileIndex)	
					{	
						if(gFileIndex==pheader->FileIndex)	
						{
							gFileIndex++;	
							SW_SF_Write(filehandle, dataBuffer,length, (void *)&byteswritten);
							gHasWrite=pheader->FileIndex;
							SW_SerialSendAck(uart,pheader,0);
						}else
							SW_SerialSendAck(uart,pheader,USBSer_RET_PACKET_LOSS);
					}else
					{
							SW_SerialSendAck(uart,pheader,0);
					}
					break;
				}
				case USBSer_FILE_END:
				{
					///	 		SW_FS_Close((void*)filehandle);
					gFileIndex=0;
					SW_SerialSendAck(uart,pheader,0);
					SW_SF_Close(filehandle); 
					filehandle=NULL;
					gHasWrite=-1;
					break;
				}
				case USBSer_FW_Update:
					SW_SerialSendAck(uart,pheader,0);
					break;

					default:
						break;
					}
			break;
		}
		case USBSer_ACTION:	
		{ 

			switch(pheader->dataType)
			{				
				case USBSer_ACTION_LOGONOFF:
				{
					length=(((pheader->lenH)<<8)|(pheader->lenL));
					if(strstr((char *)dataBuffer,"0"))
						dbg_deep=0;
					else if(strstr((char *)dataBuffer,"1"))
						dbg_deep=1;
					else if(strstr((char *)dataBuffer,"2"))
						dbg_deep=2;
					else if(strstr((char *)dataBuffer,"3"))
						dbg_deep=3;	
					HAL_Delay(50);
					SW_SerialSendAck(uart,pheader,0);					
					
				}
				break;
				case USBSer_ACTION_AUTOPROGRAM:
				{
					int retry,ret=0;
					run_led(2,0);
RETRY:
					ret=program_process();
					if(ret == DAP_AGAIN)
					{
						HAL_Delay(1000);
						goto RETRY;
					}
					if(ret!=DAP_OK)
					{
						run_led(3,0);	
						run_led(5,0);							
						FailStatistics_CNT++;
						WriteBufToSpiFlash((uint8_t *)&FailStatistics_CNT,FailStatistics_Offset,2,FAIL_STATISTICS);
						FailStatistics_Offset+=2;
						SW_SerialSendAck(uart,pheader,1);
						ReportMsg(IDS_CMD_PROGRAM,IDS_FAIL);
						break;
					}else if(ret==DAP_OK)
					{
						run_led(3,0);	
						run_led(4,0);	
						Statistics_CNT++;
						WriteBufToSpiFlash((uint8_t *)&Statistics_CNT,Statistics_Offset,2,STATISTICS);
						Statistics_Offset+=2;
						SW_SerialSendAck(uart,pheader,0);
						
							HAL_Delay(50);
		
					}	
					gHaRetried = 0;
					output_0v_io(); // cut off power to target
				}
				break;
				case USBSer_ACTION_CONNECT:
				{
					int retry=0;
					run_led(2,0);
						//		MyMemInit();
					cmdLine("sap");
					do{
						SetFunTodo(DO_CONNECT);
						//ret=cmdLine("auto");
						ret = Quark_Action_From_External(USBSer_ACTION_CONNECT);
						if(ret==DAP_AGAIN)
						{
							output_0v_io();
							retry++;
						}else if(ret!=DAP_OK)
						{
							run_led(5,0);
							vTaskDelay(200);
							ReportMsg(IDS_CONNECT,IDS_FAIL);
							SW_SerialSendAck(uart,pheader,1);

								output_0v_io();
								retry+=2;
								break;
						}
					}while(ret==DAP_AGAIN&&retry<=1);
					HAL_Delay(50);
					if(retry>1)
					{
							run_led(5,0);	
							vTaskDelay(200);
							ReportMsg(IDS_CONNECT,IDS_FAIL);
							SW_SerialSendAck(uart,pheader,1);	
							
					}
					else
					{
						SW_SerialSendAck(uart,pheader,0);
						vTaskDelay(200);
						uP("Target:Connected\r\n\r\n");	
					}
				}
				run_led(3,0);
				break;
				case USBSer_ACTION_BLANKCHECK:
				{				
					run_led(2,0);
					SetFunTodo(DO_BLANKCHECK);
					//ret=cmdLine("auto");
					ret = Quark_Action_From_External(USBSer_ACTION_BLANKCHECK);
					if(ret!=DAP_OK)
					{
						run_led(3,0);	
						run_led(5,0);	
						SW_SerialSendAck(uart,pheader,1);	
						ReportMsg(IDS_CMD_BLANK, IDS_FAIL);
						break;
					}
						HAL_Delay(50);
					run_led(4,0);
					SW_SerialSendAck(uart,pheader,0);
				}
							run_led(3,0);	
				break;
				case USBSer_ACTION_ERASE:
				{
					run_led(2,0);
					SetFunTodo(DO_ERASECHIP);
					//ret=cmdLine("auto");
					ret = Quark_Action_From_External(USBSer_ACTION_ERASE);					
					if(ret!=DAP_OK)
					{
						run_led(3,0);	
						run_led(5,0);
						SW_SerialSendAck(uart,pheader,1);
						ReportMsg(IDS_CMD_ERASE_CHIP,IDS_FAIL);
						break;
					}
					run_led(3,0);	
					run_led(4,0);	
					HAL_Delay(50);
					SW_SerialSendAck(uart,pheader,0);
				}

				break;			
				case USBSer_ACTION_PROGRAM:
				{
					int retry=0;
//					SendUSBSyncToLcd(1);
					run_led(2,0);
						SetFunTodo(DO_PROGRAM);
						//ret=cmdLine("auto");
						ret = Quark_Action_From_External(USBSer_ACTION_PROGRAM);						
						if(ret!=DAP_OK)
						{
							run_led(3,0);	
							run_led(5,0);							
							FailStatistics_CNT++;
							WriteBufToSpiFlash((uint8_t *)&FailStatistics_CNT,FailStatistics_Offset,2,FAIL_STATISTICS);
							FailStatistics_Offset+=2;
							SW_SerialSendAck(uart,pheader,1);
							ReportMsg(IDS_CMD_PROGRAM,IDS_FAIL);
							break;
						}else if(ret==DAP_OK)
						{
							run_led(3,0);	
							run_led(4,0);
							Statistics_CNT++;
							WriteBufToSpiFlash((uint8_t *)&Statistics_CNT,Statistics_Offset,2,STATISTICS);
							Statistics_Offset+=2;
								HAL_Delay(50);
							SW_SerialSendAck(uart,pheader,0);
						}				
						
				}
//				SendUSBSyncToLcd(0);
				break;
				case USBSer_ACTION_CRC:
				{
					run_led(2,0);
					SetFunTodo(DO_CRC32);
					//ret=cmdLine("auto");
					ret = Quark_Action_From_External(USBSer_ACTION_CRC);						
					if(ret!=DAP_OK)
					{
						run_led(3,0);	
						run_led(5,0);
						SW_SerialSendAck(uart,pheader,1);	
						ReportMsg(IDS_CMD_VERIFY,IDS_FAIL);
						if(ret==DAP_AGAIN)
						{
							output_0v_io();
							HAL_Delay(50);
						}
						break;
					}
					run_led(3,0);	
					run_led(4,0);
					HAL_Delay(50);
					SW_SerialSendAck(uart,pheader,0);
				}

				break;
				case USBSer_ACTION_DISCONNECT:
				{

					output_0v_io();
						HAL_Delay(50);
					run_led(3,0);
					run_led(4,0);
					ret = Quark_Action_From_External(USBSer_ACTION_DISCONNECT);	
					SW_SerialSendAck(uart,pheader,0);
					vTaskDelay(200);
					uP("Target:DisConnected\r\n");

				}
				break;
					case USBSer_ACTION_RESETSTATISTICS:
				{
					ResetStatistics();
					HAL_Delay(50);
					SW_SerialSendAck(uart,pheader,0);
				}
				break;									
			}
		}		
	
		case USBSer_ACK:
		break;
		
		default:
		break;
	}
}

int SW_BLE_SendFile(char* filename)
{
	if(g_bHasReq) return -1;
	{
			g_bHasReq = 1;
			int len = strlen(filename)+1;
			gHeadReq.pktType = USBSer_GET_DATA;
			gHeadReq.dataType = USBSer_FILE_START ;
		  gHeadReq.lenH = 0; gHeadReq.lenL = len; 
			memcpy(gDataReq,filename,len);	
	}
	return 0;
}
int SW_BLE_FirmwareUpgrade(char* filename)
{
	if(g_bHasReq) return -1;
	{
			g_bHasReq = 1;
			int len = strlen(filename)+1;
			gHeadReq.pktType = USBSer_GET_DATA;
			gHeadReq.dataType = USBSer_DATA_SW_FIRMWARE ;
		  gHeadReq.lenH = 0; gHeadReq.lenL = len; 
			memcpy(gDataReq,filename,len);	
	}
	return 0;
}

void GetMemoryFromTarget(uint8_t *buf,uint32_t addr,uint32_t size)
{
	static int gHasConnected=0;
	xDAPPacket xPacket = {0,0,0};
	
	Jtag_read_targetmem(&xPacket,addr, size, buf);
}
int SendHeartAck(void)
{
	#ifdef __ONLINE__	
	SERIAL_PACKET_HEADER header;
    uint16_t cbPacket = sizeof(dataBuffer);;
    int uart=(int)0;
				int len = 1;
		 // read from port
		//SW_SerialSendData(uart,0x00,fff,256);
	    if (gProgOnLine)//xap->logging > 0) 
		{

			header.pktType = USBSer_Heart;
		  header.lenH = 0; gHeadReq.lenL = len; 
		 cbPacket = header.lenL ;
			SW_SerialSendHeartAck(uart,&header,0);
		}
	#endif
}

#ifdef __ONLINE__ // _MTK_DEBUG_
int mtk_printf_usbser(char *str, int value)
{

	int len=strlen(str);
	char * pChar = str;
	char strBuf[25]={0};

		SW_SerialSendData(0,USBSer_DATA_LOG,pChar,len);

	if (value != -1)
	{
		sprintf(strBuf, "\t0x%08X\n\r", value);
		//itoa(value, strBuf,16);
		mtk_printf_usbser(strBuf, -1);
	}	
	return len;

}
#endif
