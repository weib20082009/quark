#include "common.h"
//#include "usb_serial.h"
#include "usb_serial_cmd.h"
#define KITLOutputDebugString(x,...) do{}while(0)
//static uint8_t g_buffer[KITL_MTU];
uint32_t g_dwTimeout=200;
extern	uint16_t gEventID;
extern uint32_t	gFileIndex;
//static int g_uartChannel= WICED_UART_USB;
#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

//------------------------------------------------------------------------------
//
//  Function:  SerialReadData
//
//  Keep getting peices of the image as the download progresses.
//
uint8_t fff[64]={0};
int SW_SerialSendData(int uart,USBSer_DATATYPE dataType,uint8_t* data, uint16_t len );
static uint8_t dataBuffer[KITL_MTU] = {0};

/*******************************************USB Serial************************/

void UartServiceEntry (void* param)
{
 //   static uint32_t dwBlockNumber = 0;
    static uint16_t cbDataBuffer = 0;

	int i=0;
    // the first uint32_t in the local g_buffer is the block header which contains
    // the sequence number of the block received
   // static PSERIAL_BLOCK_HEADER pBlockHeader = (PSERIAL_BLOCK_HEADER)dataBuffer;
   // static uint8_t* pBlock = dataBuffer + sizeof(SERIAL_BLOCK_HEADER);


    SERIAL_PACKET_HEADER header;
    uint16_t cbPacket;
    int uart=(int)param;
#if 0
		for(i=0;i<64;i++)
		{
    	fff[i]=i;
		}
		do
		{
			//SW_CDC_Send(fff, 64 );
			SW_UartSend(uart,fff,64);
			//tx_thread_sleep(10);
			HAL_Delay(10);
		}while(1);

	while(0)
	{
		uint8_t val;
		if(SW_UartReceive(uart,&val,1,g_dwTimeout))
    {
			continue;
    }else
			SW_UartSend(uart,&val,1);
		
	}
	#endif
	//while(1)
	{
		cbPacket = sizeof(dataBuffer);
		 if(RecvPacket(uart,&header, dataBuffer, &cbPacket, TRUE))
		 {
			 FreeUserBuffer();
			 ResetCDCRecvBuff();

			 BLEGetDataHandler(uart,&header, dataBuffer, cbPacket);

		 }
	}
}

//------------------------------------------------------------------------------
//
//  Function:  CalcChksum
//
//  Calculate checksum to insert into a block before we send. Also check what we receive.
//
uint8_t CalcChksum(uint8_t* pBuf, uint16_t len)
{
    uint16_t s = 0;
    uint8_t csum = 0;

    for(s = 0; s < len; s++)
        csum += *(pBuf + s);

    return csum;
}
//------------------------------------------------------------------------------
//
//  Function:  SerialSendBootRequest
//
//  Send a BOOTME to the desktop to establish a connection.
//
int SW_SerialSendRequest(int uart,uint8_t dataType, uint8_t* data, int len)
{
//    uint8_t g_buffer[sizeof(SERIAL_PACKET_HEADER) + sizeof(SERIAL_BOOT_REQUEST)] = {0};
	uint8_t g_buffer[sizeof(PSERIAL_PACKET_HEADER)]={0};
	PSERIAL_PACKET_HEADER pHeader = (PSERIAL_PACKET_HEADER)g_buffer;
    uint8_t* pData = data+ sizeof(PSERIAL_PACKET_HEADER);
    // create header
    memcpy(pHeader->headerSig, packetHeaderSig, HEADER_SIG_BYTES);
    pHeader->pktType = USBSer_GET_DATA;
    pHeader->dataType = dataType;
    SetPayloadSize( pHeader,len);

    memcpy(pData,data,len);
    *(pData+len) = CalcChksum((uint8_t*)g_buffer, sizeof(SERIAL_PACKET_HEADER) + len );
    SW_UartSend(uart,g_buffer, sizeof(SERIAL_PACKET_HEADER) + len +1 );
    return TRUE;
}
#if 0
//------------------------------------------------------------------------------
//
//  Function:  SerialSendBlockAck
//
//  Acknowledge a block has been received from the desktop.
//
int SerialSendBlockAck(uint32_t uBlockNumber)
{
	uint8_t g_buffer[sizeof(SERIAL_PACKET_HEADER) + sizeof(SERIAL_BLOCK_HEADER)];
    PSERIAL_PACKET_HEADER pHeader = (PSERIAL_PACKET_HEADER)g_buffer;
    uint8_t* pBlockAck = (g_buffer + sizeof(SERIAL_PACKET_HEADER));

    // create block ack
    pBlockAck[0] = (BYTE)(uBlockNumber >> 0) & 0xFF;
    pBlockAck[1] = (BYTE)(uBlockNumber >> 8) & 0xFF;
    pBlockAck[2] = (BYTE)(uBlockNumber >> 16) & 0xFF;
    pBlockAck[3] = (BYTE)(uBlockNumber >> 24) & 0xFF;

    // create header
    memcpy(pHeader->headerSig, packetHeaderSig, HEADER_SIG_BYTES);
    pHeader->pktType = KS_PKT_DLACK;
    pHeader->payloadSize = sizeof(SERIAL_BLOCK_HEADER);
    pHeader->crcData = CalcChksum((uint8_t*)pBlockAck, sizeof(SERIAL_BLOCK_HEADER));
    pHeader->crcHdr = CalcChksum((uint8_t*)pHeader,
        sizeof(SERIAL_PACKET_HEADER) - sizeof(pHeader->crcHdr));

    OEMSerialSendRaw(g_buffer, sizeof(SERIAL_PACKET_HEADER) + sizeof(SERIAL_BLOCK_HEADER));

    return TRUE;
}
#endif
int SW_SerialSendAck(int uart,PSERIAL_PACKET_HEADER pHeaderIn,uint8_t status)
{
    uint8_t g_buffer[sizeof(SERIAL_PACKET_HEADER) + 1];
    PSERIAL_PACKET_HEADER pHeader = (PSERIAL_PACKET_HEADER)g_buffer;
    uint8_t* pData = (g_buffer + sizeof(SERIAL_PACKET_HEADER));
    int len = 1;
    memcpy(pHeader, pHeaderIn , sizeof(SERIAL_PACKET_HEADER));
    // create header
    pHeader->pktType = USBSer_ACK;
    pHeader->lenL = 1; pHeader->lenH = 0;
		pHeader->PacketID	= gEventID;

    *pData = status;
    *(pData+len) = CalcChksum((uint8_t*)g_buffer, sizeof(SERIAL_PACKET_HEADER) + len );
     SW_UartSend(uart,g_buffer, sizeof(SERIAL_PACKET_HEADER) + len +1 );

    return TRUE;
}
int SW_SerialSendHeartAck(int uart,PSERIAL_PACKET_HEADER pHeaderIn,uint8_t status)
{
    uint8_t g_buffer[sizeof(SERIAL_PACKET_HEADER) + 1];
    PSERIAL_PACKET_HEADER pHeader = (PSERIAL_PACKET_HEADER)g_buffer;
    uint8_t* pData = (g_buffer + sizeof(SERIAL_PACKET_HEADER));
    int len = 1;
    memcpy(pHeader, pHeaderIn , sizeof(SERIAL_PACKET_HEADER));
    // create header
    pHeader->pktType = USBSer_Heart;
    pHeader->lenL = 1; pHeader->lenH = 0;
		pHeader->PacketID	= gEventID;

    *pData = status;
    *(pData+len) = CalcChksum((uint8_t*)g_buffer, sizeof(SERIAL_PACKET_HEADER) + len );
     SW_UartSend(uart,g_buffer, sizeof(SERIAL_PACKET_HEADER) + len +1 );

    return TRUE;
}
int SW_SerialSendData(int uart,USBSer_DATATYPE dataType,uint8_t* data, uint16_t len )
{
    uint8_t g_buffer[sizeof(SERIAL_PACKET_HEADER) + len+1];
    unsigned long written = 0;
    int i=0;
    PSERIAL_PACKET_HEADER pHeader = (PSERIAL_PACKET_HEADER)g_buffer;
    uint8_t* pData = (g_buffer + sizeof(SERIAL_PACKET_HEADER));
    //memcpy(pHeader, pHeaderIn , sizeof(SERIAL_PACKET_HEADER));
    // create header
	pHeader->headerSig[0] = packetHeaderSig[0];
	pHeader->pktType = USBSer_SEND_DATA;
	pHeader->dataType  = dataType;
	pHeader->PacketID	= gEventID;
	pHeader->FileIndex=gFileIndex;//avoid the repeat packet
	SetPayloadSize(pHeader, len);
	memcpy(pData,data,len);

    *(pData+len) = CalcChksum((uint8_t*)g_buffer, sizeof(SERIAL_PACKET_HEADER) + len );
    SW_UartSend(uart,g_buffer, sizeof(SERIAL_PACKET_HEADER) + len +1 );//weib modify for debug -1009
	//SerialSend(g_buffer, sizeof(SERIAL_PACKET_HEADER) + len +1 );
#if 0
///    myprintf("\r\n");
///    myprintf("Send data to BLE:\r\n");
///    for(i=0;i<0x10;i++)
///    	myprintf("%x ",g_buffer[i]);
///    myprintf("\r\n");
#endif
    return TRUE;
}
int SW_SerialSendActionCMD(int uart,uint8_t dataType, uint8_t* data, int len)
{
//    uint8_t g_buffer[sizeof(SERIAL_PACKET_HEADER) + sizeof(SERIAL_BOOT_REQUEST)] = {0};
		int written;
		uint8_t g_buffer[32];
    PSERIAL_PACKET_HEADER pHeader = (PSERIAL_PACKET_HEADER)g_buffer;
    uint8_t* pData = g_buffer+ sizeof(SERIAL_PACKET_HEADER);
    // create header
    memcpy(pHeader->headerSig, packetHeaderSig, HEADER_SIG_BYTES);
    pHeader->pktType = USBSer_ACTION;
    pHeader->dataType = dataType;
    SetPayloadSize( pHeader,len);
	pHeader->PacketID=gEventID;
    memcpy(pData,data,len);
    *(pData+len) = CalcChksum((uint8_t*)g_buffer, sizeof(SERIAL_PACKET_HEADER) + len );
    //SW_UartSend(g_uartChannel,g_buffer, sizeof(SERIAL_PACKET_HEADER) + len +1 );
	SW_UartSend(uart,g_buffer, sizeof(SERIAL_PACKET_HEADER) + len +1 );
    return TRUE;
}
//------------------------------------------------------------------------------
//
//  Function:  RecvPacket
//
//  SerialReadData helper function. Eventually cals USBSerialRecvRaw.
//
m_rtc_time_t ctime;
int RecvPacket(int uart, PSERIAL_PACKET_HEADER pHeader, uint8_t* pbFrame, uint16_t* pcbFrame, int bWaitInfinite)
{
    // receive header
	int payloadSize =0;
	uint8_t err = 0;
    if(!RecvHeader(uart,pHeader, bWaitInfinite))
    {
    	KITLOutputDebugString("failed to receive header\r\n");
        return FALSE;
    }
    payloadSize=GetPayloadSize(pHeader);
    // make sure sufficient g_buffer is provided
    if(*pcbFrame < payloadSize)
    {
    	KITLOutputDebugString("insufficient g_buffer size; ignoring packet\r\n");
        return FALSE;
    }

    // receive data
    *pcbFrame = payloadSize;
    //if(!OEMSerialRecvRaw(pbFrame, pcbFrame, bWaitInfinite))
    if(SW_UartReceive(uart,pbFrame,*pcbFrame+1,g_dwTimeout))
    {
    	 err = 1;
        KITLOutputDebugString("failed to read packet data\r\n");
        return FALSE;
    }
    // verify data checksum
		uint8_t crc=CalcChksum((uint8_t*)pHeader,sizeof(SERIAL_PACKET_HEADER))+CalcChksum(pbFrame, *pcbFrame);
    if(crc!=pbFrame[*pcbFrame])
    {
    	 err = 1;
        KITLOutputDebugString("data checksum failure. expected 0x%X, received 0x%X\r\n",pHeader->crcData,CalcChksum(pbFrame,*pcbFrame));

        return FALSE;
    }
    //SerialSendAck(pHeader, err);
    return TRUE;
}


//------------------------------------------------------------------------------
//
//  Function:  RecvHeader
//
//  SerialReadData helper function. Eventually cals USBSerialRecvRaw.
//
int RecvHeader(int uart,PSERIAL_PACKET_HEADER pHeader, int bWaitInfinite)
{
    uint16_t cbRead;
    uint32_t i = 0;
    cbRead = sizeof(SERIAL_PACKET_HEADER);
    if(SW_UartReceive(uart,pHeader,cbRead,g_dwTimeout))
    {
         return FALSE;
    }
 
    return TRUE;
}
#undef NO_ACK
//#define NO_ACK
#ifndef NO_ACK
int WaitAck(int uart,uint8_t type)
{
	SERIAL_PACKET_HEADER header;
    uint8_t g_buffer[sizeof(SERIAL_PACKET_HEADER)+1];
    int i=0;
	unsigned short cbPacket =5;//= sizeof(g_buffer);
    if(RecvPacket(uart,&header, g_buffer, &cbPacket, TRUE))
	{
		uint8_t status = g_buffer[0] ;
		uint8_t *buf=(uint8_t *)&header;
		if(uart == USB_CDC_SERIAL)
		{
			 FreeUserBuffer();
			 ResetCDCRecvBuff();	
		}			
#if 0
		 for(i=0;i<sizeof(SERIAL_PACKET_HEADER);i++)
///			 myprintf("%x ",buf[i]);
	    for(i=0;i<cbPacket;i++)
///	    	myprintf("%x ",g_buffer[i]);
///	    myprintf("\r\n");
#endif
		if(header.pktType == USBSer_ACK)
		{
			return status;
		}

	}
///    myprintf("NO Recv ACK\r\n");
	return 0xff;
}
#else
int WaitAck(int uart,uint8_t type)
{
	return 1;
}
#endif

