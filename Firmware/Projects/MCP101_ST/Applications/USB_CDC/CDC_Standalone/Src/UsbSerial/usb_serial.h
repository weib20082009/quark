
//  Common RS232/USB Serial routines .
//
#ifndef __OAL_BLSERIAL_H
#define __OAL_BLSERIAL_H

typedef struct
{
    uint8_t sec;
    uint8_t min;
    uint8_t hr;
    uint8_t weekday;/* 1-sunday... 7-saturday */
    uint8_t date;
    uint8_t month;
    uint8_t year;
}m_rtc_time_t;
#if 0
//cmd  definition
typedef enum __BLE_COMMAND_t
{
BLE_GET_DATA=0xA1,
BLE_SEND_DATA,
BLE_ACK,
BLE_SLEEP
//BLE_SLEEP
}BLE_CMD;
#endif


typedef enum __BLE_STATUS_CODE
{
	BLE_NO_CONNECT = 0xAA01,
	BLE_PROFILE_OPEN,
	BLE_PROFILE_CLOSE,
	BLE_CHECKSUM_ERROR,
	BLE_VAULE_ERROR,
	BLE_SYSTEM_BUSY
}BLE_STATUS_CODE;

typedef enum __BLE_ERROR_CODE
{

	BLE_ERROR_PACKET_LOSS = 0xA601,
	BLE_ERROR_CHECKSUM_ERROR,
	BLE_ERROR_FEAT_NOT_SUPPORT,
	BLE_ERROR_CMD_NOT_SUPPORT
}BLE_ERROR_CODE;

typedef enum _BLE_RET_CODE_
{
	BLE_RET_OK = 0,
	BLE_RET_CMD_ERR,
	BLE_RET_TYPE_ERR,
	BLE_RET_CHECKSUM_ERR,
	BLE_RET_TIME_OUT,
	BLE_RET_BUSY,
	BLE_RET_FLAS_CHECKSUM_ERR,
	BLE_RET_NO_CONNECT_CLIENT,
	BLE_RET_MISS_START,
	BLE_RET_FILENAME_TOOLONG
} BLE_RET_CODE;


//------------------------------------------------------------------------------






#if __cplusplus
extern "C" {
#endif

// Use the serial bootloader in conjunction with the serial kitl transport
// Both the transport and the download service expect packets with the
// same header format. Packets not of type DLREQ, DLPKT, or DLACK are
// ignored by the serial bootloader.
// serial packet header and trailer definitions

#define __in
static const uint8_t packetHeaderSig[] = { 0xA5 };
#define HEADER_SIG_BYTES    sizeof(packetHeaderSig)

#define KS_PKT_KITL         0xAA
#define KS_PKT_DLREQ        0xBB
#define KS_PKT_DLPKT        0xCC
#define KS_PKT_DLACK        0xDD
#define KS_PKT_JUMP         0xEE
#define KITL_MTU  256

// packet header
int WaitAck(int uart, uint8_t type);

typedef struct tagSERIAL_PACKET_HEADER {
    uint8_t headerSig[HEADER_SIG_BYTES];
    uint8_t pktType;
    uint8_t dataType;
    uint8_t lenH;
    uint8_t lenL; // not including this header
//    uint8_t crcData;
//    uint8_t crcHdr;
} __attribute__ ((packed)) SERIAL_PACKET_HEADER, *PSERIAL_PACKET_HEADER;


typedef struct SERIAL_BOOT_ACK {
    uint32_t fJumping;
} SERIAL_BOOT_ACK, *PSERIAL_BOOT_ACK;


typedef struct SERIAL_BLOCK_HEADER {
    uint32_t uBlockNum;
} SERIAL_BLOCK_HEADER, *PSERIAL_BLOCK_HEADER;

int
RecvHeader(
		int uart,
    __in PSERIAL_PACKET_HEADER pHeader, 
    int bWaitInfinite
    );

int
RecvPacket(
		int uart,
    __in PSERIAL_PACKET_HEADER pHeader, 
    __in uint8_t* pbFrame,
    __in uint16_t* pcbFrame,
    int bWaitInfinite
    );

int
SerialSendBlockAck(
    uint32_t uBlockNumber
    );

int
SerialSendBootRequest(
    __in const char * platformString
    );

int
SerialReadData(
    uint32_t cbData,
    uint8_t* pbData
    );
static inline uint16_t GetPayloadSize(PSERIAL_PACKET_HEADER pHeader)
{
	uint16_t l = pHeader->lenH;
	l <<= 8;
	l+= pHeader->lenL;
	return l;
}
static inline void SetPayloadSize(PSERIAL_PACKET_HEADER pHeader,uint16_t len)
{
	pHeader->lenH = len>>8;
	pHeader->lenL = len&0xFF;
}
//------------------------------------------------------------------------------

#if __cplusplus
}
#endif

#endif
