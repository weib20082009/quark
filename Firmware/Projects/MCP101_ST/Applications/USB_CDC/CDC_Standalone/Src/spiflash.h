#ifndef SPIFLASH_H_
#define SPIFLASH_H_
#include "common.h"


#define u32 uint32_t
#define u16 uint16_t
#define u8  uint8_t
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define SPI_FLASH_SectorSize         4096
#define SPI_FLASH_PageSize           256
#define SPI_FLASH_PerWritePageSize   256

int spiflash_read(uint8_t *buf,uint32_t address,uint32_t length);

int spiflash_write(uint8_t *buf,uint32_t address,uint32_t length);

int spiflash_erase(uint32_t address,uint32_t length);


int spiflash_size(void);

int spiflash_probe(uint8_t bus, uint8_t cs);



#endif

