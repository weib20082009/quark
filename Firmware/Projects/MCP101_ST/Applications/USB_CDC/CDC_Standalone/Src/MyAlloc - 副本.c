
#include "stdlib.h"

#include "MyAlloc.h" 
extern int gProgOnGoing;
static  struct Mem_List_t *Mem_Pages=NULL;
static int MemPageCnt=0;

void *MyMalloc(size_t size)
{
	void *t=malloc(size);
	struct Mem_List_t **p= &(Mem_Pages->next);
	int cnt=0;
	if(gProgOnGoing)
	{
		if(t)
		{
			while (*p)
			{
				p=&((*p)->next);
				cnt++;
			}
			if(!*p)
			{
				*p=malloc(sizeof( struct Mem_List_t));
				if(!*p)
					return NULL;
				(*p)->next = NULL;
			}
			(*p)->address=t;
		}
		if(cnt!=MemPageCnt)
			return 0;
		MemPageCnt++;
	}

	return t; 
}
void *MyCalloc(size_t nmemb, size_t size)
{
	struct Mem_List_t **p= &(Mem_Pages->next);
	void *t=calloc(nmemb,size);
	int cnt=0;
	if(gProgOnGoing)
	{
		if(t)
		{
			while (*p)
			{
				p=&((*p)->next);
				cnt++;
			}
			if(!*p)
			{
				*p=malloc(sizeof( struct Mem_List_t));
				if(!*p)
					return NULL;
				(*p)->next = NULL;
			}
			(*p)->address=t;
		}
		if(cnt!=MemPageCnt)
			return 0;
		MemPageCnt++;
	}

	return t; 
}
void *MyRealloc(void * ptr, size_t size)
{
	struct Mem_List_t **p= &(Mem_Pages->next);
	struct Mem_List_t *t= Mem_Pages;
	void *rt=realloc(ptr,size);
	if(gProgOnGoing)
	{
		while(*p)
		{
			
			if((*p)->address!=ptr)
			{
				t=*p;
				p=&((*p)->next);
			}
			else
			{
				if(rt)
					(*p)->address=rt;
				else
				{
					if((*p)->next)
						t->next=(*p)->next;
					else
						t->next=NULL;
					free(*p);
					*p=NULL;
				}
			}
		}
		MemPageCnt++;
	}

	return rt; 
}
void MyFree(void *ptr)
{
	struct Mem_List_t **p=&(Mem_Pages->next);
	struct Mem_List_t *t= Mem_Pages;
	int cnt=0,cnt2;
	if(gProgOnGoing)
	{		
		while(*p)
		{
	
			if((*p)->address!=ptr)
			{
				t=*p;
				p=&((*p)->next);
				
			}
			else
			{
				if((*p)->next)
					t->next=(*p)->next;
				else
					t->next=NULL;
				free(*p);
				break;
			}
		}
		
		if(MemPageCnt>0)
		MemPageCnt--;
	}
	#if 0
	p=&(Mem_Pages->next);
	while (*p)
	{
		p=&((*p)->next);
		cnt++;
	}
	if(cnt!=MemPageCnt)
		cnt2=0;
	#endif
	free(ptr);
}
void MyMemInit(void)
{
	Mem_Pages=malloc(sizeof(struct Mem_List_t));//head 
	Mem_Pages->address=0;
	Mem_Pages->next=0;
	MemPageCnt=0;

}
void MyMemReInit(void)
{
		struct Mem_List_t *p= Mem_Pages->next;		
		while(p)
		{	
			struct Mem_List_t *t= p;	
			free(p->address);
			p=p->next;
			free(t);
			if(MemPageCnt>0)
				MemPageCnt--;			
		}
		free(Mem_Pages);
		Mem_Pages=NULL;
}