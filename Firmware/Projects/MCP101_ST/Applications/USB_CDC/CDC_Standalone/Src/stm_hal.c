
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "stm3210e_eval.h"
#include "stm32f10x_hal_usart.h"
#include "stm32f10x_hal_gpio.h"
#include "stm32f10x_hal_spi.h"
#include "stm32f10x_hal_adc.h"
#include "stm32f10x_hal_i2c.h"
#include "common.h"
#include "sst25vf.h"
USART_TypeDef* UART = USART3;  // Using UART1 for debug

#define NULL 0


int uart_init(int number, int baud)
{
	USART_InitTypeDef USART_InitStruct;
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_ClockInitTypeDef USART_ClockInitStruct;
	
   if(UART == USART3)
   {	   
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;			// Tx Pin
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init( GPIOB, &GPIO_InitStructure );
			
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;			// Rx Pin
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init( GPIOB, &GPIO_InitStructure );
	
   }else if(UART == USART1){
	   
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;			// Tx Pin
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init( GPIOA, &GPIO_InitStructure );
			
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;			// Rx Pin
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	   
   }
	
	USART_ClockStructInit( &USART_ClockInitStruct );
	USART_ClockInit( UART, &USART_ClockInitStruct  );

	USART_StructInit(&USART_InitStruct);
	USART_InitStruct.USART_BaudRate = baud;
	USART_Init(UART, &USART_InitStruct);
	
	USART_Cmd(UART, ENABLE);  
	return 0;
}

int sendchar (int ch)  {                 /* Write character to Serial Port */

   USART_SendData(UART,ch);

  /* Loop until the end of transmission */
  while(!(USART_GetFlagStatus(UART, USART_FLAG_TC) == SET));
	return ch;
}

int getkey (void)  {                     /* Read Character from Serial Port */

  while(!(USART_GetFlagStatus(UART, USART_FLAG_RXNE) == SET));
 
    return USART_ReceiveData(UART);
}

int getkey_nowait (void)  {                     /* Read Character from Serial Port */
  int ii;
  for(ii=0;ii<400;ii++)
    if((USART_GetFlagStatus(UART, USART_FLAG_RXNE) == SET))
      return USART_ReceiveData(UART);                    /* Read Character */
 
	return 0;
}
#if 0 // _MTK_DEBUG_
int mtk_printf(char *str, int value)
{

	int len=strlen(str);
	char * pChar = str;
	char strBuf[25]={0};

    for(;len>0;len--)
	{
		sendchar(*pChar++);
	}
	if (value != -1)
	{
		sprintf(strBuf, "\t0x%08X\n\r", value);
		//itoa(value, strBuf,16);
		mtk_printf(strBuf, -1);
	}	
	return len;

}
#endif	
int mtk_char_printf(char *str,unsigned char value)
{
#if 1 // _MTK_DEBUG_
	int len=strlen(str);
	char * pChar = str;
	char strBuf[25]={0};

    for(;len>0;len--)
	{
		sendchar(*pChar++);
	}
	if (value !=0xff)
	{
		sprintf(strBuf, "%02X", value);
		//itoa(value, strBuf,16);
		mtk_printf(strBuf, -1);
	}	
	return len;
#endif	
}
#if 0
void printf_memory( void *str, int len)
{
	int ii,value,jj=0;
	char * ptr = (void*)str;
		
	mtk_printf("\n\r", -1);
	for(ii=len;ii>0;ii--,ptr++,jj++)
	{
	  if((jj%4) == 0)mtk_printf(" ",-1);
	  if((jj%8) == 0)mtk_printf(" ",-1);
	  if((jj%32) == 0)mtk_printf("\n\r",-1);
	  mtk_char_printf(" ", *ptr);
	}
	mtk_printf("\n\r", -1);
	  
}
#endif
/* ******************** */
/* GPIO for LED, Button, Voltage control */
/* ******************** */

#define LED_GPIO        GPIOA
#define LED_G_GPIO      GPIOA
#define LED_G_BIT       GPIO_Pin_9

#define LED_R_GPIO      GPIOA
#define LED_R_BIT       GPIO_Pin_10

#define LED_Y_GPIO      GPIOA
#define LED_Y_BIT       GPIO_Pin_2

#define BUTTON_GPIO      GPIOA
#define BUTTON_BIT       GPIO_Pin_3

#define VDD_DETECT_GPIO      GPIOB
#define VDD_DETECT_BIT       GPIO_Pin_0

#define VDD_5V_O_GPIO      GPIOB
#define VDD_5V_O_BIT       GPIO_Pin_1
#define VDD_V_EN_GPIO      GPIOB
#define VDD_V_EN_BIT       GPIO_Pin_3
#define VDD_3V3_O_GPIO      GPIOC
#define VDD_3V3_O_BIT       GPIO_Pin_6
#define VDD_2V5_O_GPIO      GPIOC
#define VDD_2V5_O_BIT       GPIO_Pin_7
#define VDD_1V8_O_GPIO      GPIOC
#define VDD_1V8_O_BIT       GPIO_Pin_8

#define ERASE_PIN_GPIO		GPIOB
#define ERASE_PIN_BIT		GPIO_Pin_10

#define EXT_DIR1_GPIO		GPIOC
#define EXT_DIR1_BIT		GPIO_Pin_0

#define EXT_DIR2_GPIO		GPIOC
#define EXT_DIR2_BIT		GPIO_Pin_4
void led_green_on()
{
	 LED_G_GPIO->ODR &= ~LED_G_BIT;	
}	
void led_green_off()
{
	LED_G_GPIO->ODR |= LED_G_BIT;	
}	

void led_red_on()
{
	 LED_R_GPIO->ODR  &= ~LED_R_BIT;
}	
void led_red_off()
{
	LED_R_GPIO->ODR |= LED_R_BIT;	
}	
void led_yellow_on()
{
	 LED_Y_GPIO->ODR &= ~LED_Y_BIT;	
}	
void led_yellow_off()
{
	LED_Y_GPIO->ODR  |= LED_Y_BIT;	
}	

uint8_t led_status = 0;  // 0bit - green on, 1bit - red on, 2bit - yellow on
						 // 4bit - green blink, 5bit - red blink, 6 bit - yellow blink
int led_blink_id;

void led_blinking(void)
{
	if(led_status & 0x10)  //green
	{ 
		if(led_status & 1)
		{	led_green_off();led_status &= ~1;}
		else  if((led_status & 1) == 0)			
		{	led_green_on();led_status |= 1;}
	}
		
	if(led_status & 0x20) //red
	{	
		if(led_status & 2)
		{	led_red_off();led_status &= ~2;}
		else  if((led_status & 2) == 0)			
		{	led_red_on();led_status |=  2;}
	}
	if(led_status & 0x40)
		if(led_status & 4)
		{ led_yellow_off(); led_status &= ~4;}
		else if((led_status & 4) == 0)		
		{ led_yellow_on(); led_status |=  4;}
}

void led_green_blink()
{
	led_status |= 0x10;
	led_blink_id = PIT_Enable(led_blinking, 100);
}
void led_green_blink_off()
{
	led_status &= ~0x10;
	led_green_off();
	if(led_status & 0xF0 == 0)
		PIT_Disable(led_blink_id);
}
void led_red_blink()
{
	led_status |= 0x20;
	led_blink_id = PIT_Enable(led_blinking, 100);
}
void led_red_blink_off()
{
	led_status &= ~0x20;
	led_red_off();
	if(led_status & 0xF0 == 0)
		PIT_Disable(led_blink_id);
}
void led_yellow_blink()
{
	led_status |= 0x40;
	led_blink_id = PIT_Enable(led_blinking, 100);
}
void led_yellow_blink_off()
{
	led_status &= ~0x40;
	led_yellow_off();
	if(led_status & 0xF0 != 0)
		PIT_Disable(led_blink_id);
}

void SPI0_PIN_Init()
{


}


int is_button_pressed()
{
	return (BUTTON_GPIO->IDR & GPIO_Pin_3);
}


void output_5v_io()
{
	output_0v_io();
	VDD_5V_O_GPIO->ODR |=  VDD_5V_O_BIT;
}

void output_3v3_io()
{
	output_0v_io();
	VDD_3V3_O_GPIO->ODR |=  VDD_3V3_O_BIT;
	VDD_V_EN_GPIO->ODR |=  VDD_V_EN_BIT;

}
void output_2v5_io()
{
		output_0v_io();
	VDD_2V5_O_GPIO->ODR |=  VDD_2V5_O_BIT;
	VDD_V_EN_GPIO->ODR |=  VDD_V_EN_BIT;
}
void output_1v8_io()
{
	output_0v_io();
	VDD_1V8_O_GPIO->ODR |=  VDD_1V8_O_BIT;
	VDD_V_EN_GPIO->ODR |=  VDD_V_EN_BIT;
}

void output_0v_io()
{
	VDD_5V_O_GPIO->ODR &=  ~VDD_5V_O_BIT;
	VDD_V_EN_GPIO->ODR &=  ~VDD_V_EN_BIT;
	VDD_3V3_O_GPIO->ODR &=  ~VDD_3V3_O_BIT;
	VDD_2V5_O_GPIO->ODR &=  ~VDD_2V5_O_BIT;
	VDD_1V8_O_GPIO->ODR &=  ~VDD_1V8_O_BIT;
}

void output_dir1_io()
{
	EXT_DIR1_GPIO->ODR|=EXT_DIR1_BIT;
}
void input_dir1_io()
{
	EXT_DIR1_GPIO->ODR&=~EXT_DIR1_BIT;
}
void output_dir2_io()
{
	EXT_DIR2_GPIO->ODR|=EXT_DIR2_BIT;
}
void input_dir2_io()
{
	EXT_DIR2_GPIO->ODR&=~EXT_DIR2_BIT;
}


void mtk_USB_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, ENABLE); 
  /* USB port */
   /* Configure USB pins:  */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
  	
}

void init_gpio()
{
 GPIO_InitTypeDef GPIO_InitStructure;
 //  AFIO->MAPR &= 0xF8FFFFFF;
 // AFIO->MAPR |= 0x02000000;
  	
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
 
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); 

  if(UART != USART1) 
     GPIO_PinRemapConfig(GPIO_Remap_USART1,DISABLE);
  GPIO_PinRemapConfig(GPIO_Remap_USART2,DISABLE);

  GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
  
  
  /* Configure  as outputs push-pull, max speed 50 MHz */
  // LED
 if(UART != USART1)  //UART1 used LED_G_BIT & LED_Y_BIT
 {	 
  GPIO_InitStructure.GPIO_Pin =  LED_G_BIT | LED_R_BIT  | LED_Y_BIT;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(LED_GPIO, &GPIO_InitStructure);
 } else
 {
  GPIO_InitStructure.GPIO_Pin =  LED_Y_BIT ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(LED_GPIO, &GPIO_InitStructure); 
 }
  // Button
  GPIO_InitStructure.GPIO_Pin =  BUTTON_BIT ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(BUTTON_GPIO, &GPIO_InitStructure);
 
  // VCC_Detect
  GPIO_InitStructure.GPIO_Pin =  VDD_DETECT_BIT ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(VDD_DETECT_GPIO, &GPIO_InitStructure);
 

  GPIO_InitStructure.GPIO_Pin =  VDD_5V_O_BIT | VDD_V_EN_BIT ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin =  VDD_3V3_O_BIT | VDD_2V5_O_BIT | VDD_1V8_O_BIT ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
    
 GPIO_InitStructure.GPIO_Pin =  ERASE_PIN_BIT ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(ERASE_PIN_GPIO, &GPIO_InitStructure);

	
	 GPIO_InitStructure.GPIO_Pin =  EXT_DIR1_BIT ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 // GPIO_Init(EXT_DIR1_GPIO, &GPIO_InitStructure);
	
	
		 GPIO_InitStructure.GPIO_Pin =  EXT_DIR2_BIT ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  //GPIO_Init(EXT_DIR2_GPIO, &GPIO_InitStructure);
	{
	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10 | GPIO_Pin_11 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	}
	
	led_green_off();
	led_red_off();
	led_yellow_off();

	output_0v_io();

}

/* ************************************************* */
/* **********    SPI Flash mapping     ************* */
/* ************************************************* */

void SPI_FLASH_CS_LOW(void)
{
	GPIO_ResetBits(GPIOA, GPIO_Pin_4);
}


void SPI_FLASH_CS_HIGH(void)
{
	GPIO_SetBits(GPIOA, GPIO_Pin_4);
}


void SPI_FLASH_Init(void)
{
  SPI_InitTypeDef  SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
   
  /* Enable SPI1 and GPIOA clocks */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1 | RCC_APB2Periph_GPIOA, ENABLE);
  
  /* Configure SPI1 pins: NSS, SCK, MISO and MOSI */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Configure PA.4 as Output push-pull, used as Flash Chip select */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  /* SPI1 configuration */ 
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPI1, &SPI_InitStructure);
  
  /* Enable SPI1  */
  SPI_Cmd(SPI1, ENABLE);   
}
int flash_size;
int spiflash_size(void)
{
	return flash_size;
}
extern void SST25V_Init(SPI_TypeDef * nSPI);
int spiflash_probe(uint8_t bus, uint8_t cs)
{
	uint32_t flash_id,sector_size=4096,sector_num;
	volatile uint32_t size,page_size=256;
	char name[30]={0};
	SPI_FLASH_Init();
	SST25V_Init(SPI1);
	//SPI_FLASH_block_protect();
	flash_id = SST25V_ReadJedecID();
	mtk_printf("SPI FLASH:\n\rID:",flash_id);

    if((flash_id & 0x000000ff)== 0x8d)
	{
		sector_num = 128;
		sprintf(name,"Name:\t%s\n\r","SST25VF040B");
		mtk_printf(name, -1);
	}	
	if((flash_id & 0x000000ff)== 0x8e)
	{
		sector_num = 256;
		sprintf(name,"Name:\t%s\n\r","SST25VF080B");
		mtk_printf(name, -1);
	}	
	if((flash_id & 0x000000ff)== 0x41)
	{
		sector_num = 512;
		sprintf(name,"Name:\t%s\n\r","SST25VF160B");
		mtk_printf(name, -1);
	}	
	flash_size = size = sector_size * sector_num;
	mtk_printf("Size:",size);
	
    return 0;
}

int spiflash_read(uint8_t *buf,uint32_t address,uint32_t length)
{
	SST25V_HighSpeedBufferRead(buf, address, length);
	
    return 1;
}

int spiflash_write(uint8_t *buf,uint32_t address,uint32_t length)
{
	if(!length%2)
		SST25V_AAI_BufferProgram(buf, address, length);
	else
		SST25V_BufferWrite(buf, address, length);
	return 1;
}
int spiflash_erasechip(void)
{
		SST25V_ChipErase();
	return 1;
}
int spiflash_erase(uint32_t address,uint32_t length)
{
	int ii=0;
	while(length > 0)
	{
	    SST25V_SectorErase_4KByte(address+ii*4096);
		length -= 4096;
		ii++;
	}	
	
	return 1;
}

/* ******** ADC      ******************************  */

int get_targe_voltage(void)
{
	 uint16_t  ADC1ConvertedValue = 0, ADC1ConvertedVoltage = 0;
	ADC_InitTypeDef     ADC_InitStructure;
	GPIO_InitTypeDef    GPIO_InitStructure;

  /* GPIOB Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  /* ADC1 Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  
  /* Configure ADC Channel11 as analog input */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  
  /* ADCs DeInit */  
  ADC_DeInit(ADC1);
  
  /* Initialize ADC structure */
  ADC_StructInit(&ADC_InitStructure);
  
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
  ADC_Init(ADC1, &ADC_InitStructure); 
  
  /* Convert the ADC1 Channel 8 with 239.5 Cycles as sampling time */ 
  ADC_RegularChannelConfig(ADC1,ADC_Channel_8, 1, ADC_SampleTime_239Cycles5);

  ADC_ResetCalibration(ADC1);  
 /* Enable ADCperipheral[PerIdx] */
  ADC_Cmd(ADC1, ENABLE);    
  //Delay_ms(100);  
  while(ADC_GetResetCalibrationStatus(ADC1));  
  
  /* ADC1 regular Software Start Conv */ 
  ADC_StartCalibration(ADC1);
  ADC_Cmd(ADC1, ENABLE);    
     /* Test EOC flag */
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
  
    /* Get ADC1 converted data */
  ADC1ConvertedValue =ADC_GetConversionValue(ADC1);
    
    /* Compute the voltage */
   ADC1ConvertedVoltage = (ADC1ConvertedValue *3300)/0xFFF;
   ADC_Cmd(ADC1, DISABLE); 
   return ADC1ConvertedVoltage;	
 }

/* ***************** I2C ********************* */
I2C_TypeDef * I2C_master = I2C2;
I2C_TypeDef * I2C_slave = I2C1;

int i2c_master_init()
{
	I2C_InitTypeDef I2C_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);
     I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
 	 I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
 	 I2C_InitStructure.I2C_OwnAddress1 = 7;
 	 I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
 	 I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
 	 I2C_InitStructure.I2C_ClockSpeed = 200000;
	I2C_Cmd(I2C_master, ENABLE);
	I2C_Init(I2C_master, &I2C_InitStructure);
	
	// I2C bug, firstly set GPIO out high level
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10  | GPIO_Pin_11;	
	GPIO_InitStructure.GPIO_Mode  =  GPIO_Mode_Out_PP ;
    GPIO_InitStructure.GPIO_Speed =  GPIO_Speed_10MHz;
    GPIO_Init( GPIOB, &GPIO_InitStructure );
    GPIO_SetBits(GPIOB,GPIO_Pin_10  | GPIO_Pin_11);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10  | GPIO_Pin_11;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init( GPIOB, &GPIO_InitStructure );
	
	return 0;
}
int i2c_master_read(int address, char* pBuffer, int len)
{
	//unsigned char temp; 	

	while(I2C_GetFlagStatus(I2C_master, I2C_FLAG_BUSY));
  	//??I2C
  	I2C_GenerateSTART(I2C_master, ENABLE);
  	//??????
  	while(!I2C_CheckEvent(I2C_master, I2C_EVENT_MASTER_MODE_SELECT));
    //EV5
  	I2C_Send7bitAddress(I2C_master, address, I2C_Direction_Transmitter);
	//????
  	while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
  	//EV6
  	I2C_Cmd(I2C2, ENABLE);
 	//????????EV6
  	I2C_SendData(I2C2, *pBuffer);  
	//??????
  	while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));	
	//EV8
	
	
	I2C_GenerateSTART(I2C_master, ENABLE);  // Send STRAT condition 
     
    while(!I2C_CheckEvent(I2C_master, I2C_EVENT_MASTER_MODE_SELECT)); // Test on EV5 and clear it 
       
    I2C_Send7bitAddress(I2C_master, address, I2C_Direction_Receiver); // Send  Module_address for read 
     
    while(!I2C_CheckEvent(I2C_master, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)); // Test on EV6 and clear it

    I2C_AcknowledgeConfig(I2C_master, DISABLE);      // Disable Acknowledgement
	I2C_GenerateSTOP(I2C_master, ENABLE);      // Send STOP Condition 

    while(len)  // Receive  data 
    {
         if(I2C_CheckEvent(I2C_master, I2C_EVENT_MASTER_BYTE_RECEIVED))  // Test on EV7 and clear it
        {      
            // Read bytes from the module 
            *pBuffer = I2C_ReceiveData(I2C_master);
            pBuffer++ ; 
            len-- ;        
        }   
    }  
	
    I2C_AcknowledgeConfig(I2C_master, ENABLE);  // Enable Acknowledgement to be ready for another reception 
/*	
	I2C_GenerateSTART(I2C2,ENABLE);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_MODE_SELECT));
	I2C_AcknowledgeConfig(I2C_master,DISABLE);
	I2C_Send7bitAddress(I2C_master,id,I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_BYTE_TRANSMITTING));
	I2C_SendData(I2C_master,address);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_BYTE_TRANSMITTING));
	I2C_GenerateSTART(I2C2,ENABLE);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_MODE_SELECT));
	I2C_SendData(I2C_master,byte);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_BYTE_TRANSMITTING));
	I2C_GenerateSTOP(I2C2,ENABLE);
*/	
	return 0;
}
int i2c_master_write(int address, char* pBuffer, int len)
{
	
	 u8 i; 
    
    I2C_GenerateSTART(I2C_master, ENABLE);  //  Send STRAT condition
    
    while(!I2C_CheckEvent(I2C_master, I2C_EVENT_MASTER_MODE_SELECT));  //Test on EV5 and clear it
 //  I2C_AcknowledgeConfig(I2C_master, DISABLE);      // Disable Acknowledgement
    I2C_Send7bitAddress(I2C_master, address, I2C_Direction_Transmitter);  // Send module address for write 
   // for(i=0;i<200;i++);
    while(!I2C_CheckEvent(I2C_master, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)); // Test on EV6 and clear it 
    for(i=0;i<len;i++)   // Send data 
	{       
        I2C_SendData(I2C_master, *pBuffer); // Send the current byte 
		pBuffer++;    
        while(!I2C_CheckEvent(I2C_master, I2C_EVENT_MASTER_BYTE_TRANSMITTED)); // Test on EV8 and clear it
    //for(i=0;i<200;i++);
    }    
    I2C_GenerateSTOP(I2C_master, ENABLE);   // Send STOP condition 
/*	
	I2C_GenerateSTART(I2C2,ENABLE);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_MODE_SELECT));
	I2C_Send7bitAddress(I2C_master,id,I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_BYTE_TRANSMITTING));
	I2C_SendData(I2C_master,address);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_BYTE_TRANSMITTING));
	I2C_SendData(I2C_master,byte);
	while(!I2C_CheckEvent(I2C_master,I2C_EVENT_MASTER_BYTE_TRANSMITTING));
	I2C_GenerateSTOP(I2C2,ENABLE);
*/	
	return 0;
}
int i2c_slave_init()
{
	I2C_InitTypeDef I2C_InitStruct;
	
	I2C_StructInit(&I2C_InitStruct);
    I2C_InitStruct.I2C_Mode = I2C_Mode_SMBusDevice;
	I2C_InitStruct.I2C_OwnAddress1 = 0;              /// MCP-101 I2C address
	I2C_Init(I2C2, &I2C_InitStruct);
	I2C_Cmd(I2C2, ENABLE);
	return 0;
}

int i2c_slave_response()
{
	return 0;
}


int i2c_master_gpio_read(char address, char* pBuffer, int len)
{
		
 		return 0;
}

int i2c_master_gpio_write(char address, char* pBuffer, int len)
{
	
		return 0;
	
}
extern  void NVIC_SystemReset(void);
void SystemReset(void)
{
	NVIC_SystemReset();

}
#if 0
__asm void SystemReset(void) 
{ 
 MOV R0, #1           //;  
 MSR FAULTMASK, R0    //; ??FAULTMASK ???????? 
 LDR R0, =0xE000ED0C  //; 
 LDR R1, =0x05FA0004  //;  
 STR R1, [R0]         //; ??????    
  
deadloop 
    B deadloop        //; ??????????????? 
}
#endif
