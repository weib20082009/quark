
#include "stdlib.h"

#include "MyAlloc.h" 
extern int gProgOnGoing;
static  struct Mem_List_t *Mem_Pages=NULL;
static int MemPageCnt=0;
static int MallocCnt=0;
#if 0
int GetLisTCnt(struct Mem_List_t *head)
{
	struct Mem_List_t **p= &(Mem_Pages->next);
	int cnt=0;	
	while (*p)
	{
		p=&((*p)->next);
		cnt++;
	}
	return cnt;
}
struct Mem_List_t *Delete(struct Mem_List_t *head,void* key)  
{  
    struct Mem_List_t *node1=head;  
    struct Mem_List_t *node2=NULL;  
    if (head==NULL)  
    {  
        return NULL;  
    }   
    else  
    {  
        if (node1->address==(int)key)  
        {  
            head=head->next;  
            free(node1);  
            return head;  
        }   
        else  
        {  
            while (node1!=NULL)  
            {  
                node2=node1;  
                node2=node2->next;  
                if (node2->address==(int)key)  
                {  
                    node1->next=node2->next;  
                    free(node2);  
                    break;  
                }  
                node1=node1->next;  
            }  
            return head;  
        }  
    }  
}  

void *MyMalloc(size_t size)
{
	void *t=malloc(size);
	struct Mem_List_t **p= &(Mem_Pages->next);
	if(gProgOnGoing)
	{
		if(t)
		{
			while (*p)
			{
				p=&((*p)->next);
			}
			if(!*p)
			{
				*p=malloc(sizeof( struct Mem_List_t));
				if(!*p)
					return NULL;
				(*p)->next = NULL;
				(*p)->address=t;
				(*p)->index=MallocCnt++;
			}

		}
	}
	MemPageCnt++;
	return t; 
}
void *MyCalloc(size_t nmemb, size_t size)
{
	struct Mem_List_t **p= &(Mem_Pages->next);
	void *t=calloc(nmemb,size);
	int cnt;
	if(gProgOnGoing)
	{
		if(t)
		{
			while (*p)
			{
				p=&((*p)->next);
			}
			if(!*p)
			{
				*p=malloc(sizeof( struct Mem_List_t));
				if(!*p)
					return NULL;
				(*p)->next = NULL;
				(*p)->address=t;
				(*p)->index=MallocCnt++;
			}

		}
	}
	cnt=GetLisTCnt(MemPageCnt);
	MemPageCnt++;
	if(cnt!=MemPageCnt)
		cnt=0;
	return t; 
}
void *MyRealloc(void * ptr, size_t size)
{
	struct Mem_List_t **p= &(Mem_Pages->next);
	struct Mem_List_t *t= Mem_Pages;
	void *rt=realloc(ptr,size);
	int cnt;
	if(gProgOnGoing)
	{
		while(*p)
		{
			
			if((*p)->address!=ptr)
			{
				t=*p;
				p=&((*p)->next);
			}
			else
			{
				if(rt)
					(*p)->address=rt;
				else
				{
					if((*p)->next)
						t->next=(*p)->next;
					else
						t->next=NULL;
					free(*p);
					*p=NULL;
				}
			}
		}
	
	}
	cnt=GetLisTCnt(MemPageCnt);
	MemPageCnt++;
	if(cnt!=MemPageCnt)
		cnt=0;
	return rt; 
}
void MyFree(void *ptr)
{
		struct Mem_List_t **p=&(Mem_Pages->next);
		struct Mem_List_t *t= Mem_Pages;
			struct Mem_List_t *q= Mem_Pages;
	int cnt;
	if(gProgOnGoing)
	{		
#if 0	
		while(*p)
		{
	
			if((*p)->address!=ptr)
			{
				t=*p;
				p=&((*p)->next);
				
			}
			else
			{
				q=*p;
				if((*p)->next)
					t->next=(*p)->next;
				else
						t->next=NULL;
				free(q);
				break;
			
			}
		}
#endif	
		Delete(Mem_Pages,ptr);
	}
	cnt=GetLisTCnt(MemPageCnt);
	if(MemPageCnt>0)
		MemPageCnt--;
	if(cnt!=MemPageCnt)
		cnt=0;
	free(ptr);
}
void MyMemInit(void)
{
	if(Mem_Pages)
		free(Mem_Pages);
	Mem_Pages=malloc(sizeof(struct Mem_List_t));//head 
	Mem_Pages->address=NULL;
	Mem_Pages->next=NULL;
	Mem_Pages->index=0;
	MemPageCnt=0;

}
void MyMemReInit(void)
{
		struct Mem_List_t *p= Mem_Pages->next;		
		while(p)
		{	
			struct Mem_List_t *t= p;
			p=p->next;
			free(t->address);
			if(t)
				free(t);
			if(MemPageCnt>0)
				MemPageCnt--;			
		}
		free(Mem_Pages);
		Mem_Pages=NULL;
}
#else
void MyFree(void *ptr)
{
	free(ptr);
}
void *MyMalloc(size_t size)
{
	void *t=malloc(size);
	
	return t; 
}
void *MyCalloc(size_t nmemb, size_t size)
{
	void *t=calloc(nmemb,size);

	return t; 
}
void *MyRealloc(void * ptr, size_t size)
{
	void *rt=realloc(ptr,size);

	return rt; 
}
#endif