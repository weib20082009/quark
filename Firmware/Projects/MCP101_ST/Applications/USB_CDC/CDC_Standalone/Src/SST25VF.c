
#include "stm3210e_eval.h"
#include "sst25vf.h"

extern void SPI_FLASH_CS_LOW(void);
extern void SPI_FLASH_CS_HIGH(void);
#define SST25V_CS_LOW                	SPI_FLASH_CS_LOW
#define SST25V_CS_HIGH               	SPI_FLASH_CS_HIGH

extern SPI_TypeDef * SPI;
extern SPI_HandleTypeDef *h_Spi;
void SST25V_Init(SPI_HandleTypeDef * hSPI)
{
  //SPI = nSPI;
	h_Spi=hSPI;
  SST25V_CS_HIGH();                   
  SST25V_WP_HIGH();                    
  SST25V_HOLD_HIGH();                 
  SST25V_WriteStatusRegister(0x02);   
  SST25V_DBSY();                      
}

uint8_t SST25V_ByteRead(uint32_t ReadAddr)
{
  uint8_t Temp = 0;

  SST25V_CS_LOW();                                  
  SPI_Flash_SendByte(Read_Data);                    
  SPI_Flash_SendByte((ReadAddr & 0xFF0000) >> 16);  
  SPI_Flash_SendByte((ReadAddr& 0xFF00) >> 8);
  SPI_Flash_SendByte(ReadAddr & 0xFF);
 
  Temp = SPI_Flash_ReceiveByte();                   
  SST25V_CS_HIGH();                                 
  return Temp;                                     
}

void SST25V_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
  SST25V_CS_LOW();                                  
  SPI_Flash_SendByte(Read_Data);                    
  SPI_Flash_SendByte((ReadAddr & 0xFF0000) >> 16);  
  SPI_Flash_SendByte((ReadAddr& 0xFF00) >> 8);
  SPI_Flash_SendByte(ReadAddr & 0xFF);

  while(NumByteToRead--)                  
  {
    *pBuffer = SPI_Flash_ReceiveByte();   
    pBuffer++;                            
  }
  SST25V_CS_HIGH();                       
}

uint8_t SST25V_HighSpeedByteRead(uint32_t ReadAddr)
{
  uint32_t Temp = 0;

  SST25V_CS_LOW();                                  
  SPI_Flash_SendByte(HighSpeedReadData);             
  SPI_Flash_SendByte((ReadAddr & 0xFF0000) >> 16);   
  SPI_Flash_SendByte((ReadAddr& 0xFF00) >> 8);
  SPI_Flash_SendByte(ReadAddr & 0xFF);
  SPI_Flash_SendByte(Dummy_Byte);                   
  Temp = SPI_Flash_ReceiveByte();                   
  SST25V_CS_HIGH();                                 
  return Temp;                                      
}

void SST25V_HighSpeedBufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
  SST25V_CS_LOW();                                   
  SPI_Flash_SendByte(HighSpeedReadData);             
  SPI_Flash_SendByte((ReadAddr & 0xFF0000) >> 16);   
  SPI_Flash_SendByte((ReadAddr& 0xFF00) >> 8);
  SPI_Flash_SendByte(ReadAddr & 0xFF);
  SPI_Flash_SendByte(Dummy_Byte);                    

  while(NumByteToRead--)                             
  {
    *pBuffer = SPI_Flash_ReceiveByte();              
    pBuffer++;                                       
  }
  SST25V_CS_HIGH();                                  
}



void SST25V_ByteWrite(uint8_t Byte, uint32_t WriteAddr)
{
  SST25V_WriteEnable();                               
  SST25V_CS_LOW();                                    
  SPI_Flash_SendByte(Byte_Program);                   
  SPI_Flash_SendByte((WriteAddr & 0xFF0000) >> 16); 
  SPI_Flash_SendByte((WriteAddr & 0xFF00) >> 8);
  SPI_Flash_SendByte(WriteAddr & 0xFF);
 
  SPI_Flash_SendByte(Byte);                           
  SST25V_CS_HIGH();                                   
  SST25V_WaitForWriteEnd();                           
}

void SST25V_BufferWrite(uint8_t *pBuffer,uint32_t Addr,uint16_t BufferLength)
{
  while(BufferLength--)                   
  {
    SST25V_ByteWrite(*pBuffer,Addr);    
    pBuffer++;                          
    Addr++;                             
  }
}

void SST25V_WriteBytes(uint8_t Byte, uint32_t WriteAddr,uint32_t ByteLength)
{
  while(ByteLength--)
  {
    SST25V_ByteWrite(Byte,WriteAddr);   
    WriteAddr++;                        
  }
}

void SST25V_AAI_WriteBytes(uint8_t Byte, uint32_t Addr,uint32_t ByteLength)
{
  uint32_t Length = 0;

  Length = (ByteLength/2)-1;                   
  SST25V_WriteEnable();                         
  SST25V_CS_LOW();                              
  SPI_Flash_SendByte(AAI_WordProgram);          
  SPI_Flash_SendByte((Addr & 0xFF0000) >> 16);   
  SPI_Flash_SendByte((Addr & 0xFF00) >> 8);
  SPI_Flash_SendByte(Addr & 0xFF);
  SPI_Flash_SendByte(Byte);                     
  SPI_Flash_SendByte(Byte);                     
  SST25V_CS_HIGH();                              
  SST25V_WaitForWriteEnd();                     
 
  while(Length--)                               
  {
    SST25V_CS_LOW();                            
    SPI_Flash_SendByte(AAI_WordProgram);        
    SPI_Flash_SendByte(Byte);                   
    SPI_Flash_SendByte(Byte);                   
    SST25V_CS_HIGH();                           
    SST25V_WaitForWriteEnd();                   
  }
  SST25V_WriteDisable();                        
}


void SST25V_AAI_BufferProgram(uint8_t *pBuffer,uint32_t Addr,uint16_t BufferLength)
{
  uint16_t Length = 0;

  Length = (BufferLength/2)-1;                   
  SST25V_WriteEnable();                          
  SST25V_CS_LOW();                               
  SPI_Flash_SendByte(AAI_WordProgram);           
  SPI_Flash_SendByte((Addr & 0xFF0000) >> 16);   
  SPI_Flash_SendByte((Addr & 0xFF00) >> 8);
  SPI_Flash_SendByte(Addr & 0xFF);
 
  SPI_Flash_SendByte(*pBuffer);                  
  pBuffer++;                                     
  SPI_Flash_SendByte(*pBuffer);                  
  pBuffer++;                                     
 
  SST25V_CS_HIGH();                              
  SST25V_WaitForWriteEnd();                      
 
  while(Length--)     
  {
    SST25V_CS_LOW();                         
    SPI_Flash_SendByte(AAI_WordProgram);     
    SPI_Flash_SendByte(*pBuffer);            
    pBuffer++;                               
    SPI_Flash_SendByte(*pBuffer);            
    pBuffer++;                               
    SST25V_CS_HIGH();                     
    SST25V_WaitForWriteEnd();                
  }
  SST25V_WriteDisable();                     
}

void SST25V_AAI_WordProgram(uint8_t Byte1, uint8_t Byte2, uint32_t Addr)
{
  SST25V_WriteEnable();                            
  SST25V_CS_LOW();                                 
  SPI_Flash_SendByte(AAI_WordProgram);             
  SPI_Flash_SendByte((Addr & 0xFF0000) >> 16);     
  SPI_Flash_SendByte((Addr & 0xFF00) >> 8);
  SPI_Flash_SendByte(Addr & 0xFF);

  SPI_Flash_SendByte(Byte1);                       
  SPI_Flash_SendByte(Byte2);                       
 
  SST25V_CS_HIGH();                                
  SST25V_WaitForWriteEnd();                        
}

void SST25V_AAI_WordsProgram(uint8_t state,uint8_t Byte1, uint8_t Byte2)
{
  SST25V_CS_LOW();                        
  SPI_Flash_SendByte(AAI_WordProgram);    

  SPI_Flash_SendByte(Byte1);              
  SPI_Flash_SendByte(Byte2);              

  SST25V_CS_HIGH();                      
  SST25V_WaitForWriteEnd();               
 
  if(state==1)                            
  {
    SST25V_WriteDisable();
  }
}

void SST25V_SectorErase_4KByte(uint32_t Addr)
{
  SST25V_WriteEnable();                        
  SST25V_CS_LOW();                             
  SPI_Flash_SendByte(SectorErace_4KB);         
  SPI_Flash_SendByte((Addr & 0xFF0000) >> 16);  
  SPI_Flash_SendByte((Addr & 0xFF00) >> 8);
  SPI_Flash_SendByte(Addr & 0xFF);
 
  SST25V_CS_HIGH();                            
  SST25V_WaitForWriteEnd();                    
}

void SST25V_BlockErase_32KByte(uint32_t Addr)
{
  SST25V_WriteEnable();                        
  SST25V_CS_LOW();                             
  SPI_Flash_SendByte(BlockErace_32KB);         
  SPI_Flash_SendByte((Addr & 0xFF0000) >> 16); 
  SPI_Flash_SendByte((Addr & 0xFF00) >> 8);
  SPI_Flash_SendByte(Addr & 0xFF);
 
  SST25V_CS_HIGH();                            
  SST25V_WaitForWriteEnd();                    
}

void SST25V_BlockErase_64KByte(uint32_t Addr)
{
  SST25V_WriteEnable();                        
  SST25V_CS_LOW();                             
  SPI_Flash_SendByte(BlockErace_64KB);         
  SPI_Flash_SendByte((Addr & 0xFF0000) >> 16); 
  SPI_Flash_SendByte((Addr & 0xFF00) >> 8);
  SPI_Flash_SendByte(Addr & 0xFF);
 
  SST25V_CS_HIGH();                  
  SST25V_WaitForWriteEnd();          
}

void SST25V_ChipErase(void)          
{
  SST25V_WriteEnable();              
  SST25V_CS_LOW();                   
  SPI_Flash_SendByte(ChipErace);     
  SST25V_CS_HIGH();                  
  SST25V_WaitForWriteEnd();          
}

uint8_t SST25V_ReadStatusRegister(void)
{
  uint8_t StatusRegister = 0;

  SST25V_CS_LOW();                           
  SPI_Flash_SendByte(ReadStatusRegister);    
  StatusRegister = SPI_Flash_ReceiveByte();  
  SST25V_CS_HIGH();                          
  return StatusRegister;                     
}

void SST25V_WriteEnable(void)         
{
  SST25V_CS_LOW();                    
  SPI_Flash_SendByte(WriteEnable);    
  SST25V_CS_HIGH();                   
}

void SST25V_WriteDisable(void)        
{
  SST25V_CS_LOW();                    
  SPI_Flash_SendByte(WriteDisable);   
  SST25V_CS_HIGH();                   
}

void SST25V_EnableWriteStatusRegister(void)       
{
  SST25V_CS_LOW();                                
  SPI_Flash_SendByte(EnableWriteStatusRegister);  
  SST25V_CS_HIGH();                               
}

void SST25V_WriteStatusRegister(uint8_t Byte)
{
  SST25V_EnableWriteStatusRegister();        
  SST25V_CS_LOW();                           
  SPI_Flash_SendByte(WriteStatusRegister);   
  SPI_Flash_SendByte(Byte);                  
  SST25V_CS_HIGH();                          
}

void SST25V_WaitForWriteEnd(void)    
{
  uint8_t FLASH_Status = 0;

  SST25V_CS_LOW();                                
  do
  {
    FLASH_Status = SST25V_ReadStatusRegister();   

  } while((FLASH_Status & 0x01) == SET);         

  SST25V_CS_HIGH();                               
}

uint32_t SST25V_ReadJedecID(void)
{
  uint32_t JEDECID = 0, Temp0 = 0, Temp1 = 0, Temp2 = 0;

  SST25V_CS_LOW();                                  
  SPI_Flash_SendByte(ReadJedec_ID);                 
  Temp0 = SPI_Flash_ReceiveByte();                  
  Temp1 = SPI_Flash_ReceiveByte();                  
  Temp2 = SPI_Flash_ReceiveByte();                  
  SST25V_CS_HIGH();                                 
  JEDECID = (Temp0 << 16) | (Temp1 << 8) | Temp2;   
	return JEDECID;                                   
}

uint16_t SST25V_ReadManuID_DeviceID(uint32_t ReadManu_DeviceID_Addr)
{
  uint16_t ManuID_DeviceID = 0;
  uint8_t ManufacturerID = 0,  DeviceID = 0;

  SST25V_CS_LOW();                     
  SPI_Flash_SendByte(ReadDeviceID);    
 
  SPI_Flash_SendByte((ReadManu_DeviceID_Addr & 0xFF0000) >> 16);  
  SPI_Flash_SendByte((ReadManu_DeviceID_Addr & 0xFF00) >> 8);
  SPI_Flash_SendByte(ReadManu_DeviceID_Addr & 0xFF);
 
  if(ReadManu_DeviceID_Addr==1)  
  {
    DeviceID = SPI_Flash_ReceiveByte();        
    ManufacturerID = SPI_Flash_ReceiveByte();  
  }
  else 
  {
    ManufacturerID = SPI_Flash_ReceiveByte();  
    DeviceID = SPI_Flash_ReceiveByte();        
  }
 
  ManuID_DeviceID = ((ManufacturerID<<8) | DeviceID);   
  SST25V_CS_HIGH();                                     
 
  return ManuID_DeviceID;                               
}

void SST25V_EBSY(void)
{
  SST25V_CS_LOW();           
  SPI_Flash_SendByte(EBSY); 
  SST25V_CS_HIGH();         
}

void SST25V_DBSY(void)
{
  SST25V_CS_LOW();          
  SPI_Flash_SendByte(DBSY); 
  SST25V_CS_HIGH();          
}

#if 0
extern uint8_t RX_buffer[]; //[RX_BUFFER_MAX];

void SST25V_test(void)
{
	//uint8_t Rx_Buffer[16];
	uint32_t start_addr=0;
	int i, step, total=0;
#if 1
	uint32_t jedec_id;
	uint16_t manu_id;

	jedec_id = jedec_id;
	manu_id = manu_id;
		
	jedec_id = SST25V_ReadJedecID();
	manu_id = SST25V_ReadManuID_DeviceID(0);
#endif

	i=0;
	step = 16;
	for(; i<total; i+=step)
	{
		SST25V_BufferRead(RX_buffer, start_addr+i, step);

	}

}
#endif
