#ifndef _NRF51_H__
#define _NRF51_H__

/* ================================================================================ */
/* ================                      NVMC                      ================ */
/* ================================================================================ */
#include "spider-sap.h" 
#include "common.h"

/**
  * @brief Non Volatile Memory Controller. (NVMC)
  */

typedef struct {                                    /*!< NVMC Structure                                                        */
  uint32_t  READY;                             /*!< Ready flag.                                                           */
  uint32_t  CONFIG;                            /*!< Configuration register.                                               */
  uint32_t  ERASEPAGE;                         /*!< Register for erasing a non-protected non-volatile memory page.        */
  uint32_t  ERASEALL;                          /*!< Register for erasing all non-volatile user memory.                    */
  uint32_t  ERASEPROTECTEDPAGE;                /*!< Register for erasing a protected non-volatile memory page.            */
  uint32_t  ERASEUICR;                         /*!< Register for start erasing User Information Congfiguration Registers. */
} NRF_NVMC_Type;

typedef enum {                                    /*!< NVMC Structure                                                        */
  NVMC_READY_OFFSET=0x400,                             /*!< Ready flag.                                                           */
	NVMC_CONFIG_OFFSET=0x504,                            /*!< Configuration register.                                               */
  NVMC_ERASEPAGE_OFFSET=0x508,                         /*!< Register for erasing a non-protected non-volatile memory page.        */
  NVMC_ERASEALL_OFFSET=0x50C,                          /*!< Register for erasing all non-volatile user memory.                    */
  NVMC_ERASEPROTECTEDPAGE_OFFSET=0x510,                /*!< Register for erasing a protected non-volatile memory page.            */
  NVMC_ERASEUICR_OFFSET=0x514                         /*!< Register for start erasing User Information Congfiguration Registers. */
} NRF_NVMC_Offset;
/* ================================================================================ */
/* ================                      FICR                      ================ */
/* ================================================================================ */


/**
  * @brief Factory Information Configuration. (FICR)
  */

typedef struct {                                    /*!< FICR Structure                                                        */
  uint32_t  CODEPAGESIZE;                      /*!< Code memory page size in bytes.                                       */
  uint32_t  CODESIZE;                          /*!< Code memory size in pages.                                            */
  uint32_t  CLENR0;                            /*!< Length of code region 0 in bytes.                                     */
  uint32_t  PPFC;                              /*!< Pre-programmed factory code present.                                  */
  uint32_t  RESERVED2;
  uint32_t  NUMRAMBLOCK;                       /*!< Number of individualy controllable RAM blocks.                        */
  uint32_t  SIZERAMBLOCK[4];                   /*!< Size of RAM block in bytes.                                           */
  uint32_t  CONFIGID;                          /*!< Configuration identifier.                                             */
  uint32_t  DEVICEID[2];                       /*!< Device identifier.                                                    */
  uint32_t  ER[4];                             /*!< Encryption root.                                                      */
  uint32_t  IR[4];                             /*!< Identity root.                                                        */
  uint32_t  DEVICEADDRTYPE;                    /*!< Device address type.                                                  */
  uint32_t  DEVICEADDR[2];                     /*!< Device address.                                                       */
  uint32_t  OVERRIDEEN;                        /*!< Radio calibration override enable.                                    */
  uint32_t  BLE_1MBIT[5];                      /*!< Override values for the OVERRIDEn registers in RADIO for BLE_1Mbit
                                                         mode.                                                                 */
} NRF_FICR_Type;

typedef enum {                                    /*!< FICR Structure                                                        */
  FICR_CODEPAGESIZE_OFFSET=0x10,                      /*!< Code memory page size in bytes.                                       */
  FICR_CODESIZE_OFFSET=0x14,                          /*!< Code memory size in pages.                                            */
  FICR_CLENR0_OFFSET=0x28,                            /*!< Length of code region 0 in bytes.                                     */
  FICR_PPFC_OFFSET=0x2C,                              /*!< Pre-programmed factory code present.                                  */
  FICR_NUMRAMBLOCK_OFFSET=0x34,                       /*!< Number of individualy controllable RAM blocks.                        */
  FICR_SIZERAMBLOCK_OFFSET=0x38,                   /*!< Size of RAM block in bytes.                                           */
  FICR_CONFIGID_OFFSET=0x5C,                          /*!< Configuration identifier.                                             */
  FICR_DEVICEID_OFFSET=0x60,                       /*!< Device identifier.                                                    */
  FICR_ER_OFFSET=0x80,                             /*!< Encryption root.                                                      */
  FICR_IR_OFFSET=0x90,                             /*!< Identity root.                                                        */
  FICR_DEVICEADDRTYPE_OFFSET=0xA0,                    /*!< Device address type.                                                  */
  FICR_DEVICEADDR_OFFSET=0xA4,                     /*!< Device address.                                                       */
  FICR_OVERRIDEEN_OFFSET=0xAC,                        /*!< Radio calibration override enable.                                    */
  FICR_BLE_1MBIT_OFFSET=0xEC                      /*!< Override values for the OVERRIDEn registers in RADIO for BLE_1Mbit
                                                         mode.                                                                 */
} NRF_FICR_Offset;
/* ================================================================================ */
/* ================                      UICR                      ================ */
/* ================================================================================ */


/**
  * @brief User Information Configuration. (UICR)
  */

typedef struct NRF_UICR_Type{                                    /*!< UICR Structure                                                        */
  uint32_t  CLENR0;                            /*!< Length of code region 0.                                              */
  uint32_t  RBPCONF;                           /*!< Readback protection configuration.                                    */
  uint32_t  XTALFREQ;                          /*!< Reset value for CLOCK XTALFREQ register.                              */
  uint32_t  FWID;                              /*!< Firmware ID.                                                          */
  uint32_t  BOOTLOADERADDR;                    /*!< Bootloader start address.                                             */
} NRF_UICR_Type;

typedef enum {                                    /*!< UICR Structure                                                        */
  UICR_CLENR0_OFFSET=0x00,                            /*!< Length of code region 0.                                              */
  UICR_RBPCONF_OFFSET=0x04,                           /*!< Readback protection configuration.                                    */
  UICR_XTALFREQ_OFFSET=0x08,                          /*!< Reset value for CLOCK XTALFREQ register.                              */
  UICR_FWID_OFFSET=0x10,                              /*!< Firmware ID.                                                          */
  UICR_BOOTLOADERADDR_OFFSET=0x14                    /*!< Bootloader start address.                                             */
} NRF_UICR_OFFSET;


/* ================================================================================ */
/* ================              Peripheral memory map             ================ */
/* ================================================================================ */


#define NRF_NVMC_BASE                   0x4001E000UL

#define NRF_FICR_BASE                   0x10000000UL
#define NRF_UICR_BASE                   0x10001000UL

void LockNrfChip(xDAPPacket *xPacket);
void GetNrfFICR(xDAPPacket *xPacket);

void GetNrfUICR(xDAPPacket *xPacket);

int ChkNrfLock(void);

void Nrf51ErasePage(xDAPPacket *xPacket,uint32_t add,uint32_t end);
void Nrf51EraseAll(xDAPPacket *xPacket,uint32_t add);
void SetNrfUICR(xDAPPacket *xPacket,NRF_UICR_Type pnrf_uicr_t);
#endif
