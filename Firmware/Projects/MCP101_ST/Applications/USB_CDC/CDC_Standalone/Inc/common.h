#define MAR_VER '2'
#define MIN_VER '2'
#define DBG_VER 'D'
#define DEV_VER '3'
#ifndef PRINT
#define PRINT mtk_printf
#endif

#undef WITH_DEBUG



#include "main.h"
#include "STM_DAP_config.h"
#include "../DAP/DAP.h"
#include "serialprog.h"
//#define ADC0  0



#define MEDIA_SECTORS_NUM 2
#define MAX_FILENAME_LEN 11
#define APP_ROOT_ADD 0x10000
#define USB_BUF_SIZE (256+6)

#define I2CRXBUFFERSIZE 64


#define QUARK_X10XX						0x10//weib add
#define QUARK_D20XX						0x00
#define QUARK_SE_C10XX				0x01

#define USB_CDC_SERIAL 0x0
#define USART_SERIAL  0x1

#define malloc(n) MyMalloc(n)
#define calloc(n,p) MyCalloc(n,p)
#define free(p) MyFree(p)
#define realloc(r,n) MyRealloc(r,n)

typedef void (*T_PIT_Callback)(void);

void PIT_Init( unsigned char cChannel, unsigned int PitModulo );
int PIT_Enable( T_PIT_Callback CallBack, int time );
int PIT_Disable(int id );
void init_serial (void);
void init_led(void);
void init_button(void);

void init_gpio(void);

int GetLedMode(uint8_t *led_mode);
void output_0v_io(void);

void output_5v_io(void);

void output_3v3_io(void);
void output_2v5_io(void);
void output_1v8_io(void);

void led_blinking(void);
void led_green_on(void);

void led_green_off(void);


void led_red_on(void);

void led_red_off(void);

void led_yellow_on(void);

void led_yellow_off(void);
void led_green_blink(void);
void led_green_blink_off(void);
void led_red_blink(void);
void led_red_blink_off(void);
void led_yellow_blink(void);
void led_yellow_blink_off(void);

int spiflash_probe(uint8_t bus, uint8_t cs);


int spiflash_read(uint8_t *buf,uint32_t address,uint32_t length);

int spiflash_write(uint8_t *buf,uint32_t address,uint32_t length);
int spiflash_erasechip(void);
int spiflash_erase(uint32_t address,uint32_t length);

int spiflash_size(void);

void SPI_FLASH_CS_LOW(void);


void SPI_FLASH_CS_HIGH(void);
int i2c_master_gpio_init(void); 
int i2c_start(void) ; 
void i2c_stop(void) ; 

void i2c_read(unsigned char addr, unsigned char* buf, int len)  ;  
void i2c_write (unsigned char addr, unsigned char* buf, int len);
void I2CServiceEntry (void* param);


int cmdLine(uint8_t *command);

void  mtk_command(int ch);

int iDAPOpen(void);

int is_button_pressed(void);

int program_process(void);

int getkey_nowait (void);
uint32_t getTick(void) ;
int get_targe_voltage(void);
int GetSAPFile(char * filename,unsigned char* buff,uint32_t sz,int offset);
int GetSAPFileName(char * filename);

void SystemReset(void);

void NVIC_DisableIRQ(IRQn_Type IRQn);
void NVIC_EnableIRQ(IRQn_Type IRQn);	

int print_PIT_info(void);

		
void decTEA(uint32_t* v, uint32_t* k);	
uint32_t calcCRC32(const uint8_t* buffer, size_t length, uint32_t crc);

void  USBRouteTxEntry (void);
void UartServiceEntry (void* param);
void USBRouteRxEntry(uint8_t* data, uint32_t Len);
int SW_CDC_Receive(uint8_t* pdata, uint32_t size );
int SW_CDC_Send(uint8_t* pdata, uint32_t size );

void *SW_SF_Open(char *FileName,FileMode mode);
int SW_SF_Write(SFHandle *handle,uint8_t *dataBuffer,int length, void *byteswritten);
int SW_SF_Read(SFHandle *handle,uint8_t *dataBuffer,int length, void *byteswritten);
void SW_SF_Close(SFHandle *handle);

 int InitStatistics(void);
 
	int mtk_printf_usbser(char *str, int value);
	
	//void printf_memory( void *str, int len); 
int 	mtk_printf_iic(char *str, int value);
#define mtk_printf(...) do{}while(0)
 #define printf_memory(...) do{}while(0)
void assert_failed(char *, int);
#ifdef DEBUG
#define ASSERT(expr) \
    if (!(expr)) \
        assert_failed(__FILE__, __LINE__)
#else
#define ASSERT(expr)
#endif
