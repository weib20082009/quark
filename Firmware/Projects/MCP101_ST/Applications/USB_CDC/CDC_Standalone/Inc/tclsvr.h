

#ifdef __cplusplus 
extern "C" {
#endif

//int Jim_OpenProjectCommand(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_HelpCommand(Jim_Interp *interp, int argc, Jim_Obj *const *args);



int Jim_DoProgram(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_DoBlankCheck(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_DoVerify(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_DoEraseChip(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_LogOnOff(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_SendFile(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_GetLog(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_Success(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int Jim_Fail(Jim_Interp *interp, int argc, Jim_Obj *const *args) ;
int  Jim_DoLoopback(Jim_Interp *interp, int argc, Jim_Obj *const *args);

int  Jim_DoAutoProgram(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int  Jim_DoConnect(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int  Jim_Open(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int  Jim_Close(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int  Jim_GetFile(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int  Jim_GetFileInfo(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int  Jim_GetMem(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int  Jim_DoDisConnect(Jim_Interp *interp, int argc, Jim_Obj *const *args);
int  Jim_ResetStatistics(Jim_Interp *interp, int argc, Jim_Obj *const *args);

 #ifdef __cplusplus 
}
#endif

#define JIM_MCP_COMMANDS						\
	 {"opencom",		Jim_Open},			\
	 {"closecom",		Jim_Close},			\
	 {"sendfile",		Jim_SendFile},			\
	 {"getfile",		Jim_GetFile},			\
	 {"getfileinfo",	Jim_GetFileInfo},			\
	 {"getlog",		Jim_GetLog},				\
	 {"getmem",		Jim_GetMem},				\
	 {"log",		Jim_LogOnOff},				\
	 {"okcnt",		Jim_Success},			\
	 {"failcnt",		Jim_Fail},					\
	 {"resetstatis",Jim_ResetStatistics},					\
	 {"autoprg",		Jim_DoAutoProgram},			\
	 {"target",		Jim_DoConnect},			\
	 {"connect",		Jim_DoConnect},			\
	 {"disconnect",		Jim_DoDisConnect},			\
	 {"program",		Jim_DoProgram},			\
	 {"erasechip",		Jim_DoEraseChip},		\
	 {"blank",		Jim_DoBlankCheck},			\
	 {"verify",		Jim_DoVerify},				\
	 {"loopback",		Jim_DoLoopback},				\
 	 {"help",		Jim_HelpCommand},		