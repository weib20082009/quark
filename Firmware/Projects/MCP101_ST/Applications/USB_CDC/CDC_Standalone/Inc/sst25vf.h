#ifndef __SST25V_H__
#define __SST25V_H__

#include <stdint.h>

#define BufferSize                    	(countof(Tx_Buffer)-1)
#define countof(a)                    	(sizeof(a) / sizeof(*(a)))
#define SST25V_PageSize               	256
#define Dummy_Byte                    	0xA5

#define SST25V_CS_LOW()               	GPIO_ResetBits(GPIOB, GPIO_Pin_12)
#define SST25V_CS_HIGH()              	GPIO_SetBits(GPIOB, GPIO_Pin_12)

#define SST25V_WP_LOW()
#define SST25V_WP_HIGH()

#define SST25V_HOLD_LOW()
#define SST25V_HOLD_HIGH()

#define Read_Data                     	0x03
#define HighSpeedReadData             	0x0B
#define SectorErace_4KB               	0x20
#define BlockErace_32KB               	0x52
#define BlockErace_64KB               	0xD8
#define ChipErace                     	0xC7

#define Byte_Program                  	0x02
#define AAI_WordProgram               	0xAD
#define ReadStatusRegister            	0x05
#define EnableWriteStatusRegister     	0x50
#define WriteStatusRegister           	0x01

#define WriteEnable                   	0x06
#define WriteDisable                 	0x04
#define ReadDeviceID                  	0xAB

#define ReadJedec_ID                  	0x9F

#define EBSY                         	0X70
#define DBSY                         	0X80

extern void SST25V_Init(void);
extern uint8_t SST25V_ByteRead(uint32_t ReadAddr);
extern void SST25V_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);
extern uint8_t SST25V_HighSpeedByteRead(uint32_t ReadAddr);
extern void SST25V_HighSpeedBufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);
extern uint8_t SPI_Flash_SendByte(uint8_t byte);
extern uint8_t SPI_Flash_ReceiveByte(void);
extern void SST25V_ByteWrite(uint8_t Byte, uint32_t WriteAddr);
extern void SST25V_BufferWrite(uint8_t *pBuffer, uint32_t Addr, uint16_t BufferLength);
extern void SST25V_WriteBytes(uint8_t Byte, uint32_t WriteAddr,uint32_t ByteLength);
extern void SST25V_AAI_WriteBytes(uint8_t Byte, uint32_t Addr,uint32_t ByteLength);
extern void SST25V_AAI_BufferProgram(uint8_t *pBuffer,uint32_t Addr,uint16_t BufferLength);
extern void SST25V_AAI_WordProgram(uint8_t Byte1, uint8_t Byte2, uint32_t Addr);
extern void SST25V_AAI_WordsProgram(uint8_t state,uint8_t Byte1, uint8_t Byte2);
extern void SST25V_SectorErase_4KByte(uint32_t Addr);
extern void SST25V_BlockErase_32KByte(uint32_t Addr);
extern void SST25V_BlockErase_64KByte(uint32_t Addr);
extern void SST25V_ChipErase(void);
extern uint8_t SST25V_ReadStatusRegister(void);
extern void SST25V_WriteEnable(void);
extern void SST25V_WriteDisable(void);
extern void SST25V_EnableWriteStatusRegister(void);
extern void SST25V_WriteStatusRegister(uint8_t Byte);
extern void SST25V_WaitForWriteEnd(void);
extern uint32_t SST25V_ReadJedecID(void);
extern uint16_t SST25V_ReadManuID_DeviceID(uint32_t ReadManu_DeviceID_Addr);
extern void SST25V_EBSY(void);
extern void SST25V_DBSY(void);

extern void SST25V_test(void);
#endif

