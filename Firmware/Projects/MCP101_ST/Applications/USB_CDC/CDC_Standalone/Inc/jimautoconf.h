#ifndef _JIMAUTOCONF_H
#define _JIMAUTOCONF_H
#ifdef WIN32
#define HAVE_DLOPEN_COMPAT 1
#define HAVE_ARPA_INET_H 1
/* #undef HAVE_BACKTRACE */
/* #undef HAVE_CRT_EXTERNS_H */
//#define HAVE_DIRENT_H 1
//#define HAVE_DLFCN_H 1
//#define HAVE_DLOPEN 1
//#define HAVE_EXECVPE 1
//#define HAVE_FORK 1
//#define HAVE_GETADDRINFO 1
//#define HAVE_GETEUID 1
//#define HAVE_INET_NTOP 1
#define HAVE_ISATTY 1
#define HAVE_LONG_LONG 1
//#define HAVE_LSTAT 1
//#define HAVE_MKSTEMP 1
//#define HAVE_NETDB_H 1
//#define HAVE_NETINET_IN_H 1
//#define HAVE_OPENDIR 1
//#define HAVE_PIPE 1
//#define HAVE_READLINK 1
//#define HAVE_REALPATH 1
#define HAVE_REGCOMP 1
#define HAVE_SELECT 1
//#define HAVE_SIGACTION 1
#define HAVE_SLEEP 1
//#define HAVE_SOCKET 1
//#define HAVE_STDLIB_H 1
#define HAVE_STRPTIME 1
#define HAVE_STRUCT_SYSINFO_UPTIME 1
#define HAVE_SYSINFO 1
//#define HAVE_SYSLOG 1
#define HAVE_SYSTEM 1
//#define HAVE_SYS_SIGLIST 1
/* #undef HAVE_SYS_SIGNAME */
//#define HAVE_SYS_SOCKET_H 1
#define HAVE_SYS_STAT_H 1
#define HAVE_SYS_SYSINFO_H 1
//#define HAVE_SYS_TIME_H 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_SYS_UN_H 1
#define HAVE_TERMIOS_H 1
#define HAVE_UALARM 1
//#define HAVE_UNISTD_H 1
#define HAVE_USLEEP 1
//#define HAVE_UTIMES 1
#define HAVE_VFORK 1
#define HAVE_WAITPID 1
#define HAVE_WINDOWS 1
/* #undef HAVE__NSGETENVIRON */
/* #undef JIM_INSTALL */
#define JIM_REFERENCES 1
#define JIM_REGEXP 1
#define JIM_STATICLIB 1
/* #undef JIM_UTF8 */
//#define TCL_LIBRARY "./tcllib"
//#define TCL_PLATFORM_OS "cygwin"
#define TCL_PLATFORM_PATH_SEPARATOR ":"
//#define TCL_PLATFORM_PLATFORM "unix"
//#define USE_LINENOISE 1
#define jim_ext_aio 1
#define jim_ext_array 1
#define jim_ext_binary 1
//#define jim_ext_clock 1
#define jim_ext_eventloop 1
//#define jim_ext_exec 1
#define jim_ext_file 1
#define jim_ext_glob 1
#define jim_ext_history 1
//#define jim_ext_load 1
#define jim_ext_namespace 1
#define jim_ext_nshelper 1
#define jim_ext_oo 1
#define jim_ext_pack 1
#define jim_ext_package 1
//#define jim_ext_posix 1
//#define jim_ext_readdir 1
#define jim_ext_regexp 1
//#define jim_ext_signal 1
#define jim_ext_stdlib 1
//#define jim_ext_syslog 1
#define jim_ext_tclcompat 1
#define jim_ext_tclprefix 1
#define jim_ext_tree 1
#define fdopen _fdopen
#define fileno _fileno
#define access _access
#define mkdir _mkdir
#define unlink _unlink
#define chdir _chdir
#define getcwd _getcwd
#define strdup _strdup
#else
#include "jimautoconf_linux.h"
#endif
#define JIM_MATH_FUNCTIONS 1
#endif
//#define USE_LINENOISE 1

#define JIM_VERSION  100