#ifndef __MYALLOC_H__
#define __MYALLOC_H__


typedef unsigned int size_t;
struct Mem_List_t
{
	int index;
	int address;
	struct Mem_List_t *next;
};

void *MyMalloc(size_t size);
void *MyCalloc(size_t nmemb, size_t size);
void MyFree(void *ptr);
void MyMemReInit(void);
#endif