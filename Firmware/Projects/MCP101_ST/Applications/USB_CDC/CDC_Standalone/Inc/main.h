/**
  ******************************************************************************
  * @file    USB_Device/CDC_Standalone/Inc/main.h 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    17-December-2014
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stdbool.h"
#include "stm32f1xx_hal.h"
#include "stm3210e_eval.h"
#include "usbd_core.h"
#include "stm32f1xx_hal_pcd.h"
 #include "stm32f1xx_hal_dac.h"
#include "usbd_desc.h"
#include "usbd_cdc.h" 
#include "usbd_cdc_interface.h"
#include "errno.h"
#include <helper/replacements.h>

#define SF_FILESIZE_START 0
#define SF_FILESIZE_SIZE 4

#define SF_FILENAME_START (SF_FILESIZE_START+SF_FILESIZE_SIZE)
#define SF_FILENAME_SIZE 28

#define SF_TIME_START (SF_FILENAME_START+SF_FILENAME_SIZE)
#define SF_TIME_SIZE 32


#define I2C_ADDRESS        0x3E
#define MASTER_REQ_READ    0x12
#define MASTER_REQ_WRITE   0x34

/* I2C SPEEDCLOCK define to max value: 400 KHz on STM32F1xx*/
#define I2C_SPEEDCLOCK   400000
#define I2C_DUTYCYCLE    I2C_DUTYCYCLE_16_9//I2C_DUTYCYCLE_2

/* Definition for I2Cx clock resources */
#define I2Cx                            I2C1
#define I2Cx_CLK_ENABLE()               __HAL_RCC_I2C1_CLK_ENABLE()
#define I2Cx_SDA_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()
#define I2Cx_SCL_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE() 

#define I2Cx_FORCE_RESET()              __HAL_RCC_I2C1_FORCE_RESET()
#define I2Cx_RELEASE_RESET()            __HAL_RCC_I2C1_RELEASE_RESET()

/* Definition for I2Cx Pins */
#define I2Cx_SCL_PIN                    GPIO_PIN_6
#define I2Cx_SCL_GPIO_PORT              GPIOB
#define I2Cx_SDA_PIN                    GPIO_PIN_7
#define I2Cx_SDA_GPIO_PORT              GPIOB

/* Definition for I2Cx's NVIC */
#define I2Cx_EV_IRQn                    I2C1_EV_IRQn
#define I2Cx_ER_IRQn                    I2C1_ER_IRQn
#define I2Cx_EV_IRQHandler              I2C1_EV_IRQHandler
#define I2Cx_ER_IRQHandler              I2C1_ER_IRQHandler

#define assert(_Expression)     ((void)0)
	
/* Definition for TIMx clock resources */
#define TIMxx                           TIM1
#define TIMxx_CLK_ENABLE()              __HAL_RCC_TIM1_CLK_ENABLE()

/* Definition for TIMx Channel Pins */
#define TIMxx_CHANNEL_GPIO_PORT()       __HAL_RCC_GPIOA_CLK_ENABLE()
#define TIMxx_GPIO_PORT_CHANNEL1        GPIOA
#define TIMxx_GPIO_PORT_CHANNEL2        GPIOA
#define TIMxx_GPIO_PORT_CHANNEL3        GPIOA
#define TIMxx_GPIO_PORT_CHANNEL4        GPIOA
#define TIMxx_GPIO_PIN_CHANNEL1         GPIO_PIN_8
#define TIMxx_GPIO_AF_CHANNEL1          /
#define TIMxx_GPIO_AF_CHANNEL2          /
#define TIMxx_GPIO_AF_CHANNEL3          /
#define TIMxx_GPIO_AF_CHANNEL4          /
/*
 * Operations on timevals.
 *
 * NB: timercmp does not work for >= or <=.
 */
#define PRIx8  
#define PRIx32 "x"
#define PRId32 "d"
#define SCNx32 "x"
#define PRIi32 "i"
#define PRIu32 "u"
#define PRId8 PRId32
#define SCNx64 "llx"
#define PRIx64 "llx"

#define timerisset(tvp)         ((tvp)->tv_sec || (tvp)->tv_usec)
#define timercmp(tvp, uvp, cmp) \
        ((tvp)->tv_sec cmp (uvp)->tv_sec || \
         (tvp)->tv_sec == (uvp)->tv_sec && (tvp)->tv_usec cmp (uvp)->tv_usec)
#define timerclear(tvp)         (tvp)->tv_sec = (tvp)->tv_usec = 0

#define exit(status) return status;


#define usleep(T) Delayms(T/1000)
#define msleep(T) Delayms(T)
#define sleep(T) Delayms(T)

 

char * strdup(const char * _Src);

/* Exported types ------------------------------------------------------------*/
typedef enum{
	STATISTICS=0,
	FAIL_STATISTICS,
	LOGBUF,
	CONFIGFILE,
	QUARKROMFILE,
	SAPFILE,
	
	UNKNOWFILE=0xFF
}FileBufType;
typedef enum{
	MODE_W=1,
	MODE_R=2
}FileMode;
typedef struct
{
	FileBufType type;
	uint32_t offset;
	uint32_t size;
	uint32_t mode;
	char FileName[SF_FILENAME_SIZE];
	char time[SF_TIME_SIZE];
}SFHandle;


typedef enum
{
	USERMSG,
	ERRMSG,
	WARNINGMSG,
	INFOMSG,
	DEBUGMSG,
	FULLMSG,
	IICMSG,
}MsgType;
typedef enum __USBSer_DATA_TYPE_t
{
/**   Parameters Define Start  **/


	/**   File Transport 	**/
	USBSer_FILE_START = 0xE0,
	USBSer_FILE_DATA,
	USBSer_FILE_END,


	USBSer_MEM_START,
	USBSer_MEM_DATA,
	USBSer_MEM_END,

	USBSer_DATA_SW_FIRMWARE,

	
	USBSer_STATISTICS_FAIL,
	USBSer_STATISTICS_OK,
	USBSer_STATISTICS_SUM,

	USBSer_DATA_LOG,//response for log
	
	
	USBSer_ACTION_LOGONOFF,
	

	USBSer_ACTION_CONNECT,
	USBSer_ACTION_BLANKCHECK,
	USBSer_ACTION_ERASE,
	USBSer_ACTION_PROGRAM,
	USBSer_ACTION_CRC,
	USBSer_ACTION_DISCONNECT,
	USBSer_ACTION_RESETSTATISTICS,
	USBSer_GET_VERSION,
	
	USBSer_FW_Update,
	USBSer_Data_ChipIndex,
	USBSer_Data_ERR,
	
	USBSer_FILE_Info,
		USBSer_ACTION_AUTOPROGRAM,
	USBSer_AUTO_SAMPLE,
	


}USBSer_DATATYPE;
typedef struct I2COPStage
{
	uint8_t IsBusy;
	uint8_t IsOK;
	uint8_t Msg[22];
}I2COPStage_t;

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define PRIx32  
#define PRIu32
#define PRId32

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void Toggle_Leds(void);
void Error_Handler(void);


int MsgOutput(MsgType type,const char fmt[], ...);
extern int dbg_deep;
#ifndef USE_SYSLOG
#define  uP(...)       if(dbg_deep>=0)	MsgOutput(USERMSG,__VA_ARGS__)
#else
#define  uP(...)       if(dbg_deep>=0)	mtk_sf_printf(__VA_ARGS__,-1)
#endif

#ifndef USE_SYSLOG
#define  tP(...)       if(dbg_deep>=0)	MsgOutput(-1,__VA_ARGS__)
#else
#define  tP(...)       if(dbg_deep>=0)	mtk_sf_printf(__VA_ARGS__,-1)
#endif

#ifndef USE_SYSLOG
#define  cP(...)       if(dbg_deep>1)	MsgOutput(USERMSG,__VA_ARGS__)
#else
#define  cP(...)       if(dbg_deep>1)	mtk_sf_printf(USERMSG,__VA_ARGS__,-1)
#endif

#ifndef USE_SYSLOG
#define  eP(...)       if(dbg_deep>=0) MsgOutput(ERRMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // Error
#else
#define  eP(...)       if(dbg_deep>=0) mtk_sf_printf(__VA_ARGS__,-1)//mtk_sf_printf(__VA_ARGS__,-1) // Error
#endif

#define  wP(...)       if(dbg_deep>1) MsgOutput(WARNINGMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)	    // Warning
#define  iP(...)       if(dbg_deep>2) MsgOutput(INFOMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)	    // Info

#define  dP(...)       	  if(dbg_deep>1) MsgOutput(DEBUGMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)  // debug
#define  DP(...)       	  if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1)  // full print
#define  vP(...)       	   if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // full print
#define  VP(...)       	   if(dbg_deep>1) MsgOutput(FULLMSG,__VA_ARGS__)//mtk_printf(__VA_ARGS__,-1) // full print
#define  printPacket(x,y)  if(dbg_deep>2) printf_memory(y,x) // 

#define	iiP(...)				if(dbg_deep>=0)		MsgOutput(IICMSG,__VA_ARGS__)
#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
