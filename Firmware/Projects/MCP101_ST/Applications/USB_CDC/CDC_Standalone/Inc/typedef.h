#ifndef __TYPEDEF_H__
#define __TYPEDEF_H__
typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;
typedef int int32_t;
typedef short int16_t;
typedef char int8_t;
typedef int bool;
typedef unsigned long long uint64_t ;
typedef long long int64_t ;
#define inline __inline
#define false 0
#define true 1

#endif