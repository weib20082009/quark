/**
  ******************************************************************************
  * @file    stm3210e_eval.c
  * @author  MCD Application Team
  * @version V6.0.0
  * @date    16-December-2014
  * @brief   This file provides a set of firmware functions to manage Leds, 
  *          push-button and COM ports for STM3210E_EVAL
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/
//#include "stm3210e_eval.h"
#include "common.h"
/** @addtogroup BSP
  * @{
  */ 


/** @defgroup STM3210E_EVAL_Private_TypesDefinitions Private Types Definitions
  * @{
  */ 
extern int gYellowBlink;
/**
 * @brief BUS variables
 */
#ifdef HAL_SPI_MODULE_ENABLED
uint32_t SpixTimeout = EVAL_SPIx_TIMEOUT_MAX;        /*<! Value of Timeout when SPI communication fails */
SPI_HandleTypeDef heval_Spi;
#endif /* HAL_SPI_MODULE_ENABLED */

#ifdef HAL_I2C_MODULE_ENABLED
uint32_t I2cxTimeout = EVAL_I2Cx_TIMEOUT_MAX;   /*<! Value of Timeout when I2C communication fails */
I2C_HandleTypeDef heval_I2c;
#endif /* HAL_I2C_MODULE_ENABLED */
#define ADCCONVERTEDVALUES_BUFFER_SIZE ((uint32_t)    1) 
__IO uint16_t   aADCxConvertedValues[ADCCONVERTEDVALUES_BUFFER_SIZE];

DAC_HandleTypeDef hdac;

/**
  * @}
  */ 



/* I2Cx bus function */
#ifdef HAL_I2C_MODULE_ENABLED
/* Link function for I2C EEPROM peripheral */
void               I2Cx_Init(void);
static void               I2Cx_WriteData(uint16_t Addr, uint8_t Reg, uint8_t Value);
static HAL_StatusTypeDef  I2Cx_WriteBuffer(uint16_t Addr, uint8_t Reg, uint16_t RegSize, uint8_t *pBuffer, uint16_t Length);
static uint8_t            I2Cx_ReadData(uint16_t Addr, uint8_t Reg);
static HAL_StatusTypeDef  I2Cx_ReadBuffer(uint16_t Addr, uint8_t Reg, uint16_t RegSize, uint8_t *pBuffer, uint16_t Length);
static HAL_StatusTypeDef  I2Cx_IsDeviceReady(uint16_t DevAddress, uint32_t Trials);
static void               I2Cx_Error (void);
static void               I2Cx_MspInit(I2C_HandleTypeDef *hi2c);  



#endif /* HAL_I2C_MODULE_ENABLED */

#ifdef HAL_SPI_MODULE_ENABLED
/* SPIx bus function */
static HAL_StatusTypeDef  SPIx_Init(void);
static uint8_t            SPIx_Write(uint8_t Value);
static uint8_t            SPIx_Read(void);
static void               SPIx_Error (void);
static void               SPIx_MspInit(SPI_HandleTypeDef *hspi);


#endif /* HAL_SPI_MODULE_ENABLED */

ADC_HandleTypeDef    AdcHandle;
ADC_HandleTypeDef    Adc2Handle;
uint8_t usbrxbuf[USB_BUF_SIZE*2];//buffer for usb receive
uint8_t usbtxbuf[USB_BUF_SIZE*2];

/** @defgroup STM3210E_EVAL_Exported_Functions Exported Functions
  * @{
  */ 





/**
  * @}
  */ 

/** @defgroup STM3210E_EVAL_BusOperations_Functions Bus Operations Functions
  * @{
  */ 

/*******************************************************************************
                            BUS OPERATIONS
*******************************************************************************/

#ifdef HAL_I2C_MODULE_ENABLED
/******************************* I2C Routines**********************************/

/**
  * @brief Eval I2Cx MSP Initialization
  * @param hi2c: I2C handle
  * @retval None
  */
static void I2Cx_MspInit(I2C_HandleTypeDef *hi2c)
{
  GPIO_InitTypeDef  gpioinitstruct = {0};  

  if (hi2c->Instance == EVAL_I2Cx)
  {
    /*## Configure the GPIOs ################################################*/  

    /* Enable GPIO clock */
    EVAL_I2Cx_SDA_GPIO_CLK_ENABLE();
    EVAL_I2Cx_SCL_GPIO_CLK_ENABLE();
          
    /* Configure I2C Tx as alternate function  */
    gpioinitstruct.Pin       = EVAL_I2Cx_SCL_PIN;
    gpioinitstruct.Mode      = GPIO_MODE_AF_OD;
    gpioinitstruct.Pull      = GPIO_NOPULL;
    gpioinitstruct.Speed     = GPIO_SPEED_HIGH;
    HAL_GPIO_Init(EVAL_I2Cx_SCL_GPIO_PORT, &gpioinitstruct);
      
    /* Configure I2C Rx as alternate function  */
    gpioinitstruct.Pin = EVAL_I2Cx_SDA_PIN;
    HAL_GPIO_Init(EVAL_I2Cx_SDA_GPIO_PORT, &gpioinitstruct);
    
    
    /*## Configure the Eval I2Cx peripheral #######################################*/ 
    /* Enable Eval_I2Cx clock */
    EVAL_I2Cx_CLK_ENABLE();
    
    /* Add delay related to RCC workaround */
    while (READ_BIT(RCC->APB1ENR, RCC_APB1ENR_I2C1EN) != RCC_APB1ENR_I2C1EN) {};
      
    /* Force the I2C Periheral Clock Reset */  
    EVAL_I2Cx_FORCE_RESET();
      
    /* Release the I2C Periheral Clock Reset */  
    EVAL_I2Cx_RELEASE_RESET(); 
    
    /* Enable and set Eval I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(EVAL_I2Cx_EV_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(EVAL_I2Cx_EV_IRQn);
    
    /* Enable and set Eval I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(EVAL_I2Cx_ER_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(EVAL_I2Cx_ER_IRQn);  
  }
}

/**
  * @brief Eval I2Cx Bus initialization
  * @retval None
  */
void I2Cx_Init(void)
{
    heval_I2c.Instance              = EVAL_I2Cx;
    heval_I2c.Init.ClockSpeed       = BSP_I2C_SPEED;
    heval_I2c.Init.DutyCycle        = I2C_DUTYCYCLE_2;
    heval_I2c.Init.OwnAddress1      = I2C_ADDRESS;
    heval_I2c.Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
    heval_I2c.Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
    heval_I2c.Init.OwnAddress2      = 0xFF;
    heval_I2c.Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
    heval_I2c.Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;  

    /* Init the I2C */
    I2Cx_MspInit(&heval_I2c);
    HAL_I2C_Init(&heval_I2c);
}

/**
  * @brief  Write a value in a register of the device through BUS.
  * @param  Addr: Device address on BUS Bus.  
  * @param  Reg: The target register address to write
  * @param  Value: The target register value to be written 
  * @retval  None
  */
static void I2Cx_WriteData(uint16_t Addr, uint8_t Reg, uint8_t Value)
{
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Mem_Write(&heval_I2c, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, I2cxTimeout);
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    I2Cx_Error();
  }
}

/**
  * @brief  Write a value in a register of the device through BUS.
  * @param  Addr: Device address on BUS Bus.  
  * @param  Reg: The target register address to write
  * @param  RegSize: The target register size (can be 8BIT or 16BIT)
  * @param  pBuffer: The target register value to be written 
  * @param  Length: buffer size to be written
  * @retval None
  */
static HAL_StatusTypeDef I2Cx_WriteBuffer(uint16_t Addr, uint8_t Reg, uint16_t RegSize, uint8_t *pBuffer, uint16_t Length)
{
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Mem_Write(&heval_I2c, Addr, (uint16_t)Reg, RegSize, pBuffer, Length, I2cxTimeout); 

/* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initiaize the BUS */
    I2Cx_Error();
  }        
  return status;
}

/**
  * @brief  Read a value in a register of the device through BUS.
  * @param  Addr: Device address on BUS Bus.  
  * @param  Reg: The target register address to write
  * @retval Data read at register @
  */
static uint8_t I2Cx_ReadData(uint16_t Addr, uint8_t Reg)
{
  HAL_StatusTypeDef status = HAL_OK;
  uint8_t value = 0;
  
  status = HAL_I2C_Mem_Read(&heval_I2c, Addr, Reg, I2C_MEMADD_SIZE_8BIT, &value, 1, I2cxTimeout);
 
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    I2Cx_Error();
  
  }
  return value;
}

/**
  * @brief  Reads multiple data on the BUS.
  * @param  Addr: I2C Address
  * @param  Reg: Reg Address 
  * @param  RegSize : The target register size (can be 8BIT or 16BIT)
  * @param  pBuffer: pointer to read data buffer
  * @param  Length: length of the data
  * @retval 0 if no problems to read multiple data
  */
static HAL_StatusTypeDef I2Cx_ReadBuffer(uint16_t Addr, uint8_t Reg, uint16_t RegSize, uint8_t *pBuffer, uint16_t Length)
{
  HAL_StatusTypeDef status = HAL_OK;

  status = HAL_I2C_Mem_Read(&heval_I2c, Addr, (uint16_t)Reg, RegSize, pBuffer, Length, I2cxTimeout);
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initiaize the BUS */
    I2Cx_Error();
  }        
  return status;
}

/**
* @brief  Checks if target device is ready for communication. 
* @note   This function is used with Memory devices
* @param  DevAddress: Target device address
* @param  Trials: Number of trials
* @retval HAL status
*/
static HAL_StatusTypeDef I2Cx_IsDeviceReady(uint16_t DevAddress, uint32_t Trials)
{ 
  return (HAL_I2C_IsDeviceReady(&heval_I2c, DevAddress, Trials, I2cxTimeout));
}


/**
  * @brief Eval I2Cx error treatment function
  * @retval None
  */
static void I2Cx_Error (void)
{
  /* De-initialize the I2C communication BUS */
  HAL_I2C_DeInit(&heval_I2c);
  
  /* Re- Initiaize the I2C communication BUS */
  I2Cx_Init();
}

#endif /* HAL_I2C_MODULE_ENABLED */

/******************************* SPI Routines**********************************/
#ifdef HAL_SPI_MODULE_ENABLED
/**
  * @brief  Initializes SPI MSP.
  * @retval None
  */
static void SPIx_MspInit(SPI_HandleTypeDef *hspi)
{
  GPIO_InitTypeDef  gpioinitstruct = {0};
  
  /*** Configure the GPIOs ***/  
  /* Enable GPIO clock */
  EVAL_SPIx_SCK_GPIO_CLK_ENABLE();
  EVAL_SPIx_MISO_MOSI_GPIO_CLK_ENABLE();
  
  /* configure SPI SCK */
  gpioinitstruct.Pin        = EVAL_SPIx_SCK_PIN;
  gpioinitstruct.Mode       = GPIO_MODE_AF_PP;
  gpioinitstruct.Pull       = GPIO_NOPULL;
  gpioinitstruct.Speed      = GPIO_SPEED_HIGH;
  HAL_GPIO_Init(EVAL_SPIx_SCK_GPIO_PORT, &gpioinitstruct);

  /* configure SPI MISO and MOSI */
  gpioinitstruct.Pin        = (EVAL_SPIx_MISO_PIN | EVAL_SPIx_MOSI_PIN);
  gpioinitstruct.Mode       = GPIO_MODE_AF_PP;
  gpioinitstruct.Pull       = GPIO_NOPULL;
  gpioinitstruct.Speed      = GPIO_SPEED_HIGH;
  HAL_GPIO_Init(EVAL_SPIx_MISO_MOSI_GPIO_PORT, &gpioinitstruct);

  /*** Configure the SPI peripheral ***/ 
  /* Enable SPI clock */
  EVAL_SPIx_CLK_ENABLE();
}

/**
  * @brief  Initializes SPI HAL.
  * @retval None
  */
HAL_StatusTypeDef SPIx_Init(void)
{
  /* DeInitializes the SPI peripheral */
  heval_Spi.Instance = EVAL_SPIx;
  HAL_SPI_DeInit(&heval_Spi);

  /* SPI Config */
  /* SPI baudrate is set to 36 MHz (PCLK2/SPI_BaudRatePrescaler = 72/2 = 36 MHz) */
  heval_Spi.Init.BaudRatePrescaler  = SPI_BAUDRATEPRESCALER_2;
  heval_Spi.Init.Direction          = SPI_DIRECTION_2LINES;
  heval_Spi.Init.CLKPhase           = SPI_PHASE_1EDGE;
  heval_Spi.Init.CLKPolarity        = SPI_POLARITY_LOW;
  heval_Spi.Init.CRCCalculation     = SPI_CRCCALCULATION_DISABLE;
  heval_Spi.Init.CRCPolynomial      = 7;
  heval_Spi.Init.DataSize           = SPI_DATASIZE_8BIT;
  heval_Spi.Init.FirstBit           = SPI_FIRSTBIT_MSB;
  heval_Spi.Init.NSS                = SPI_NSS_SOFT;
  heval_Spi.Init.TIMode             = SPI_TIMODE_DISABLE;
  heval_Spi.Init.Mode               = SPI_MODE_MASTER;
  
  return (HAL_SPI_Init(&heval_Spi));
}


/**
  * @brief  SPI Write a byte to device
  * @param  WriteValue to be written
  * @retval The value of the received byte.
  */
static uint8_t SPIx_Write(uint8_t WriteValue)
{
  HAL_StatusTypeDef status = HAL_OK;
  uint8_t ReadValue = 0;

  status = HAL_SPI_TransmitReceive(&heval_Spi, (uint8_t*) &WriteValue, (uint8_t*) &ReadValue, 1, SpixTimeout);

  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    SPIx_Error();
  }
  
   return ReadValue;
}


/**
  * @brief SPI Read 1 byte from device
  * @retval Read data
*/
static uint8_t SPIx_Read(void)
{
  return (SPIx_Write(FLASH_SPI_DUMMY_BYTE));
}


/**
  * @brief SPI error treatment function
  * @retval None
  */
static void SPIx_Error (void)
{
  /* De-initialize the SPI communication BUS */
  HAL_SPI_DeInit(&heval_Spi);
  
  /* Re- Initiaize the SPI communication BUS */
  SPIx_Init();
}
#endif /* HAL_SPI_MODULE_ENABLED */



void led_green_on()
{
	 LED_G_GPIO->ODR &= ~LED_G_BIT;	
}	
void led_green_off()
{
	LED_G_GPIO->ODR |= LED_G_BIT;	
}	

void led_red_on()
{
	 LED_R_GPIO->ODR  &= ~LED_R_BIT;
}	
void led_red_off()
{
	LED_R_GPIO->ODR |= LED_R_BIT;	
}	
void led_yellow_on()
{
	 LED_Y_GPIO->ODR &= ~LED_Y_BIT;	
}	
void led_yellow_off()
{
	LED_Y_GPIO->ODR  |= LED_Y_BIT;	
}	

uint8_t led_status = 0;  // 0bit - green on, 1bit - red on, 2bit - yellow on
						 // 4bit - green blink, 5bit - red blink, 6 bit - yellow blink
int led_blink_id;

void led_blinking(void)
{
	if(led_status & 0x10)  //green
	{ 
		if(led_status & 1)
		{	led_green_off();led_status &= ~1;}
		else  if((led_status & 1) == 0)			
		{	led_green_on();led_status |= 1;}
	}
		
	if(led_status & 0x20) //red
	{	
		if(led_status & 2)
		{	led_red_off();led_status &= ~2;}
		else  if((led_status & 2) == 0)			
		{	led_red_on();led_status |=  2;}
	}
	if(led_status & 0x40)
		if(led_status & 4)
		{ led_yellow_off(); led_status &= ~4;}
		else if((led_status & 4) == 0)		
		{ led_yellow_on(); led_status |=  4;}
}

void led_green_blink()
{
	led_status |= 0x10;
///	led_blink_id = PIT_Enable(led_blinking, 100);
}
void led_green_blink_off()
{
	led_status &= ~0x10;
	led_green_off();
///	if(led_status & 0xF0 == 0)
///		PIT_Disable(led_blink_id);
}
void led_red_blink()
{
	led_status |= 0x20;
///	led_blink_id = PIT_Enable(led_blinking, 100);
}
void led_red_blink_off()
{
	led_status &= ~0x20;
	led_red_off();
	///if(led_status & 0xF0 == 0)
///		PIT_Disable(led_blink_id);
}
void led_yellow_blink()
{
	//led_status |= 0x40;
	///led_blink_id = PIT_Enable(led_blinking, 100);
	gYellowBlink=1;
}
void led_yellow_blink_off()
{
	led_status &= ~0x40;
	gYellowBlink=0;
	led_yellow_off();
	///if(led_status & 0xF0 != 0)
///		PIT_Disable(led_blink_id);
}

void SPI0_PIN_Init()
{


}

extern int gProgOnLine;
int is_button_pressed()
{
	if(!gProgOnLine)
		return (BUTTON_GPIO->IDR & GPIO_PIN_3);
	else
		return 1;
}

#define VIN_CTL_GPIO      GPIOA
#define VIN_CTL_BIT       GPIO_PIN_15   //APW7272_En
#define VIN_7_5_GPIO      GPIOC
#define VIN_7_5_BIT       GPIO_PIN_9	//switch_power_sel
#define VOUT_EN_GPIO      GPIOC
#define VOUT_EN_BIT       GPIO_PIN_10	//Boost_power_en
#define VOUT_DAC_GPIO     GPIOA
#define VOUT_DAC_BIT     GPIO_PIN_4    //DAC 
int MCP_DAC_Output(int v,int tuning)
{
	
	if(v == 5)  /* Set DAC Channel1 DHR12L register */
     HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1,DAC_ALIGN_12B_R,0xCDE + tuning);	//0xCDE 
	if(v == 3)  /* Set DAC Channel1 DHR12L register */
     HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R,0x640 + tuning);//0x640
	if(v == 2)  /* Set DAC Channel1 DHR12L register */
     HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R,0x3E8 + tuning);//0x3E8	
	if(v == 1)  /* Set DAC Channel1 DHR12L register */
     HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R,0x1C2+ tuning);//0x1C2 	

  if (HAL_DAC_Start(&hdac, DAC_CHANNEL_1) != HAL_OK)
  {
    /* Start Error */
  }

	 return 0;
}
void output_0v_io()
{
	VIN_CTL_GPIO->ODR &=  ~VIN_CTL_BIT;
	VOUT_EN_GPIO->ODR &=  ~VOUT_EN_BIT;
}
int MCP_Check_PowerCOM(int v)
{
	int VCom=get_COM_voltage(v);
	if(v == 5)  /* Set DAC Channel1 DHR12L register */
	{
		if(VCom*2<5000*4/5||VCom>5000*6/5)
			return -1;
	}
	if(v == 3)  /* Set DAC Channel1 DHR12L register */
	{
		if(VCom*2<3300*4/5||VCom>3300*6/5)
			return -1;
	}
	if(v == 2)  /* Set DAC Channel1 DHR12L register */
	{
		if(VCom*2<2500*4/5||VCom>2500*6/5)
			return -1;
	}
	if(v == 1)  /* Set DAC Channel1 DHR12L register */
	{
		if(VCom*2<1800*4/5||VCom>1800*6/5)
			return -1;
	}
	return 0;
}
int MCP_Ajust_PowerCOM(int v)
{
	int Acc=20;
	int VCom=0;
	int AdjVal=0;
	if(MCP_Check_PowerCOM(v)!=0)
	{
		return -1;
	}
	VCom=get_COM_voltage(v);
	if(v == 5)  /* Set DAC Channel1 DHR12L register */
	{
		if(VCom*2<5000)
			AdjVal=((5000-VCom*2)*0xFFF)/5000;
	}
	if(v == 3)  /* Set DAC Channel1 DHR12L register */
	{
		if(VCom*2<3300)
			AdjVal=((3300-VCom*2)*0xFFF)/3300;
	}
	if(v == 2)  /* Set DAC Channel1 DHR12L register */
	{
		if(VCom*2<2500)
			AdjVal=((2500-VCom*2)*0xFFF);
	}
	if(v == 1)  /* Set DAC Channel1 DHR12L register */
	{
		if(VCom*2<1800)
			AdjVal=((1800-VCom*2)*0xFFF);
	}
	if(AdjVal<=500&&AdjVal>=100)//protect the V from too high
	{
		VOUT_EN_GPIO->ODR &=  ~VOUT_EN_BIT;	
		MCP_DAC_Output(v,AdjVal);
		if(MCP_Check_PowerCOM(v)==0)
		{
			//output
			VOUT_EN_GPIO->ODR |=  VOUT_EN_BIT;
		}
	}		
	
	return 0;
}
// v is 5/3/2/1
void output_voltage(int v)
{
	int MCP_DAC_Output(int v,int tuning);
	if(v==5)
	{
		VIN_7_5_GPIO->ODR |=  VIN_7_5_BIT;
		VIN_CTL_GPIO->ODR |=  VIN_CTL_BIT;
		
	}else{
	    VIN_7_5_GPIO->ODR &=  ~VIN_7_5_BIT;	
	    VIN_CTL_GPIO->ODR &=  ~VIN_CTL_BIT;
	}	
		
    //Open DA
	MCP_DAC_Output(v,0);
	
	//check out voltage
	////if(MCP_Check_PowerCOM(v)==0)
	{
		//output
		VOUT_EN_GPIO->ODR |=  VOUT_EN_BIT;
	}
	//if(MCP_Ajust_PowerCOM(v)!=0)
	//{
	//	VOUT_EN_GPIO->ODR &=  ~VOUT_EN_BIT;
		
	//}
	
}
void output_5v_io()
{
	output_voltage(5);
}
void output_3v3_io()
{
	output_voltage(3);
}
void output_2v5_io()
{
	output_voltage(2);
}
void output_1v8_io()
{
	output_voltage(1);
}
/*
void output_5v_io()
{
	//output_0v_io();
	//VDD_5V_O_GPIO->ODR |=  VDD_5V_O_BIT;
}

void output_3v3_io()
{
	//output_0v_io();
	//VDD_3V3_O_GPIO->ODR |=  VDD_3V3_O_BIT;
	//VDD_V_EN_GPIO->ODR |=  VDD_V_EN_BIT;

}
void output_2v5_io()
{
	//	output_0v_io();
	//VDD_2V5_O_GPIO->ODR |=  VDD_2V5_O_BIT;
	//VDD_V_EN_GPIO->ODR |=  VDD_V_EN_BIT;
}
void output_1v8_io()
{
	//output_0v_io();
	//VDD_1V8_O_GPIO->ODR |=  VDD_1V8_O_BIT;
	//VDD_V_EN_GPIO->ODR |=  VDD_V_EN_BIT;
}

void output_0v_io(void)
{
	//VDD_5V_O_GPIO->ODR &=  ~VDD_5V_O_BIT;
	//VDD_V_EN_GPIO->ODR &=  ~VDD_V_EN_BIT;
	//VDD_3V3_O_GPIO->ODR &=  ~VDD_3V3_O_BIT;
	//VDD_2V5_O_GPIO->ODR &=  ~VDD_2V5_O_BIT;
	//VDD_1V8_O_GPIO->ODR &=  ~VDD_1V8_O_BIT;
	
}

void output_dir1_io()
{
	EXT_DIR1_GPIO->ODR|=EXT_DIR1_BIT;
}
void input_dir1_io()
{
	EXT_DIR1_GPIO->ODR&=~EXT_DIR1_BIT;
}
void output_dir2_io()
{
	EXT_DIR2_GPIO->ODR|=EXT_DIR2_BIT;
}
void input_dir2_io()
{
	EXT_DIR2_GPIO->ODR&=~EXT_DIR2_BIT;
}
*/
#if 0
void mtk_USB_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, ENABLE); 
  /* USB port */
   /* Configure USB pins:  */
  GPIO_InitStructure.GPIO_Pin = GPIO_PIN_11 | GPIO_PIN_12;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
  	
}
#endif
void Jtag_IO_Init()
{
	  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13|GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15|PIN_SWCLK_TCK|GPIO_PIN_5|GPIO_PIN_8, GPIO_PIN_RESET);




  /*Configure GPIO pins : PC13 PC5 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_5|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_RESET);
	
  /*Configure GPIO pin : PB14 */
  GPIO_InitStruct.Pin = GPIO_PIN_14|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PB15 PB3 PB5 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_15|PIN_SWCLK_TCK|GPIO_PIN_8;//|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8, 0);//TDO_DIR
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,1);//SWDIO_DIR
}
void Jtag_IO_DeInit()
{
	  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13|GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15|PIN_SWCLK_TCK|GPIO_PIN_5|GPIO_PIN_8, GPIO_PIN_RESET);




  /*Configure GPIO pins : PC13 PC5 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PB14 */
  GPIO_InitStruct.Pin = GPIO_PIN_14|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PB15 PB3 PB5 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_15|PIN_SWCLK_TCK|GPIO_PIN_8;//|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	
	 GPIO_InitStruct.Pin = GPIO_PIN_8;//|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	GPIO_InitStruct.Pin = GPIO_PIN_13;//|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8, 0);//TDO_DIR
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,0);//SWDIO_DIR
}
void Init_GPIO(void)
{
 GPIO_InitTypeDef GPIO_InitStructure;
 //  AFIO->MAPR &= 0xF8FFFFFF;
 // AFIO->MAPR |= 0x02000000;
 __HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_AFIO_CLK_ENABLE();
 // RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); 
__HAL_AFIO_REMAP_SWJ_NOJTAG();//PB3 is JTAGD by default

	//LED
	  GPIO_InitStructure.Pin =  LED_G_BIT | LED_R_BIT  | LED_Y_BIT;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	  HAL_GPIO_Init(LED_GPIO, &GPIO_InitStructure);
  // Button
  GPIO_InitStructure.Pin =  BUTTON_BIT ;
  GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull	=	GPIO_PULLDOWN;
  HAL_GPIO_Init(BUTTON_GPIO, &GPIO_InitStructure);
 /*
  // VCC_Detect
  GPIO_InitStructure.Pin =  VDD_DETECT_BIT ;
  GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull	=	GPIO_NOPULL;
  HAL_GPIO_Init(VDD_DETECT_GPIO, &GPIO_InitStructure);
 

  GPIO_InitStructure.Pin =  VDD_5V_O_BIT | VDD_V_EN_BIT ;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
  GPIO_InitStructure.Pull	=	GPIO_PULLUP;
  HAL_GPIO_Init(VDD_5V_O_GPIO, &GPIO_InitStructure);
	
  GPIO_InitStructure.Pin =  VDD_3V3_O_BIT | VDD_2V5_O_BIT | VDD_1V8_O_BIT ;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull	=	GPIO_PULLUP;
  HAL_GPIO_Init(VDD_3V3_O_GPIO, &GPIO_InitStructure);
    */
	GPIO_InitStructure.Pin =  ERASE_PIN_BIT ;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull	=	GPIO_NOPULL;
 // HAL_GPIO_Init(ERASE_PIN_GPIO, &GPIO_InitStructure);

	
	GPIO_InitStructure.Pin =  EXT_DIR1_BIT ;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull	=	GPIO_NOPULL;
 // HAL_GPIO_Init(EXT_DIR1_GPIO, &GPIO_InitStructure);
	
	
	GPIO_InitStructure.Pin =  EXT_DIR2_BIT ;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull	=	GPIO_NOPULL;
  //HAL_GPIO_Init(EXT_DIR2_GPIO, &GPIO_InitStructure);
	{
	  GPIO_InitStructure.Pin =  GPIO_PIN_10 | GPIO_PIN_11 ;
  GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStructure.Pull	=	GPIO_PULLDOWN;
 // HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
	}
	  /*Configure GPIO pin : PB5 */
  GPIO_InitStructure.Pin = GPIO_PIN_5;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_InitStructure.Pin = GPIO_PIN_9|GPIO_PIN_10;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.Pin = GPIO_PIN_15;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);		
	
	
	GPIO_InitStructure.Pin = CS_BIT;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);	
	
	GPIO_InitStructure.Pin = GPIO_PIN_11;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);	
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_11,1);
	
	

	
	Jtag_IO_Init();
	

	
	//HAL_GPIO_WritePin(GPIOB,GPIO_PIN_15,0);
	//HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,0);
	//HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,0);
	
	led_green_off();
	led_red_off();
	led_yellow_off();

	output_0v_io();

}

/* ************************************************* */
/* **********    SPI Flash mapping     ************* */
/* ************************************************* */

void SPI_FLASH_CS_LOW(void)
{
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6,GPIO_PIN_RESET);	
}


void SPI_FLASH_CS_HIGH(void)
{
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6,GPIO_PIN_SET);
}

SPI_TypeDef * SPI;
SPI_HandleTypeDef *h_Spi;
void SPI_FLASH_Init(void)
{
  SPI_InitTypeDef  SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
   
  /* Enable SPI1 and GPIOA clocks */
  //RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1 | RCC_APB2Periph_GPIOA, ENABLE);
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();	
	__HAL_RCC_SPI1_CLK_ENABLE();
  /* Configure SPI1 pins: NSS, SCK, MISO and MOSI */
  GPIO_InitStructure.Pin = GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
  GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Configure PA.4 as Output push-pull, used as Flash Chip select */
  GPIO_InitStructure.Pin = GPIO_PIN_6;//GPIO_PIN_4;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  /* SPI1 configuration */ 
	SPIx_Init();
	__HAL_SPI_ENABLE(&heval_Spi);

	extern void SST25V_Init(SPI_HandleTypeDef * hSPI);
	SST25V_Init(&heval_Spi);
	#if 0
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPI1, &SPI_InitStructure);

  /* Enable SPI1  */
  SPI_Cmd(SPI1, ENABLE);  
  #endif	
}
int flash_size;
int spiflash_size(void)
{
	return flash_size;
}
extern void SST25V_Init(SPI_HandleTypeDef * nSPI);
int iSST=1;
int spiflash_probe(uint8_t bus, uint8_t cs)
{
	uint32_t flash_id,sector_size=4096,sector_num;
	volatile uint32_t size,page_size=256;
	char name[30]={0};
	SPI_FLASH_Init();
	//SST25V_Init(SPI1);
	//SPI_FLASH_block_protect();
	flash_id = SST25V_ReadJedecID();
	mtk_printf("SPI FLASH:\n\rID:",flash_id);
	if(((flash_id>>16)&0xFF)==0xEF)
	{
			sector_num=512;
			iSST = 0;
	}
    if((flash_id & 0x000000ff)== 0x8d)
	{
		sector_num = 128;
		sprintf(name,"Name:\t%s\n\r","SST25VF040B");
		mtk_printf(name, -1);
	}	
	if((flash_id & 0x000000ff)== 0x8e)
	{
		sector_num = 256;
		sprintf(name,"Name:\t%s\n\r","SST25VF080B");
		mtk_printf(name, -1);
	}	
	if((flash_id & 0x000000ff)== 0x41)
	{
		sector_num = 512;
		sprintf(name,"Name:\t%s\n\r","SST25VF160B");
		mtk_printf(name, -1);
	}	
	flash_size = size = sector_size * sector_num;
	mtk_printf("Size:",size);
	
    return 0;
}

int spiflash_read(uint8_t *buf,uint32_t address,uint32_t length)
{
	if(iSST)
	SST25V_HighSpeedBufferRead(buf, address, length);
	else
	{
		WX25_BufferRead(buf, address, length);
	}
	
    return 1;
}

int spiflash_write(uint8_t *buf,uint32_t address,uint32_t length)
{
	int tmp=0;
	if(iSST)
	{
		///if(!length%2)
		if(address%2)
		{
			SST25V_BufferWrite(buf, address, 1);
			buf++;
			address++;
			length--;
		}
		///else
		tmp=length%2;
		
		SST25V_AAI_BufferProgram(buf,address,length-tmp);
		
		if(tmp)
			SST25V_BufferWrite(buf+length-tmp, address+length-tmp, tmp);
	}
	else
	{
		WX25_BufferWrite(buf,address,length);
	}
	return 1;
}
int spiflash_erasechip(void)
{
	if(iSST)	
		SST25V_ChipErase();
	else
	{
	
	
	}
	return 1;
}
int spiflash_erase(uint32_t address,uint32_t length)
{
	int ii=0;
	if(iSST)
	{
		while(length > 0)
		{
				SST25V_SectorErase_4KByte(address+ii*4096);
			if(length>=4096)
				length -= 4096;
			else
				break;
			ii++;
		}	
	}else
	{
	
		while(length > 0)
		{
				WX25_SectorErase(address+ii*4096);
			if(length>=4096)
				length -= 4096;
			else
				break;
			ii++;
		}		
	}
	return 1;
}
uint8_t SPI_Flash_SendByte(uint8_t byte)
{
  uint8_t buf;
	//while(SPI_I2S_GetFlagStatus(SPI, SPI_I2S_FLAG_TXE) == RESET);
 //  SPI_SendData8(SPI, byte);
   //SPI_I2S_SendData(SPI, byte);
	if(HAL_SPI_TransmitReceive(h_Spi,&byte,&buf,1,5000)!=HAL_OK)
	{
		return -1;
	}
	//HAL_SPI_Transmit(h_Spi,&byte,1,1000);
 // while(SPI_I2S_GetFlagStatus(SPI, SPI_FLAG_TXE) == RESET);
  //return SPI_ReceiveData8(SPI);
  /* Return the byte read from the SPI bus */
 // return SPI_I2S_ReceiveData(SPI);
	//HAL_SPI_Receive(h_Spi,&buf,1,1000);
	return buf;
}

uint8_t SPI_Flash_ReceiveByte(void)
{
  return (SPI_Flash_SendByte(0xFF));
}
/* ******** ADC      ******************************  */

/**
  * @brief  ADC configuration
  * @param  None
  * @retval None
  */
void ADC_Config(void)
{
  ADC_ChannelConfTypeDef   sConfig;
  
  /* Configuration of ADCx init structure: ADC parameters and regular group */
  AdcHandle.Instance = ADCx;
  
  AdcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  AdcHandle.Init.ScanConvMode          = ADC_SCAN_DISABLE;               /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
  AdcHandle.Init.ContinuousConvMode    = ENABLE;                       /* Continuous mode disabled to have only 1 rank converted at each conversion trig, and because discontinuous mode is enabled */
  AdcHandle.Init.NbrOfConversion       = 1;                             /* Sequencer of regular group will convert the 3 first ranks: rank1, rank2, rank3 */
  AdcHandle.Init.DiscontinuousConvMode = DISABLE;                        /* Sequencer of regular group will convert the sequence in several sub-divided sequences */
  AdcHandle.Init.NbrOfDiscConversion   = 1;                             /* Sequencer of regular group will convert ranks one by one, at each conversion trig */
  AdcHandle.Init.ExternalTrigConv      = ADC_SOFTWARE_START;            /* Trig of conversion start done manually by software, without external event */

  if (HAL_ADC_Init(&AdcHandle) != HAL_OK)
  {
    /* ADC initialization error */
   // Error_Handler();
		return ;
  }
  
  /* Configuration of channel on ADCx regular group on sequencer rank 1 */
  /* Note: Considering IT occurring after each ADC conversion (IT by DMA end  */
  /*       of transfer), select sampling time and ADC clock with sufficient   */
  /*       duration to not create an overhead situation in IRQHandler.        */
  /* Note: Set long sampling time due to internal channels (VrefInt,          */
  /*       temperature sensor) constraints. Refer to device datasheet for     */
  /*       min/typ/max values.                                                */
  sConfig.Channel      = ADCx_CHANNELa;
  sConfig.Rank         = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
    /* Channel Configuration Error */
    //Error_Handler();
		return;
  }
  
   Adc2Handle.Instance = ADCy;
  
  Adc2Handle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  Adc2Handle.Init.ScanConvMode          = ADC_SCAN_DISABLE;               /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
  Adc2Handle.Init.ContinuousConvMode    = ENABLE;                       /* Continuous mode disabled to have only 1 rank converted at each conversion trig, and because discontinuous mode is enabled */
  Adc2Handle.Init.NbrOfConversion       = 1;                             /* Sequencer of regular group will convert the 3 first ranks: rank1, rank2, rank3 */
  Adc2Handle.Init.DiscontinuousConvMode = DISABLE;                        /* Sequencer of regular group will convert the sequence in several sub-divided sequences */
  Adc2Handle.Init.NbrOfDiscConversion   = 1;                             /* Sequencer of regular group will convert ranks one by one, at each conversion trig */
  Adc2Handle.Init.ExternalTrigConv      = ADC_SOFTWARE_START;            /* Trig of conversion start done manually by software, without external event */

  if (HAL_ADC_Init(&Adc2Handle) != HAL_OK)
  {
    /* ADC initialization error */
   // Error_Handler();
		return ;
  }
  
  /* Configuration of channel on ADCx regular group on sequencer rank 1 */
  /* Note: Considering IT occurring after each ADC conversion (IT by DMA end  */
  /*       of transfer), select sampling time and ADC clock with sufficient   */
  /*       duration to not create an overhead situation in IRQHandler.        */
  /* Note: Set long sampling time due to internal channels (VrefInt,          */
  /*       temperature sensor) constraints. Refer to device datasheet for     */
  /*       min/typ/max values.                                                */
  sConfig.Channel      = ADCy_CHANNELa;
  sConfig.Rank         = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  
  if (HAL_ADC_ConfigChannel(&Adc2Handle, &sConfig) != HAL_OK)
  {
    /* Channel Configuration Error */
    //Error_Handler();
		return;
  }
  
}
int gADCConvComplete=0;

int get_targe_voltage(void)
{
	 uint16_t  ADC1ConvertedValue = 0, ADC1ConvertedVoltage = 0;
	ADC_InitTypeDef     ADC_InitStructure;
	GPIO_InitTypeDef    GPIO_InitStructure;

  /* GPIOB Periph clock enable */
  //RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	//  __HAL_RCC_GPIOB_CLK_ENABLE();
  /* ADC1 Periph clock enable */
  //RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
 //   __HAL_RCC_ADC1_CLK_ENABLE();
  /* Configure ADC Channel11 as analog input */
 // GPIO_InitStructure.Pin = GPIO_PIN_0 ;
//  GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
 // GPIO_Init(GPIOB, &GPIO_InitStructure);
  
  /* ADCs DeInit */  
 // ADC_DeInit(ADC1);
  
  /* Initialize ADC structure */
 // ADC_StructInit(&ADC_InitStructure);
  
 // ADC_InitStructure.ExternalTrigConv = ADC_EXTERNALTRIGCONVEDGE_NONE;
 // ADC_Init(ADC1, &ADC_InitStructure); 
  
  /* Convert the ADC1 Channel 8 with 239.5 Cycles as sampling time */ 
 // ADC_RegularChannelConfig(ADC1,ADC_CHANNEL_8, 1, ADC_SAMPLETIME_239CYCLES_5);

 // ADC_ResetCalibration(ADC1);  
 /* Enable ADCperipheral[PerIdx] */
 // ADC_Cmd(ADC1, ENABLE);    
  //Delay_ms(100);  
 // while(ADC_GetResetCalibrationStatus(ADC1));  
  
  /* ADC1 regular Software Start Conv */ 
 // ADC_StartCalibration(ADC1);
 // ADC_Cmd(ADC1, ENABLE);    
     /* Test EOC flag */
 // while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

   /* Run the ADC calibration */  
  if (HAL_ADCEx_Calibration_Start(&AdcHandle) != HAL_OK)
  {
    /* Calibration Error */
    return -1;
  }
  /* Start ADC conversion on regular group with transfer by DMA */
  if (HAL_ADC_Start(&AdcHandle
                       ) != HAL_OK)
  {
    /* Start Error */
    return -1;
  }
  
    HAL_ADC_PollForConversion(&AdcHandle, 1);
    /* Get ADC1 converted data */
 // ADC1ConvertedValue =ADC_GetConversionValue(ADC1);
	ADC1ConvertedValue=HAL_ADC_GetValue(&AdcHandle);
    //    HAL_ADC_PollForConversion(&AdcHandle, 1);
	
    /* Compute the voltage */
	//ADC1ConvertedVoltage=COMPUTATION_DIGITAL_12BITS_TO_VOLTAGE[];
   ADC1ConvertedVoltage = (ADC1ConvertedValue *3300)/0xFFF;	
   //ADC_Cmd(ADC1, DISABLE); 
	HAL_ADC_Stop(&AdcHandle);
   return ADC1ConvertedVoltage;	
 }
int get_COM_voltage(int refV)
{
	 uint16_t  ADC2ConvertedValue = 0, ADC2ConvertedVoltage = 0;
	ADC_InitTypeDef     ADC_InitStructure;
	GPIO_InitTypeDef    GPIO_InitStructure;

  /* GPIOB Periph clock enable */
  //RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	//  __HAL_RCC_GPIOB_CLK_ENABLE();
  /* ADC1 Periph clock enable */
  //RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
 //   __HAL_RCC_ADC1_CLK_ENABLE();
  /* Configure ADC Channel11 as analog input */
 // GPIO_InitStructure.Pin = GPIO_PIN_0 ;
//  GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
 // GPIO_Init(GPIOB, &GPIO_InitStructure);
  
  /* ADCs DeInit */  
 // ADC_DeInit(ADC1);
  
  /* Initialize ADC structure */
 // ADC_StructInit(&ADC_InitStructure);
  
 // ADC_InitStructure.ExternalTrigConv = ADC_EXTERNALTRIGCONVEDGE_NONE;
 // ADC_Init(ADC1, &ADC_InitStructure); 
  
  /* Convert the ADC1 Channel 8 with 239.5 Cycles as sampling time */ 
 // ADC_RegularChannelConfig(ADC1,ADC_CHANNEL_8, 1, ADC_SAMPLETIME_239CYCLES_5);

 // ADC_ResetCalibration(ADC1);  
 /* Enable ADCperipheral[PerIdx] */
 // ADC_Cmd(ADC1, ENABLE);    
  //Delay_ms(100);  
 // while(ADC_GetResetCalibrationStatus(ADC1));  
  
  /* ADC1 regular Software Start Conv */ 
 // ADC_StartCalibration(ADC1);
 // ADC_Cmd(ADC1, ENABLE);    
     /* Test EOC flag */
 // while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

   /* Run the ADC calibration */  
  if (HAL_ADCEx_Calibration_Start(&Adc2Handle) != HAL_OK)
  {
    /* Calibration Error */
    return -1;
  }
  /* Start ADC conversion on regular group with transfer by DMA */
  if (HAL_ADC_Start(&Adc2Handle
                       ) != HAL_OK)
  {
    /* Start Error */
    return -1;
  }
  
   HAL_ADC_PollForConversion(&Adc2Handle, 1);
    /* Get ADC1 converted data */
 // ADC1ConvertedValue =ADC_GetConversionValue(ADC1);
	 ADC2ConvertedValue=HAL_ADC_GetValue(&Adc2Handle);
    //    HAL_ADC_PollForConversion(&AdcHandle, 1);
	
    /* Compute the voltage */
	//ADC1ConvertedVoltage=COMPUTATION_DIGITAL_12BITS_TO_VOLTAGE[];
		if(refV!=5)
   ADC2ConvertedVoltage = (ADC2ConvertedValue *3300)/0xFFF;
		else
   ADC2ConvertedVoltage = (ADC2ConvertedValue *5000)/0xFFF;			
   //ADC_Cmd(ADC1, DISABLE); 
	HAL_ADC_Stop(&Adc2Handle);
   return ADC2ConvertedVoltage;	
 }
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *AdcHandle)
{
  uint32_t tmp_index = 0;
  
  /* For the purpose of this example, dispatch dual conversion values         */
  /* into 2 arrays corresponding to each ADC conversion values.               */
	gADCConvComplete=1;
}

/**
  * @brief  EXTI line detection callback
  * @param GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */


extern int gButtonPressed;
extern int gProgOnLine;
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	gButtonPressed=1;
	if(!gProgOnLine)
		run_led(2,0);
}
#if 1
/************************************DAC*******************************/
//DAC_HandleTypeDef    DacHandle;
static DAC_ChannelConfTypeDef sConfig;
void DAC_Config(void)
{
  /*##-1- Configure the DAC peripheral #######################################*/
  hdac.Instance = DAC;

  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-2- Configure DAC channel1 #############################################*/
  sConfig.DAC_Trigger = DAC_TRIGGER_SOFTWARE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;

  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    /* Channel configuration Error */
    Error_Handler();
  }

}
#endif
/*************************************RTC*********************************/

static void RTC_CalendarConfig(void);
static void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate);

RTC_HandleTypeDef RtcHandle;
 /*##-1- Configure the RTC peripheral #######################################*/
  /* Configure RTC prescaler and RTC data registers */
  /* RTC configured as follow:
      - Asynch Prediv  = Calculated automatically by HAL */
void RTC_Config(void)
{
  RtcHandle.Instance = RTC; 
  RtcHandle.Init.AsynchPrediv = RTC_AUTO_1_SECOND;

  if (HAL_RTC_Init(&RtcHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-2- Check if Data stored in BackUp register1: No Need to reconfigure RTC#*/
  /* Read the Back Up Register 1 Data */
  if (HAL_RTCEx_BKUPRead(&RtcHandle, RTC_BKP_DR1) != 0x32F2)
  {
    /* Configure RTC Calendar */
    RTC_CalendarConfig();
  }
}
/**
  * @brief  Configure the current time and date.
  * @param  None
  * @retval None
  */
static void RTC_CalendarConfig(void)
{
  RTC_DateTypeDef sdatestructure;
  RTC_TimeTypeDef stimestructure;

  /*##-1- Configure the Date #################################################*/
  /* Set Date: Tuesday February 18th 2014 */
  sdatestructure.Year = 0x14;
  sdatestructure.Month = RTC_MONTH_FEBRUARY;
  sdatestructure.Date = 0x18;
  sdatestructure.WeekDay = RTC_WEEKDAY_TUESDAY;
  
  if(HAL_RTC_SetDate(&RtcHandle,&sdatestructure,RTC_FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-2- Configure the Time #################################################*/
  /* Set Time: 02:00:00 */
  stimestructure.Hours = 0x02;
  stimestructure.Minutes = 0x00;
  stimestructure.Seconds = 0x00;

  if (HAL_RTC_SetTime(&RtcHandle, &stimestructure, RTC_FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-3- Writes a data in a RTC Backup data Register1 #######################*/
  HAL_RTCEx_BKUPWrite(&RtcHandle, RTC_BKP_DR1, 0x32F2);
}

/**
  * @brief  Display the current time and date.
  * @param  showtime : pointer to buffer
  * @param  showdate : pointer to buffer
  * @retval None
  */
static void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate)
{
  RTC_DateTypeDef sdatestructureget;
  RTC_TimeTypeDef stimestructureget;

  /* Get the RTC current Time */
  HAL_RTC_GetTime(&RtcHandle, &stimestructureget, RTC_FORMAT_BIN);
  /* Get the RTC current Date */
  HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, RTC_FORMAT_BIN);
  /* Display time Format : hh:mm:ss */
  sprintf((char *)showtime, "%0.2d:%0.2d:%0.2d", stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);
  /* Display date Format : mm-dd-yy */
  sprintf((char *)showdate, "%0.2d-%0.2d-%0.2d", sdatestructureget.Month, sdatestructureget.Date, 2000 + sdatestructureget.Year);
}
void RTC_GetCalendar(uint8_t *showtime, uint8_t *showdate)
{

	RTC_CalendarShow(showtime, showdate);

}
/************************************System*******************************/
extern  void NVIC_SystemReset(void);
void SystemReset(void)
{
	NVIC_SystemReset();

}
#if 0
__asm void SystemReset(void) 
{ 
 MOV R0, #1           //;  
 MSR FAULTMASK, R0    //; ??FAULTMASK ???????? 
 LDR R0, =0xE000ED0C  //; 
 LDR R1, =0x05FA0004  //;  
 STR R1, [R0]         //; ??????    
  
deadloop 
    B deadloop        //; ??????????????? 
}
#endif

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */    
  
/**
  * @}
  */    
  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

